<?php
/*
 * @wordpress-plugin
 * Plugin Name:       Assignment Form
 * Plugin URI:        http://wordpress.org/plugins/better-font-awesome
 * Description:       The ultimate Font Awesome icon plugin for WordPress.
 * Version:           1.7.1
 * Author:            Mustafa jamal
 * Author URI:        mustafajamalattari@live.com
 * License:           GPLv2+
 * Text Domain:       better-font-awesome
 * Domain Path:       /languages
 * GitHub Plugin URI: https://github.com/MickeyKay/better-font-awesome
 */

function assignment_form( $atts ) {
                        
                     if (isset( $_POST['submit_assignment'] )) {
						$assign_address = $_POST['assign_address'];
						$assign_phone = $_POST['assign_phone'];
						$assign_email = $_POST['assign_email'];
						$assign_title = $_POST['assign_title'];
						$assign_description = $_POST['assign_description'];
						$assign_amount = $_POST['assign_amount'];
						$payment_type = $_POST['payment_type'];
						$assign_deadline = $_POST['assign_deadline'];
						
						
						// create post object with the form values
						$my_cptpost_args = array(
													
						'post_title'    => $assign_title,
						'post_content'  => $assign_description,
						'post_status'   => 'pending',
						'post_type' => 'assignment'

						);

						// insert the post into the database
						$post_ins_id = wp_insert_post( $my_cptpost_args);
						//}

						//Set data to Post metas	
						update_post_meta( $post_ins_id, 'payment_type', $payment_type );
						update_post_meta( $post_ins_id, 'amount', $assign_amount );
						update_post_meta( $post_ins_id, 'deadline', $assign_deadline );
						update_post_meta( $post_ins_id, 'address', $assign_address );
						update_post_meta( $post_ins_id, 'phone', $assign_phone );
						update_post_meta( $post_ins_id, 'email', $assign_email );
						
						echo '<div class="alert alert-success"><strong>Assignment</strong> has been submitted. Waiting for approval.</div>';

					}

                    wp_get_current_user();
                    //$test = wp_get_current_user();
                    $user_id = $current_user->ID;
                    $user_meta = get_user_meta( $user_id ,'address');
                    print_r($user_meta[0]);
                    $current_email =  $current_user->user_email;

                    ob_start();  ?>
                    <form class="panel-login agency-form" id="submit_assignment_form" action="" method="post"  >
					  <div class="row">
						<div class="col-md-12">
								<h4>Add Assignment</h4>
						</div>                        
                        </div>
                        <div class="row">

                            <div class="col-md-12">
                                <input type="text" name="assign_address" placeholder="Address" required="" >
                            </div>  
                            <div class="col-md-12">
                                <input type="text" name="assign_phone" placeholder="Phone" required="" >
                            </div>
                            <div class="col-md-12">
                                <input type="text" name="assign_email" placeholder="Email" value="<?php echo $current_email; ?>" required="">
                            </div>
                             <div class="col-md-12">
									<select id="payment_type" name="payment_type" required1="">
										<option value=""> - Select a Payment Type- </option>
										<option value="voluntary"> Voluntary </option>
										<option value="paid">Paid</option>
									</select>
							</div>
							<div class="col-md-12" id="assign_amount" style="display:none;">
				                <input type="text" name="assign_amount" placeholder="Amount" required="" value="0">
				            </div>
                            <div class="col-md-12">
                                <input type="text" name="assign_deadline" placeholder="Deadline" required="">
                            </div>
                                                    
                        </div>
                    

                        <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                 <input type="submit" name="submit_assignment" tabindex="4" class="form-control btn btn-register btn-pink" value="Register"> 
                            </div>
                        </div>
                    </div>

                     </form>
                     <?php
                     $form_data = ob_get_clean();
                     $user = wp_get_current_user();
	    			 $current_role = $user->roles;

	    			 if( $current_role[0] =='agency'){
	    			 	return $form_data;
	    			 }else{
                  	   $form_data_normal = "<div class='not_agency_user_notice'>Only agency can add assignment. <a href='http://test.talentbasen.no/agency-registration/'>Register as Agency</a></div>";
                  	   return $form_data_normal; 			 	
	    			 }


}
add_shortcode( 'assignment-form-show', 'assignment_form' );


