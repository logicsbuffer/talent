<?php

/* Template name:  Agent Detaljer */

get_header(); ?>
<style>
.navbar-default {
    background-color: #ffffff;
    border-color: #ffffff;
    border-bottom: 1px solid #ec008c;
}
.et_header_style_split #main-header .container, .et_header_style_split #main-header .logo_container {
    z-index: 2;
    text-align: center;
    margin-top: 9px;
}
.borderimg{
border:2px solid #ee1495;
padding:10px 0px;
}
.btn-pink{
background-color: #ee1495;
    color: white;
}
#main-content .container:before {
    position: absolute;
    top: 0;
    width: 0px;
    height: 100%;
    background-color: #e2e2e2;
    content: "";
}
input.text, input.title, input[type=email], input[type=password], input[type=tel], input[type=text], select, textarea {
    padding: 5px;
    border: 1px solid #bbb;
    color: #4e4e4e;
    background-color: #fff;
}
.dashboard-grid ul li a {
    color: #111111;
    padding: 10px;
    line-height: 35px;
    
}
.dashboard-grid ul li a:hover {
    text-decoration:none;
}
.dashboard-grid ul li{
    color: #111111;
    padding: 10px;
    line-height: 35px;
    background-color: #dad9d9;
}
.dashboard-grid ul li:hover{
    color: #ffffff;
    padding: 10px;
    line-height: 35px;
    background-color: #ee1495;
}
.dashboard-grid ul li i {
    float: right;
padding: 10px;
}
.mt10b0{
margin:10px 0px;
}
.fluid-width-video-wrapper iframe, .fluid-width-video-wrapper object, .fluid-width-video-wrapper embed {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 50%;
}
.vid {
    position: relative;
    padding-bottom: 56.25%;
    padding-top: 0px; height: 0; overflow: hidden;     margin: 5px 0px;
}
 

.vid iframe,
.vid object,
.vid embed {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}
</style>
	<div id="main-content">

			<article id="post-1165" class="post-1165 page type-page status-publish hentry">

			<div class="entry-content">
			
			<section>    				
	<div class="container-fluid">
		<div class="row">

		<div class="col-sm-12">
		<div class="dashboard-grid">
			<div class="col-sm-12"><br>
<h2 class="text-center">profil informasjon</h2><br>
				<div class="row">

				 <div class="col-sm-3 text-center">
				 <?php
			
				global $wpdb;
				
				if(isset($_POST['submit']))
				{
					
					$subject="Contact Mail";    
					$headers="MIME-Version: 1.0\n"; 
					$headers.="Content-type: text/html; charset=iso-8859-1 \n"; 
					
				   
					$headers.="from:Talentbasen<".$_POST['email'].">";
					$mail_body="<table cellpadding='0' cellspacing='0' border='0'><tr><td>Navn: ".$_POST['fname']."</td></tr>";
					$mail_body.="<tr><td>E-post: ".$_POST['email']."</td></tr>";
					$mail_body.="<tr><td>Antall: ".$_POST['phone']."</td></tr></table>";   
					
					$mail_to="pinkwebsolutionz@gmail.com";

					if(@wp_mail($mail_to, $subject, $mail_body, $headers)) 
					
					{
						?>
						
					   <script type="text/javascript">
						 alert("Thank You! Your Message Has Been Sent Successfully");
						</script>
							
						<?php 
					} 
					else
					{
					  print("<h1><font color=\"#880000\">Sorry! An Error Occurred While Sending Message. Please Try Again</font></h1>"); 
					}
			}	
			$select_agent= $wpdb->get_results('select * from `tlntb_agent_register` where `tlntb_agent_register`.`code` = "'.$_GET['code'].'" and `tlntb_agent_register`.`status` = "1"');
			
			?>
           
            
				<img src="<?php echo $select_agent[0]->image_browse; ?>" width="279" height="279"/>		
<br><br>

					<form class="panel-login dashboard-form" id="dashboard-form" action="" method="post">
						<h2>kontakt oss</h2>
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<input type="text" name="fname" tabindex="1" class="form-control"   placeholder="Navn">
									</div>	
								</div>
								
							</div>
							
							<div class="form-group">
 <div class="row">
									
									<div class="col-md-12">
										<input type="email" name="email" tabindex="1" class="form-control"   placeholder="E-post">
									</div>	
								</div>
								
							</div>
							<div class="form-group">
<div class="row">
									
									<div class="col-md-12">
										<input type="tel" name="phone" tabindex="1" class="form-control" placeholder="Antall">
									</div>	
								</div>
								
							</div>
                            
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6 text-center">
										<input type="submit" name="submit" tabindex="4" class="form-control btn btn-register btn-pink" value="Submit">
									</div>
								</div>
							</div>
						</form>			
					</div>
					<div class="col-sm-9">
					
						<div class="panel-login dashboard-form" id="dashboard-form">
							<div class="form-group">
								<div class="row">
		
									<div class="col-md-12">
									<strong>Navn: </strong> <?php echo $select_agent[0]->firstname." ".$select_agent[0]->lastname; ?>
									</div>
										
								</div>
								
							</div>
                            
                            <div class="form-group">
								<div class="row">
									<div class="col-md-12">
									<strong>Adresse: </strong> <?php echo $select_agent[0]->address; ?>
									</div>
										
								</div>
								
							</div>
							
                            <div class="form-group">
								<div class="row">
									<div class="col-md-12">
									<strong>gate: </strong> <?php echo $select_agent[0]->street; ?>
									</div>
										
								</div>
								
							</div>
                            
                            <div class="form-group">
								<div class="row">
									<div class="col-md-12">
									<strong>post: </strong> <?php echo $select_agent[0]->post; ?>
									</div>
										
								</div>
								
							</div>
                            
                            <div class="form-group">
<div class="row">
									<div class="col-md-12">
									<strong>By: </strong> <?php echo $select_agent[0]->city; ?>
									</div>
										
								</div>
								
							</div>
                            
							<div class="form-group">
 <div class="row">
									<div class="col-md-12">
									<strong>e-post: </strong> <?php echo $select_agent[0]->email; ?>
									</div>
										
								</div>
								
							</div>
							
							<div class="form-group">
<div class="row">
									<div class="col-md-12">
									<strong>telefon: </strong> <?php echo $select_agent[0]->phone; ?>
									</div>
										
								</div>
								
							</div>
							
							<div class="form-group">
<div class="row">
									<div class="col-md-12">
									<strong>Selskap: </strong> <?php echo $select_agent[0]->company; ?>
									</div>
									
								</div>
								
								
							</div>
							<div class="form-group">
<div class="row">
									<div class="col-md-12">
									<strong>Selskap registrere Antall: </strong> <?php echo $select_agent[0]->com_reg; ?>
									</div>
										
								</div>
								
								
								
							</div>
							
							
						

						</div>
                        
                        	
                        
						<!--<h3><strong>Image gallery</strong></h3>
						<div class="row">

							<div class="col-md-3">
								<img src="http://www.excellenceonlinetutors.com/images/courses/2.jpg" class="img-responsive mt10b0">
							</div>
<div class="col-md-3">
								<img src="http://www.excellenceonlinetutors.com/images/courses/2.jpg" class="img-responsive mt10b0">
							</div>
<div class="col-md-3">
								<img src="http://www.excellenceonlinetutors.com/images/courses/2.jpg" class="img-responsive mt10b0">
							</div>
<div class="col-md-3">
								<img src="http://www.excellenceonlinetutors.com/images/courses/2.jpg" class="img-responsive mt10b0">
							</div>
<div class="col-md-3">
								<img src="http://www.excellenceonlinetutors.com/images/courses/2.jpg" class="img-responsive mt10b0">
							</div>
<div class="col-md-3">
								<img src="http://www.excellenceonlinetutors.com/images/courses/2.jpg" class="img-responsive mt10b0">
							</div>
<div class="col-md-3">
								<img src="http://www.excellenceonlinetutors.com/images/courses/2.jpg" class="img-responsive mt10b0">
							</div>
<div class="col-md-3">
								<img src="http://www.excellenceonlinetutors.com/images/courses/2.jpg" class="img-responsive mt10b0">
							</div>
						</div>-->
<br><br>
						<div class="row">
							 <div class="col-md-6">
            
           
        </div><!--.col -->
 <div class="col-md-6">
            
            
        </div><!--.col -->
<div class="col-sm-12">
<h3>Handle om :</h3>
										<p><?php echo $select_agent[0]->about; ?></p>
									</div>
						</div>

					</div>
				</div>
			</div>
			</div>
		</div>
		</div>
	</div>
</section>
			</div>
			
			</article> <!-- .et_pb_post -->

	</div>
	
		
		
<?php get_footer(); ?>
		
		
