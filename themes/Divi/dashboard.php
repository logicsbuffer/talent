<?php

/* Template name:  Dashboard */


get_header();
?>


<?php

//echo $_SESSION['secode'];
if($_SESSION['secode'] == "")
{
	$url = "http://test.talentbasen.no/logg-inn/";
	wp_redirect( $url ); 
}
 
?>



<style>
.navbar-default {
    background-color: #ffffff;
    border-color: #ffffff;
    border-bottom: 1px solid #ec008c;
}
.et_header_style_split #main-header .container, .et_header_style_split #main-header .logo_container {
    z-index: 2;
    text-align: center;
    margin-top: 9px;
}
.borderimg{
border:2px solid #ee1495;
}
.btn-pink{
background-color: #ee1495;
    color: white;
}
#main-content .container:before {
    position: absolute;
    top: 0;
    width: 0px;
    height: 100%;
    background-color: #e2e2e2;
    content: "";
}
input.text, input.title, input[type=email], input[type=password], input[type=tel], input[type=text], select, textarea {
    padding: 5px;
    border: 1px solid #bbb;
    color: #4e4e4e;
    background-color: #fff;
}
.dashboard-grid ul li a {
    color: #111111;
    padding-left: 10px;
    padding-right: 10px;
    /*line-height: 35px;*/
    
}
.dashboard-grid ul li a:hover {
    text-decoration:none;
}
.dashboard-grid ul li{
    color: #111111;
    /*padding: 10px;*/
    line-height: 35px;
    background-color: #dad9d9;
	width: 200px;
	border-bottom: 1px dotted;
}
.dashboard-grid ul li:hover{
    color: #ffffff;
   /*padding: 10px;*/
    line-height: 35px;
    background-color: #ee1495;
}
.dashboard-grid ul li i {
    float: right;
padding: 10px;
}

</style>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>

$(document).ready(function(){
    //alert("hello");
	
	var kategr = $("#kategor-i").val();
	if(kategr == "Musikk")
	{
		$("#musik-k").show();
		$("#dan-s").hide();
		$("#reklamefilmer").hide();
		$("#bandsec").hide();
		$("#sangersec").hide();
		$("#instrusec").hide();
		var katebnd = $("#kate-band").val();
		if(katebnd == "Band")
		{
			$("#bandsec").show();
			$("#sangersec").hide();
			$("#instrusec").hide();
			
		}
		else if(katebnd == "Partyband")
		{
			$("#bandsec").show();
			$("#sangersec").hide();
			$("#instrusec").hide();
		}
		else if(katebnd == "Dj")
		{
			$("#bandsec").hide();
			$("#sangersec").hide();
			$("#instrusec").hide();
		}
		else if(katebnd == "Sanger")
		{
			$("#sangersec").show();
			$("#bandsec").hide();
			$("#instrusec").hide();
		}	
		else
		{
			$("#instrusec").hide();
			$("#bandsec").hide();
			$("#sangersec").hide();
		}
		
		$("#kate-band").change(function(){
			var katebnd = $("#kate-band").val();
			if(katebnd == "Band")
			{
				$("#bandsec").show();
				$("#sangersec").hide();
				$("#instrusec").hide();
				
			}
			else if(katebnd == "Partyband")
			{
				$("#bandsec").show();
				$("#sangersec").hide();
				$("#instrusec").hide();
			}
			else if(katebnd == "Dj")
			{
				$("#bandsec").hide();
				$("#sangersec").hide();
				$("#instrusec").hide();
			}
			else if(katebnd == "Sanger")
			{
				$("#sangersec").show();
				$("#bandsec").hide();
				$("#instrusec").hide();
			}	
			else
			{
				$("#instrusec").show();
				$("#bandsec").hide();
				$("#sangersec").hide();
			}
		});
		
	}
	else if(kategr == "Danse")
	{
		$("#dan-s").show();
		$("#musik-k").hide();
		$("#reklamefilmer").hide();
	}
	else if(kategr == "Reklamefilmer")
	{
		$("#reklamefilmer").show();
		$("#musik-k").hide();
		$("#dan-s").hide();
	}
	else
	{
		$("#musik-k").hide();
		$("#dan-s").hide();
		$("#reklamefilmer").hide();
	}
	
	$("#kategor-i").change(function(){
		var kategor = $("#kategor-i").val();
        if(kategor == "Musikk")
		{
			$("#musik-k").show();
			$("#dan-s").hide();
			$("#reklamefilmer").hide();
			$("#bandsec").hide();
			$("#sangersec").hide();
			$("#instrusec").hide();
		}
		else if(kategor == "Danse")
		{
			$("#dan-s").show();
			$("#musik-k").hide();
			$("#reklamefilmer").hide();
		}
		else if(kategor == "Reklamefilmer")
		{
			$("#reklamefilmer").show();
			$("#musik-k").hide();
			$("#dan-s").hide();
		}
		else
		{
			$("#musik-k").hide();
			$("#dan-s").hide();
			$("#reklamefilmer").hide();
		}
    });
	
});
</script>




	<div id="main-content">

			<article id="post-1165" class="post-1165 page type-page status-publish hentry">

			<div class="entry-content">
			
			<section>    				
	<div class="container">
		<div class="row">

		<div class="col-sm-12">
		<div class="dashboard-grid">
			<div class="col-sm-12"><br>
<h2 class="text-center">Profile Information</h2><br>
				<div class="row">
<div class="col-sm-3">
				<div class="navbar-collapse" id="bs-sidebar-navbar-collapse-1">
				  <ul class="nav navbar-nav">		
					<li class=""><a href="http://test.talentbasen.no/dashboard/">Min side<i class="fa fa-home" aria-hidden="true"></i></a></li>
					<li class=""><a href="http://test.talentbasen.no/legg-til-galleri/">Legg til galleri<i class="fa fa-home" aria-hidden="true"></i></a></li>
					<li class=""><a href="http://test.talentbasen.no/legg-til-video/">Legg til video<i class="fa fa-home" aria-hidden="true"></i></a></li>
					<li class=""><a href="http://test.talentbasen.no/abonnere-medlems/">Abonnere<i class="fa fa-home" aria-hidden="true"></i></a></li>
					<li ><a href="http://test.talentbasen.no/logg-inn/?s=log">Logg ut<i class="fa fa-sign-out" aria-hidden="true"></i></a></li>
				  </ul>
				</div>					
					</div>
					
					<?php
					global $wpdb;
					
					
					if(isset($_POST['register-submit']))
						{
							
							if($_FILES['image_browse']['name'] != "")
							{
								$time = time();
								$file = $time.$_FILES['image_browse']['name'];
								$image_browse = "http://talentbasen.no/test/wp-content/uploads/".date("Y")."/".date("m")."/".$file;
								
								function my_upload_dir($upload) {

								  return $upload;

								}
							
								if ( ! function_exists( 'wp_handle_upload' ) ) 
									$t = time();
									require_once( ABSPATH . 'wp-admin/includes/file.php' );
									$uploadedfile = $_FILES['image_browse']; 
									
									$nm = $time.$uploadedfile['name'];
									$tp = $uploadedfile['type'];
									$tmpname = $uploadedfile['tmp_name'];
									$er = $uploadedfile['error'];
									$sz = $uploadedfile['size'];
									$file_nm = array(name => $nm, type => $tp, tmp_name => $tmpname, error => $er, size => $sz);
									//print_r($uploadedfile);
									//print_r($file_nm);die();
									$upload_overrides = array( 'test_form' => false );
									//add_filter('upload_dir', 'my_upload_dir');
									$movefile = wp_handle_upload( $file_nm, $upload_overrides );
									//remove_filter('upload_dir', 'my_upload_dir');

									
									if ( $movefile ) {
										//echo "File is valid, and was successfully uploaded.\n";
										//var_dump( $movefile);
										
										$category = $_POST['category'];
										$band = $_POST['band'];
										$sjanger = $_POST['sjanger'];
										$stiftet = $_POST['stiftet'];
										$repertoar = $_POST['repertoar'];
										$besetning = $_POST['besetning'];
										$solo = $_POST['solo'];
										$typeinstru = $_POST['typeinstru'];
										$spiltsiden = $_POST['spiltsiden'];
										$moderne = $_POST['moderne'];
										$hip = $_POST['hip'];
										$ballett = $_POST['ballett'];
										$film = $_POST['film-theater'];
										$experience = $_POST['experience'];
										$assignments = $_POST['assignments'];
										$age = $_POST['age'];
										$gender = $_POST['gender'];
										$height = $_POST['height'];
										$weight = $_POST['weight'];
										$hair_color = $_POST['hair_color'];
										$dialect = $_POST['dialect'];
										$lang = $_POST['lang'];
										$firstname = $_POST['firstname'];
										$lastname = $_POST['lastname'];
										$post = $_POST['post'];
										$city = $_POST['city'];
										$street = $_POST['street'];
										$phone = $_POST['phone'];
										$about = $_POST['about'];
										
										
										$resultss = $wpdb->query('UPDATE `tlntb_member_register` SET `tlntb_member_register`.`firstname` ="'.$firstname.'",`tlntb_member_register`.`lastname` ="'.$lastname.'",`tlntb_member_register`.`street` ="'.$street.'",`tlntb_member_register`.`post` ="'.$post.'",`tlntb_member_register`.`city` ="'.$city.'",`tlntb_member_register`.`phone` ="'.$phone.'",`tlntb_member_register`.`category` ="'.$category.'",`tlntb_member_register`.`experience` ="'.$experience.'",`tlntb_member_register`.`assignments` ="'.$assignments.'",`tlntb_member_register`.`about` ="'.$about.'",`tlntb_member_register`.`band` ="'.$band.'",`tlntb_member_register`.`sjanger` ="'.$sjanger.'",`tlntb_member_register`.`stiftet` ="'.$stiftet.'",`tlntb_member_register`.`repertoar` ="'.$repertoar.'",`tlntb_member_register`.`besetning` ="'.$besetning.'",`tlntb_member_register`.`solo` ="'.$solo.'",`tlntb_member_register`.`typeinstru` ="'.$typeinstru.'",`tlntb_member_register`.`spiltsiden` ="'.$spiltsiden.'",`tlntb_member_register`.`moderne` ="'.$moderne.'",`tlntb_member_register`.`hip` ="'.$hip.'",`tlntb_member_register`.`ballett` ="'.$ballett.'",`tlntb_member_register`.`age` ="'.$age.'",`tlntb_member_register`.`gender` ="'.$gender.'",`tlntb_member_register`.`height` ="'.$height.'",`tlntb_member_register`.`weight` ="'.$weight.'",`tlntb_member_register`.`hair_color` ="'.$hair_color.'",`tlntb_member_register`.`dialect` ="'.$dialect.'",`tlntb_member_register`.`lang` ="'.$lang.'",`tlntb_member_register`.`film-theater` ="'.$film.'",`tlntb_member_register`.`image_browse` ="'.$image_browse.'" WHERE `tlntb_member_register`.`code` = "'.$_GET['cd'].'"');
								
									} else {
										//echo "Possible file upload attack!\n";
									}
								}
							
								
								 $category = $_POST['category'];
								 $band = $_POST['band'];
								 $sjanger = $_POST['sjanger'];
								 $stiftet = $_POST['stiftet'];
								 $repertoar = $_POST['repertoar'];
								 $besetning = $_POST['besetning'];
								 $solo = $_POST['solo'];
								 $typeinstru = $_POST['typeinstru'];
								 $spiltsiden = $_POST['spiltsiden'];
								 $moderne = $_POST['moderne'];
								 $hip = $_POST['hip'];
								 $ballett = $_POST['ballett'];
								 $film = $_POST['film-theater'];
								 $experience = $_POST['experience'];
								 $assignments = $_POST['assignments'];
								 $age = $_POST['age'];
								 $gender = $_POST['gender'];
								 $height = $_POST['height'];
								 $weight = $_POST['weight'];
								 $hair_color = $_POST['hair_color'];
								 $dialect = $_POST['dialect'];
								 $lang = $_POST['lang'];
								 $firstname = $_POST['firstname'];
								 $lastname = $_POST['lastname'];
								 $post = $_POST['post'];
								 $city = $_POST['city'];
								 $street = $_POST['street'];
								 $phone = $_POST['phone'];
								 $about = $_POST['about'];
							
								$resultss = $wpdb->query('UPDATE `tlntb_member_register` SET `tlntb_member_register`.`firstname` ="'.$firstname.'",`tlntb_member_register`.`lastname` ="'.$lastname.'",`tlntb_member_register`.`street` ="'.$street.'",`tlntb_member_register`.`post` ="'.$post.'",`tlntb_member_register`.`city` ="'.$city.'",`tlntb_member_register`.`phone` ="'.$phone.'",`tlntb_member_register`.`category` ="'.$category.'",`tlntb_member_register`.`experience` ="'.$experience.'",`tlntb_member_register`.`assignments` ="'.$assignments.'",`tlntb_member_register`.`about` ="'.$about.'",`tlntb_member_register`.`band` ="'.$band.'",`tlntb_member_register`.`sjanger` ="'.$sjanger.'",`tlntb_member_register`.`stiftet` ="'.$stiftet.'",`tlntb_member_register`.`repertoar` ="'.$repertoar.'",`tlntb_member_register`.`besetning` ="'.$besetning.'",`tlntb_member_register`.`solo` ="'.$solo.'",`tlntb_member_register`.`typeinstru` ="'.$typeinstru.'",`tlntb_member_register`.`spiltsiden` ="'.$spiltsiden.'",`tlntb_member_register`.`moderne` ="'.$moderne.'",`tlntb_member_register`.`hip` ="'.$hip.'",`tlntb_member_register`.`ballett` ="'.$ballett.'",`tlntb_member_register`.`age` ="'.$age.'",`tlntb_member_register`.`gender` ="'.$gender.'",`tlntb_member_register`.`height` ="'.$height.'",`tlntb_member_register`.`weight` ="'.$weight.'",`tlntb_member_register`.`hair_color` ="'.$hair_color.'",`tlntb_member_register`.`dialect` ="'.$dialect.'",`tlntb_member_register`.`lang` ="'.$lang.'",`tlntb_member_register`.`film-theater` ="'.$film.'" WHERE `tlntb_member_register`.`code` = "'.$_SESSION['secode'].'"');
								
							}
					
					
					
					
					
					
					
					
					$select_member= $wpdb->get_results('select * from `tlntb_member_register` where `tlntb_member_register`.`code` = "'.$_SESSION['secode'].'"');
					
					?>
					
					<div class="col-sm-3 text-center">
						<?php
						 if($select_member[0]->image_browse == "")
						 {
							 ?>
							 <img class="img-responsive borderimg center-block " src="http://anandvatika.org/img/Profile-icon-man-150x150.jpg">						
							<?php
						 }
						 else
						 {
							 ?>
							 <img class="img-responsive borderimg center-block " src="<?php echo $select_member[0]->image_browse; ?>">						
							 <?php
						 }
						 ?>	
						<div class="row">
							<div class="col-sm-12">
							</div>
						</div>
					</div>
					<div class="col-sm-6">
					
						<form class="panel-login dashboard-form" enctype="multipart/form-data"  action="" method="post" role="form">
							
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>Brukernavn</strong>
									</div>
									<div class="col-md-7">
										<input type="text" id="brukernavn" name="username" class="form-control" value="<?php echo $select_member[0]->username;?>"  readonly>
									</div>	
								</div>
								
							</div>
							<div class="form-group">
							   <div class="row">
									<div class="col-md-5">
									<strong>Passord</strong>
									</div>
									<div class="col-md-7">
										<input type="text" name="password" tabindex="1" class="form-control" value="<?php echo $select_member[0]->password;?>" readonly>
									</div>	
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>Epost</strong>
									</div>
									<div class="col-md-7">
										<input type="email" id="epost" name="email" class="form-control" value="<?php echo $select_member[0]->email;?>" readonly>
									</div>	
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>Kategori</strong>
									</div>
									<div class="col-md-7">
										<select id="kategor-i" class="form-control" name="category" required="">
											<option value="">-- Velg --</option>
											<?php
											if($select_member[0]->category == "Danse")
											{
												?>
												<option value="Danse" selected>Danse</option>
												<option value="Musikk">Musikk</option>
												<option value="Sport">Sport</option>
												<option value="Reklamefilmer">Film og Teater</option>
												<option value="Kultur">Kultur</option>
												<?php
											}
											else if($select_member[0]->category == "Musikk")
											{
												?>
												<option value="Danse">Danse</option>
												<option value="Musikk" selected>Musikk</option>
												<option value="Sport">Sport</option>
												<option value="Reklamefilmer">Film og Teater</option>
												<option value="Kultur">Kultur</option>
												<?php
											}
											else if($select_member[0]->category == "Sport")
											{
												?>
												<option value="Danse">Danse</option>
												<option value="Musikk">Musikk</option>
												<option value="Sport" selected>Sport</option>
												<option value="Reklamefilmer">Film og Teater</option>
												<option value="Kultur">Kultur</option>
												<?php
											}
											else if($select_member[0]->category == "Reklamefilmer")
											{
												?>
												<option value="Danse">Danse</option>
												<option value="Musikk">Musikk</option>
												<option value="Sport">Sport</option>
												<option value="Reklamefilmer" selected>Film og Teater</option>
												<option value="Kultur">Kultur</option>
												<?php
											}
											else
											{
												?>
												<option value="Danse">Danse</option>
												<option value="Musikk">Musikk</option>
												<option value="Sport">Sport</option>
												<option value="Reklamefilmer">Film og Teater</option>
												<option value="Kultur" selected>Kultur</option>
												<?php
											}
											?>
											
										</select>
									</div>	
								</div>
							</div>
							
							<div id="musik-k">
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>Kategori 2</strong>
									</div>
									<div class="col-md-7">
										<select class="form-control" id="kate-band" name="band" required="">
											<option value="">-- Velg --</option>
											<?php
											if($select_member[0]->band == "Band")
											{
												?>
												<option value="Band" selected>Band</option>
												<option value="Partyband">Partyband</option>
												<option value="Dj">Dj</option>
												<option value="Sanger">Sanger</option>
												<?php
											}
											else if($select_member[0]->band == "Partyband")
											{
												?>
												<option value="Band">Band</option>
												<option value="Partyband" selected>Partyband</option>
												<option value="Dj">Dj</option>
												<option value="Sanger">Sanger</option>
												<option value="Instrumentals">Instrumentals</option>
												<?php
											}
											else if($select_member[0]->band == "Dj")
											{
												?>
												<option value="Band">Band</option>
												<option value="Partyband">Partyband</option>
												<option value="Dj" selected>Dj</option>
												<option value="Sanger">Sanger</option>
												<option value="Instrumentals">Instrumentals</option>
												<?php
											}
											else if($select_member[0]->band == "Sanger")
											{
												?> 
												<option value="Band">Band</option>
												<option value="Partyband">Partyband</option>
												<option value="Dj">Dj</option>
												<option value="Sanger" selected>Sanger</option>
												<option value="Instrumentals">Instrumentals</option>
												<?php
											}
											else if($select_member[0]->band == "Instrumentals")
											{
												?> 
												<option value="Band">Band</option>
												<option value="Partyband">Partyband</option>
												<option value="Dj">Dj</option>
												<option value="Sanger">Sanger</option>
												<option value="Instrumentals" selected>Instrumentals</option>
												<?php
											}
											else 
											{
												?> 
												<option value="Band">Band</option>
												<option value="Partyband">Partyband</option>
												<option value="Dj">Dj</option>
												<option value="Sanger">Sanger</option>
												<option value="Instrumentals">Instrumentals</option>
												<?php
											}
											?>
											
										</select>
									</div>	
								</div>
							</div>
							
							
							<div id="bandsec">
								<div class="form-group">
									<div class="row">
										<div class="col-md-5">
										<strong>Sjanger</strong>
										</div>
										<div class="col-md-7">
											<input type="text" name="sjanger" class="form-control" value="<?php echo $select_member[0]->sjanger;?>">
										</div>	
									</div>
								</div>
								
								<div class="form-group">
									<div class="row">
										<div class="col-md-5">
										<strong>Stiftet</strong>
										</div>
										<div class="col-md-7">
											<input type="text" name="stiftet" class="form-control" value="<?php echo $select_member[0]->stiftet;?>">
										</div>	
									</div>
								</div>
								
								<div class="form-group">
									<div class="row">
										<div class="col-md-5">
										<strong>Repertoar</strong>
										</div>
										<div class="col-md-7">
											<input type="text" name="repertoar" class="form-control" value="<?php echo $select_member[0]->repertoar;?>">
										</div>	
									</div>
								</div>
								
								<div class="form-group">
									<div class="row">
										<div class="col-md-5">
										<strong>Besetning</strong>
										</div>
										<div class="col-md-7">
											<input type="text" name="besetning" class="form-control" value="<?php echo $select_member[0]->besetning;?>">
										</div>	
									</div>
								</div>
								
							</div>
							
							
							<div id="sangersec">
								<div class="form-group">
									<div class="row">
										<div class="col-md-5">
										<strong>Solo</strong>
										</div>
										<div class="col-md-7">
											<input type="text" name="solo" class="form-control" value="<?php echo $select_member[0]->solo;?>">
										</div>	
									</div>
								</div>
							</div>
							
							
							<div id="instrusec">
								<div class="form-group">
									<div class="row">
										<div class="col-md-5">
										<strong>Type Instrumentals</strong>
										</div>
										<div class="col-md-7">
											<input type="text" name="typeinstru" class="form-control" value="<?php echo $select_member[0]->typeinstru;?>">
										</div>	
									</div>
								</div>
								
								<div class="form-group">
									<div class="row">
										<div class="col-md-5">
										<strong>Spilt Siden</strong>
										</div>
										<div class="col-md-7">
											<input type="text" name="spiltsiden" class="form-control" value="<?php echo $select_member[0]->spiltsiden;?>">
										</div>	
									</div>
								</div>
							</div>
							
							
							
							</div>
							
							
							
							<div id="dan-s">
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>Moderne Dans Erfaring</strong>
									</div>
									<div class="col-md-7">
										<select class="form-control" name="moderne" required="">
											<option value="">-- Velg --</option>
											<?php
											if($select_member[0]->moderne == "Middels")
											{
												?>
												<option value="Middels" selected>Middels</option>
												<option value="Profesjonell">Profesjonell</option>
												<option value="Bare som hobby">Bare som hobby</option>
												<?php
											}
											else if($select_member[0]->moderne == "Profesjonell")
											{
												?> 
												<option value="Middels">Middels</option>
												<option value="Profesjonell" selected>Profesjonell</option>
												<option value="Bare som hobby">Bare som hobby</option>
												<?php
											}
											else if($select_member[0]->moderne == "Bare som hobby")
											{
												?> 
												<option value="Middels">Middels</option>
												<option value="Profesjonell">Profesjonell</option>
												<option value="Bare som hobby" selected>Bare som hobby</option>
												<?php
											}
											else 
											{
												?> 
												<option value="Middels">Middels</option>
												<option value="Profesjonell">Profesjonell</option>
												<option value="Bare som hobby">Bare som hobby</option>
												<?php
											}
											?>
										</select>
									</div>	
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>Hip Hop Erfaring</strong>
									</div>
									<div class="col-md-7">
										<select class="form-control" name="hip" required="">
											<option value="">-- Velg --</option>
											<?php
											if($select_member[0]->hip == "Middels")
											{
												?>
												<option value="Middels" selected>Middels</option>
												<option value="Profesjonell">Profesjonell</option>
												<option value="Bare som hobby">Bare som hobby</option>
												<?php
											}
											else if($select_member[0]->hip == "Profesjonell")
											{
												?> 
												<option value="Middels">Middels</option>
												<option value="Profesjonell" selected>Profesjonell</option>
												<option value="Bare som hobby">Bare som hobby</option>
												<?php
											}
											else if($select_member[0]->hip == "Bare som hobby")
											{
												?> 
												<option value="Middels">Middels</option>
												<option value="Profesjonell">Profesjonell</option>
												<option value="Bare som hobby" selected>Bare som hobby</option>
												<?php
											}
											else 
											{
												?> 
												<option value="Middels">Middels</option>
												<option value="Profesjonell">Profesjonell</option>
												<option value="Bare som hobby">Bare som hobby</option>
												<?php
											}
											?>
										</select>
									</div>	
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>Ballett Erfaring</strong>
									</div>
									<div class="col-md-7">
										<select class="form-control" name="ballett" required="">
											<option value="">-- Velg --</option>
											<?php
											if($select_member[0]->ballett == "Middels")
											{
												?>
												<option value="Middels" selected>Middels</option>
												<option value="Profesjonell">Profesjonell</option>
												<option value="Bare som hobby">Bare som hobby</option>
												<?php
											}
											else if($select_member[0]->ballett == "Profesjonell")
											{
												?> 
												<option value="Middels">Middels</option>
												<option value="Profesjonell" selected>Profesjonell</option>
												<option value="Bare som hobby">Bare som hobby</option>
												<?php
											}
											else if($select_member[0]->ballett == "Bare som hobby")
											{
												?> 
												<option value="Middels">Middels</option>
												<option value="Profesjonell">Profesjonell</option>
												<option value="Bare som hobby" selected>Bare som hobby</option>
												<?php
											}
											else 
											{
												?> 
												<option value="Middels">Middels</option>
												<option value="Profesjonell">Profesjonell</option>
												<option value="Bare som hobby">Bare som hobby</option>
												<?php
											}
											?>
										</select>
									</div>	
								</div>
							</div>
							
							</div>

							
							<div id="reklamefilmer">
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>Kategori 2</strong>
									</div>
									<div class="col-md-7">
										<select class="form-control" name="film-theater" required="">
											<option value="">-- Velg --</option>
											<?php
											if($select_member[0]->film-theater == "Skuespiller")
											{
												?>
												<option value="Skuespiller" selected>Skuespiller</option>
												<option value="Reklamemodell">Reklamemodell</option>
												<option value="Fotograf">Fotograf</option>
												<option value="Voiceover">Voiceover</option>
												<option value="Statist">Statist</option>
												<?php
											}
											else if($select_member[0]->film-theater == "Reklamemodell")
											{
												?> 
												<option value="Skuespiller">Skuespiller</option>
												<option value="Reklamemodell" selected>Reklamemodell</option>
												<option value="Fotograf">Fotograf</option>
												<option value="Voiceover">Voiceover</option>
												<option value="Statist">Statist</option>
												<?php
											}
											else if($select_member[0]->film-theater == "Fotograf")
											{
												?> 
												<option value="Skuespiller">Skuespiller</option>
												<option value="Reklamemodell">Reklamemodell</option>
												<option value="Fotograf" selected>Fotograf</option>
												<option value="Voiceover">Voiceover</option>
												<option value="Statist">Statist</option>
												<?php
											}
											else if($select_member[0]->film-theater == "Voiceover")
											{
												?> 
												<option value="Skuespiller">Skuespiller</option>
												<option value="Reklamemodell">Reklamemodell</option>
												<option value="Fotograf">Fotograf</option>
												<option value="Voiceover" selected>Voiceover</option>
												<option value="Statist">Statist</option>
												<?php
											}
											else if($select_member[0]->film-theater == "Statist")
											{
												?> 
												<option value="Skuespiller">Skuespiller</option>
												<option value="Reklamemodell">Reklamemodell</option>
												<option value="Fotograf">Fotograf</option>
												<option value="Voiceover">Voiceover</option>
												<option value="Statist" selected>Statist</option>
												<?php
											}
											else 
											{
												?> 
												<option value="Skuespiller">Skuespiller</option>
												<option value="Reklamemodell">Reklamemodell</option>
												<option value="Fotograf">Fotograf</option>
												<option value="Voiceover">Voiceover</option>
												<option value="Statist">Statist</option>
												<?php
											}
											?>
										</select>
									</div>	
								</div>
							</div>
							
							</div>
						
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>Erfaring</strong>
									</div>
									<div class="col-md-7">
										<select class="form-control" name="experience" required="">
											<option value="">-- Velg --</option>
											<?php
											if($select_member[0]->experience == "Nybegynner")
											{
												?>
												<option value="Nybegynner" selected>Nybegynner</option>
												<option value="Litt erfaring">Litt erfaring</option>
												<option value="Mye erfaring">Mye erfaring</option>
												<option value="Profesjonell">Profesjonell</option>
												<?php
											}
											else if($select_member[0]->experience == "Litt erfaring")
											{
												?> 
												<option value="Nybegynner">Nybegynner</option>
												<option value="Litt erfaring" selected>Litt erfaring</option>
												<option value="Mye erfaring">Mye erfaring</option>
												<option value="Profesjonell">Profesjonell</option>
												<?php
											}
											else if($select_member[0]->experience == "Mye erfaring")
											{
												?> 
												<option value="Nybegynner">Nybegynner</option>
												<option value="Litt erfaring">Litt erfaring</option>
												<option value="Mye erfaring" selected>Mye erfaring</option>
												<option value="Profesjonell">Profesjonell</option>
												<?php
											}
											else if($select_member[0]->experience == "Profesjonell")
											{
												?> 
												<option value="Nybegynner">Nybegynner</option>
												<option value="Litt erfaring">Litt erfaring</option>
												<option value="Mye erfaring">Mye erfaring</option>
												<option value="Profesjonell" selected>Profesjonell</option>
												<?php
											}
											else 
											{
												?> 
												<option value="Nybegynner">Nybegynner</option>
												<option value="Litt erfaring">Litt erfaring</option>
												<option value="Mye erfaring">Mye erfaring</option>
												<option value="Profesjonell">Profesjonell</option>
												<?php
											}
											?>
										</select>
									</div>	
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>Kan ta Oppdrag</strong>
									</div>
									<div class="col-md-7">
										<select class="form-control" name="assignments" required="">
											<option value="">-- Velg --</option>
											<?php
											if($select_member[0]->assignments == "Når som helst")
											{
												?>
												<option value="Når som helst" selected>Når som helst</option>
												<option value="Etter avtale">Etter avtale</option>
												<option value="Kun via byrå og agent">Kun via byrå og agent</option>
												<?php
											}
											else if($select_member[0]->assignments == "Etter avtale")
											{
												?> 
												<option value="Når som helst">Når som helst</option>
												<option value="Etter avtale" selected>Etter avtale</option>
												<option value="Kun via byrå og agent">Kun via byrå og agent</option>
												<?php
											}
											else if($select_member[0]->assignments == "Kun via byrå og agent")
											{
												?> 
												<option value="Når som helst">Når som helst</option>
												<option value="Etter avtale">Etter avtale</option>
												<option value="Kun via byrå og agent" selected>Kun via byrå og agent</option>
												<?php
											}
											else 
											{
												?> 
												<option value="Når som helst">Når som helst</option>
												<option value="Etter avtale">Etter avtale</option>
												<option value="Kun via byrå og agent">Kun via byrå og agent</option>
												<?php
											}
											?>
											
										</select>
									</div>	
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>Alder</strong>
									</div>
									<div class="col-md-7">
										<input type="text" name="age" id="age" class="form-control" value="<?php echo $select_member[0]->age;?>" required>
									</div>	
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>Kjønn</strong>
									</div>
									<div class="col-md-7">
										<input type="text" name="gender" id="gender" class="form-control" value="<?php echo $select_member[0]->gender;?>" required>
									</div>	
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>Høyde</strong>
									</div>
									<div class="col-md-7">
										<input type="text" name="height" id="height" class="form-control" value="<?php echo $select_member[0]->height;?>" required>
									</div>	
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>Vekt</strong>
									</div>
									<div class="col-md-7">
										<input type="text" name="weight" id="weight" class="form-control" value="<?php echo $select_member[0]->weight;?>" required>
									</div>	
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>Hårfarge</strong>
									</div>
									<div class="col-md-7">
										<input type="text" name="hair_color" id="hair_color" class="form-control" value="<?php echo $select_member[0]->hair_color;?>" required>
									</div>	
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>Dialekt</strong>
									</div>
									<div class="col-md-7">
										<input type="text" name="dialect" id="dialect" class="form-control" value="<?php echo $select_member[0]->dialect;?>" required>
									</div>	
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>Språk</strong>
									</div>
									<div class="col-md-7">
										<input type="text" name="lang" id="lang" class="form-control" value="<?php echo $select_member[0]->lang;?>" required>
									</div>	
								</div>
							</div>							
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>Fornavn</strong>
									</div>
									<div class="col-md-7">
										<input type="text" name="firstname" id="fornavn" tabindex="1" class="form-control" value="<?php echo $select_member[0]->firstname;?>" required>
									</div>	
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>Etternavn</strong>
									</div>
									<div class="col-md-7">
										<input type="text" name="lastname" id="etternavn" tabindex="1" class="form-control" value="<?php echo $select_member[0]->lastname;?>" required>
									</div>	
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>Gate</strong>
									</div>
									<div class="col-md-7">
										<input type="text" name="post" id="gate" tabindex="1" class="form-control" value="<?php echo $select_member[0]->post;?>" required>
									</div>	
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>Post</strong>
									</div>
									<div class="col-md-7">
										<input type="text" name="city" id="post" tabindex="1" class="form-control"  value="<?php echo $select_member[0]->city;?>" required>
									</div>	
								</div>
							</div>
								
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>By</strong>
									</div>
									<div class="col-md-7">
										<input type="text" name="street" id="by" tabindex="1" class="form-control"  value="<?php echo $select_member[0]->street;?>" required>
									</div>	
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>Telefon</strong>
									</div>
									<div class="col-md-7">
										<input type="text" name="phone" id="telefon" tabindex="1" class="form-control" value="<?php echo $select_member[0]->phone;?>" required>
									</div>	
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>Fortell litt om deg selv...</strong>
									</div>
									<div class="col-md-7">
										<textarea name="about" class="form-control input" required><?php echo $select_member[0]->about;?></textarea>
									</div>	
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-5">
									<strong>Profilbilde</strong>
									</div>
									<div class="col-md-7">
										<input type="file" name="image_browse" id="image_browse" class="form-control">
									</div>	
								</div>
							</div>
							
							
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6 col-sm-offset-3">
										<input type="submit" name="register-submit"  class="form-control btn btn-register btn-pink" value="Submit"/>
									</div>
								</div>
							</div>
							
						</form>
						
						
						
						
					</div>
				</div>
			</div>
			</div>
		</div>
		</div>
	</div>
</section>
			
			
			
			
			
			
			
			</div>
			
			</article> <!-- .et_pb_post -->

	</div>
		
		
		
<?php get_footer(); ?>
		
		
