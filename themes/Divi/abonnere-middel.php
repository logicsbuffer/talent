<?php

/* Template name:  Abonnere Middel */

get_header(); ?>

<?php

//echo $_SESSION['secode'];
if($_SESSION['seccode'] == "")
{
	$url = "http://test.talentbasen.no/logg-inn/";
	wp_redirect( $url ); 
}
 

?>


<style>
.navbar-default {
    background-color: #ffffff;
    border-color: #ffffff;
    border-bottom: 1px solid #ec008c;
}
.et_header_style_split #main-header .container, .et_header_style_split #main-header .logo_container {
    z-index: 2;
    text-align: center;
    margin-top: 9px;
}
.borderimg{
border:2px solid #ee1495;
padding:10px 0px;
}
.btn-pink{
background-color: #ee1495;
    color: white;
}
#main-content .container:before {
    position: absolute;
    top: 0;
    width: 0px;
    height: 100%;
    background-color: #e2e2e2;
    content: "";
}
input.text, input.title, input[type=email], input[type=password], input[type=tel], input[type=text], select, textarea {
    padding: 5px;
    border: 1px solid #bbb;
    color: #4e4e4e;
    background-color: #fff;
}
.dashboard-grid ul li a {
    color: #111111;
    padding: 10px;
    line-height: 35px;
    
}
.dashboard-grid ul li a:hover {
    text-decoration:none;
}
.dashboard-grid ul li{
    color: #111111;
    padding: 10px;
    line-height: 35px;
    background-color: #dad9d9;
	width: 200px;
	border-bottom: 1px dotted;
}
.dashboard-grid ul li:hover{
    color: #ffffff;
    padding: 10px;
    line-height: 35px;
    background-color: #ee1495;
}
.dashboard-grid ul li i {
    float: right;
padding: 10px;
}

</style>
	<div id="main-content">

			<article id="post-1165" class="post-1165 page type-page status-publish hentry">

			<div class="entry-content">
			
			<section>    				
	<div class="container">
		<div class="row">

		<div class="col-sm-12">
		<div class="dashboard-grid">
			<div class="col-sm-12"><br>
<h2 class="text-center">Abonnere</h2><br>
				<div class="row">
<div class="col-sm-3">
				<div class="navbar-collapse" id="bs-sidebar-navbar-collapse-1">
				  <ul class="nav navbar-nav">		
					<li class=""><a href="http://test.talentbasen.no/middel-dashboard/">Min side<i class="fa fa-home" aria-hidden="true"></i></a></li>
					<li class=""><a href="http://test.talentbasen.no/abonnere-middel/">Abonnere<i class="fa fa-home" aria-hidden="true"></i></a></li>
					<li ><a href="http://test.talentbasen.no/logg-inn/?s=log">Logg ut<i class="fa fa-sign-out" aria-hidden="true"></i></a></li>
				  </ul>
				</div>					
					</div>
					
				 <div class="col-sm-9 text-center">
				 <h1 class="">Du er ikke abonnentagent. Klikk her for <a style="color: #ec008c;" href="#">abonnere</a></h1>
				 </div>
					
				</div>
			</div>
			</div>
		</div>
		</div>
	</div>
</section>
			
			
			
			
			
			
			
			</div>
			
			</article> <!-- .et_pb_post -->

	</div>
		
	
		
<?php get_footer(); ?>
		
		
