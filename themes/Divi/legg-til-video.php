<?php

/* Template name:  Legg til video */


get_header();
?>


<?php

//echo $_SESSION['secode'];
if($_SESSION['secode'] == "")
{
	$url = "http://test.talentbasen.no/logg-inn/";
	wp_redirect( $url ); 
}
 
?>



<style>
.navbar-default {
    background-color: #ffffff;
    border-color: #ffffff;
    border-bottom: 1px solid #ec008c;
}
.et_header_style_split #main-header .container, .et_header_style_split #main-header .logo_container {
    z-index: 2;
    text-align: center;
    margin-top: 9px;
}
.borderimg{
border:2px solid #ee1495;
}
.btn-pink{
background-color: #ee1495;
    color: white;
}
#main-content .container:before {
    position: absolute;
    top: 0;
    width: 0px;
    height: 100%;
    background-color: #e2e2e2;
    content: "";
}
input.text, input.title, input[type=email], input[type=password], input[type=tel], input[type=text], select, textarea {
    padding: 5px;
    border: 1px solid #bbb;
    color: #4e4e4e;
    background-color: #fff;
}
.dashboard-grid ul li a {
    color: #111111;
    padding-left: 10px;
    padding-right: 10px;
    /*line-height: 35px;*/
    
}
.dashboard-grid ul li a:hover {
    text-decoration:none;
}
.dashboard-grid ul li{
    color: #111111;
    /*padding: 10px;*/
    line-height: 35px;
    background-color: #dad9d9;
	width: 200px;
	border-bottom: 1px dotted;
}
.dashboard-grid ul li:hover{
    color: #ffffff;
    /*padding: 10px;*/
    line-height: 35px;
    background-color: #ee1495;
}
.dashboard-grid ul li i {
    float: right;
padding: 10px;
}
.gal {
	padding-bottom: 15px;
}
.gal > span{
    position:absolute;
    top:-5px;
    right:15px;
	height: 16px;
    width: 16px;
    display:none; 
}

.gal:hover span{
    display:block;  
}
.bla{
	margin-left: 0px;
}
</style>




	<div id="main-content">

			<article id="post-1165" class="post-1165 page type-page status-publish hentry">

			<div class="entry-content">
			
			<section>    				
	<div class="container">
		<div class="row">

		<div class="col-sm-12">
		<div class="dashboard-grid">
			<div class="col-sm-12"><br>
<h2 class="text-center">Profile Information</h2><br>
				<div class="row">
<div class="col-sm-3">
				<div class="navbar-collapse" id="bs-sidebar-navbar-collapse-1">
				  <ul class="nav navbar-nav">		
					<li class=""><a href="http://test.talentbasen.no/dashboard/">Min side<i class="fa fa-home" aria-hidden="true"></i></a></li>
					<li class=""><a href="http://test.talentbasen.no/legg-til-galleri/">Legg til galleri<i class="fa fa-home" aria-hidden="true"></i></a></li>
					<li class=""><a href="http://test.talentbasen.no/legg-til-video/">Legg til video<i class="fa fa-home" aria-hidden="true"></i></a></li>
					<li class=""><a href="http://test.talentbasen.no/abonnere-medlems/">Abonnere<i class="fa fa-home" aria-hidden="true"></i></a></li>
					<li ><a href="http://test.talentbasen.no/logg-inn/?s=log">Logg ut<i class="fa fa-sign-out" aria-hidden="true"></i></a></li>
				  </ul>
				</div>					
					</div>
					
					<?php
					global $wpdb;
					
					
					if(isset($_POST['register-submit1']))
						{
							
							if($_FILES['video']['name'] != "")
							{
								$time = time();
								$file = $time.$_FILES['video']['name'];
								$video = "http://talentbasen.no/test/wp-content/uploads/".date("Y")."/".date("m")."/".$file;
								
								function my_upload_dir($upload) {

								  return $upload;

								}
							
								if ( ! function_exists( 'wp_handle_upload' ) ) 
									$t = time();
									require_once( ABSPATH . 'wp-admin/includes/file.php' );
									$uploadedfile = $_FILES['video']; 
									
									$nm = $time.$uploadedfile['name'];
									$tp = $uploadedfile['type'];
									$tmpname = $uploadedfile['tmp_name'];
									$er = $uploadedfile['error'];
									$sz = $uploadedfile['size'];
									$file_nm = array(name => $nm, type => $tp, tmp_name => $tmpname, error => $er, size => $sz);
									//print_r($uploadedfile);
									//print_r($file_nm);die();
									$upload_overrides = array( 'test_form' => false );
									//add_filter('upload_dir', 'my_upload_dir');
									$movefile = wp_handle_upload( $file_nm, $upload_overrides );
									//remove_filter('upload_dir', 'my_upload_dir');

									
									if ( $movefile ) {
										//echo "File is valid, and was successfully uploaded.\n";
										//var_dump( $movefile);
										
										
										
										//$resultss = $wpdb->query('UPDATE `tlntb_member_register` SET `tlntb_member_register`.`video` ="'.$video.'" WHERE `tlntb_member_register`.`code` = "'.$_SESSION['secode'].'"');
										$resultss = $wpdb->query("insert into `tlntb_member_video` (`member_code`,`title`,`video`) values ('".$_SESSION['secode']."','".$_POST['title']."','".$video."')");
								
									} else {
										echo "Possible file upload attack!\n";
									}
								}
						}
								
					if(isset($_GET['id']))	
					{
						$select_member= $wpdb->get_results('delete from `tlntb_member_video` WHERE id= "'.$_GET['id'].'"');
					}
					$select_member= $wpdb->get_results('select * from `tlntb_member_video` where `tlntb_member_video`.`member_code` = "'.$_SESSION['secode'].'"');
					
					?>

					<div class="col-sm-9 text-center">
						
						<div class="row">
						
						<?php
						foreach($select_member as $drft)
						{
							?>
							<div class="col-sm-3 gal">						
								
								
								
							<div class="thumb column-thumb" data-colname="Image">
								
											<?php 
											$videolink = $drft->video;
											echo do_shortcode('[video width="150" height="170" mp4="'.$videolink.'"][/video]'); ?>
										
							</div>					
							<h5><?php echo $drft->title; ?></h5>
								<span><a href="http://test.talentbasen.no/legg-til-video/?id=<?php echo $drft->id; ?>"><img src="http://test.talentbasen.no/wp-content/themes/Divi/images/cross.png"/></a></span>
							
							</div>
							<?php
						}
						?>
							
													
							
						</div>
					</div>
					
					
					
					<div class="row">
					
					<div class="col-sm-6">
					
						<form class="panel-login dashboard-form" enctype="multipart/form-data"  action="" method="post" role="form">
						
							
							
							<div class="form-group">
								<div class="row" style="margin-top: 50px;">
									<div class="col-md-2">
									<strong>Title</strong>
									</div>
									<div class="col-md-4">
										<input type="text" name="title" class="form-control">
									</div>	
								</div>
							</div>
							
							<div class="form-group">
								<div class="row" style="margin-top: 50px;">
									<div class="col-md-2">
									<strong>Bilde</strong>
									</div>
									<div class="col-md-4">
										<input type="file" name="video" id="video" class="form-control">
									</div>	
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-sm-3 bla col-sm-offset-3">
										<input type="submit" name="register-submit1"  class="form-control btn btn-register btn-pink" value="Bla gjennom"/>
									</div>
								</div>
							</div> 
							
							</form>
							
						</div>
					</div>
					
					
				</div>
			</div>
			</div>
		</div>
		</div>
	</div>
</section>
			
			
			
			
			
			
			
			</div>
			
			</article> <!-- .et_pb_post -->

	</div>
		
		
		
<?php get_footer(); ?>
		
		
