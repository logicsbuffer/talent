<?php

/* Template name:  Register */

get_header(); ?>

<style>
.et_header_style_split #main-header .container, .et_header_style_split #main-header .logo_container {
    z-index: 2;
    text-align: center;
    margin-top: 9px;
}
.borderimg{
border:2px solid #ee1495;
padding:10px 0px;
}
.btn-pink{
background-color: #ee1495;
    color: white;
}
#main-content .container:before {
    position: absolute;
    top: 0;
    width: 0px;
    height: 100%;
    background-color: #e2e2e2;
    content: "";
}
input.text, input.title, input[type=email], input[type=password], input[type=tel], input[type=text], select, textarea {
        padding: 5px;
    border: 1px solid #bbb;
    color: #4e4e4e;
    background-color: #fff;
    margin: 5px 0px;
}
.dashboard-grid ul li a {
    color: #111111;
    padding: 10px;
    line-height: 35px;
    
}
.dashboard-grid ul li a:hover {
    text-decoration:none;
}
.dashboard-grid ul li{
    color: #111111;
    padding: 10px;
    line-height: 35px;
    background-color: #dad9d9;
}
.dashboard-grid ul li:hover{
    color: #ffffff;
    padding: 10px;
    line-height: 35px;
    background-color: #ee1495;
}
.dashboard-grid ul li i {
    float: right;
padding: 10px;
}

</style>
	<div id="main-content">

			<article id="post-1165" class="post-1165 page type-page status-publish hentry">

			<div class="entry-content">
			
	<section>    				
	<div class="container">
		<div class="row">

		<div class="col-sm-12">
		<div class="dashboard-grid">
			<div class="col-sm-12"><br>
<h2 class="text-center">Profile Registration</h2><br>
				<div class="row">
					<div class="col-sm-6">
					<h3>Registrere talentet ditt hos oss</h3>
					<p class="text-justify">I motsetning til hva mange tror, er ikke Lorem Ipsum bare tilfeldig tekst. Dets rtter springer helt tilbake til et stykke klassisk latinsk litteratur fra 45 r f.kr., hvilket gjr det over 2000 ar gammelt. </p>
<p class="text-justify">Richard McClintock  professor i latin ved Hampden-Sydney College i Virginia, USA  slo opp flere av de mer obskure latinske ordene, consectetur, fra en del av Lorem Ipsum, og fant dets utvilsomme opprinnelse gjennom a studere bruken av disse ordene i klassisk litteratur. </p>
<p class="text-justify">Lorem Ipsum kommer fra seksjon 1.10.32 og 1.10.33 i de Finibus Bonorum et Malorum (The Extremes of Good and Evil) av Cicero, skrevet i ar 45 f.kr. Boken er en avhandling om teorier rundt etikk, og var veldig popular under renessansen. Den frste linjen av Lorem Ipsum, Lorem Ipsum dolor sit ame, er hentet fra en linje i seksjon 1.10.32.</p>
					</div>
				 
					<div class="col-sm-6">
					
						<form class="panel-login dashboard-form" enctype="multipart/form-data" id="dashboard-form" action="" method="post" role="form" style="">
							<div class="form-group">
								<div class="row">
									
									<div class="col-md-6">
										<input type="text" id="firstname" tabindex="1" class="form-control"   placeholder="Firstname">
									</div>	
								
									
									<div class="col-md-6">
										<input type="text" id="lastname" tabindex="1" class="form-control"   placeholder="Lastname">
									</div>	
								</div>
							</div>
							<div class="form-group">
 <div class="row">
									
									<div class="col-md-6">
										<input type="email" id="email" tabindex="1" class="form-control"   placeholder="Email">
									</div>	
	
									
									<div class="col-md-6">
										<input type="text" name="phone" id="phone" tabindex="1" class="form-control" placeholder="Phone">
									</div>	
								</div>
								
							</div>
							<div class="form-group">
<div class="row">
									
									<div class="col-md-6">
										<input type="text" name="address1" id="address1" tabindex="1" class="form-control" placeholder="Address" >
									</div>	

									<div class="col-md-6">
										<input type="text" name="city" id="city" tabindex="1" class="form-control" placeholder="City">
									</div>	
								</div>
								
							</div>
							<div class="form-group">
<div class="row">
									
									<div class="col-md-6">
										<input type="text" name="state" id="state" tabindex="1" class="form-control" placeholder="State">
									</div>	
	
									
									<div class="col-md-6">
										<select class="form-control" name="erfaring" required>
								<option value="">-- Erfaring --</option><option value="Nybegynner">Nybegynner</option><option value="Litt erfaring">Litt erfaring</option><option value="Mye erfaring">Mye erfaring</option><option value="Profesjonell">Profesjonell</option>
							</select>
									</div>	
								</div>
								
								
								
							</div>
							<div class="form-group">
<div class="row">
									
									<div class="col-md-6">
										<select class="form-control" name="erfaring" required>
								<option value="">-- Erfaring --</option><option value="Nybegynner">Nybegynner</option><option value="Litt erfaring">Litt erfaring</option><option value="Mye erfaring">Mye erfaring</option><option value="Profesjonell">Profesjonell</option>
							</select>
									</div>	
<div class="col-md-6">
										<select class="form-control" name="erfaring" required>
								<option value="">-- Erfaring --</option><option value="Nybegynner">Nybegynner</option><option value="Litt erfaring">Litt erfaring</option><option value="Mye erfaring">Mye erfaring</option><option value="Profesjonell">Profesjonell</option>
							</select>
									</div>
								</div>
								
								
																

							</div>
							<div class="form-group">
								<div class="row">
									
									<div class="col-md-12">
										<textarea name="fortell" class="form-control input" placeholder="Fortell litt om deg selv..." required></textarea>
									</div>	
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6">
										<input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register btn-pink" value="Submit">
									</div>
								</div>
							</div>
						</form>
						
						
						
						
					</div>
				</div>
			</div>
			</div>
		</div>
		</div>
	</div>
</section>
			
			
			
			
			
			
			
			</div>
			
			</article> <!-- .et_pb_post -->

	</div>
		
		
		
<?php get_footer(); ?>
		
		
