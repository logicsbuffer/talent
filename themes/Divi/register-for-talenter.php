<?php

/* Template name:  Registrering For Talenter */

get_header(); ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<style>
.et_header_style_split #main-header .container, .et_header_style_split #main-header .logo_container {
    z-index: 2;
    text-align: center;
    margin-top: 9px;
}
.borderimg{
border:2px solid #ee1495;
padding:10px 0px;
}
.btn-pink{
background-color: #ee1495;
    color: white;
}
#main-content .container:before {
    position: absolute;
    top: 0;
    width: 0px;
    height: 100%;
    background-color: #e2e2e2;
    content: "";
}
input.text, input.title, input[type=email], input[type=password], input[type=tel], input[type=text], select, textarea {
        padding: 5px;
    border: 1px solid #bbb;
    color: #4e4e4e;
    background-color: #fff;
    margin: 5px 0px;
}
.dashboard-grid ul li a {
    color: #111111;
    padding: 10px;
    line-height: 35px;
    
}
.dashboard-grid ul li a:hover {
    text-decoration:none;
}
.dashboard-grid ul li{
    color: #111111;
    padding: 10px;
    line-height: 35px;
    background-color: #dad9d9;
}
.dashboard-grid ul li:hover{
    color: #ffffff;
    padding: 10px;
    line-height: 35px;
    background-color: #ee1495;
}
.dashboard-grid ul li i {
    float: right;
padding: 10px;
}

</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<div id="main-content">

			<article id="post-1165" class="post-1165 page type-page status-publish hentry">

			<div class="entry-content">
			
	<section>    				
	<div class="container">
		<div class="row">

		<div class="col-sm-12">
		<div class="dashboard-grid">
			<div class="col-sm-12"><br>
<!--<h2 class="text-center">Registrering For Talenter</h2><br>-->
				<div class="row">
					<div class="col-sm-6">
					<?php $the_query = new WP_Query( 'page_id=1430' ); ?>
					<?php while ($the_query -> have_posts()) : $the_query -> the_post();  ?>
					<?php the_content(); ?>
					<?php endwhile;?>
					</div>
				 
					<div class="col-sm-6">
					
					<h2><i class="fa fa-check"></i>Jeg ønsker å registrere min profil:</h2>
					
					<?php
					function GeraHash($qtd){ 
					//Under the string $Caracteres you write all the characters you want to be used to randomly generate the code. 
					$Caracteres = 'ABCDEFGHIJKLMOPQRSTUVXWYZ0123456789'; 
					$QuantidadeCaracteres = strlen($Caracteres); 
					$QuantidadeCaracteres--; 

					$Hash=NULL; 
						for($x=1;$x<=$qtd;$x++){ 
							$Posicao = rand(0,$QuantidadeCaracteres); 
							$Hash .= substr($Caracteres,$Posicao,1); 
						} 

					return $Hash; 
					}
					
					
					global $wpdb;
					
					if(isset($_POST['registrere']))
					{
						$rand = GeraHash(30);
						$code = $rand;
						$username = $_POST['brukernavn'];
						$password = $_POST['passord'];
						$firstname = $_POST['fornavn'];
						$lastname = $_POST['etternavn'];
						$post = $_POST['gate'];
						$city = $_POST['post'];
						$street = $_POST['by'];
						$email = $_POST['epost'];
						$phone = $_POST['telefon'];
						$category = $_POST['kategori'];
						$experience = $_POST['erfaring'];
						$assignments = $_POST['oppdrag'];
						$about = $_POST['fortell'];
						$status = '0';
						$dat = date('Y-m-d');
						
						
						$execut= $wpdb->query('INSERT INTO `tlntb_member_register` (`code`,`username`,`password`,`firstname`,`lastname`,`street`,`post`,`city`,`email`,`phone`,`category`,`experience`,`assignments`,`about`,`band`,`status`,`dat`) VALUES ("'.$code.'","'.$username.'","'.$password.'","'.$firstname.'","'.$lastname.'","'.$street.'","'.$post.'","'.$city.'","'.$email.'","'.$phone.'","'.$category.'","'.$experience.'","'.$assignments.'","'.$about.'","'.$band.'","'.$status.'","'.$dat.'")');
						//print_r($execut); exit;
						?>
						<script>
						$(document).ready(function(){
							alert('Vi tar 24 timer å aktivere kontoen din');
						});
						</script>
						<?php
					}
				
				?>	
					
					
					
					
						<form class="panel-login dashboard-form" enctype="multipart/form-data" id="dashboard-form" action="" method="post" role="form" style="">
							
							<div class="form-group">
								<div class="row">
									
									<div class="col-md-6">
										<input type="test" tabindex="1" class="form-control" name="fornavn"  placeholder="Fornavn" required>
									</div>	
	
									
									<div class="col-md-6">
										<input type="text" tabindex="1" class="form-control" name="etternavn" placeholder="Etternavn" required>
									</div>	
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
									
									<div class="col-md-6">
										<input type="text" tabindex="1" class="form-control" name="gate" placeholder="Gate" required>
									</div>	
									
									<div class="col-md-6">
										<input type="text" tabindex="1" class="form-control" name="post" placeholder="Post" required>
									</div>
									
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
								
									<div class="col-md-6">
										<input type="text" tabindex="1" class="form-control" name="by" placeholder="By" required>
									</div>

									<div class="col-md-6">
										<input type="email" tabindex="1" class="form-control" name="epost" placeholder="Epost" required>
									</div>	
							
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<input type="tel" tabindex="1" class="form-control" name="telefon" placeholder="Telefon" required>
									</div>
									<div class="col-md-6">
										<select class="form-control" name="kategori" required>
											<option value="">-- Kategori --</option><option value="Danse">Danse</option><option value="Musikk">Musikk</option><option value="Sport">Sport</option><option value="Reklamefilmer">Film og Teater</option><option value="Kultur">Kultur</option>
										</select>
									</div>	
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">					
									<div class="col-md-6">
										<select class="form-control" name="erfaring" required>
											<option value="">-- Erfaring --</option><option value="Nybegynner">Nybegynner</option><option value="Litt erfaring">Litt erfaring</option><option value="Mye erfaring">Mye erfaring</option><option value="Profesjonell">Profesjonell</option>
										</select>
									</div>	
									<div class="col-md-6">
										<select class="form-control" name="oppdrag" required>
											<option value="">-- Kan ta oppdrag --</option><option value="Når som helst">Når som helst</option><option value="Etter avtale">Etter avtale</option><option value="Kun via byrå og agent">Kun via byrå og agent</option>
										</select>
									</div>
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<input type="text" tabindex="1" name="brukernavn" class="form-control"   placeholder="Brukernavn" required>
									</div>
									<div class="col-md-6">
										<input type="text" tabindex="1" class="form-control" name="passord"  placeholder="Passord" required>
									</div>	
								</div>
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<textarea name="fortell" class="form-control input" placeholder="Fortell litt om deg selv..." required></textarea>
									</div>	
								</div>
							</div>
							
							<p><input type="checkbox" name="ag18" required  value="18"> Jeg er over 18 år og myndig, og trenger ikke samtykke for å registrere min profil</p>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6">
										<input type="submit" name="registrere" tabindex="4" class="form-control btn btn-register btn-pink" value="Registrer">
									</div>
								</div>
							</div>
							
						</form>
						
						
						
						
					</div>
				</div>
			</div>
			</div>
		</div>
		</div>
	</div>


	
</section>
			
			
			
			
			
			
			
			</div>
			
			</article> <!-- .et_pb_post -->

	</div>
		
		


				
					
		
		
<?php get_footer(); ?>
		
		
