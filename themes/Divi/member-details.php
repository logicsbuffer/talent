<?php

/* Template name:  Medlem Detaljer */

get_header(); ?>
<style>
.navbar-default {
    background-color: #ffffff;
    border-color: #ffffff;
    border-bottom: 1px solid #ec008c;
}
.et_header_style_split #main-header .container, .et_header_style_split #main-header .logo_container {
    z-index: 2;
    text-align: center;
    margin-top: 9px;
}
.borderimg{
border:2px solid #ee1495;
padding:10px 0px;
}
.btn-pink{
background-color: #ee1495;
    color: white;
}
#main-content .container:before {
    position: absolute;
    top: 0;
    width: 0px;
    height: 100%;
    background-color: #e2e2e2;
    content: "";
}
input.text, input.title, input[type=email], input[type=password], input[type=tel], input[type=text], select, textarea {
    padding: 5px;
    border: 1px solid #bbb;
    color: #4e4e4e;
    background-color: #fff;
}
.dashboard-grid ul li a {
    color: #111111;
    padding: 10px;
    line-height: 35px;
    
}
.dashboard-grid ul li a:hover {
    text-decoration:none;
}
.dashboard-grid ul li{
    color: #111111;
    padding: 10px;
    line-height: 35px;
    background-color: #dad9d9;
}
.dashboard-grid ul li:hover{
    color: #ffffff;
    padding: 10px;
    line-height: 35px;
    background-color: #ee1495;
}
.dashboard-grid ul li i {
    float: right;
padding: 10px;
}
.mt10b0{
margin:10px 0px;
}
.fluid-width-video-wrapper iframe, .fluid-width-video-wrapper object, .fluid-width-video-wrapper embed {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 50%;
}
.vid {
    position: relative;
    padding-bottom: 56.25%;
    padding-top: 0px; height: 0; overflow: hidden;     margin: 5px 0px;
}
 

.vid iframe,
.vid object,
.vid embed {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}
</style>
	<div id="main-content">

			<article id="post-1165" class="post-1165 page type-page status-publish hentry">

			<div class="entry-content">
			
			<section>    				
	<div class="container-fluid">
		<div class="row">

		<div class="col-sm-12">
		<div class="dashboard-grid">
			<div class="col-sm-12"><br>
<h2 class="text-center">profil informasjon</h2><br>
				<div class="row">

				 <div class="col-sm-3 text-center">
				 <?php
			
				global $wpdb;
				
				if(isset($_POST['submit']))
				{
					
					$subject="Contact Mail";    
					$headers="MIME-Version: 1.0\n"; 
					$headers.="Content-type: text/html; charset=iso-8859-1 \n"; 
					
				   
					$headers.="from:Talentbasen<".$_POST['email'].">";
					$mail_body="<table cellpadding='0' cellspacing='0' border='0'><tr><td>Navn: ".$_POST['fname']."</td></tr>";
					$mail_body.="<tr><td>E-post: ".$_POST['email']."</td></tr>";
					$mail_body.="<tr><td>Antall: ".$_POST['phone']."</td></tr></table>";   
					
					$mail_to="pinkwebsolutionz@gmail.com";

					if(@wp_mail($mail_to, $subject, $mail_body, $headers)) 
					
					{
						?>
						
					   <script type="text/javascript">
						 alert("Thank You! Your Message Has Been Sent Successfully");
						</script>
							
						<?php 
					} 
					else
					{
					  print("<h1><font color=\"#880000\">Sorry! An Error Occurred While Sending Message. Please Try Again</font></h1>"); 
					}
				}	
					
				$select_smember= $wpdb->get_results('select * from `tlntb_member_register` where `tlntb_member_register`.`code` = "'.$_GET['code'].'" and `tlntb_member_register`.`status` = "1"');
			
				?>
            
				<img class="img-responsive borderimg center-block " src="<?php echo $select_smember[0]->image_browse; ?>">			
				<br><br>
				<form class="panel-login dashboard-form" id="dashboard-form" action="" method="post">
						<h2>kontakt oss</h2>
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<input type="text" name="fname" tabindex="1" class="form-control"   placeholder="Navn">
									</div>	
								</div>
								
							</div>
							
							<div class="form-group">
 <div class="row">
									
									<div class="col-md-12">
										<input type="email" name="email" tabindex="1" class="form-control"   placeholder="E-post">
									</div>	
								</div>
								
							</div>
							<div class="form-group">
<div class="row">
									
									<div class="col-md-12">
										<input type="tel" name="phone" tabindex="1" class="form-control" placeholder="Antall">
									</div>	
								</div>
								
							</div>
                            
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6 text-center">
										<input type="submit" name="submit" tabindex="4" class="form-control btn btn-register btn-pink" value="Submit">
									</div>
								</div>
							</div>
						</form>			
					</div>
					<div class="col-sm-9">
					
						<div class="panel-login dashboard-form" id="dashboard-form">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>Navn: </strong> <?php echo $select_smember[0]->firstname." ".$select_smember[0]->lastname; ?>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>Adresse: </strong> <?php echo $select_smember[0]->address; ?>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>gate: </strong> <?php echo $select_smember[0]->street; ?>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>post: </strong> <?php echo $select_smember[0]->post; ?>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>By: </strong> <?php echo $select_smember[0]->city; ?>
											</div>
										</div>
										
									</div>
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>e-post: </strong> <?php echo $select_smember[0]->email; ?>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>telefon: </strong> <?php echo $select_smember[0]->phone; ?>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>Alder: </strong> <?php echo $select_smember[0]->age; ?>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>Kjønn: </strong> <?php echo $select_smember[0]->gender; ?>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>Høyde: </strong> <?php echo $select_smember[0]->height; ?>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>Vekt: </strong> <?php echo $select_smember[0]->weight; ?>
											</div>
										</div>
									</div>
								</div>
								
								
								
								<div class="col-md-6">
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>Kategori: </strong> <?php echo $select_smember[0]->category; ?>
											</div>
										</div>
									</div>
									
									<?php
									if($select_smember[0]->category == "Musikk")
									{
										?>
										<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>Band: </strong> <?php echo $select_smember[0]->band; ?>
											</div>
										</div>
										</div> 
										
										<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>Partyband: </strong> <?php echo $select_smember[0]->partyband; ?>
											</div>
										</div>
										</div>
										
										<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>Dj Erfaring: </strong> <?php echo $select_smember[0]->dj; ?>
											</div>
										</div>
										</div>
										
										<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>Sanger Solo Erfaring: </strong> <?php echo $select_smember[0]->sanger; ?>
											</div>
										</div>
										</div>
		 
										<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>Instrumentals Type: </strong> <?php echo $select_smember[0]->instrumentals; ?>
											</div>
										</div>
										</div>

										<?php
									}
									elseif($select_smember[0]->category == "Danse")
									{
										?>
										<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>Moderne Dans Erfaring: </strong> <?php echo $select_smember[0]->moderne; ?>
											</div>
										</div>
										</div>  

										
										<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>Hip Hop Erfaring: </strong> <?php echo $select_smember[0]->hip; ?>
											</div>
										</div>
										</div>
										
										<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>Ballett Erfaring: </strong> <?php echo $select_smember[0]->ballett; ?>
											</div>
										</div>
										</div>

										<?php
									}
									elseif($select_smember[0]->category == "Danse")
									{
										?>
										<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>Film og teater: </strong> <?php echo $select_smember[0]->film-theater; ?>
											</div>
										</div>
										</div>

										<?php
									}
									else
									{}
									?>
									
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>Erfaring: </strong> <?php echo $select_smember[0]->experience; ?>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>Oppdrag: </strong> <?php echo $select_smember[0]->assignments; ?>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>Hårfarge: </strong> <?php echo $select_smember[0]->hair_color; ?>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>Dialekt: </strong> <?php echo $select_smember[0]->dialect; ?>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-12">
											<strong>Språk: </strong> <?php echo $select_smember[0]->lang; ?>
											</div>
										</div>
									</div>
									
								</div>
							</div>
							
						

						</div>
						<h3><strong>Bildegalleri</strong></h3>
<style>.gallery
{
    display: inline-block;
    margin-top: 20px;
}</style>					
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>


<div class="container1">
	
<div class="row">
		
<div class='list-group gallery'>
   
         <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
  
              <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $select_smember[0]->gal_image1; ?>">
  
                  <img class="img-responsive" alt="" src="<?php echo $select_smember[0]->gal_image1; ?>" />
   
                <!-- <div class='text-right'>
    
                    <small class='text-muted'>Image Title</small>
   
                 </div>  text-right / end -->
 
               </a>
            </div> <!-- col-6 / end -->
     
       <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
    
            <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $select_smember[0]->gal_image2; ?>">
  
                  <img class="img-responsive" alt="" src="<?php echo $select_smember[0]->gal_image2; ?>" />
    
             <!--   <div class='text-right'>
            
            <small class='text-muted'>Image Title</small>
     
               </div>  text-right / end -->
      
          </a>
            </div> <!-- col-6 / end -->
      
      <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
     
           <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $select_smember[0]->gal_image3; ?>">
    
                <img class="img-responsive" alt="" src="<?php echo $select_smember[0]->gal_image3; ?>" />
     
             <!--  <div class='text-right'>
                    
    <small class='text-muted'>Image Title</small>
         
           </div>  text-right / end -->
          
      </a>
            </div> <!-- col-6 / end -->
        
    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
        
        <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $select_smember[0]->gal_image4; ?>">
    
                <img class="img-responsive" alt="" src="<?php echo $select_smember[0]->gal_image4; ?>" />
          
       <!--   <div class='text-right'>
                    
    <small class='text-muted'>Image Title</small>
          
          </div>  text-right / end -->
           
     </a>
            </div> <!-- col-6 / end -->
        
    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
    
            <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $select_smember[0]->gal_image5; ?>">
      
              <img class="img-responsive" alt="" src="<?php echo $select_smember[0]->gal_image5; ?>" />
      
            <!--  <div class='text-right'>
                      
  <small class='text-muted'>Image Title</small>
          
          </div>  text-right / end -->
              
  </a>
            </div> <!-- col-6 / end -->
  
  <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
    
            <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $select_smember[0]->gal_image6; ?>">
      
              <img class="img-responsive" alt="" src="<?php echo $select_smember[0]->gal_image6; ?>" />
      
             <!-- <div class='text-right'>
                      
  <small class='text-muted'>Image Title</small>
          
          </div>  text-right / end -->
              
  </a>
            </div> <!-- col-6 / end -->
 
  <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
    
            <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $select_smember[0]->gal_image7; ?>">
      
              <img class="img-responsive" alt="" src="<?php echo $select_smember[0]->gal_image7; ?>" />
      
              <!--<div class='text-right'>
                      
  <small class='text-muted'>Image Title</small>
          
          </div>  text-right / end -->
              
  </a>
            </div> <!-- col-6 / end -->
         
  <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
      
          <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $select_smember[0]->gal_image8; ?>">
  
                  <img class="img-responsive" alt="" src="<?php echo $select_smember[0]->gal_image8; ?>" />
        
            <!-- <div class='text-right'>
                   
     <small class='text-muted'>Image Title</small>
      
              </div> text-right / end -->
           
     </a>
            </div> <!-- col-6 / end -->
        </div> <!-- list-group / end -->

</div> <!-- row / end -->

</div> <!-- container / end -->
<!--	<div class="row">

							<div class="col-md-3">
								<img src="http://www.excellenceonlinetutors.com/images/courses/2.jpg" class="img-responsive mt10b0">
							</div>
<div class="col-md-3">
								<img src="http://www.excellenceonlinetutors.com/images/courses/2.jpg" class="img-responsive mt10b0">
							</div>
<div class="col-md-3">
								<img src="http://www.excellenceonlinetutors.com/images/courses/2.jpg" class="img-responsive mt10b0">
							</div>
<div class="col-md-3">
								<img src="http://www.excellenceonlinetutors.com/images/courses/2.jpg" class="img-responsive mt10b0">
							</div>
<div class="col-md-3">
								<img src="http://www.excellenceonlinetutors.com/images/courses/2.jpg" class="img-responsive mt10b0">
							</div>
<div class="col-md-3">
								<img src="http://www.excellenceonlinetutors.com/images/courses/2.jpg" class="img-responsive mt10b0">
							</div>
<div class="col-md-3">
								<img src="http://www.excellenceonlinetutors.com/images/courses/2.jpg" class="img-responsive mt10b0">
							</div>
<div class="col-md-3">
								<img src="http://www.excellenceonlinetutors.com/images/courses/2.jpg" class="img-responsive mt10b0">
							</div>
						</div> -->
<script>
$(document).ready(function(){
    //FANCYBOX
    //https://github.com/fancyapps/fancyBox
    $(".fancybox").fancybox({
        openEffect: "none",
        closeEffect: "none"
    });
});
   
</script>

						<div class="row">
							 <div class="col-md-6">
            <div class="vid">
                <?php echo $select_smember[0]->video1; ?>
            </div><!--./vid -->
           
        </div><!--.col -->
 <div class="col-md-6">
            <div class="vid">
                <?php echo $select_smember[0]->video2; ?>
            </div><!--./vid -->
            
        </div><!--.col -->
<div class="col-sm-12">
<h3>Handle om :</h3>
										<p><?php echo $select_smember[0]->about; ?></p>
									</div>
						</div>

					</div>
				</div>
			</div>
			</div>
		</div>
		</div>
	</div>
</section>
			</div>
			
			</article> <!-- .et_pb_post -->

	</div>
		
		
		
<?php get_footer(); ?>
		
		
