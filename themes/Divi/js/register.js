    /* ---------------------------------------------------------------------------
	 * Register form
	 * --------------------------------------------------------------------------- */

	jQuery(document).ready(function() {
     
     /*  jQuery("#tel_music").hide();
      jQuery("#tel_dance").hide();
      jQuery("#tel_actor").hide();
      jQuery("#tel_model").hide();
      jQuery("#tel_culture").hide(); */
      jQuery("#assignment_attchment").on('click', function() {
        console.log('afsadf');
        jQuery("#multi-4714-0").show();
      });
      jQuery("#talent_category,#talent_category2").on('change', function() {
        var selected_cat = jQuery(this).val();
        console.log(selected_cat);
        
        if(selected_cat == 'music'){        
          jQuery("#tel_music_gender").show();
          jQuery("#tel_music").show();
          jQuery("#tel_dance").hide();
          jQuery("#tel_actor").hide();
          jQuery("#tel_statist").hide();
          jQuery("#tel_model").hide();
          jQuery("#tel_voiceover").hide();
          jQuery("#tel_film").hide();
          jQuery("#tel_culture").hide();
		  jQuery("#tel_sports").hide();
		  jQuery("#tel_band").hide();
        }
        else if(selected_cat == 'dancer'){
		  jQuery("#tel_dance").show(); 
		  jQuery("#tel_music").hide();
          jQuery("#tel_actor").hide();
          jQuery("#tel_statist").hide();
          jQuery("#tel_model").hide();
          jQuery("#tel_voiceover").hide();
          jQuery("#tel_film").hide();
          jQuery("#tel_culture").hide();
		  jQuery("#tel_sports").hide();
		  jQuery("#tel_band").hide();
        }
        else if(selected_cat == 'actor'){
		  jQuery("#tel_actor").show();
		  jQuery("#tel_music").hide();
			jQuery("#tel_dance").hide();
          jQuery("#tel_statist").hide();
          jQuery("#tel_model").hide();
          jQuery("#tel_voiceover").hide();
          jQuery("#tel_film").hide();
          jQuery("#tel_culture").hide();
		  jQuery("#tel_sports").hide();
		  jQuery("#tel_band").hide();
        }
        else if(selected_cat == 'film'){
		  jQuery("#tel_film").show();
		  jQuery("#tel_music").hide();
         jQuery("#tel_dance").hide();
          jQuery("#tel_actor").hide();
          jQuery("#tel_statist").hide();
          jQuery("#tel_model").hide();
          jQuery("#tel_voiceover").hide();
          jQuery("#tel_culture").hide();
		  jQuery("#tel_sports").hide();
		  jQuery("#tel_band").hide();

        }
        else if(selected_cat == 'culture'){
		  jQuery("#tel_culture").show(); 
		  jQuery("#tel_music").hide();
         jQuery("#tel_dance").hide();
          jQuery("#tel_actor").hide();
          jQuery("#tel_statist").hide();
          jQuery("#tel_model").hide();
          jQuery("#tel_voiceover").hide();
          jQuery("#tel_film").hide();
		  jQuery("#tel_sports").hide();
			jQuery("#tel_band").hide();


        }
		 else if(selected_cat == 'sports'){
		  jQuery("#tel_culture").hide(); 
		  jQuery("#tel_music").hide();
          jQuery("#tel_dance").hide();
          jQuery("#tel_actor").hide();
          jQuery("#tel_film").hide();
		  jQuery("#tel_music").hide();
          jQuery("#tel_sports").show();
		  jQuery("#tel_dance").hide();
          jQuery("#tel_actor").hide();
          jQuery("#tel_statist").hide();
          jQuery("#tel_model").hide();
          jQuery("#tel_voiceover").hide();
          jQuery("#tel_film").hide();
          jQuery("#tel_culture").hide();
          jQuery("#tel_band").hide();
        }
        else{
		  jQuery("#tel_dance").hide();
          jQuery("#tel_actor").hide();
          jQuery("#tel_statist").hide();
          jQuery("#tel_model").hide();
		  jQuery("#tel_music").hide();
          jQuery("#tel_voiceover").hide();
          jQuery("#tel_film").hide();
          jQuery("#tel_culture").hide();
		  jQuery("#tel_sports").hide();
		jQuery("#tel_band").hide();

        }
      });
		jQuery("#music_category").on('change', function() {
			var selected_cat = jQuery(this).val();
			console.log(selected_cat);
			if(selected_cat == 'band'){
				jQuery("#tel_band").show(); 
				jQuery("#tel_dj_experience").hide();
				jQuery("#tel_singer").hide();
				jQuery("#tel_instrumentals ").hide();
			}
			else if(selected_cat == 'dj'){
				jQuery("#tel_band").hide(); 
				jQuery("#tel_dj_experience").show();
				jQuery("#tel_singer").hide();
				jQuery("#tel_instrumentals ").hide();
			}
			else if(selected_cat == 'singer'){
				jQuery("#tel_band").hide(); 
				jQuery("#tel_dj_experience").hide();
				jQuery("#tel_singer").show();
				jQuery("#tel_instrumentals ").hide();
			}
			else if(selected_cat == 'instrumentals'){
				jQuery("#tel_band").hide(); 
				jQuery("#tel_dj_experience").hide();
				jQuery("#tel_singer").hide();
				jQuery("#tel_instrumentals ").show();
			}else{
				jQuery("#tel_band").hide(); 
				jQuery("#tel_dj_experience").hide();
				jQuery("#tel_singer").hide();
				jQuery("#tel_instrumentals").hide();
			}
		});
		
		jQuery("#dancer_type").on('change', function() {
			var selected_cat = jQuery(this).val();
			console.log(selected_cat);
			if(selected_cat == 'modern_dance'){
				jQuery("#tel_modern_dance").show(); 
				jQuery("#tel_hip_hop_dance").hide();
				jQuery("#tel_ballet_dance").hide();
			}
			else if(selected_cat == 'hip_hop'){
				jQuery("#tel_modern_dance").show(); 
				jQuery("#tel_hip_hop_dance").hide();
				jQuery("#tel_ballet_dance ").hide();
			}
			else if(selected_cat == 'ballet'){
				jQuery("#tel_modern_dance").show(); 
				jQuery("#tel_hip_hop_dance").hide();
				jQuery("#tel_ballet_dance").hide();
			}else{
				jQuery("#tel_modern_dance").hide(); 
				jQuery("#tel_hip_hop_dance").hide();
				jQuery("#tel_ballet_dance").hide();
			}
		});
		jQuery("#film_category").on('change', function() {
			var selected_cat = jQuery(this).val();
			console.log(selected_cat);
			if(selected_cat == 'statist'){
				jQuery("#tel_statist").show(); 
				jQuery("#tel_actor").hide();
				jQuery("#tel_model").hide();
				jQuery("#tel_voiceover").hide();
			}
			else if(selected_cat == 'actor'){
				jQuery("#tel_statist").hide(); 
				jQuery("#tel_actor").show();
				jQuery("#tel_model").hide();
				jQuery("#tel_voiceover").hide();
			}
			else if(selected_cat == 'model'){
				jQuery("#tel_statist").hide(); 
				jQuery("#tel_actor").hide();
				jQuery("#tel_model").show();
				jQuery("#tel_voiceover").hide();
			}
			else if(selected_cat == 'voiceover'){
				jQuery("#tel_statist").hide(); 
				jQuery("#tel_actor").hide();
				jQuery("#tel_model").hide();
				jQuery("#tel_voiceover").show();
			}else{
				jQuery("#tel_statist").hide(); 
				jQuery("#tel_actor").hide();
				jQuery("#tel_model").hide();
				jQuery("#tel_voiceover").hide();
			}
		});
		jQuery("#payment_type").on('change', function() {
          var payment_type = jQuery(this).val();
			console.log(payment_type);
			if(payment_type == 'voluntary'){
				jQuery("#assign_amount").hide();
			}
			else if(payment_type == 'paid'){
				jQuery("#assign_amount").show();
			}
          	else{
				jQuery("#assign_amount").hide();
			}
        });
		jQuery("#second_cat").on('change', function() {
          var second_cat = jQuery(this).val();
		  if (jQuery('#second_cat').is(':checked')) {
			var category_div = jQuery('#div_talent_category').html();
			jQuery('#div_talent_category_second').html('<div id="div_talent_category"><label for="phone_number1">Talent Category</label><select name="category[]" id="talent_category2" required1=''><option value=''>-Talent Category -</option><option value="dancer">Dance</option><option value="film">Film &amp; theater</option><option value="actor" style="display: none;">Actor</option><option value="music">Music</option><option value="culture">Culture</option><option value="sports">Sports</option></select></div>');
			
		  }
			console.log(second_cat);
			if(second_cat == 'voluntary'){
				jQuery("#assign_amount").hide();
			}
			else if(second_cat == 'paid'){
				jQuery("#assign_amount").show();
			}
          	else{
				jQuery("#assign_amount").hide();
			}
        });
		
    });



