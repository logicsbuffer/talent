<?php

/* Template name:  Middel Dashboard */

get_header(); ?>

<?php

//echo $_SESSION['secode'];
if($_SESSION['seccode'] == "")
{
	$url = "http://test.talentbasen.no/logg-inn/";
	wp_redirect( $url ); 
}
 

?>


<style>
.navbar-default {
    background-color: #ffffff;
    border-color: #ffffff;
    border-bottom: 1px solid #ec008c;
}
.et_header_style_split #main-header .container, .et_header_style_split #main-header .logo_container {
    z-index: 2;
    text-align: center;
    margin-top: 9px;
}
.borderimg{
border:2px solid #ee1495;
}
.btn-pink{
background-color: #ee1495;
    color: white;
}
#main-content .container:before {
    position: absolute;
    top: 0;
    width: 0px;
    height: 100%;
    background-color: #e2e2e2;
    content: "";
}
input.text, input.title, input[type=email], input[type=password], input[type=tel], input[type=text], select, textarea {
    padding: 5px;
    border: 1px solid #bbb;
    color: #4e4e4e;
    background-color: #fff;
}
.dashboard-grid ul li a {
    color: #111111;
    padding: 10px;
    line-height: 35px;
    
}
.dashboard-grid ul li a:hover {
    text-decoration:none;
}
.dashboard-grid ul li{
    color: #111111;
    padding: 10px;
    line-height: 35px;
    background-color: #dad9d9;
	width: 200px;
	border-bottom: 1px dotted;
}
.dashboard-grid ul li:hover{
    color: #ffffff;
    padding: 10px;
    line-height: 35px;
    background-color: #ee1495;
}
.dashboard-grid ul li i {
    float: right;
padding: 10px;
}

</style>
	<div id="main-content">

			<article id="post-1165" class="post-1165 page type-page status-publish hentry">

			<div class="entry-content">
			
			<section>    				
	<div class="container">
		<div class="row">

		<div class="col-sm-12">
		<div class="dashboard-grid">
			<div class="col-sm-12"><br>
<h2 class="text-center">Profil Informasjon</h2><br>
				<div class="row">
<div class="col-sm-3">
				<div class="navbar-collapse" id="bs-sidebar-navbar-collapse-1">
				  <ul class="nav navbar-nav">		
					<li class=""><a href="http://test.talentbasen.no/middel-dashboard/">Min side<i class="fa fa-home" aria-hidden="true"></i></a></li>
					<li class=""><a href="http://test.talentbasen.no/abonnere-middel/">Abonnere<i class="fa fa-home" aria-hidden="true"></i></a></li>
					<li ><a href="http://test.talentbasen.no/logg-inn/?s=log">Logg ut<i class="fa fa-sign-out" aria-hidden="true"></i></a></li>
				  </ul>
				</div>					
					</div>
					<?php
					global $wpdb;
					
					
						if(isset($_POST['register-submit']))
						{
							
							if($_FILES['image_browse']['name'] != "")
							{
								$time = time();
								$file = $time.$_FILES['image_browse']['name'];
								$image_browse = "http://talentbasen.no/test/wp-content/uploads/".date("Y")."/".date("m")."/".$file;
								
								function my_upload_dir($upload) {

								  return $upload;

								}
							
								if ( ! function_exists( 'wp_handle_upload' ) ) 
									$t = time();
									require_once( ABSPATH . 'wp-admin/includes/file.php' );
									$uploadedfile = $_FILES['image_browse']; 
									
									$nm = $time.$uploadedfile['name'];
									$tp = $uploadedfile['type'];
									$tmpname = $uploadedfile['tmp_name'];
									$er = $uploadedfile['error'];
									$sz = $uploadedfile['size'];
									$file_nm = array(name => $nm, type => $tp, tmp_name => $tmpname, error => $er, size => $sz);
									//print_r($uploadedfile);
									//print_r($file_nm);die();
									$upload_overrides = array( 'test_form' => false );
									//add_filter('upload_dir', 'my_upload_dir');
									$movefile = wp_handle_upload( $file_nm, $upload_overrides );
									//remove_filter('upload_dir', 'my_upload_dir');

									
									if ( $movefile ) {
										//echo "File is valid, and was successfully uploaded.\n";
										//var_dump( $movefile);
										$company = $_POST['firma'];
										$com_reg = $_POST['com_reg'];
										$firstname = $_POST['fornavn'];
										$lastname = $_POST['etternavn'];
										$address = $_POST['address'];
										$post = $_POST['gate'];
										$city = $_POST['post'];
										$street = $_POST['by'];
										$phone = $_POST['telefon'];
										$about = $_POST['fortell'];
										$results = $wpdb->query('UPDATE `tlntb_agent_register` SET `tlntb_agent_register`.`firstname` ="'.$firstname.'",`tlntb_agent_register`.`lastname` ="'.$lastname.'",`tlntb_agent_register`.`address` ="'.$address.'",`tlntb_agent_register`.`street` ="'.$street.'",`tlntb_agent_register`.`post` ="'.$post.'",`tlntb_agent_register`.`city` ="'.$city.'",`tlntb_agent_register`.`phone` ="'.$phone.'",`tlntb_agent_register`.`company` ="'.$company.'",`tlntb_agent_register`.`com_reg` ="'.$com_reg.'",`tlntb_agent_register`.`about` ="'.$about.'",`tlntb_agent_register`.`image_browse` ="'.$image_browse.'" WHERE `tlntb_agent_register`.`code` = "'.$_GET['cd'].'"');
								
									} else {
										//echo "Possible file upload attack!\n";
									}
								}
							
							
							
								$company = $_POST['firma'];
								$com_reg = $_POST['com_reg'];
								$firstname = $_POST['fornavn'];
								$lastname = $_POST['etternavn'];
								$address = $_POST['address'];
								$post = $_POST['gate'];
								$city = $_POST['post'];
								$street = $_POST['by'];
								$phone = $_POST['telefon'];
								$about = $_POST['fortell'];
							
								$results = $wpdb->query('UPDATE `tlntb_agent_register` SET `tlntb_agent_register`.`firstname` ="'.$firstname.'",`tlntb_agent_register`.`lastname` ="'.$lastname.'",`tlntb_agent_register`.`address` ="'.$address.'",`tlntb_agent_register`.`street` ="'.$street.'",`tlntb_agent_register`.`post` ="'.$post.'",`tlntb_agent_register`.`city` ="'.$city.'",`tlntb_agent_register`.`phone` ="'.$phone.'",`tlntb_agent_register`.`company` ="'.$company.'",`tlntb_agent_register`.`com_reg` ="'.$com_reg.'",`tlntb_agent_register`.`about` ="'.$about.'" WHERE `tlntb_agent_register`.`code` = "'.$_SESSION['seccode'].'"');
								//echo $results;
							}
							

					
					$select_agent= $wpdb->get_results('select * from `tlntb_agent_register` where `tlntb_agent_register`.`code` = "'.$_SESSION['seccode'].'"');
						
					?>
				 <div class="col-sm-3 text-center">
				 <?php
				 if($select_agent[0]->image_browse == "")
				 {
					 ?>
					 <img class="img-responsive borderimg center-block " src="http://anandvatika.org/img/Profile-icon-man-150x150.jpg">						
					<?php
				 }
				 else
				 {
					 ?>
					 <img class="img-responsive borderimg center-block " src="<?php echo $select_agent[0]->image_browse; ?>">						
					 <?php
				 }
				 ?>
					</div>
					<div class="col-sm-6">
						
					
						<form class="panel-login dashboard-form" enctype="multipart/form-data" id="dashboard-form" action="" method="post" role="form" style="">
							<div class="form-group">
								<div class="row">
									<div class="col-md-3">
									<strong>Brukernavn</strong>
									</div>
									<div class="col-md-9">
										<input type="text" id="brukernavn" tabindex="1" class="form-control" value="<?php echo $select_agent[0]->username;?>"  readonly>
									</div>	
								</div>
								
							</div>
							<div class="form-group">
							   <div class="row">
									<div class="col-md-3">
									<strong>Passord</strong>
									</div>
									<div class="col-md-9">
										<input type="text" id="passord" tabindex="1" class="form-control" value="<?php echo $select_agent[0]->password;?>" readonly>
									</div>	
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-3">
									<strong>Epost</strong>
									</div>
									<div class="col-md-9">
										<input type="email" id="epost" tabindex="1" class="form-control" value="<?php echo $select_agent[0]->email;?>" readonly>
									</div>	
								</div>
							</div>
							<div class="form-group">
<div class="row">
									<div class="col-md-3">
									<strong>Firma</strong>
									</div>
									<div class="col-md-9">
										<input type="text" name="firma" id="firma" tabindex="1" class="form-control" value="<?php echo $select_agent[0]->company;?>" required>
									</div>	
								</div>
								
							</div>
							<div class="form-group">
<div class="row">
									<div class="col-md-3">
									<strong>Company Reg no.</strong>
									</div>
									<div class="col-md-9">
										<input type="text" name="com_reg" id="com_reg" tabindex="1" class="form-control" value="<?php echo $select_agent[0]->com_reg;?>" required>
									</div>	
								</div>
								
							</div>
							<div class="form-group">
<div class="row">
									<div class="col-md-3">
									<strong>Fornavn</strong>
									</div>
									<div class="col-md-9">
										<input type="text" name="fornavn" id="fornavn" tabindex="1" class="form-control" value="<?php echo $select_agent[0]->firstname;?>" required>
									</div>	
								</div>
								
							</div>
							<div class="form-group">
<div class="row">
									<div class="col-md-3">
									<strong>Etternavn</strong>
									</div>
									<div class="col-md-9">
										<input type="text" name="etternavn" id="etternavn" tabindex="1" class="form-control" value="<?php echo $select_agent[0]->lastname;?>" required>
									</div>	
								</div>
								
								
							</div>
							<div class="form-group">
<div class="row">
									<div class="col-md-3">
									<strong>Address</strong>
									</div>
									<div class="col-md-9">
										<input type="text" name="address" id="address" tabindex="1" class="form-control" value="<?php echo $select_agent[0]->address;?>" required>
									</div>	
								</div>
								</div>
							
							<div class="form-group">
<div class="row">
									<div class="col-md-3">
									<strong>Postnr</strong>
									</div>
									<div class="col-md-9">
										<input type="text" name="gate" id="gate" tabindex="1" class="form-control"  value="<?php echo $select_agent[0]->post;?>" required>
									</div>	
								</div>
								</div>
								
								<div class="form-group">
<div class="row">
									<div class="col-md-3">
									<strong>Poststed</strong>
									</div>
									<div class="col-md-9">
										<input type="text" name="post" id="post" tabindex="1" class="form-control"  value="<?php echo $select_agent[0]->city;?>" required>
									</div>	
								</div>
							</div>
							
							<div class="form-group">
<div class="row">
									<div class="col-md-3">
									<strong>By</strong>
									</div>
									<div class="col-md-9">
										<input type="text" name="by" id="by" tabindex="1" class="form-control" value="<?php echo $select_agent[0]->street;?>" required>
									</div>	
								</div>
								
							</div>
							
							<div class="form-group">
<div class="row">
									<div class="col-md-3">
									<strong>Telefon</strong>
									</div>
									<div class="col-md-9">
										<input type="text" name="telefon" id="telefon" tabindex="1" class="form-control" value="<?php echo $select_agent[0]->phone;?>" required>
									</div>	
								</div>
								
							</div>
							
							<div class="form-group">
<div class="row">
									<div class="col-md-3">
									<strong>Fortell litt om deg selv...</strong>
									</div>
									<div class="col-md-9">
										<textarea name="fortell" class="form-control input" required><?php echo $select_agent[0]->about;?></textarea>
									</div>	
								</div>
								
							</div>
							
							<div class="form-group">
<div class="row">
									<div class="col-md-3">
									<strong>Profilbilde</strong>
									</div>
									<div class="col-md-9">
										<input type="file" name="image_browse" id="image_browse" class="form-control">
									</div>	
								</div>
								
							</div>	
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6 col-sm-offset-3">
										<input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register btn-pink" value="Sende inn">
									</div>
								</div>
							</div>
						</form>
						
						
						
						
					</div>
				</div>
			</div>
			</div>
		</div>
		</div>
	</div>
</section>
			
			
			
			
			
			
			
			</div>
			
			</article> <!-- .et_pb_post -->

	</div>
		
	
		
<?php get_footer(); ?>
		
		
