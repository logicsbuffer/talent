<?php

/* Template name:  Rekalmefilmer */

get_header(); ?>



<style>
.et_pb_button_module_wrapper a.et_pb_button{
	padiing:.3em 2em .3em .7em !important;
	color:#fff;	
}
.et_pb_button_module_wrapper a.et_pb_button:hover{
	padiing:.3em 2em .3em .7em !important;
}
.et_pb_section_parallax_fullheight {
    height: 90vh !important;
    background-size: cover !important;
    background-position: center center !important;
}
.home_container_outer{
	width: calc(100% - 30px);
    max-width: 1170px;
    display: block;
    margin: auto;
    clear: both;
    overflow: hidden;
}
.linktab{
	width:100%;
	float:left;
	margin:30px 0;
	}
.linktab ul{
	margin:0;
	padding:0;
	width:100%;
	float:left;
	}
.linktab ul li {
    margin: 0px 5px 5px;
    display: inline-block;
    list-style: none;
    position: relative;
    z-index: 10;
}
.linktab ul li a{
	width:100%;
	float:left;
	background:#ec008c;
	padding:10px 20px;
	font-size:12px;
	text-transform:uppercase;
	color:#fff;
	text-decoration:none;
	transition:all 0.3s ease-in-out;
	-webkit-transition:all 0.3s ease-in-out;
	-moz-transition:all 0.3s ease-in-out;
	-ms-transition:all 0.3s ease-in-out;
	-o-transition:all 0.3s ease-in-out; 
	}
.linktab ul li:hover > a{ 
	background:#000; 
	color:#fff; 
	}
.linktab ul li > ul{
	position:absolute;
	top:100%;
	left:0;
	width:200px;	
	display:none;
}
.linktab ul li:hover > ul{ 
	display:block;
}
.linktab ul li > ul li{
	width:100%;
	float:left;
	margin:0px;
	display:block;
	list-style:none;
	position:relative;
	}
.linktab ul li > ul li{
	background:#000;
	color:#fff;
	border-bottom:1px solid #4c4c4c;
	}
.linktab ul li > ul li:last-child{ 
	border-bottom:none;
	}
.linktab ul li > ul li:hover > a{
	background:#ec008c;
	color:#fff;
	}
.linktab ul li > ul li > a{
	background:#000; 
	color:#fff; 
	}
	
.gallery_personthumb{
	width:100%;
	float:left;
	margin:0 0 80px 0;
	}
.gallery_personthumb ul{
	margin:0;
	padding:0;
	width:100%;
	float:left;
	text-align: center;
	}
.gallery_personthumb ul li{
	margin:0px 5px 9px;
	display:inline-block;
	list-style:none;
	position:relative;
	/*width:calc(33.1% - 11px);*/
	width:calc(24.1% - 3px);
	overflow:hidden;
	float: left;
	}
.gallery_personthumb ul li a{
	width:100%;
	float:left;
	}
.gallery_personthumb ul li a img{
	width:100%;
	float:left;
	height:auto;
	position:relative;
	z-index:1;
	}
.gallery_personthumb ul li a .personthumb_overlay{
	position:absolute;
	top:0;
	right:100%;
	bottom:0;
	width:100%;
	height:100%;
	background:#ec008c;
	z-index:2;
	transition:all 0.3s ease-in-out;
	-webkit-transition:all 0.3s ease-in-out;
	-moz-transition:all 0.3s ease-in-out;
	-ms-transition:all 0.3s ease-in-out;
	-o-transition:all 0.3s ease-in-out;
	text-align:left;
	}
.gallery_personthumb ul li:hover a .personthumb_overlay{
	right:0%;
}
.personthumb_overlay_inner{
	position:absolute;
	top:0%;
	/*transform:translateY(-50%);
	-webkit-transform:translateY(-50%);
	-moz-transform:translateY(-50%);
	-ms-transform:translateY(-50%);
	-o-transform:translateY(-50%);*/
	left:0;
	right:0;
	width: 100%;
    padding: 25px;
	}
.personthumb_overlay_inner span{
	font-size:13px;
	color:#FFF;
	margin-bottom:15px;
	width:100%;
	float:left;
	display:block;
	} 
.personthumb_overlay_inner span strong{
	font-weight:600;
}


@media only screen and (max-width:768px) {
	
.gallery_personthumb ul li{
    display: block;
    margin: 9px auto;
    width: 60%;
    min-width: 320px;
}
}
@media only screen and (min-width:769px)  and (max-width:991px) {
	
	.gallery_personthumb ul li { 
		width: calc(32.1% - 3px);
	}
}
</style>
<div id="main-content">


			
				<article id="post-453" class="post-453 page type-page status-publish hentry">

				
					<div class="entry-content">
					<div class="et_pb_section et_pb_section_parallax et_pb_section_parallax_fullheight et_pb_section_0 et_pb_with_background et_section_regular" style="background: url('<?php echo bloginfo('template_url');?>/images/talentbasen-front.jpg');">
				<!--<div class="et_parallax_bg et_pb_parallax_css" style="background: url('<?php /*?><?php echo bloginfo('template_url');?><?php */?>/images/talentbasen-front.jpg');"></div>-->
				
				
					<div class=" et_pb_row et_pb_row_0 et_pb_equal_columns et_pb_gutters2 et_pb_row_fullwidth">
				<div class="et_pb_column et_pb_column_1_3  et_pb_column_0">
				
				
				<div class="et_pb_text et_pb_module et_pb_bg_layout_dark et_pb_text_align_center  et_pb_text_0">
				
				
				<div class="et_pb_text_inner">
					<p style="color: white;">Vi formidler oppdrag til skuespillere, musikere, dansere, sangere tryllekunstnere og mye mer. Kan du underholde? Da er Talentbasen stedet for deg.</p>
				</div>
			</div> <!-- .et_pb_text --><div class="et_pb_button_module_wrapper et_pb_module et_pb_button_alignment_center">
				<a class="et_pb_button  et_pb_button_0 et_pb_module et_pb_bg_layout_dark" href="http://test.talentbasen.no/registrering-for-talenter/">Er du et talent?</a>
			</div>
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3  et_pb_column_1">
				
				
				<div class="et_pb_text et_pb_module et_pb_bg_layout_dark et_pb_text_align_center  et_pb_text_1">
				
				
				<div class="et_pb_text_inner">
					
				</div>
			</div> <!-- .et_pb_text -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3  et_pb_column_2 et-last-child">
				
				
				<div class="et_pb_text et_pb_module et_pb_bg_layout_dark et_pb_text_align_center  et_pb_text_2">
				
				
				<div class="et_pb_text_inner">
					<p style="color: white;">Vi kobler ditt oppdrag med våre talenter. Søker du underholdning til et event, til bryllup, firmafest, bursdag eller lignende? Vi har talentene!</p>
				</div>
			</div> <!-- .et_pb_text --><div class="et_pb_button_module_wrapper et_pb_module et_pb_button_alignment_center">
				<a class="et_pb_button  et_pb_button_1 et_pb_module et_pb_bg_layout_dark" href="http://test.talentbasen.no/registrering-for-oppdragsgivere/">Er du en oppdragsgiver?</a>
			</div>
			</div> <!-- .et_pb_column -->
				
				
			</div> <!-- .et_pb_row -->
			</div> <!-- .et_pb_section -->
			</div> <!-- .entry-content -->

				
				</article> <!-- .et_pb_post -->

			

</div>

<div class="home_container_outer">

    <div class=" linktab">
        <ul >
            <li><a href="http://test.talentbasen.no/">All</a></li>
            <li><a href="">Danse</a>
                <!--<ul>
                    <li><a href="">Creative</a></li>
                    <li><a href="">Influencers</a></li>
                    <li><a href="">Main Board</a></li>
                </ul>-->
        	</li>
            <li><a href="">Musikk</a></li>
            <li><a href="">Sport</a></li>
            <li><a href="">Reklamefilmer</a></li>
            <li><a href="">Kultur</a></li>
        </ul>
     </div>
     
    <div class="gallery_personthumb">
        <ul >
		
            <?php
			
			global $wpdb;
					
			$select_member= $wpdb->get_results('select * from `tlntb_member_register` where `tlntb_member_register`.`category` = "Reklamefilmer" and `tlntb_member_register`.`status` = "1"');
			?>
			
			<?php
			foreach($select_member as $val)
			{
				?>
				<li>
					<a href="http://test.talentbasen.no/medlem-detaljer/?code=<?php echo $val->code;?>">
						<img src="<?php echo $val->image_browse; ?>" width="279" height="279"/>
						<div class="personthumb_overlay">
							<div class="personthumb_overlay_inner">
								<span><strong>Fullt Navn: </strong> <?php echo $val->firstname." ".$val->lastname; ?></span> 
								<span><strong>Alder: </strong> <?php echo $val->age; ?></span> 
								<span><strong>Kjønn: </strong> <?php echo $val->gender; ?></span> 
								<span><strong>Høyde: </strong> <?php echo $val->height; ?></span> 
								<span><strong>Vekt: </strong> <?php echo $val->weight; ?></span> 
								<span><strong>Språk: </strong> <?php echo $val->lang; ?></span>  
							</div>
						</div>
					</a>
				</li>
				<?php
			}
			?>
			
			
        </ul>
     </div>
</div>



	
		
		
<?php get_footer(); ?>
		
		
