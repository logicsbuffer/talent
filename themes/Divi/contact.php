<?php

/* Template name:  Kontakt */

get_header(); ?>

<style>
.et_header_style_split #main-header .container, .et_header_style_split #main-header .logo_container {
    z-index: 2;
    text-align: center;
    margin-top: 9px;
}
.borderimg{
border:2px solid #ee1495;
padding:10px 0px;
}
.btn-pink{
background-color: #ee1495;
    color: white;
}
#main-content .container:before {
    position: absolute;
    top: 0;
    width: 0px;
    height: 100%;
    background-color: #e2e2e2;
    content: "";
}
input.text, input.title, input[type=email], input[type=password], input[type=tel], input[type=text], select, textarea {
        padding: 5px;
    border: 1px solid #bbb;
    color: #4e4e4e;
    background-color: #fff;
    margin: 5px 0px;
}
.dashboard-grid ul li a {
    color: #111111;
    padding: 10px;
    line-height: 35px;
    
}
.dashboard-grid ul li a:hover {
    text-decoration:none;
}
.dashboard-grid ul li{
    color: #111111;
    padding: 10px;
    line-height: 35px;
    background-color: #dad9d9;
}
.dashboard-grid ul li:hover{
    color: #ffffff;
    padding: 10px;
    line-height: 35px;
    background-color: #ee1495;
}
.dashboard-grid ul li i {
    float: right;
padding: 10px;
}

</style>
	<div id="main-content">

			<article id="post-1165" class="post-1165 page type-page status-publish hentry">

			<div class="entry-content">
			
	<section>    				
	<div class="container">
		<div class="row">

		<div class="col-sm-12">
		<div class="dashboard-grid">
			<div class="col-sm-12"><br>
<h2 class="text-center">Profile Registration</h2><br>
				<div class="row">
					<div class="col-sm-6">
					<h3>Kontakt oss</h3>
					
					<?php $the_query = new WP_Query( 'page_id=1205' ); ?>
					<?php while ($the_query -> have_posts()) : $the_query -> the_post();  ?>
					<?php the_content(); ?>
					<?php endwhile;?>
					
					</div>
					<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
					
					<?php
					
					if(isset($_POST['submit']))
					{
						
						$subject="Kontakt Post";    
						$headers="MIME-Version: 1.0\n"; 
						$headers.="Content-type: text/html; charset=iso-8859-1 \n"; 
						
					   
						$headers.="from:Talentbasen<".$_POST['postadresse'].">"; 
						$mail_body="<table cellpadding='0' cellspacing='0' border='0'><tr><td>Fornavn: ".$_POST['fornavn']."</td></tr>";
						$mail_body.="<tr><td>Etternavn: ".$_POST['etternavn']."</td></tr>";
						$mail_body.="<tr><td>Postadresse: ".$_POST['postadresse']."</td></tr>";
						$mail_body.="<tr><td>Fortell: ".$_POST['fortell']."</td></tr></table>";   
						
						$mail_to="alvi@talentbasen.no";

						if(@wp_mail($mail_to, $subject, $mail_body, $headers)) 
						
						{
						?>
						
					   <script type="text/javascript">
						 alert("Thank You! Your Message Has Been Sent Successfully");
						</script>
							
						<?php 
						} 
						   else
						{
						  print("<h1><font color=\"#880000\">Sorry! An Error Occurred While Sending Message. Please Try Again</font></h1>"); 
						} 
						
					}
					?>
					
					
				 
					<div class="col-sm-6">
					
						<form class="panel-login dashboard-form" enctype="multipart/form-data" id="dashboard-form" action="" method="post" role="form" style="">
							<div class="form-group">
								<div class="row">
									
									<div class="col-md-12">
										<input type="text" name="fornavn" tabindex="1" class="form-control"   placeholder="Fornavn">
									</div>	
								
								</div>
							</div>
							<div class="form-group">
 <div class="row">
									
									<div class="col-md-12">
										<input type="text" name="etternavn" tabindex="1" class="form-control"   placeholder="Etternavn">
									</div>	
										
								</div>
								
							</div>
							<div class="form-group">
<div class="row">
									
									<div class="col-md-12">
										<input type="email" name="postadresse" tabindex="1" class="form-control"   placeholder="E-postadresse">
									</div>	
								</div>
								
							</div>
							
							<div class="form-group">
								<div class="row">
									
									<div class="col-md-12">
										<textarea name="fortell" class="form-control input" placeholder="Spørsmål eller kommentarer." required></textarea>
									</div>	
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6">
										<input type="submit" name="submit" id="register-submit" tabindex="4" class="form-control btn btn-register btn-pink" value="Kontakt oss">
									</div>
								</div>
							</div>
						</form>
						
						
						
						
					</div>
				</div>
			</div>
			</div>
		</div>
		</div>
	</div>
</section>
			
			
			
			
			
			
			
			</div>
			
			</article> <!-- .et_pb_post -->

	</div>
		
		
		
<?php get_footer(); ?>
		
		
