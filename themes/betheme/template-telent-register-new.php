<?php
/**
 * Template Name: Register Telent New
 * Description: A Page Template that display portfolio items.
 *
 * @package Betheme
 * @author Muffin Group
 */

get_header(); 

?>

					<?php
/* if ( is_user_logged_in() ) {
	wp_redirect($edit_profile_link);
	exit;
} */
$edit_profile_link = site_url().'/edit-profile/';
$talent_post_id = $_GET['profile_id'];
$second_post_ins_id = get_post_meta($talent_post_id,'talent_post_id_second');
if($second_post_ins_id){
	$post_ins_id = $second_post_ins_id;
}
			if (isset( $_POST['register_telent'] )) {
						$password = $_POST['password'];
						$firstname = $_POST['firstname'];
						$lastname = $_POST['lastname'];
						$city = $_POST['city'];
						$street = $_POST['street'];
						$email = $_POST['email'];
						$phone = $_POST['phone'];
						$slected_category = $_POST['category'];
						$category = $_POST['category'];
						$multi_category = $_POST['multi_category'];
						/* if($multi_category){
							$category = $multi_category;
						} */
						$dancer_gender =$_POST['dancer_gender'];
						$experience = $_POST['experience'];
						$assignment = $_POST['assignments'];
						$description = $_POST['description'];
						$talent_video_url = $_POST['video_url'];
						$address = $street.' '.$city;
						$full_name = $firstname.' '.$lastname;
						//Music
						$tel_music_category = $_POST['music_category'];
						$tel_band = $_POST['band'];
						$tel_genre = $_POST['genre'];
						$tel_crew = $_POST['crew'];
						$tel_dj_experience = $_POST['dj_experience'];
						$tel_singer_x_solo_exp = $_POST['singer_x_solo_exp'];
						$tel_instrumentals_type = $_POST['instrumentals_type'];
						$tel_played_number_of_years = $_POST['played_number_of_years'];
						$tel_music_gender = $_POST['music_gender'];

						//Actor
						$tel_actor_age = $_POST['actor_age'];
						$tel_actor_gender = $_POST['actor_gender'];
						$tel_actor_height = $_POST['actor_height'];
						$tel_actor_weight = $_POST['actor_weight'];
						$tel_actor_hair_color = $_POST['actor_hair_color'];
						$tel_actor_language = $_POST['actor_language'];
						$tel_actor_experience = $_POST['actor_experience'];
						$tel_email = $_POST['email'];

						//film_
					

						//Model
						$tel_model_name = $_POST['model_name'];
						$tel_model_age = $_POST['model_age'];
						$tel_model_gender = $_POST['model_gender'];
						$tel_model_height = $_POST['model_height'];
						$tel_model_weight = $_POST['model_weight'];
						$tel_model_exprience = $_POST['model_exprience'];
						
						//statist
						$tel_statist_age = $_POST['statist_age'];
						$tel_statist_gender = $_POST['statist_gender'];
						$tel_statist_height = $_POST['statist_height'];
						$statist_hair_color = $_POST['statist_hair_color'];
						$tel_statist_weight = $_POST['statist_weight'];
						$statist_dialect = $_POST['statist_dialect'];
						$statist_language = $_POST['statist_language'];
						$tel_statist_exprience = $_POST['statist_experience'];
						$film_category = $_POST['film_category'];
						
						//Voiceover
						$tel_voiceover_gender = $_POST['voiceover_gender'];
						$tel_voiceover_height = $_POST['voiceover_height'];
						$voiceover_hair_color = $_POST['voiceover_hair_color'];
						$tel_voiceover_weight = $_POST['voiceover_weight'];
						$voiceover_dialect = $_POST['voiceover_dialect'];
						$voiceover_language = $_POST['voiceover_language'];
						$tel_voiceover_exprience_arr = $_POST['voiceover_experience'];
						$tel_voiceover_exprience = implode(",",$tel_voiceover_exprience_arr);
						$voiceover_description = $_POST['voiceover_description'];
						$talent_audio_get = $_POST['talent_audio'];
						$talent_audio_arr = explode(',',$talent_audio_get);
						$talent_audio = $talent_audio_arr[0];
						$film_category = $_POST['film_category'];

						//$multi_category_count = count($multi_category);
					if($film_category == 'statist'){
							$tel_age = $tel_statist_age;
							$tel_gender = $tel_statist_gender;
							$tel_height = $tel_statist_height;
							$tel_weight = $tel_statist_weight;
							$tel_hair_color = $statist_hair_color;
							$tel_dialect = $statist_dialect;
							$tel_language = $statist_language;
 							$tel_experience = $tel_statist_exprience;
						}
						if($film_category == 'voiceover'){
							$tel_age = $tel_statist_age;
							$tel_gender = $tel_voiceover_gender;
							$tel_height = $tel_voiceover_height;
							$tel_weight = $tel_voiceover_weight;
							$tel_hair_color = $statist_hair_color;
							$tel_language = $statist_language;
 							$tel_experience = $tel_voiceover_exprience;
 							$tel_experienc_description = $voiceover_description;
 							$tel_audio = $talent_audio;
						}
						if($film_category == 'actor' || $film_category == 'model'){
							$category = $film_category;
						}
						if($category == 'sports'){
							$category = 'culture';
						}
						// Dancer
						$dance_experience = $_POST['dance_experience'];
						$dance_experience_description = $_POST['dance_experience_description'];
						$dancer_type = $_POST['dancer_type'];
						//else{
							echo '<div class="alert alert-success text-center"><strong>You</strong> have successfully registered on our website, Please check your email and click on the link we sent you to verify your email address.</div>';
							$my_post = array(
							  'post_title'    		 => $full_name,
							  'post_content' 		 => '',
							  'post_status'          => 'publish',				  
							  'post_type'            => $category
							 );
							// Insert the post into the database
							$post_ins_id = wp_insert_post( $my_post );
							update_option('second_post_ins_id',$post_ins_id);
						//}
					//update_user_meta( $user_status, 'talent_post_id', $post_ins_id );
					if($post_ins_id){
						//update_user_meta( $post_ins_id, 'city', $city );
	
						//Image
						$talent_image = $_POST['talent_image'];
						$tel_talent_image_arr = explode(',',$talent_image);
						$talent_image = $tel_talent_image_arr[0];
						if($talent_image){
						//Feature Image Code
						
						// Add Featured Image to Post
						$image_url        = $talent_image; // Define the image URL here
						$image_name       = 'telent.png';
						$upload_dir       = wp_upload_dir(); // Set upload folder
						$image_data       = file_get_contents($image_url); // Get image data
						$unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
						$filename         = basename( $unique_file_name ); // Create image file name

						// Check folder permission and define file location
						if( wp_mkdir_p( $upload_dir['path'] ) ) {
						    $file = $upload_dir['path'] . '/' . $filename;
						} else {
						    $file = $upload_dir['basedir'] . '/' . $filename;
						}

						// Create the image  file on the server
						file_put_contents( $file, $image_data );

						// Check image file type
						$wp_filetype = wp_check_filetype( $filename, null );

						// Set attachment data
						$attachment = array(
						    'post_mime_type' => $wp_filetype['type'],
						    'post_title'     => sanitize_file_name( $filename ),
						    'post_content'   => '',
						    'post_status'    => 'inherit'
						);

						// Create the attachment
						$attach_id = wp_insert_attachment( $attachment, $file, $post_ins_id );

						// Include image.php
						require_once(ABSPATH . 'wp-admin/includes/image.php');

						// Define attachment metadata
						$attach_data = wp_generate_attachment_metadata( $attach_id, $file );

						// Assign metadata to attachment
						wp_update_attachment_metadata( $attach_id, $attach_data );

						// And finally assign featured image to post
						set_post_thumbnail( $post_ins_id, $attach_id );
						//Feature Image Code Ends	
						
						}
						//Default Image if no image Uplaoded
						else{
							
							$gender_check = '';	
							
							//Male
							if($tel_music_gender == 'male'){	
								$gender_check = 'male';
							 }
							if($tel_actor_gender == 'male'){	
								$gender_check = 'male';
							 }
							if($tel_model_gender == 'male'){	
								$gender_check = 'male';
							 }
							if($tel_statist_gender == 'male'){	
								$gender_check = 'male';
							 }
							if($tel_voiceover_gender == 'male'){	
								$gender_check = 'male';
							 }
							if($tel_gender == 'male'){	
								$gender_check = 'male';
							 }
							
							//Female
							if($tel_music_gender == 'female'){	
								$gender_check = 'female';
							 }
							if($tel_actor_gender == 'female'){	
								$gender_check = 'female';
							 }
							if($tel_model_gender == 'female'){	
								$gender_check = 'female';
							 }
							if($tel_statist_gender == 'female'){	
								$gender_check = 'female';
							 }
							if($tel_voiceover_gender == 'female'){	
								$gender_check = 'female';
							 }
							if($tel_gender == 'female'){	
								$gender_check = 'female';
							 }
							 
							 //Condition
							 if ($gender_check == 'male') {
							 	set_post_thumbnail( $post_ins_id, 4825 );
							 }
							 else if ($gender_check == 'female') {
							 	set_post_thumbnail( $post_ins_id, 4826 );
							 }	 	
						}
						//Default Image if no image Uplaoded Ends

						//Culture
						if($category == 'culture'){
						$tel_culture_expertise = $_POST['culture_expertise'];
						$tel_culture_experience = $_POST['culture_experience'];
						$tel_culture_experience_description = $_POST['culture_experience_description'];
						$tel_cul_gender = $_POST['cul_gender'];
						update_post_meta( $post_ins_id, 'gender', $tel_cul_gender );
						update_post_meta( $post_ins_id, 'gallery_images', $talent_image );
						update_post_meta( $post_ins_id, 'gallery_videos', $talent_video_url );
						update_post_meta( $post_ins_id, 'expertise', $tel_culture_expertise );
						update_post_meta( $post_ins_id, 'experience', $tel_culture_experience );
						update_post_meta( $post_ins_id, 'experince_description', $tel_culture_experience_description );
						update_post_meta( $post_ins_id, 'phone', $phone );
						update_post_meta( $post_ins_id, 'email', $email );
						update_post_meta( $post_ins_id, 'address', $address );
						update_post_meta( $post_ins_id, 'assignment', $assignment );
						}
						elseif($category == 'actor'){
						
						wp_set_object_terms( $post_ins_id, $category, 'actors_category');	
						update_post_meta( $post_ins_id, 'age', $tel_actor_age );
						update_post_meta( $post_ins_id, 'gender', $tel_actor_gender );
						update_post_meta( $post_ins_id, 'height', $tel_actor_height );
						update_post_meta( $post_ins_id, 'weight', $tel_actor_weight );
						update_post_meta( $post_ins_id, 'city', $city );
						update_post_meta( $post_ins_id, 'hair_color', $tel_actor_hair_color );
						update_post_meta( $post_ins_id, 'language', $tel_actor_language );
						update_post_meta( $post_ins_id, 'experience', $tel_actor_experience );
						update_post_meta( $post_ins_id, 'phone', $phone );
						update_post_meta( $post_ins_id, 'email', $email );
						update_post_meta( $post_ins_id, 'address', $address );
						update_post_meta( $post_ins_id, 'assignment', $assignment );
						update_post_meta( $post_ins_id, 'gallery_images', $talent_image );
						update_post_meta( $post_ins_id, 'gallery_videos', $talent_video_url );
						}
						 elseif($category == 'music'){
							$music_category = $_POST['music_category'];
							$band_category = $_POST['band_category'];
														
							update_post_meta( $post_ins_id, 'gender', $tel_music_gender );


							if($music_category == 'band'){
								$music_categoies = array($music_category,$band_category);
								$tel_band_genre = $_POST['band_genre'];
								$tel_founded = $_POST['founded'];
								$tel_repertoire = $_POST['repertoire'];
								$tel_crew = $_POST['crew'];
								
								update_post_meta( $post_ins_id, 'band_type', $band_category );
								update_post_meta( $post_ins_id, 'genre', $tel_band_genre );
								update_post_meta( $post_ins_id, 'crew', $tel_crew );
								update_post_meta( $post_ins_id, 'repertoire', $tel_repertoire );
								update_post_meta( $post_ins_id, 'founded', $tel_founded );
								update_post_meta( $post_ins_id, 'phone', $phone );
								update_post_meta( $post_ins_id, 'email', $email );
								update_post_meta( $post_ins_id, 'address', $address );
								update_post_meta( $post_ins_id, 'assignment', $assignment );
								update_post_meta( $post_ins_id, 'gallery_images', $talent_image );
						update_post_meta( $post_ins_id, 'gallery_videos', $talent_video_url );
								wp_set_object_terms( $post_ins_id, $music_categoies, 'music_category');
							}elseif($music_category == 'dj'){
								$dj_experience = $_POST['dj_experience'];
								$dj_description = $_POST['dj_description'];
								update_post_meta( $post_ins_id, 'experience', $dj_experience );
								update_post_meta( $post_ins_id, 'experince_description', $dj_description );
								update_post_meta( $post_ins_id, 'phone', $phone );
								update_post_meta( $post_ins_id, 'email', $email );
								update_post_meta( $post_ins_id, 'address', $address );
								update_post_meta( $post_ins_id, 'assignment', $assignment );
								update_post_meta( $post_ins_id, 'gallery_images', $talent_image );
						update_post_meta( $post_ins_id, 'gallery_videos', $talent_video_url );
								wp_set_object_terms( $post_ins_id, $music_category, 'music_category');
								
							}elseif($music_category == 'singer'){
								$singer_experience = $_POST['singer_experience'];
								$singer_description = $_POST['singe_description'];
								update_post_meta( $post_ins_id, 'experience', $singer_experience );
								update_post_meta( $post_ins_id, 'experince_description', $singer_description );
								update_post_meta( $post_ins_id, 'phone', $phone );
								update_post_meta( $post_ins_id, 'email', $email );
								update_post_meta( $post_ins_id, 'address', $address );
								update_post_meta( $post_ins_id, 'assignment', $assignment );
								update_post_meta( $post_ins_id, 'gallery_images', $talent_image );
								update_post_meta( $post_ins_id, 'gallery_videos', $talent_video_url );
								wp_set_object_terms( $post_ins_id, $music_category, 'music_category');
							}elseif($music_category == 'instrumentals'){
								$type_instrument = $_POST['type_instrument'];
								$instrumental_experience = $_POST['instrumental_experience'];
								$instrumental_description = $_POST['instrumental_description'];
								$tel_crew = $_POST['crew'];
								update_post_meta( $post_ins_id, 'type_instrument', $type_instrument );
								update_post_meta( $post_ins_id, 'instrumental_experience', $instrumental_experience );
								update_post_meta( $post_ins_id, 'experince_description', $instrumental_description );
								update_post_meta( $post_ins_id, 'phone', $phone );
								update_post_meta( $post_ins_id, 'email', $email );
								update_post_meta( $post_ins_id, 'address', $address );
								update_post_meta( $post_ins_id, 'assignment', $assignment );
								update_post_meta( $post_ins_id, 'gallery_images', $talent_image );
								update_post_meta( $post_ins_id, 'gallery_videos', $talent_video_url );
								wp_set_object_terms( $post_ins_id, $music_category, 'music_category');
							}
							//wp_set_object_terms( $post_ins_id, $band_category, 'music_category');

						}
						elseif($category == '22'){	
						}
						 elseif($category == 'film'){	
							update_post_meta( $post_ins_id, 'age', $tel_age );
							update_post_meta( $post_ins_id, 'gender', $tel_gender );
							update_post_meta( $post_ins_id, 'height', $tel_height );
							update_post_meta( $post_ins_id, 'weight', $tel_weight );
							update_post_meta( $post_ins_id, 'hair_color', $tel_hair_color );
							update_post_meta( $post_ins_id, 'dialect', $tel_dialect );
							update_post_meta( $post_ins_id, 'language', $tel_language );
							update_post_meta( $post_ins_id, 'experience', $tel_experience );
							update_post_meta( $post_ins_id, 'talent_audio', $talent_audio );
							update_post_meta( $post_ins_id, 'experience_description', $tel_experienc_description );
							update_post_meta( $post_ins_id, 'phone', $phone );
							update_post_meta( $post_ins_id, 'email', $email );
							update_post_meta( $post_ins_id, 'address', $address );
							update_post_meta( $post_ins_id, 'assignment', $assignment );
							update_post_meta( $post_ins_id, 'gallery_images', $talent_image );
							update_post_meta( $post_ins_id, 'gallery_videos', $talent_video_url );
							wp_set_object_terms( $post_ins_id, $film_category, 'film_category');
						 }
						 elseif($category == 'dancer'){
							update_post_meta( $post_ins_id, 'experience', $dance_experience );
							update_post_meta( $post_ins_id, 'experience_description', $dance_experience_description );
							update_post_meta( $post_ins_id, 'dancer_type', $dancer_type );
							update_post_meta( $post_ins_id, 'phone', $phone );
							update_post_meta( $post_ins_id, 'email', $email );
							update_post_meta( $post_ins_id, 'address', $address );
							update_post_meta( $post_ins_id, 'assignment', $assignment );
							update_post_meta( $post_ins_id, 'gallery_images', $talent_image );
							update_post_meta( $post_ins_id, 'gallery_videos', $talent_video_url );
							wp_set_object_terms( $post_ins_id, $dancer_type, 'dancer_category');
						 } 
						elseif($category == 'model'){	
						 //Model
							wp_set_object_terms( $post_ins_id, $category, 'models_category');
							update_post_meta( $post_ins_id, 'age', $tel_model_age );
							update_post_meta( $post_ins_id, 'gender', $tel_model_gender );
							update_post_meta( $post_ins_id, 'height', $tel_model_height );
							update_post_meta( $post_ins_id, 'weight', $tel_model_weight );
							update_post_meta( $post_ins_id, 'exprience', $tel_model_exprience );
							update_post_meta( $post_ins_id, 'phone', $phone );
							update_post_meta( $post_ins_id, 'email', $email );
							update_post_meta( $post_ins_id, 'address', $address );
							update_post_meta( $post_ins_id, 'gallery_images', $talent_image );
							update_post_meta( $post_ins_id, 'gallery_videos', $talent_video_url );
							update_post_meta( $post_ins_id, 'assignment', $assignment );
						}
						if($slected_category == 'sports'){
							$tel_sports_expertise = $_POST['sports_expertise'];
							$tel_sports_experience = $_POST['sports_experience'];
							$tel_sports_experience_description = $_POST['sports_experience_description'];
							$tel_cul_gender = $_POST['sports_gender'];
							$cul_sports_age = $_POST['sports_age'];
							$tel_cul_video_url = $talent_video_url;
							$tel_cul_image_url = $talent_image;
							update_post_meta( $post_ins_id, 'gender', $tel_cul_gender );
							update_post_meta( $post_ins_id, 'age', $cul_sports_age );
							update_post_meta( $post_ins_id, 'gallery_images', $talent_image );
							update_post_meta( $post_ins_id, 'gallery_videos', $talent_video_url );
							update_post_meta( $post_ins_id, 'expertise', $tel_sports_expertise );
							update_post_meta( $post_ins_id, 'experience', $tel_sports_experience );
							update_post_meta( $post_ins_id, 'experince_description', $tel_sports_experience_description );
							update_post_meta( $post_ins_id, 'phone', $phone );
							update_post_meta( $post_ins_id, 'email', $email );
							update_post_meta( $post_ins_id, 'address', $address );
							update_post_meta( $post_ins_id, 'assignment', $assignment );
							wp_set_object_terms( $post_ins_id, $slected_category, 'culture_categories');
						}
					update_post_meta( $post_ins_id, 'city', $city );
					}
			//	}
			}
			
			$post_ins_id = get_option('second_post_ins_id');
			//$data_gender = get_post_meta($talent_post_id, 'gender', true);
				//Film
				$data_age = get_post_meta( $post_ins_id, 'age', true );
				$data_gender = get_post_meta( $talent_post_id, 'gender', true );
				$data_height = get_post_meta( $post_ins_id, 'height', true );
				$data_weight = get_post_meta( $post_ins_id, 'weight', true );
				$data_hair_color = get_post_meta( $post_ins_id, 'hair_color', true );
				$data_language = get_post_meta( $post_ins_id, 'language', true );
				$data_experience = get_post_meta( $post_ins_id, 'experience', true );
				$data_phone = get_post_meta( $post_ins_id, 'phone', true );
				$data_email = get_post_meta( $post_ins_id, 'email', true );
				$data_address = get_post_meta( $post_ins_id, 'address', true );
				$data_assignment = get_post_meta( $post_ins_id, 'assignment', true );
				$data_dialect = get_post_meta( $post_ins_id, 'dialect', true );
				
				$data_film_category_term = wp_get_post_terms($post_ins_id, 'film_category');
				$data_film_category = $data_film_category_term[0]->slug;
				
				//echo $data_film_category;

				//Dance
				$data_dancer = get_post_meta( $post_ins_id, 'dancer', true );
				$data_dance_experience = get_post_meta( $post_ins_id, 'dance_experience', true );
				$data_dance_experience_description = get_post_meta( $post_ins_id, 'dance_experience_description', true );
				$data_dancer_type = get_post_meta( $post_ins_id, 'dancer_type', true );
				$data_city = get_post_meta( $post_ins_id, 'city', true );
				//Music
				$data_type_instrument = get_post_meta( $post_ins_id, 'type_instrument', true );
				if(is_array ($data_type_instrument)){
					$data_type_instrument = $data_type_instrument[0];
				}
				$data_instrumental_experience = get_post_meta( $post_ins_id, 'instrumental_experience', true );
				if(is_array ($data_instrumental_experience)){
					$data_instrumental_experience = $data_instrumental_experience[0];
				}
				$data_crew = get_post_meta( $post_ins_id, 'crew', true );
				if(is_array ($data_crew)){
					$data_crew = $data_crew[0];
				}
				$data_singer_x_solo_x_core_experience = get_post_meta( $post_ins_id, 'singer_x_solo_x_core_experience', true );
				if(is_array ($data_singer_x_solo_x_core_experience)){
					$data_singer_x_solo_x_core_experience = $data_singer_x_solo_x_core_experience[0];
				}
				$data_band_type = get_post_meta( $post_ins_id, 'band_type', true );
				if(is_array ($data_band_type)){
					$data_band_type = $data_band_type[0];
				}
				$dj_experience = get_post_meta( $post_ins_id, 'experience' );
				if(is_array ($dj_experience)){
					$dj_experience = $dj_experience[0];
				}
				$dj_description = get_post_meta( $post_ins_id, 'experince_description');
				if(is_array ($dj_description)){
					$dj_description = $dj_description[0];
				}
				$singer_experience = get_post_meta( $post_ins_id, 'music_experience' );
				if(is_array ($singer_experience)){
					$singer_experience = $singer_experience[0];
				}
				$singer_description = get_post_meta( $post_ins_id, 'music_experince_description' );
				if(is_array ($singer_description)){
					$singer_description = $singer_description[0];
				}
				$type_instrument = get_post_meta( $post_ins_id, 'type_instrument' );
				if(is_array ($type_instrument)){
					$type_instrument = $type_instrument[0];
				}
				$instrumental_experience = get_post_meta( $post_ins_id, 'instrumental_experience' );
				if(is_array ($instrumental_experience)){
					$instrumental_experience = $instrumental_experience[0];
				}
				$instrumental_description = get_post_meta( $post_ins_id, 'music_experince_description' );
				if(is_array ($instrumental_description)){
					$instrumental_description = $instrumental_description[0];
				}
				$dance_experience_description = get_post_meta( $post_ins_id, 'dance_experience_description' );
				if(is_array ($dance_experience_description)){
					$dance_experience_description = $dance_experience_description[0];
				}
				$tel_sports_experience_description = update_post_meta( $post_ins_id, 'cul_experince_description', $tel_sports_experience_description );
				if(is_array ($tel_sports_experience_description)){
					$tel_sports_experience_description = $tel_sports_experience_description[0];
				}
				 /* grab the url for the full size featured image */
				$featured_img_url = get_the_post_thumbnail_url($post_ins_id,'full'); 
				
				//Sports
				$tel_sports_experience = update_post_meta( $post_ins_id, 'culture_experience', $tel_sports_experience );
				
				$tel_band_genre = get_post_meta( $post_ins_id, 'genre' );
				if(is_array ($tel_band_genre)){
					$tel_band_genre = $tel_band_genre[0];
				}
				$tel_crew = get_post_meta( $post_ins_id, 'crew' );
				if(is_array ($tel_crew)){
					$tel_crew = $tel_crew[0];
				}
				$tel_repertoire = get_post_meta( $post_ins_id, 'repertoire' );
				if(is_array ($tel_repertoire)){
					$tel_repertoire = $tel_repertoire[0];
				}
				$tel_founded = get_post_meta( $post_ins_id, 'founded' );
				if(is_array ($tel_founded)){
					$tel_founded = $tel_founded[0];
				}
				
				$music_category = wp_get_post_terms($post_ins_id, 'music_category');
				$experience_description = get_post_meta( $post_ins_id, 'experience_description');
				$get_posted_images_url = get_post_meta( $post_ins_id, 'gallery_images', true );
				$get_posted_videos_url = get_post_meta( $post_ins_id, 'gallery_videos', true );
				$video_custom = get_post_meta( $post_ins_id, 'video_custom', true );
			
			//// Start edit
			
			$post_inner_data = get_post_meta($post_ins_id);
				
				//Get Current Post type
				$current_post_type_get = get_post($post_ins_id);
				$current_post_type = $current_post_type_get->post_type;
				$content = $current_post_type_get->post_content;
				$content = apply_filters('the_content', $content);
				$content = str_replace(']]>', ']]&gt;', $content);
				$content = strip_tags($content);
				//Title
				$post_title = $current_post_type_get->post_title;
				$data_firstname = explode(' ',trim($post_title));
				//echo $data_firstname[0]; 
				$data_lastname = $data_firstname[1].$data_firstname[2].$data_firstname[3].$data_firstname[4];

				//Film
				$data_age = get_post_meta( $post_ins_id, 'age', true );
				//$data_gender = get_post_meta( $post_ins_id, 'gender', true );
				$data_height = get_post_meta( $post_ins_id, 'height', true );
				$data_weight = get_post_meta( $post_ins_id, 'weight', true );
				$data_hair_color = get_post_meta( $post_ins_id, 'hair_color', true );
				$talent_audio = get_post_meta( $post_ins_id, 'talent_audio', true  );
				$data_language = get_post_meta( $post_ins_id, 'language', true );
				$data_experience = get_post_meta( $post_ins_id, 'experience', true );
				$data_phone = get_post_meta( $post_ins_id, 'phone', true );
				$data_email = get_post_meta( $post_ins_id, 'email', true );
				$data_address = get_post_meta( $post_ins_id, 'address', true );
				$data_assignment = get_post_meta( $post_ins_id, 'assignment', true );
				$data_dialect = get_post_meta( $post_ins_id, 'dialect', true );
				
				$data_film_category_term = wp_get_post_terms($post_ins_id, 'film_category');
				$data_film_category = $data_film_category_term[0]->slug;
				
				//echo $data_film_category;

				//Dance
				$data_dancer = get_post_meta( $post_ins_id, 'dancer', true );
				$data_dance_experience = get_post_meta( $post_ins_id, 'dance_experience', true );
				$data_dance_experience_description = get_post_meta( $post_ins_id, 'dance_experience_description', true );
				$data_dancer_type = get_post_meta( $post_ins_id, 'dancer_type', true );
				$data_city = get_post_meta( $post_ins_id, 'city', true );
				//Music
				$data_type_instrument = get_post_meta( $post_ins_id, 'type_instrument', true );
				if(is_array ($data_type_instrument)){
					$data_type_instrument = $data_type_instrument[0];
				}
				$data_instrumental_experience = get_post_meta( $post_ins_id, 'instrumental_experience', true );
				if(is_array ($data_instrumental_experience)){
					$data_instrumental_experience = $data_instrumental_experience[0];
				}
				$data_crew = get_post_meta( $post_ins_id, 'crew', true );
				if(is_array ($data_crew)){
					$data_crew = $data_crew[0];
				}
				$data_singer_x_solo_x_core_experience = get_post_meta( $post_ins_id, 'singer_x_solo_x_core_experience', true );
				if(is_array ($data_singer_x_solo_x_core_experience)){
					$data_singer_x_solo_x_core_experience = $data_singer_x_solo_x_core_experience[0];
				}
				$data_band_type = get_post_meta( $post_ins_id, 'band_type', true );
				if(is_array ($data_band_type)){
					$data_band_type = $data_band_type[0];
				}
				$dj_experience = get_post_meta( $post_ins_id, 'experience' );
				if(is_array ($dj_experience)){
					$dj_experience = $dj_experience[0];
				}
				$dj_description = get_post_meta( $post_ins_id, 'experince_description');
				if(is_array ($dj_description)){
					$dj_description = $dj_description[0];
				}
				$singer_experience = get_post_meta( $post_ins_id, 'music_experience' );
				if(is_array ($singer_experience)){
					$singer_experience = $singer_experience[0];
				}
				$singer_description = get_post_meta( $post_ins_id, 'experince_description' );
				if(is_array ($singer_description)){
					$singer_description = $singer_description[0];
				}
				$type_instrument = get_post_meta( $post_ins_id, 'type_instrument' );
				if(is_array ($type_instrument)){
					$type_instrument = $type_instrument[0];
				}
				$instrumental_experience = get_post_meta( $post_ins_id, 'instrumental_experience' );
				if(is_array ($instrumental_experience)){
					$instrumental_experience = $instrumental_experience[0];
				}
				$instrumental_description = get_post_meta( $post_ins_id, 'experince_description' );
				if(is_array ($instrumental_description)){
					$instrumental_description = $instrumental_description[0];
				}
				$dance_experience_description = get_post_meta( $post_ins_id, 'experience_description' );
				if(is_array ($dance_experience_description)){
					$dance_experience_description = $dance_experience_description[0];
				}
				$tel_sports_experience_description = get_post_meta( $post_ins_id, 'experince_description', $tel_sports_experience_description );
				if(is_array ($tel_sports_experience_description)){
					$tel_sports_experience_description = $tel_sports_experience_description[0];
				}
				 /* grab the url for the full size featured image */
				$featured_img_url = get_the_post_thumbnail_url($post_ins_id,'full'); 
				
				//Sports
				$tel_sports_experience = update_post_meta( $post_ins_id, 'culture_experience', $tel_sports_experience );
				
				$tel_band_genre = get_post_meta( $post_ins_id, 'genre' );
				if(is_array ($tel_band_genre)){
					$tel_band_genre = $tel_band_genre[0];
				}
				$tel_crew = get_post_meta( $post_ins_id, 'crew' );
				if(is_array ($tel_crew)){
					$tel_crew = $tel_crew[0];
				}
				$tel_repertoire = get_post_meta( $post_ins_id, 'repertoire' );
				if(is_array ($tel_repertoire)){
					$tel_repertoire = $tel_repertoire[0];
				}
				$tel_founded = get_post_meta( $post_ins_id, 'founded' );
				if(is_array ($tel_founded)){
					$tel_founded = $tel_founded[0];
				}
				$tel_sports_expertise = get_post_meta( $post_ins_id, 'expertise');
				$tel_sports_experience = get_post_meta( $post_ins_id, 'experience' );
				$tel_sports_experience_description = get_post_meta( $post_ins_id, 'experince_description');
				$music_category = wp_get_post_terms($post_ins_id, 'music_category');
				$experience_description = get_post_meta( $post_ins_id, 'experience_description');
				$get_posted_images_url = get_post_meta( $post_ins_id, 'gallery_images', true );
				$get_posted_videos_url = get_post_meta( $post_ins_id, 'gallery_videos', true );
				$video_custom = get_post_meta( $post_ins_id, 'video_custom', true );
			
			
			//// end edit
			$get_permalink_profile_second = get_permalink( $post_ins_id ); 
			
			update_post_meta($talent_post_id,'talent_post_id_second',$get_permalink_profile_second);
			$post_title = get_the_title($talent_post_id);
			$data_firstname = explode(' ',trim($post_title));
			//echo $data_firstname[0]; 
			$data_lastname = $data_firstname[1].$data_firstname[2].$data_firstname[3].$data_firstname[4];
			$data_city = get_post_meta( $talent_post_id, 'city', true );
			$data_phone = get_post_meta( $talent_post_id, 'phone', true );
			$data_email = get_post_meta( $talent_post_id, 'email', true );
			$data_address = get_post_meta( $talent_post_id, 'address', true );
			
			
				?>
<!-- #Content -->
<div id="content_wrapper" class="span12 reg_telent_wrapper">
	
<div class="row-fluid">
			
	<div class="span12 f_col moudle">
		<div id="form_container_main">
		<div style="text-align: left;font-size: 16px;margin-top: 10px;">
		<a style="margin-left:10px;background: #ff3d00;color: #fff;padding: 6px 10px !important;border-radius: 7px;" href="<?php echo $edit_profile_link; ?>"><i style="margin-right:6px;"class="fa fa-arrow-left"></i>Back</a></div>
		<h2><i style="margin-right:6px;" class="fa fa-check"></i>I want to add new talent:</h2>
		
			<form class="panel-login dashboard-form telent_register_form" id="register_telent_form" action="" method="post"  >

				    
				    <div class="form-group">
					<div id="basic_info">
				            <div class="col-md-6">
								 <label for="phone_number1">First Name</label>
				                <input type="text" name="firstname" placeholder="First Name" value="<?php echo $data_firstname[0]; ?>" required1="" >
				            </div>	

				            <div class="col-md-6">
				                 <label for="phone_number1">Last Name</label>
								<input type="text" name="lastname" placeholder="Last Name" value="<?php echo $data_lastname; ?>" required1="">
				            </div>
				            <div class="col-md-6">
							 <label for="phone_number1">City</label>
				                <input type="text" name="city" placeholder="City" value="<?php echo $data_city; ?>" required1="">
				            </div>
				            <div class="col-md-6">
							 <label for="phone_number1">Address</label>
				                <input type="text" name="street" placeholder="street" value="<?php echo $data_address; ?>" required1="">
				            </div>

				            <div class="col-md-6">
							 <label for="phone_number1">Email</label>
				                <input type="email" name="email" placeholder="Email" value="<?php echo $data_email; ?>" required1="">
				            </div>	
							<div class="col-md-6">
							 <label for="phone_number1">Phone</label>
				                <input type="text" name="phone" placeholder="Phone" value="<?php echo $data_phone; ?>" required1="">
				            </div>	
					</div>	
				           
					  
				            <div class="col-md-6">
				            <div id="div_talent_category">
							 <label for="phone_number1">Talent Category</label>
				                <select name="category" id="talent_category" required1="">
				                    <option value="">-Talent Category -</option>
				                		                 				               
				                	<?php if($current_post_type == 'culture'){ ?>
					                    <option value="dancer">Dance</option>
					                    <option value="film">Film & theater</option>
					                    <option value="music">Music</option>
					                    <option value="culture" selected>Culture</option>
					                    <option value="sports">Sports</option>
				                	<?php } ?>
				                	 <?php if($current_post_type == 'music'){ ?>
					                    <option value="dancer">Dance</option>
					                    <option value="film">Film & theater</option>
					                    <option value="music" selected>Music</option>
					                    <option value="culture" >Culture</option>
					                    <option value="sports">Sports</option>
				                	<?php } ?>
				                	<?php if($current_post_type == 'dancer'){ ?>
					                    <option value="dancer" selected>Dance</option>
					                    <option value="film">Film & theater</option>
					                    <option value="music">Music</option>
					                    <option value="culture" >Culture</option>
					                    <option value="sports">Sports</option>
				                	<?php } ?>
				                	<?php if($current_post_type == 'sports'){ ?>
					                    <option value="dancer">Dance</option>
					                    <option value="film">Film & theater</option>
					                    <option value="music">Music</option>
					                    <option value="culture" >Culture</option>
					                    <option value="sports" selected>Sports</option>
				                	<?php } ?>
				                	<?php if($current_post_type == 'film' || $current_post_type == 'actor' || $current_post_type == 'model'){ ?>
					                    <option value="dancer">Dance</option>
					                    <option value="film" selected>Film & theater</option>
					                    <option value="music">Music</option>
					                    <option value="culture" >Culture</option>
					                    <option value="sports">Sports</option>
				                	<?php } ?>
									<?php if($current_post_type == 'page'){ ?>
					                    <option value="dancer">Dance</option>
					                    <option value="film">Film & theater</option>
					                    <option value="music">Music</option>
					                    <option value="culture" >Culture</option>
					                    <option value="sports">Sports</option>
				                	<?php } ?>


				                </select>
							</div>
							<!--<div id="new_category" class="text-left">
							   <input type="checkbox" id="second_cat" name="terms" required1="" value="true"><span style="">Did you have another talent ?</span>
							</div>
				            </div>
							<div class="col-md-6" id="div_talent_category_second" style="display:none;">
							 <label for="phone_number1">Another Category</label>
				           <div class="mini-box"><a target="_blank" href="<?php // echo site_url().'/new-talent/?profile_id='.$talent_post_id; ?>"><input type="button" name="view_profile" tabindex="4" class="view_profile form-control btn btn-register btn-pink" value="Click here"></a></div>-->
							<!--<div id="div_talent_category"><label for="phone_number1">Multi Talent Category</label>
							<div class="multi_category_input"><input type="checkbox" id="dancer" name="multi_category[]" class="multi_talent" required1="" value="dancer"><span style="">Dance</span></div>
							<div class="multi_category_input"><input type="checkbox" id="film" name="multi_category[]" class="multi_talent" required1="" value="film"><span style="">Film & theater</span></div>
							<div class="multi_category_input"><input type="checkbox" id="music" name="multi_category[]" class="multi_talent" required1="" value="music"><span style="">Music</span></div>
							<div class="multi_category_input"><input type="checkbox" id="culture" name="multi_category[]" class="multi_talent" required1="" value="culture"><span style="">Culture</span></div>
							<div class="multi_category_input"><input type="checkbox" id="sports" name="multi_category[]" class="multi_talent" required1="" value="sports"><span style="">Sports</span></div>
							</div> -->
							</div>
							
				    <?php if($current_post_type == 'film' || $current_post_type == 'actor' || $current_post_type == 'model'){ ?>
				  		<div id="tel_film" class="">
				  	<?php } else{ ?>
				  		<div id="tel_film" class="hide_this">
				  	<?php } ?>
						<div class="col-md-6">
							 <label for="phone_number1">Film Category</label>
								<select name="film_category" id="film_category">
									<option value="">- Film Category -</option>
									
									<?php 
						if($data_film_category == 'statist' || $data_film_category == 'voiceover' || $current_post_type == 'actor' || $current_post_type == 'model'){
									
									if($data_film_category == 'statist'){ ?>
										<option value="statist" selected="selected">Statist</option>
									<?php } else{ ?>
								  		<option value="statist">Statist</option>
								  	<?php } ?>
								  	
								  	<?php if($data_film_category == 'voiceover'){ ?>
										<option value="voiceover" selected="selected">Voiceover</option>
									<?php } else{ ?>
								  		<option value="voiceover">Voiceover</option>
								  	<?php }
								  									  	
								  	//Else for actor and model
									if($current_post_type == 'actor'){ ?>
										<option value="actor" selected="selected">Actor</option>
									<?php } else{ ?>
								  		<option value="actor">Actor</option>
								  	<?php } ?>
									
									<?php if($current_post_type == 'model'){ ?>
										<option value="model" selected="selected">Advertising Model</option>
									<?php } else{ ?>
								  		<option value="model" >Advertising Model</option>
								  	<?php } ?>
								  	<?php }else{
										?>
										<option value="statist">Statist</option>
										<option value="voiceover">Voiceover</option>
										<option value="actor">Actor</option>
										<option value="model" >Advertising Model</option>
									<?php } ?>
									
								</select>
						</div>
						</div>  
					  
					

					<?php if($current_post_type == 'music'){
						$data_music_type = $music_category[0]->slug;
					 ?><div id="tel_music" class="">
						<?php } 
							  else{ ?>
				<div id="tel_music" class="hide_this">
						<?php 
					} ?>
					<div class="col-md-6">
						 <label for="phone_number1">Music Category</label>
				                <select name="music_category" id="music_category">
				                    <option value="">- Music Category -</option>
				                    
				                    <?php 
									
									if($data_music_type == 'band'){ ?>
					                    <option value="band" selected>Band</option>
					                    <option value="dj">Dj</option>
					                    <option value="singer">Singer</option>
					                    <option value="instrumentals">Instrumentals</option>
				                	<?php } ?>
				                	<?php if($data_music_type == 'dj'){ ?>
					                    <option value="band" >Band</option>
					                    <option value="dj" selected>Dj</option>
					                    <option value="singer">Singer</option>
					                    <option value="instrumentals">Instrumentals</option>
				                	<?php } ?>
				                	<?php if($data_music_type == 'singer'){ ?>
					                    <option value="band" >Band</option>
					                    <option value="dj" >Dj</option>
					                    <option value="singer" selected>Singer</option>
					                    <option value="instrumentals">Instrumentals</option>
				                	<?php } ?>
				                	<?php if($data_music_type == 'instrumentals'){ ?>
					                    <option value="band" >Band</option>
					                    <option value="dj" >Dj</option>
					                    <option value="singer" >Singer</option>
					                    <option value="instrumentals" selected>Instrumentals</option>
				                	<?php } else{ ?>
					                    <option value="band" >Band</option>
					                    <option value="dj" >Dj</option>
					                    <option value="singer" >Singer</option>
					                    <option value="instrumentals" >Instrumentals</option>
				                	<?php }?> 

				                </select>
				    </div>
				</div>
					
					<?php 
					if($data_music_type == 'band'){ ?>
				     <div id="tel_band" class="">
				    <?php } 
				    	  else{ ?>
					<div id="tel_band" class="hide_this">
				    <?php } ?>
				   		<div class="col-md-6">
						 <label for="phone_number1">Band</label>
				                <select name="band_category">
				                    <option value="<?php echo $data_band_type; ?>" selected><?php echo $data_band_type; ?></option>
				                    <option value="party_band">Party Band</option>
				                    <option value="jazz">Jazz</option>
				                    <option value="country">Country</option>
				                    <option value="rock">rock</option>
				                    <option value="heavy_rock">Heavy Rock</option>
				                    <option value="other">Other</option>
				                </select>
				    	</div>
						<div class="col-md-12 telent_description">
							<label for="phone_number1">Genre</label>
							<textarea name="band_genre" placeholder="Genre" required1="" ><?php echo strip_tags($tel_band_genre); ?></textarea>
						</div>
						<div class="col-md-12">
							<label for="phone_number1">Founded</label>						
							<input type="text" name="founded" value="<?php echo strip_tags($tel_founded); ?>" placeholder="Founded" />
				    	</div>
						<div class="col-md-12">
							<label for="phone_number1">Repertoire</label>						
							<textarea name="repertoire" placeholder="Repertoire" ><?php echo strip_tags($tel_repertoire); ?></textarea>
				    	</div>
						<div class="col-md-12">
							<label for="phone_number1">Crew</label>						
							<textarea name="crew" placeholder="Crew" ><?php echo strip_tags($tel_crew); ?></textarea>
				    	</div>
					</div>
					
					<?php if($data_band_type == 'dj'){ ?>
				     <div id="tel_dj_experience" class="">
				    <?php } 
				    	  else{ ?>
				     <div id="tel_dj_experience" class="hide_this">
				    <?php } ?>

						<div class="col-md-6">
							<label for="phone_number1">Dj Experience</label>			            
				                <select name="dj_experience">
				                    <?php if($dj_experience){
									?><option value="<?php echo $dj_experience; ?>" selected>- <?php echo $dj_experience; ?> -</option>	<?php
									} else { ?>
									<option value="" selected>-Select Experience-</option>	<?php
									} ?>
									
				                    <option value="low_experience">Low experience</option>
				                    <option value="medium">Medium</option>
				                    <option value="professional">Professional</option>
				                </select>
						</div>
						<div class="col-md-12">
							<textarea name="dj_description" placeholder="Dj Experience" ><?php echo strip_tags($dj_description); ?></textarea>
				    	</div>
					</div>
					

					<?php if($data_band_type == 'singer'){ ?>
				     <div id="tel_singer" class="">
				    <?php } 
				    	  else{ ?>
				     <div id="tel_singer" class="hide_this">
				    <?php } ?>
						<div class="col-md-6">
							<label for="phone_number1">Singer Experience</label>			            
				                <select name="singer_experience">
				                    <option value="<?php echo $singer_experience; ?>">- <?php echo $singer_experience; ?> -</option>
									 <?php if($singer_experience){
									?><option value="<?php echo $singer_experience; ?>" selected>- <?php echo $singer_experience; ?> -</option>	<?php
									} else { ?>
									<option value="" selected>-Select Experience-</option>	<?php
									} ?>
				                    <option value="low_experience">Low experience</option>
				                    <option value="medium">Medium</option>
				                    <option value="professional">Professional</option>
				                </select>
							</div>
						<div class="col-md-12">
								<textarea name="singer_description" placeholder="Singer Experience" ><?php echo strip_tags($singer_description); ?></textarea>
				    	</div>
					</div>
					
					<?php if($data_band_type == 'instrumentals'){ ?>
				     <div id="tel_instrumentals" class="">
				    <?php } 
				    	  else{ ?>
				     <div id="tel_instrumentals" class="hide_this">
				    <?php } ?>
						<div class="col-md-6">
							<label for="phone_number1">Type of instrument</label>						
							<input type="text" value="<?php if($data_type_instrument) print_r($data_type_instrument); ?>" name="type_instrument" placeholder="Type of instrument" />
						</div>
						<div class="col-md-6">
							<label for="phone_number1">Played number of years</label>
							<input type="text" value="<?php if($instrumental_experience) echo $instrumental_experience; ?>" name="instrumental_experience" placeholder="Enter years" />
						</div>
						<div class="col-md-12">
							<textarea name="instrumental_description" placeholder="instrumentals Experience" ><?php echo strip_tags($instrumental_description);?></textarea>
						</div>
					</div>
				</div>

				<?php if($current_post_type == 'dancer'){ ?>
			     <div id="tel_dance" class="">
			    <?php } 
			        else{ ?>
			     <div id="tel_dance" class="hide_this">
			    <?php } ?>
					<div class="col-md-6">
						<label for="phone_number1">Dance Type</label>			            
							<select name="dancer_type" id="dancer_type">
								<option value="">- Dance Type -</option>
								
								<?php if($data_dancer_type == 'hip_hop' || $data_dancer_type == 'modern_dance' || $data_dancer_type == 'ballet'){ ?>
								<?php if($data_dancer_type == 'hip_hop'){ ?>
									<option value="modern_dance">Modern dance</option>
									<option value="hip_hop" selected>Hip Hop</option>
									<option value="ballet">Ballet</option>
								<?php } ?>	
								<?php if($data_dancer_type == 'modern_dance'){ ?>
									<option value="modern_dance" selected>Modern dance</option>
									<option value="hip_hop" >Hip Hop</option>
									<option value="ballet">Ballet</option>
								<?php } ?>	
								<?php if($data_dancer_type == 'ballet'){ ?>
									<option value="modern_dance" >Modern dance</option>
									<option value="hip_hop" >Hip Hop</option>
									<option value="ballet" selected>Ballet</option>
								<?php }  
								}else{ ?>
									<option value="modern_dance" >Modern dance</option>
									<option value="hip_hop" >Hip Hop</option>
									<option value="ballet" >Ballet</option>
								<?php } ?>	

							</select>
					</div>
				<div class="col-md-12">
					<div id="tel_modern_dance" class="hide_this">
			            <div class="col-md-6" style="padding-left: 0;">
							<label for="phone_number1"> Dance Experience</label>			            
				                <select name="dance_experience">
				                    <option value="">- Dance Experience -</option>
				                    
				                    <option value="low_experience">Low experience</option>
				                    <option value="medium">Medium</option>
				                    <option value="professional">Professional</option>
				                </select>
								</div>
							<div class="col-md-12" style="padding-left: 0;">
								<textarea name="dance_experience_description" placeholder="Dance Experience" ><?php echo strip_tags($dance_experience_description); ?></textarea>
							</div>
					</div>
				</div>
				</div>
				
					
						<?php if($data_film_category == 'statist'){ ?>
							<div id="tel_statist" class="">
						<?php } else{ ?>
					  		<div id="tel_statist" class="hide_this">
					  	<?php } ?>

			            <div class="col-md-6">
						<label for="phone_number1">Age</label>
							<select name="statist_age" >  
								<option value="<?php echo $data_age; ?>" selected="selected"> -- Selected Age: <?php echo $data_age; ?> -- </option>
								<option value="18">18</option><option value="19">19</option>
								<option value="20">20</option><option value="21">21</option>
								<option value="22">22</option><option value="23">23</option>
								<option value="24">24</option><option value="25">25</option>
								<option value="26">26</option><option value="27">27</option>
								<option value="28">28</option><option value="29">29</option>
								<option value="30">30</option><option value="31">31</option>
								<option value="32">32</option><option value="33">33</option>
								<option value="34">34</option><option value="35">35</option>
								<option value="36">36</option><option value="37">37</option>
								<option value="38">38</option><option value="39">39</option>
								<option value="40">40</option><option value="41">41</option>
								<option value="42">42</option><option value="43">43</option>
								<option value="44">44</option><option value="45">45</option>
								<option value="46">46</option><option value="47">47</option>
								<option value="48">48</option><option value="49">49</option>
								<option value="50">50</option><option value="51">51</option>
								<option value="52">52</option><option value="53">53</option>
								<option value="54">54</option><option value="55">55</option>
								<option value="56">56</option><option value="57">57</option>
								<option value="58">58</option><option value="59">59</option>
								<option value="60">60</option>
							</select>
			            </div>	
			    	
			            <div class="col-md-6 hide_this">
						<label for="phone_number1">Gender</label>
							<select name="statist_gender">
								<option value="">- Select Gender -</option>

								<?php if($data_gender == 'male' || $data_gender == 'Male'){ ?>	
								<option value="male" selected>- Male -</option>
								<option value="female">- Female -</option>
								<?php	} ?>
								<?php if($data_gender == 'female' || $data_gender == 'Female'){ ?>	
									<option value="male" >- Male -</option>
									<option value="female" selected>- Female -</option>
								<?php	} ?>
							</select>
			            </div>	

			            <div class="col-md-6">
						<label for="phone_number1">Height</label>
			                <input type="text" name="statist_height" value="<?php echo $data_height; ?>" placeholder="Height" >
			            </div>	
			           
			            <div class="col-md-6">
						<label for="phone_number1">Weight</label>
			                <input type="text" name="statist_weight" value="<?php echo $data_weight; ?>" placeholder="Weight" >
			            </div>	
			            <div class="col-md-6">
						<label for="phone_number1">Hair color</label>
			                <input type="text" name="statist_hair_color" value="<?php echo $data_hair_color ?>" placeholder="Hair color" >
			            </div>	

			            <div class="col-md-6">
						<label for="phone_number1">Dialect</label>
			                <input type="text" name="statist_dialect" value="<?php echo $data_dialect ?>" placeholder="Dialect" >
			            </div>	
				    	
			            <div class="col-md-6">
						<label for="phone_number1">Language</label>
			                <input type="text" name="statist_language" value="<?php echo $data_language; ?>" placeholder="Language" >
			            </div>	
			    	
			            <div class="col-md-6">
						<label for="phone_number1">Experience</label>
							<select name="statist_experience">
								<option value="">- Select Experience -</option>
								<?php if($data_experience == 'low_experience'){ ?>
									<option value="low_experience" selected="selected">Low experience</option>
									<option value="medium">Medium</option>
									<option value="professional">Professional</option>
								<?php	} ?>
								<?php if($data_experience == 'medium'){ ?>
									<option value="low_experience" >Low experience</option>
									<option value="medium" selected="selected">Medium</option>
									<option value="professional">Professional</option>
								<?php	} ?>
								<?php if($data_experience == 'professional'){ ?>
									<option value="low_experience" >Low experience</option>
									<option value="medium">Medium</option>
									<option value="professional" selected="selected">Professional</option>
								<?php	} ?>
							</select>	
							
			            </div>	
				    </div>
				    <div id="tel_actor" class="hide_this">
			            <div class="col-md-6">
						<label for="phone_number1">Age</label>
							<select name="actor_age" >
								<option value="<?php echo $data_age; ?>" selected="selected"> -- Selected Age: <?php echo $data_age; ?> -- </option>
								<option value="18">18</option><option value="19">19</option>
								<option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select>
			            </div>	
			    	
			            <div class="col-md-6 hide_this">
						<label for="phone_number1">Gender</label>
							<select name="actor_gender">
								<option value="">- Select Gender -</option>
								
								<?php

								if($data_gender == 'male' || $data_gender == 'Male'){ ?>	
								<option value="male" selected>- Male -</option>
								<option value="female">- Female -</option>
								<?php	} ?>
								<?php if($data_gender == 'female' || $data_gender == 'Female'){ ?>	
									<option value="male" >- Male -</option>
									<option value="female" selected>- Female -</option>
								<?php	} ?>
							</select>
							
			            </div>	

			            <div class="col-md-6">
						<label for="phone_number1">Height</label>
			                <input type="text" name="actor_height" value="<?php echo $data_height; ?>" placeholder="Height" >
			            </div>	
			           
			            <div class="col-md-6">
						<label for="phone_number1">Weight</label>
			                <input type="text" name="actor_weight" value="<?php echo $data_weight; ?>" placeholder="Weight" >
			            </div>	
			            <div class="col-md-6">
						<label for="phone_number1">Hair color</label>
			                <input type="text" name="actor_hair_color" value="<?php echo $data_hair_color ?>" placeholder="Hair color" >
			            </div>
			            <div class="col-md-6">
						<label for="phone_number1">Language</label>
			                <input type="text" name="actor_language" value="<?php echo $data_language; ?>" placeholder="Language" >
			            </div>	
			    	
			            <div class="col-md-6">
						<label for="phone_number1">Experience</label>
							<select name="actor_experience">
								<option value="">- Select Experience -</option>
								<?php if($data_experience == 'low_experience'){ ?>
									<option value="low_experience" selected="selected">Low experience</option>
									<option value="medium">Medium</option>
									<option value="professional">Professional</option>
								<?php	} ?>
								<?php if($data_experience == 'medium'){ ?>
									<option value="low_experience" >Low experience</option>
									<option value="medium" selected="selected">Medium</option>
									<option value="professional">Professional</option>
								<?php	} ?>
								<?php if($data_experience == 'professional'){ ?>
									<option value="low_experience" >Low experience</option>
									<option value="medium">Medium</option>
									<option value="professional" selected="selected">Professional</option>
								<?php	} ?>
							</select>	
			            </div>	
				    </div>
				    <div id="tel_model" class="hide_this">
			            <div class="col-md-6">
						<label for="phone_number1">Age</label>
							<select name="model_age" >  
								<option value="<?php echo $data_age; ?>" selected="selected"> -- Selected Age: <?php echo $data_age; ?> -- </option>
								<option value="18">18</option><option value="19">19</option>
								<option value="20">20</option><option value="21">21</option>
								<option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select>
			            </div>	

			            <div class="col-md-6">
						<label for="phone_number1 hide_this">Gender</label>
							<select name="model_gender">
								<option value="">- Select Gender -</option>
								<?php if($data_gender == 'male' || $data_gender == 'Male'){ ?>	
								<option value="male" selected>- Male -</option>
								<option value="female">- Female -</option>
								<?php	} ?>
								<?php if($data_gender == 'female' || $data_gender == 'Female'){ ?>	
									<option value="male" >- Male -</option>
									<option value="female" selected>- Female -</option>
								<?php	} ?>
							</select>
			            </div>	
			           
			            <div class="col-md-6">
						<label for="phone_number1">Height</label>
			                <input type="text" name="model_height" value="<?php echo $data_height; ?>" placeholder="Enter Height" >
			            </div>	
			            <div class="col-md-6">
						<label for="phone_number1">Weight</label>
			                <input type="text" name="model_weight" value="<?php echo $data_weight; ?>" placeholder="Enter Weight" >
			            </div>	
						 <div class="col-md-6">
							<label for="phone_number1">Model Experience</label>			            
							<select name="model_exprience">
								<option value="">- Select Experience -</option>
								<?php if($data_experience == 'low_experience'){ ?>
									<option value="low_experience" selected="selected">Low experience</option>
									<option value="medium">Medium</option>
									<option value="professional">Professional</option>
								<?php	} ?>
								<?php if($data_experience == 'medium'){ ?>
									<option value="low_experience" >Low experience</option>
									<option value="medium" selected="selected">Medium</option>
									<option value="professional">Professional</option>
								<?php	} ?>
								<?php if($data_experience == 'professional'){ ?>
									<option value="low_experience" >Low experience</option>
									<option value="medium">Medium</option>
									<option value="professional" selected="selected">Professional</option>
								<?php	} ?>
							</select>				    	
						</div>
				    </div>
					<div id="tel_voiceover" class="hide_this">
			           <div class="col-md-6 hide_this">
						<label for="phone_number1">Gender</label>
							<select name="voiceover_gender">
								<option value="">- Select Gender -</option>
								<?php if($data_gender == 'male' || $data_gender == 'Male'){ ?>	
								<option value="male" selected>- Male -</option>
								<option value="female">- Female -</option>
								<?php	} ?>
								<?php if($data_gender == 'female' || $data_gender == 'Female'){ ?>	
									<option value="male" >- Male -</option>
									<option value="female" selected>- Female -</option>
								<?php	} ?>
							</select>
			            </div>
						 <div class="col-md-6 text-left">
							<label for="phone_number1">Voiceover Experience</label>			            
							<input type="checkbox" name="voiceover_experience[]" required1="" value="advertising"><span class="label_span">Advertising</span>				    	
							<input type="checkbox" name="voiceover_experience[]" required1="" value="informative"><span class="label_span">Informative</span>				    	
						</div>
						<div class="col-md-12">
								<textarea name="voiceover_description" placeholder="Voiceover Experience" ><?php echo strip_tags($tel_experienc_description); ?></textarea>
				    	</div>
						<div class="col-md-12 text-left">
							<label for="email">Upload your voice</label>
							<?php echo do_shortcode('[ajax-file-upload disallow_remove_button="1" unique_identifier="my_talent_audio" on_success_set_input_value="#talent_audio" allowed_extensions="mp3,wav" on_success_alert="Your audio file was successfully uploaded !" max_size=10000]'); ?>
							<div class="click_here">click here to upload !</div>
							<textarea type="text"  class="form-control hide" name="talent_audio" value="" id="talent_audio"><?php echo strip_tags($talent_audio); ?></textarea>
						</div>
				    </div>
				    

					<?php if($current_post_type == 'culture'){ ?>
					     <div id="tel_culture" class="">
					    <?php } 
					        else{ ?>
					     <div id="tel_culture" class="hide_this">
					    <?php } ?>
						<div class="col-md-12">
								<label for="phone_number1">Experties</label>
								<textarea name="culture_expertise" placeholder="Player Experties" ><?php echo strip_tags($tel_sports_expertise); ?></textarea>
				    	</div>
			           
				    	

			            <div class="col-md-6">
						<label for="phone_number1">Experience</label>
							<select name="culture_experience">
								<option value="">- Select Experience -</option>
								<?php if($data_experience == 'low_experience'){ ?>
									<option value="low_experience" selected>Low experience</option>
									<option value="medium">Medium</option>
									<option value="professional">Professional</option>
								<?php }?>
								<?php if($data_experience == 'medium'){ ?>
									<option value="low_experience">Low experience</option>
									<option value="medium" selected>Medium</option>
									<option value="professional">Professional</option>
								<?php }?>
								<?php if($data_experience == 'professional'){ ?>
									<option value="low_experience">Low experience</option>
									<option value="medium" >Medium</option>
									<option value="professional" selected>Professional</option>
								<?php }?>
							</select>
					   </div>	

			           <div class="col-md-12">
								<textarea name="culture_experience_description" placeholder="Description" ><?php echo strip_tags($tel_sports_experience_description); ?></textarea>
				    	</div>
			            
						<div class="col-md-6 hide_this">
						<label for="cul_gender">Gender</label>
							<select name="cul_gender">
								<option value="">- Select Gender -</option>
							
							<?php if($data_gender == 'male' || $data_gender == 'Male'){ ?>	
								<option value="male" selected>- Male -</option>
								<option value="female">- Female -</option>
							<?php	} ?>
							<?php if($data_gender == 'female' || $data_gender == 'Female'){ ?>	
								<option value="male" >- Male -</option>
								<option value="female" selected>- Female -</option>
							<?php	} ?>

							</select>
			            </div>

				    </div>
					<div id="tel_sports" class="hide_this">
						<div class="col-md-12">
								<label for="phone_number1">Player Experties</label>
								<textarea name="sports_expertise" placeholder="Sports Experties" ><?php echo strip_tags($tel_sports_expertise); ?></textarea>
				    	</div>
			    	
			            <div class="col-md-6">
						
						<label for="phone_number1">Experience</label>
							<select name="sports_experience">
								<option value="">- Select Experience -</option>
								
								<?php if($tel_sports_experience == 'low_experience'){ ?>
									<option value="low_experience" selected>Low experience</option>
									<option value="medium">Medium</option>
									<option value="professional">Professional</option>
								<?php }?>
								<?php if($tel_sports_experience == 'medium'){ ?>
									<option value="low_experience">Low experience</option>
									<option value="medium" selected>Medium</option>
									<option value="professional">Professional</option>
								<?php }?>
								<?php if($tel_sports_experience == 'professional'){ ?>
									<option value="low_experience">Low experience</option>
									<option value="medium" >Medium</option>
									<option value="professional" selected>Professional</option>
								<?php }?>
							</select>
					   </div>		
			            <div class="col-md-12">
							<textarea name="sports_experience_description" placeholder="Description" ><?php echo strip_tags($tel_sports_experience_description); ?></textarea>
				    	</div>
			            <div class="col-md-6 hide_this">
						<label for="phone_number1">Gender</label>
							<select name="sports_gender">
								<option value="">- Select Gender -</option>
								<?php if($data_gender == 'male' || $data_gender == 'Male'){ ?>	
								<option value="male" selected>- Male -</option>
								<option value="female">- Female -</option>
								<?php	} ?>
								<?php if($data_gender == 'female' || $data_gender == 'Female'){ ?>	
								<option value="male" >- Male -</option>
								<option value="female" selected>- Female -</option>
							<?php	} ?>
							</select>
			            </div>	 

			            <div class="col-md-6">
						<label for="phone_number1">Age</label>
							<select name="sports_age" >  
								<option value="<?php echo $data_age; ?>" selected="selected"> -- Selected Age: <?php echo $data_age; ?> -- </option>
								<option value="18">18</option><option value="19">19</option>
								<option value="20">20</option><option value="21">21</option>
								<option value="22">22</option><option value="23">23</option>
								<option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select>
							
			            </div>
				    </div>

	 <div class="col-md-6">
					<label for="phone_number1">Can take assignments</label>
		                <select name="assignments" required1="">
		                    <option value=""> - Can take assignments - </option>
		                    
		                    <?php if($data_assignment == 'Anytime' || $data_assignment == 'By appointment' || $data_assignment == 'Only via agency and agent'){ ?>
		                    <?php if($data_assignment == 'Anytime'){ ?>
		                    <option value="Anytime" selected="selected"> Anytime </option>
		                    <option value="By appointment"> By appointment </option>
		                    <option value="Only via agency and agent">Only via agency and agent</option>
		                    <?php	} ?>
		                    <?php if($data_assignment == 'By appointment'){ ?>
		                    <option value="Anytime"> Anytime </option>
		                    <option value="By appointment" selected="selected"> By appointment </option>
		                    <option value="Only via agency and agent">Only via agency and agent</option>
		                    <?php	} ?>
		                    <?php if($data_assignment == 'Only via agency and agent'){ ?>
		                    <option value="Anytime"> Anytime </option>
		                    <option value="By appointment"> By appointment </option>
		                    <option value="Only via agency and agent" selected="selected">Only via agency and agent</option>
		                    <?php	} ?>
		                    <?php	}else{
								?>
								<option value="Anytime"> Anytime </option>
		                    <option value="By appointment"> By appointment </option>
		                    <option value="Only via agency and agent">Only via agency and agent</option>
							
						<?php
							} ?>
							
		                </select>
		            </div>
					<!--<div class="final_buttons row">
						<div class="col col-sm-8">
							 
							 <div class="row">
								<div class="col-xs-6" id="tel_upload_videos">
									
								</div>
								<div class="col-xs-6" id="tel_gallery_images">
										
								</div>
							</div>
						</div>
				    </div>-->
					 <div class="row">
						<div class="col col-sm-offset-2 col-sm-8" style="padding: 0;">
						<label class="col-sm-12"> Upload Gallery </label> 
							<div class="col-xs-6">
								<?php echo do_shortcode('[arfaly id="5109"]'); ?>
									<span class="upload_videos_btn">Upload Videos</span>
									<textarea  style="height: 0px;visibility: hidden;" title="upload_videos" type="text" class="upme-input " name="upload_videos" id="upload_videos"><?php echo $video_custom; ?></textarea>
								<div class="mini-box"><input type="submit" name="register_telent" tabindex="4" class="form-control btn btn-register btn-pink" value="Store my updates"></div>
							</div>
							<?php if($get_permalink_profile_second){ ?>
							<div class="col-xs-6">
							<?php echo do_shortcode('[arfaly id="4714"]'); ?>
										<span class="upload_gallary">Upload Gallary</span>
										<textarea  style="height: 0px;visibility: hidden;" title="Gallery images" type="text" class="upme-input  upme-edit-first_name" name="gallery_images" id="gallery_images"><?php echo $get_posted_images_url; ?></textarea>	
								<div class="mini-box"><a href="<?php echo $get_permalink_profile_second; ?>"><input type="button" name="view_profile" tabindex="4" class="view_profile form-control btn btn-register btn-pink" value="View Profile"></a></div>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</form>	
		</div>
	</div>
</div>	

<?php 


get_footer(); ?>