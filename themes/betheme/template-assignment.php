<?php
/**
 * Template Name: Assignment
 * Description: A Page Template that display portfolio items.
 *
 * @package Betheme
 * @author Muffin Group
 */

get_header(); 

?>

<!-- #Content -->
	<script src="<?php echo get_template_directory_uri().'/js/masking.js';?>"></script>
	<div id="content_wrapper" class="span12 assignment_wrapper_main">
			
		
					
					<?php
					
					if (isset( $_POST['submit_assignment'] )) {
						$assign_address = $_POST['assign_address'];
						$assign_phone = $_POST['assign_phone'];
						$assign_email = $_POST['assign_email'];
						$assign_title = $_POST['assign_title'];
						$assign_description = $_POST['assign_description'];
						$assign_amount = $_POST['assign_amount'];
						$payment_type = $_POST['payment_type'];
						$assign_deadline = $_POST['assign_deadline'];
						$assign_attachments = $_POST['assign_attachments'];
						$assign_attachments = rtrim($assign_attachments,',');

						
						// create post object with the form values
						$my_cptpost_args = array(
													
						'post_title'    => $assign_title,
						'post_content'  => $assign_description,
						'post_status'   => 'pending',
						'post_type' => 'assignment'

						);

						// insert the post into the database
						$post_ins_id = wp_insert_post( $my_cptpost_args);
						//}

						//Send email to admin
	
						$to      = 'post@talentbasen.no';
						$subject = 'New Assignment has been registered on TalentBasen';
						$body    = 'New assignment with title "'.$assign_title.'" has been registered on TalentBasen.';

						wp_mail( $to, $subject, $body );

						//Set data to Post metas	
						update_post_meta( $post_ins_id, 'payment_type', $payment_type );
						update_post_meta( $post_ins_id, 'amount', $assign_amount );
						update_post_meta( $post_ins_id, 'deadline', $assign_deadline );
						update_post_meta( $post_ins_id, 'address', $assign_address );
						update_post_meta( $post_ins_id, 'phone', $assign_phone );
						update_post_meta( $post_ins_id, 'email', $assign_email );
						update_post_meta( $post_ins_id, 'email', $assign_email );
						update_post_meta( $post_ins_id, 'attachments',$assign_attachments);
						
						echo '<div class="alert alert-success"><strong>Assignment</strong> has been submitted. Waiting for approval.</div>';

					}

					$current_user = wp_get_current_user();
					//$test = wp_get_current_user();
					$user_id = $current_user->ID;
					$post_id = get_user_meta( $user_id ,'talent_post_id');
					$address = get_post_meta($post_id[0],'address');
					$phone = get_post_meta($post_id[0],'phone');
					$current_address = $address[0];
					$current_phone = $phone[0];
					$current_email =  $current_user->user_email;
				?>	
				<div class="row-fluid">
			 
					<div class="span12 f_col moudle agency_wrapper">
					
					<h2><i class="fa fa-check"></i>Add Assignment</h2>	
				
				<?php     
				$user = wp_get_current_user();
    			$current_role = $user->roles;
    			//echo  $current_role[0];
    			if( $current_role[0] =='agency'){
    				
    							
    			?>		


				<form class="panel-login agency-form telent_register_form " id="register_telent_form" action="" method="post"  >

				        <div class="row">

				            <div class="col-md-6" style="display:none;">
				                <input type="hidden" name="assign_address" value="<?php echo $current_address; ?>" placeholder="Address" required1="" >
				            </div>	
				            <div class="col-md-6" style="display:none;">
				                <input type="hidden" name="assign_phone" value="<?php echo $current_phone; ?>" placeholder="Phone" required1="" >
				            </div>
				            <div class="col-md-6" style="display:none;">
				                <input type="hidden" name="assign_email" placeholder="Email" value="<?php echo $current_email; ?>" required1="" >
				            </div>
							<div class="col-md-6">
							<label for="phone_number1">Title</label>
				                <input type="text" name="assign_title" placeholder="Title" required="">
				            </div>
							<div class="col-md-12">
							<label for="phone_number1">Description</label>
				                <textarea type="text" name="assign_description" placeholder="Description" required=""></textarea>
				            </div>
							 <div class="col-md-6">
								<label for="phone_number1">Payment Type</label>
									<select id="payment_type" name="payment_type" required1="">
										<option value=""> - Select a Payment Type- </option>
										<option value="voluntary"> Voluntary </option>
										<option value="paid">Paid</option>
									</select>
							</div>
							<div class="col-md-6" id="assign_amount" style="display:none;">
							<label for="phone_number1">Amount</label>
				                <input type="text" name="assign_amount" placeholder="Amount" required="" value="0">
				            </div>
				            <div class="col-md-6">
							<label for="phone_number1">Deadline</label>
				                <input id="deadline" type="text" name="assign_deadline" placeholder="dd.mm.yyyy" required="">
				            </div>
							 <div class="col-md-12">
							<label for="phone_number1">Attchments</label>
							<div id="assignment_attchment">Upload Picture</div>
							<textarea  title="Gallery images" type="text" class="hide upme-input  upme-edit-first_name" name="assign_attachments" id="gallery_images"></textarea>
								<?php echo do_shortcode('[arfaly id="4714"]'); ?>
				            </div>
				           			            	
				        </div>
				    

						<div class="form-group">
				        <div class="row">
				            <div class="col-sm-offset-4 col-sm-4">
				                 <input type="submit" name="submit_assignment" tabindex="4" class="form-control btn btn-register btn-pink" value="Add Assignment"> 
				            </div>
				        </div>
				    </div>

				</form>

				<?php
				}else{
					echo "<div class='not_agency_user_notice'>Only agency can add assignment. <a href='http://test.talentbasen.no/agency-registration/'>Register as Agency</a></div>";
				}
    			?>

				
				</div>
				</div>
			
	</div>			
	<script>
	jQuery("#deadline").mask('99.99.9999',{placeholder:"dd.mm.yyyy"});
	</script>			

<?php get_footer(); ?>