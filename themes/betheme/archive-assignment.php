<?php
/**
 * The main template file.
 *
 * @package Betheme
 * @author Muffin group
 * @link http://muffingroup.com
 */

get_header();

// Class
$blog_classes 	= array();
$section_class 	= array();


// Class | Layout
if( $_GET && key_exists('mfn-b', $_GET) ){
	$blog_layout = $_GET['mfn-b']; // demo
} else {
	$blog_layout = mfn_opts_get( 'blog-layout', 'classic' );
}
$blog_classes[] = $blog_layout;

// Layout | Masonry Tiles | Quick Fix
if( $blog_layout == 'masonry tiles' ){
	$blog_layout = 'masonry';
}


// Class | Columns
if( $_GET && key_exists('mfn-bc', $_GET) ){
	$blog_classes[] = 'col-'. $_GET['mfn-bc']; 

  // demo
} else {
	$blog_classes[] = 'col-'. mfn_opts_get( 'blog-columns', 3 );
}


if( $_GET && key_exists('mfn-bfw', $_GET) )	$section_class[] = 'full-width'; 

// demo
if( mfn_opts_get('blog-full-width') && ( $blog_layout == 'masonry' ) )	$section_class[] = 'full-width';
$section_class = implode( ' ', $section_class );


// Isotope
if( $blog_layout == 'masonry' ) $blog_classes[] = 'isotope';


// Ajax | load more
$load_more = mfn_opts_get('blog-load-more');


// Translate
$translate['filter'] 		  = mfn_opts_get('translate') ? mfn_opts_get('translate-filter','Filter by') : __('Filter by','betheme');
$translate['tags'] 			  = mfn_opts_get('translate') ? mfn_opts_get('translate-tags','Tags') : __('Tags','betheme');
$translate['authors'] 		= mfn_opts_get('translate') ? mfn_opts_get('translate-authors','Authors') : __('Authors','betheme');
$translate['all'] 			  = mfn_opts_get('translate') ? mfn_opts_get('translate-all','Show all') : __('Show all','betheme');
$translate['categories'] 	= mfn_opts_get('translate') ? mfn_opts_get('translate-categories','Categories') : __('Categories','betheme');
$translate['item-all'] 		= mfn_opts_get('translate') ? mfn_opts_get('translate-item-all','All') : __('All','betheme');
?>
<!-- #Content -->


<div class="archive_content assignment_container" id="Content">
	<div class="content_wrapper clearfix">
		<!-- .sections_group -->
		
<div class="sections_group assignment-left">
<a class="button  button_right button_theme" href="http://test.talentbasen.no/post-assignment/"><span class="button_icon"></span><span class="button_label">Add Assignment </span></a>
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">DATE</th>
      <th scope="col">Title</th>
      <th scope="col">Description</th>
      <th scope="col">Budget</th>
      <th scope="col">Contact Details</th>
      <th scope="col">Deadline</th>
	   <th scope="col">Feedback</th>
    </tr>
  </thead>
  <tbody>
    <?php
       $args = array(
            'posts_per_page' => 99,
            'post_type' => 'assignment',
            'orderby' => 'date',
            'order' => 'DESC',
            'ignore_sticky_posts' => 1,
            'paged' => $paged);
            
        $loop = new WP_Query($args);
        if ($loop->have_posts()) :
            while ($loop->have_posts()) : $loop->the_post();
                
                $post_ins_id = $post->ID;
                $post_title = get_the_title();
                $post_desc = get_the_content();
                //Get data
                $deadline = get_post_meta( $post_ins_id, 'deadline',true);
                $post_date = get_the_date( 'l F j, Y' );
                $payment_type = get_post_meta( $post_ins_id, 'payment_type');
                $address = get_post_meta( $post_ins_id, 'address');
                $phone = get_post_meta( $post_ins_id, 'phone');
                $email = get_post_meta( $post_ins_id, 'email');
                $amount = get_post_meta( $post_ins_id, 'amount');
				$feedback = get_post_meta( $post_ins_id, 'feedback',true);
				$attachments = get_post_meta( $post_ins_id, 'attachments',true);
				$attachments_arr = explode(',',$attachments);
                if(!empty($amount[0])){
                  $amount[0] = $amount[0];
                }
                else{
                  $amount[0] = 0;
                }

                if(!empty($address[0])){
                  $address[0] = $address[0];
                }
                else{
                  $address[0] = 'N/A';
                }

                if(!empty($phone[0])){
                  $phone[0] = $phone[0];
                }
                else{
                  $phone[0] = 'N/A';
                }

                if(!empty($email[0])){
                  $email[0] = $email[0];
                }
                else{
                  $email[0] = 'N/A';
                }


                ?>
                <tr>
                  <th scope="row"><?php echo $post_ins_id; ?></th>
                  
                  <td><?php echo $post_date; ?></td>
                  <td><?php echo $post_title;?></td>
                  <td><?php echo $post_desc;
					if($attachments_arr){
						echo '<div class="attach_div">';
						$n = 1;
						foreach($attachments_arr as $attach){
							echo '<a href="'.$attach.'" download>Attachment-'.$n.' </a>';
							$n++;
						}
						echo '</div>';
					}
				  ?></td>
                  <td><?php echo $amount[0]; ?></td>
                  
                 <?php $html1 = "<p class='email_main assignment_email'>".$email[0]."</p><p>".$address[0]."</p><p>".$phone[0]."</p><button id='show_contact_form' data-assignment='".$email[0]."' class='button button_right button_theme btn_contact_assignment' value='".$post_ins_id."' href=''>Contact</button>"; ?>

                  <td>
                    <?php echo do_shortcode('[membership level="1"] '.$html1.' [/membership]');?> 
                    <h5 style="display: none;" class="paid_member_notice">Only Paid Members can Contact <div class="member_level_notice"><a href="http://test.talentbasen.no/membership-account/membership-levels/">Buy Now</a></div></h5>
                  </td>

                  <td><?php if($deadline) echo $deadline; ?></td>
                  <td><?php if($feedback) echo $feedback; ?></td>
                  
                </tr>

                <?php
            endwhile;
        endif;
        wp_reset_postdata();
    ?>
    
  </tbody>
</table>
</div>	
<?php
if (isset( $_POST['submit_request'] )) {
$your_name = $_POST["your-name"];
$your_email = $_POST["your-email"];
$assignment_email = $_POST["assignment-email"];
$your_subject = $_POST["your-subject"];
$your_message = $_POST["your-message"];
$assignment_id = $_POST["assignment_id"];
$agency_id = get_the_author_id($assignment_id);

$user_id = get_current_user_id();

$agency_mail = get_user_meta($agency_id,'e-mail',true);
$talent_id = get_user_meta($user_id,'talent_post_id',true);
//$agency_e_mail = get_post_meta($assignment_id,'email',true);
$all_meta = get_post_meta($talent_id );
unset($all_meta['_edit_lock']);
unset($all_meta['_thumbnail_id']);
unset($all_meta['_edit_last']);
unset($all_meta['slide_template']);
unset($all_meta['_vc_post_settings']);
$html ='';
$html .='<p class="cv_container">';
foreach($all_meta as $meta_key => $meta_value){
	$meta_key = str_replace("_"," ",$meta_key);
	$meta_value = str_replace("_"," ",$meta_value);
	if($meta_value[0]){
//$html .= $meta_key''
		$html .= '<p class="talent_meta" style=""><span style="text-transform: capitalize; font-weight:bold; font-size:18px; color:#ED3900;" class="talent_key">'.$meta_key.'</span> : <span style="text-transform: capitalize; font-size:15px; max-width:50%;" class="talent_value">'.$meta_value[0].'</span></p>';
	}
}
$html .='</p>';

/* $html1 = '<p class="talent_meta" style=""><span style="text-transform: capitalize; font-weight:bold; font-size:18px; color:#ED3900;" class="talent_key">asdfasdfas asda </p>';
?>< pre><?php print_r($html1); ?></pre><?php */

$talent_cv = get_post_meta($talent_id,'talent_cv',true);
$talent_cv = rtrim($talent_cv,',');
$talent_cv = wp_make_link_relative( $talent_cv );
$attachments = array(ABSPATH .$talent_cv);
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= 'From: My Name'.$your_name. "\r\n";
apply_filters( 'wp_mail_content_type','text/html');
wp_mail( $agency_mail, $your_subject, $html, $headers, $attachments );

}
?>

<!-- The Modal -->
<div id="assignment_contact_form" class="modal">
  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
	<form action="" method="post" class="wpcf7-form" novalidate="novalidate"><input type="hidden" name="pum_form_popup_id" value="5041">
	<p><label> Your Name (required)<br>
		<span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span> </label></p>
	<p><label> Your Email (required)<br>
		<span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false"></span> </label></p>
	<p><label> Assignment Email (required)<br>
		<span class="wpcf7-form-control-wrap assignment-email"><input type="email" name="assignment-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" id="assign_email" aria-required="true" aria-invalid="false"></span> </label></p>
	<p><label> Subject<br>
		<span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span> </label></p>
	<p><label> Your Message<br>
		<span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="3" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea></span> </label></p>
		<input type="text" name="assignment_id" id="assignment_id" value="" class="wpcf7-form-control wpcf7-text" aria-invalid="false">
	<p><input type="submit" name="submit_request" value="Send" class="wpcf7-form-control wpcf7-submit"></p>
	</form>
	</div>
</div>
<script type="text/javascript">
  // Get the modal
var modal = document.getElementById('assignment_contact_form');

// Get the button that opens the modal
//var btn = document.getElementById("show_contact_form");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal 
/* btn.onclick = function() {
  modal.style.display = "block";
} */

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

jQuery( document ).ready(function() {

//if(jQuery(".email_main") !== null)
  if(jQuery(".email_main").length == 0) {
  //it doesn't exist  
    jQuery( ".paid_member_notice" ).show();
  }


jQuery( ".btn_contact_assignment" ).click(function() {
	var assignment_id = jQuery(this).val();
	console.log(assignment_id);
	jQuery("#assignment_id").val(assignment_id);
	modal.style.display = "block";
	//var Something = jQuery(this).closest('td .assignment_email').find('td:eq(1)').text();
	var assignment_email = jQuery(this).attr('data-assignment')

  });
});

</script>

<!--
  <div class="assignment-right">
    
    <?php //echo do_shortcode('[assignment-form-show]');?>
  </div>

		
		.four-columns - sidebar -->
		<?php get_sidebar( 'blog' ); ?>

	</div>
</div>

<?php get_footer(); ?>