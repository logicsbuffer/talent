<?php
/**
 * The main template file.
 *
 * @package Betheme
 * @author Muffin group
 * @link http://muffingroup.com
 */

get_header();

// Class
$blog_classes 	= array();
$section_class 	= array();


// Class | Layout
if( $_GET && key_exists('mfn-b', $_GET) ){
	$blog_layout = $_GET['mfn-b']; // demo
} else {
	$blog_layout = mfn_opts_get( 'blog-layout', 'classic' );
}
$blog_classes[] = $blog_layout;

// Layout | Masonry Tiles | Quick Fix
if( $blog_layout == 'masonry tiles' ){
	$blog_layout = 'masonry';
}


// Class | Columns
if( $_GET && key_exists('mfn-bc', $_GET) ){
	$blog_classes[] = 'col-'. $_GET['mfn-bc']; 

  // demo
} else {
	$blog_classes[] = 'col-'. mfn_opts_get( 'blog-columns', 3 );
}


if( $_GET && key_exists('mfn-bfw', $_GET) )	$section_class[] = 'full-width'; 

// demo
if( mfn_opts_get('blog-full-width') && ( $blog_layout == 'masonry' ) )	$section_class[] = 'full-width';
$section_class = implode( ' ', $section_class );


// Isotope
if( $blog_layout == 'masonry' ) $blog_classes[] = 'isotope';


// Ajax | load more
$load_more = mfn_opts_get('blog-load-more');


// Translate
$translate['filter'] 		  = mfn_opts_get('translate') ? mfn_opts_get('translate-filter','Filter by') : __('Filter by','betheme');
$translate['tags'] 			  = mfn_opts_get('translate') ? mfn_opts_get('translate-tags','Tags') : __('Tags','betheme');
$translate['authors'] 		= mfn_opts_get('translate') ? mfn_opts_get('translate-authors','Authors') : __('Authors','betheme');
$translate['all'] 			  = mfn_opts_get('translate') ? mfn_opts_get('translate-all','Show all') : __('Show all','betheme');
$translate['categories'] 	= mfn_opts_get('translate') ? mfn_opts_get('translate-categories','Categories') : __('Categories','betheme');
$translate['item-all'] 		= mfn_opts_get('translate') ? mfn_opts_get('translate-item-all','All') : __('All','betheme');
?>
<!-- #Content -->


<div class="archive_content assignment_container" id="Content">
	<div class="content_wrapper clearfix">
		<!-- .sections_group -->
		
<div class="sections_group assignment-left">
<a class="button  button_right button_theme" href="http://test.talentbasen.no/post-assignment/"><span class="button_icon"></span><span class="button_label">Add Assignment </span></a>
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">DATE</th>
      <th scope="col">Title</th>
      <th scope="col">Description</th>
      <th scope="col">Budget</th>
      <th scope="col">Contact Details</th>
      <th scope="col">Deadline</th>
      <th scope="col">Feedback</th>
    </tr>
  </thead>
  <tbody>
    <?php
       $args = array(
            'posts_per_page' => 99,
            'post_type' => 'assignment',
            'orderby' => 'date',
            'order' => 'ASC',
            'ignore_sticky_posts' => 1,
            'paged' => $paged);
            
        $loop = new WP_Query($args);
        if ($loop->have_posts()) :
            while ($loop->have_posts()) : $loop->the_post();
                
                $post_ins_id = $post->ID;
                $post_title = get_the_title();
                $post_desc = get_the_content();
                //Get data
                $deadline = get_post_meta( $post_ins_id, 'deadline');
                $feedback = get_post_meta( $post_ins_id, 'feedback');
                $post_date = get_the_date( 'l F j, Y' );
                $payment_type = get_post_meta( $post_ins_id, 'payment_type');
                $address = get_post_meta( $post_ins_id, 'address');
                $phone = get_post_meta( $post_ins_id, 'phone');
                $email = get_post_meta( $post_ins_id, 'email');
                $amount = get_post_meta( $post_ins_id, 'amount');

                if(!empty($amount[0])){
                  $amount[0] = $amount[0];
                }
                else{
                  $amount[0] = 0;
                }

                if(!empty($address[0])){
                  $address[0] = $address[0];
                }
                else{
                  $address[0] = 'N/A';
                }

                if(!empty($phone[0])){
                  $phone[0] = $phone[0];
                }
                else{
                  $phone[0] = 'N/A';
                }

                if(!empty($email[0])){
                  $email[0] = $email[0];
                }
                else{
                  $email[0] = 'N/A';
                }


                ?>
                <tr>
                  <th scope="row"><?php echo $post_ins_id; ?></th>
                  
                  <td><?php echo $post_date; ?></td>
                  <td><?php echo $post_title;?></td>
                  <td><?php echo $post_desc;?></td>
                  <td><?php echo $amount[0]; ?></td>
                  
                 <?php $html1 = "<p class='email_main assignment_email'>".$email[0]."</p><p>".$address[0]."</p><p>".$phone[0]."</p><a data-assignment='".$email[0]."' class='button button_right button_theme btn_contact_assignment' href=''><span class='button_label'> Contact </span></a>"; ?>

                  <td>
                    <?php echo do_shortcode('[membership level="1"] '.$html1.' [/membership]');?> 
                    <h5 style="display: none;" class="paid_member_notice">Only Paid Members can Contact</h5>
                    <h5 style="" class="paid_member_notice">Buy Now</h5>
                  </td>

                  <td><?php echo $deadline[0]; ?></td>
                  <td><?php echo $feedback[0]; ?></td>
                  
                </tr>

                <?php
            endwhile;
        endif;
        wp_reset_postdata();
    ?>
    
  </tbody>
</table>
</div>	

<script type="text/javascript">
  
jQuery( document ).ready(function() {

//if(jQuery(".email_main") !== null)
  if(jQuery(".email_main").length == 0) {
  //it doesn't exist  
    jQuery( ".paid_member_notice" ).show();
  }


jQuery( ".btn_contact_assignment" ).click(function() {

//var Something = jQuery(this).closest('td .assignment_email').find('td:eq(1)').text();
var assignment_email = jQuery(this).attr('data-assignment')
console.log(assignment_email);
jQuery("#assign_email").val(assignment_email);

  });
});

</script>

<!--
  <div class="assignment-right">
    
    <?php //echo do_shortcode('[assignment-form-show]');?>
  </div>

		
		.four-columns - sidebar -->
		<?php get_sidebar( 'blog' ); ?>

	</div>
</div>

<?php get_footer(); ?>