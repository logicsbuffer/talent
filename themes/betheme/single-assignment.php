<?php
/**
* This loop data included "CPT Post Grid View & CPT Post Slider " widgets only
* If you want to customize this loop data
* Copy this file and add your theme root folder with same name(widget-loop.php)
*/

get_header();

	
	$post_ins_id = $post->ID;
		
	$img_url = get_the_post_thumbnail_url($post_ins_id, large);	
	
	if($img_url == NULL){
		$img_url = get_template_directory_uri().'/images/actor-avatar.png';	
	}

	$tel_title = get_the_title();
	$tel_age = get_post_meta( $post_ins_id, 'age');
	$tel_gender = get_post_meta( $post_ins_id, 'gender' );
	$tel_height = get_post_meta( $post_ins_id, 'height');
	$tel_weight = get_post_meta( $post_ins_id, 'weight');
	$tel_hair_color  = get_post_meta( $post_ins_id, 'hair_color' );
	$tel_dialect = get_post_meta( $post_ins_id, 'dialect');
	$tel_language= get_post_meta( $post_ins_id, 'language');
	$tel_experience  = get_post_meta( $post_ins_id, 'experience');
	$talent_audio = get_post_meta( $post_ins_id, 'talent_audio');
	$tel_experienc_description = get_post_meta( $post_ins_id, 'experience_description');
	$tel_video_url = get_post_meta( $post_ins_id, 'gallery_videos');
	$tel_gal_images = get_post_meta( $post_ins_id, 'gallery_images');
	$phone = get_post_meta( $post_ins_id, 'phone' );
	$email = get_post_meta( $post_ins_id, 'email');
	$address = get_post_meta( $post_ins_id, 'address');
	$assignment = get_post_meta( $post_ins_id, 'assignment');
	$product_terms = wp_get_object_terms( $post_ins_id,  'film_category' );
	$film_category = $product_terms[0]->name; ?> </pre><?php
?>
<div id="kaya-mid-content-wrapper">
   <div id="mid-content" class="site-content container">
      <div class="fullwidth mid-content">
         <!-- Middle content align -->
         
         <div class="post_single_page_content_wrapper item" id="816">
            <div class="actor_single_page row">
               <div class="single_page_image col col-lg-4">
                  <img src="<?php echo $img_url; ?>" alt="" class="">
               </div>
               <div class="single_page_details actors col col-lg-6">
                  <h2><?php echo $tel_title; ?></h2>
                  <div class="meta_fields_show actors">
                     <ul>
						<?php if($email[0]){ ?><li><strong>Email:</strong>  &nbsp; <span><?php echo $email[0]; ?></span> </li> <?php } ?>
						<?php if($phone[0]){ ?><li><strong>Phone:</strong>  &nbsp; <span><?php echo $phone[0]; ?></span> </li> <?php } ?>
						<?php if($address[0]){ ?><li><strong>Address:</strong>  &nbsp; <span><?php echo $address[0]; ?></span> </li> <?php } ?>
						<?php if($film_category){ ?><li><strong>Category:</strong>  &nbsp; <span><?php echo $film_category; ?></span> </li> <?php } ?>
						<?php if($assignment[0]){ ?><li><strong>Can Take Assignment:</strong>  &nbsp; <span><?php echo $assignment[0]; ?></span> </li> <?php } ?>
                        <?php if($tel_age[0]){ ?><li><strong>Age:</strong>  &nbsp; <span><?php echo $tel_age[0]; ?></span> </li> <?php } ?>
                        <?php if($tel_gender[0]){ ?><li><strong>Gender:</strong>  &nbsp; <span><?php echo $tel_gender[0]; ?></span> </li> <?php } ?>
                        <?php if($tel_height[0]){ ?><li><strong>Height:</strong>  &nbsp; <span><?php echo $tel_height[0]; ?></span> </li> <?php } ?>
                        <?php if($tel_weight[0]){ ?><li><strong>Weight:</strong>  &nbsp; <span><?php echo $tel_weight[0]; ?></span> </li> <?php } ?>
                        <?php if($tel_hair_color[0]){ ?><li><strong>Hair Color:</strong>  &nbsp; <span><?php echo $tel_hair_color[0]; ?></span> </li> <?php } ?>
                        <?php if($tel_dialect[0]){ ?><li><strong>Dealect:</strong>  &nbsp; <span><?php echo $tel_dialect[0]; ?></span> </li> <?php } ?>
                        <?php if($tel_language[0]){ ?><li><strong>Language:</strong>  &nbsp; <span><?php echo $tel_language[0]; ?></span> </li> <?php } ?>
                        <?php if($tel_experience[0]){ ?><li><strong>Experience Level:</strong>  &nbsp; <span><?php echo $tel_experience[0]; ?></span> </li> <?php } ?>
                        <?php if($talent_audio[0]){ ?><li><strong>Voiceover Audio:</strong>  &nbsp; <span><a href="<?php echo $talent_audio[0]; ?>">click to downlaod</a></span> </li> <?php } ?>
                     </ul>
                  </div>
               </div>
            </div>
            
         </div>

        <!-- HTML tabs --> 
			<script>
			function openCity(evt, cityName) {
			    var i, tabcontent, tablinks;
			    tabcontent = document.getElementsByClassName("tabcontent");
			    for (i = 0; i < tabcontent.length; i++) {
			        tabcontent[i].style.display = "none";
			    }
			    tablinks = document.getElementsByClassName("tablinks");
			    for (i = 0; i < tablinks.length; i++) {
			        tablinks[i].className = tablinks[i].className.replace(" active", "");
			    }
			    document.getElementById(cityName).style.display = "block";
			    evt.currentTarget.className += " active";
			}

			// Get the element with id="defaultOpen" and click on it
			//document.getElementById("defaultOpen").click();
			</script>

		   <div class="tab">
				  <button class="tablinks" onclick="openCity(event, 'Paris')">Images</button>
			  <button class="tablinks" onclick="openCity(event, 'London')" id="defaultOpen">Video</button>
				<button class="tablinks" onclick="openCity(event, 'Biography')" id="defaultOpen">Biography</button>

			</div>
		<div id="Biography" class="tabcontent">
           <h3>Biography</h3>
           <p>
            <?php echo $tel_experienc_description[0];  ?>
			</p>			

         </div>

			<div id="London" class="tabcontent">
			  <span onclick="this.parentElement.style.display='none'" class="topright">&times</span>
			  
				<?php $all_videos = explode( ',', $tel_video_url[0] ); ?>
							  
				<?php	
				if($tel_video_url != NULL) {
				    foreach($all_videos as $videos_val) {
				        ?>
				        <div style="display: inline-block;"><?php echo do_shortcode( '[youtube width=500 height=300] '.$videos_val.' [/youtube]' ); ?></div>
						<?php
				    }
				}
				?> 

			</div>

			<div id="Paris" class="tabcontent">
			  <span onclick="this.parentElement.style.display='none'" class="topright">&times</span>
			  
			  <?php $all_images = explode( ',', $tel_gal_images[0] ); ?>
			  
			  <?php	
				if($tel_gal_images != NULL) {
			        foreach($all_images as $images_val) {
						 $images_val_wo = strip_tags($images_val);
			            ?>
			            <img style="width: 300px;" src="<?php echo $images_val_wo; ?>"/>
			  			<?php
			        }
			    }
			   ?> 
			</div>

        <!-- HTML tabs End --> 



        
      </div>
   </div>
   <!-- End Middle content align -->
</div>

<?php get_footer(); ?>