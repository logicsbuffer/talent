<?php
/**
 * Template Name: Vendor Registration
 * Description: A Page Template that display portfolio items.
 *
 * @package Betheme
 * @author Muffin Group
 */

get_header(); 

?>
<section class="pagetitlewrap" style='height:<?php echo $velocity_header_title_height; ?>px; background-image: url("<?php echo $velocity_htitle_bg; ?>"); '>


<div class="<?php echo $velocity_pagetitle_class ?> bgwithparallax" data-speed="<?php echo $velocity_pagetitle_pspeed?>"  style="background:url(<?php echo $velocity_pagetitle_img ?>) 50% 0% repeat;<?php echo $velocity_pagetitle_style ?> ;background-size:100%"></div>
<div class="bgwithparallax_overlay" style="background-color:rgba(<?php echo $velocity_pagetitle_rgba.$velocity_pagetitle_opacity ?>);"></div>	
					<?php
if ( is_user_logged_in() ) {
	$edit_profile_link = site_url().'/edit-profile/';
	wp_redirect($edit_profile_link);
	exit;
}
	if (isset( $_POST['register_telent'] )) {
					$tel_agency_name = $_POST['agency_name']; 
					$vendors_category = $_POST['industry']; 
					$service_provided=$_POST['service_provided']; 
					$owner_name=$_POST['owner_name']; 
					$tel_contact_person=$_POST['agency_phone']; 
					$agency_address=$_POST['agency_address']; 
					$agency_city=$_POST['agency_city']; 
					$tel_agency_email=$_POST['agency_email']; 
					$tel_agency_pass=$_POST['agency_pass'];
					$agency_experience=$_POST['agency_experience'];
					$vendor_file = $_POST['vendor_file'];
					$gallery_videos = $_POST['gallery_videos'];
						// create post object with the form values
						
						$userdata = array(
						    'user_login' =>  $tel_agency_name,
						    'user_email' =>  $tel_agency_email,
						    'user_pass'  =>  md5($tel_agency_pass), // no plain password here!
						    'role'  =>  'vendor' // no plain password here!
						); 
						 
						//$user_id = wp_insert_user( $userdata );

					$user_status = wp_insert_user($userdata);
					$already_registered = $user_status->errors['existing_user_login'][0];
					$already_registered_user_email = $user_status->errors['existing_user_email'][0];
				if($already_registered){
					echo '<div class="alert alert-info text-center"><strong>'.$already_registered.'</strong></div>';
				}elseif($already_registered_user_email){
					echo '<div class="alert alert-info text-center"><strong>'.$already_registered_user_email.'</strong></div>';
				}else{
					echo '<div class="alert alert-success text-center"><strong>You</strong> have successfully registered on our website, Please check your email and click on the link we sent you to verify your email address.</div>';
					
					$my_cptpost_args = array(
												
					'post_title'    => $tel_agency_name,
					'post_content'  => 'NO content',
					'post_status'   => 'publish',
					'post_type' => 'vendor'

					);

					// insert the post into the database
					$post_ins_id = wp_insert_post( $my_cptpost_args, $wp_error);
					if($post_ins_id){
					// 
					wp_set_object_terms( $post_ins_id, $vendors_category,'vendors_category');

//Image
						$talent_image = $_POST['talent_image'];
						$tel_talent_image_arr = explode(',',$talent_image);
						$talent_image = $tel_talent_image_arr[0];
						if($talent_image){
							//Feature Image Code
							
							// Add Featured Image to Post
							$image_url        = $talent_image; // Define the image URL here
							$image_name       = 'telent.png';
							$upload_dir       = wp_upload_dir(); // Set upload folder
							$image_data       = file_get_contents($image_url); // Get image data
							$unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
							$filename         = basename( $unique_file_name ); // Create image file name

							// Check folder permission and define file location
							if( wp_mkdir_p( $upload_dir['path'] ) ) {
								$file = $upload_dir['path'] . '/' . $filename;
							} else {
								$file = $upload_dir['basedir'] . '/' . $filename;
							}

							// Create the image  file on the server
							file_put_contents( $file, $image_data );

							// Check image file type
							$wp_filetype = wp_check_filetype( $filename, null );

							// Set attachment data
							$attachment = array(
								'post_mime_type' => $wp_filetype['type'],
								'post_title'     => sanitize_file_name( $filename ),
								'post_content'   => '',
								'post_status'    => 'inherit'
							);

							// Create the attachment
							$attach_id = wp_insert_attachment( $attachment, $file, $post_ins_id );

							// Include image.php
							require_once(ABSPATH . 'wp-admin/includes/image.php');

							// Define attachment metadata
							$attach_data = wp_generate_attachment_metadata( $attach_id, $file );

							// Assign metadata to attachment
							wp_update_attachment_metadata( $attach_id, $attach_data );

							// And finally assign featured image to post
							set_post_thumbnail( $post_ins_id, $attach_id );
						}
						
						update_user_meta( $user_status, 'talent_post_id', $post_ins_id);
						
						//Set data to Post metas	
						update_post_meta( $post_ins_id, 'industry', $industry);
						update_post_meta( $post_ins_id, 'agency_experience', $agency_experience);
						update_post_meta( $post_ins_id, 'service_provided', $service_provided);
						update_post_meta( $post_ins_id, 'owner_name', $owner_name);
						update_post_meta( $post_ins_id, 'phone', $tel_contact_person);
						update_post_meta( $post_ins_id, 'agency_city', $agency_city);
						update_post_meta( $post_ins_id, 'agency_address', $agency_address);
						update_post_meta( $post_ins_id, 'vendor_email', $tel_agency_email);
						update_post_meta( $post_ins_id, 'gallery_videos', $gallery_videos);
						update_post_meta( $post_ins_id, 'vendor_file', $vendor_file);
						update_post_meta( $post_ins_id, 'talent_image', $tel_agency_email);
					}
				}
			}
			
?>
<!-- #Content -->
	<div id="content_wrapper" class="span12 agency_wrapper">
			
					<div class="row row-fluid ">
					<div class="col col-sm-offset-2 col-sm-8 f_col moudle">
					
					<h2><i class="fa fa-check"></i>I want to register my profile:</h2>
					
					<form class="panel-login agency-form" id="register_vendor_form" action="" method="post"  >

				        <div class="row">

				            <div class="col-md-6">
							<label for="phone_number1">Company</label>
				                <input type="text" name="agency_name" placeholder="Enter Company Name" required="" >
				            </div>
							<div class="col-md-6">
							<label for="phone_number1">Owner Name </label>
				                <input type="text" name="owner_name" placeholder="Enter Owner Name" required="">
				            </div>
							<div class="col-md-6">
				               <div id="div_experience">
								<label for="phone_number1">Experience </label>
				                <select name="agency_experience" id="agency_experience" required="">
				                    <option value="">- Select Experience -</option>
				                    <option value="less than 1 year">Less than 1 Year</option>
				                    <option value="1 to 5 years">1 - 5 Years  </option>
				                    <option value="5 to 10 years">5 - 10 Years </option>
				                    <option value="10+ years">10+ Years </option>
				                </select>
								</div>
				            </div>
							 <div class="col-md-6">
				               <div id="div_industry">
								<label for="phone_number1">Industry</label>
				                <select name="industry[]" id="talent_industry" required="">
				                    <option value="">- Select Industry -</option>
				                    <option value="Photograph">Photograph</option>
				                    <option value="Film Maker">Film Maker</option>
				                    <option value="Make-up Artist">Make-up Artist</option>
				                </select>
							</div>
				            </div>
							
				            <div class="col-md-6">
							<label for="phone_number1">Phone Number</label>
				                <input type="text" name="agency_phone" placeholder="Enter Phone Number" required="">
				            </div>
				         
					
				            <div class="col-md-6">
							<label for="phone_number1">City</label>
				                <input type="text" name="agency_city" placeholder="City code and City Name" required="">
				            </div>
							<div class="col-md-6">
							<label for="phone_number1">Phone Email</label>
				                <input type="text" name="agency_email" placeholder="Enter Email" required="">
				            </div>
				            <div class="col-md-6">
							<label for="phone_number1">Password</label>
				                <input type="password" name="agency_pass" placeholder="Enter Password"  required="">
				            </div>
							
				        </div>
				    

							    <div class="form-group">
				        <div class="row">
				            <div class="col-sm-offset-4 col-sm-4">
				                 <input type="submit" name="register_telent" tabindex="4" class="form-control btn btn-register btn-pink" value="Register"> 
				            </div>
				        </div>
				    </div>

					</form>
					
					</div>
					
					</div>
			
	</div>			

<?php get_footer(); ?>