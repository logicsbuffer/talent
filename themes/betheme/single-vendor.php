<?php
/**
* This loop data included "CPT Post Grid View & CPT Post Slider " widgets only
* If you want to customize this loop data
* Copy this file and add your theme root folder with same name(widget-loop.php)
*/

	get_header();
	$agency_title = get_the_title();
	$current_user_id = get_current_user_id();
	$user_info = get_userdata($current_user_id);
	$user_pass = $user_info->user_pass;
	$post_ins_id = get_user_meta( $current_user_id, 'talent_post_id', true );		
	$img_url = get_the_post_thumbnail_url($post_ins_id, large);	
	
	if($img_url == NULL){
		$img_url = get_template_directory_uri().'/images/agency-default.png';	
	}
	$current_user_id = get_current_user_id();
$post_ins_id = get_user_meta( $current_user_id, 'talent_post_id', true );
$user_info = get_userdata($current_user_id);
$user_pass = $user_info->user_pass;
$company_name = get_the_title($post_ins_id);
$post_ins = get_post_meta( $post_ins_id);
$profile_link = get_permalink($post_ins_id);
$post_ins_id = get_user_meta( $current_user_id, 'talent_post_id', true );
$industry = get_post_meta( $post_ins_id, 'industry',true);
$agency_experience = get_post_meta( $post_ins_id, 'agency_experience', true);
$service_provided = get_post_meta( $post_ins_id, 'service_provided', true);
$owner_name = get_post_meta( $post_ins_id, 'owner_name',true);
$phone = get_post_meta( $post_ins_id, 'vendor_phone', true);
$city = get_post_meta( $post_ins_id, 'agency_city', true);
$address = get_post_meta( $post_ins_id, 'agency_address', true);
$email = get_post_meta( $post_ins_id, 'vendor_email', true);
$gallery_videos = get_post_meta( $post_ins_id, 'gallery_videos', true);
$vendor_file = get_post_meta( $post_ins_id, 'vendor_file',true);
$talent_image = get_post_meta( $post_ins_id, 'talent_image', true);
$about_us = strip_tags($about_us_str[0]);

	
?>
<div id="kaya-mid-content-wrapper">
   <div id="mid-content" class="site-content container">
      <div class="fullwidth mid-content">
         <!-- Middle content align -->
         
         <div class="post_single_page_content_wrapper item" id="816">
            <div class="actor_single_page row">
               <div class="single_page_image col col-lg-4 agency_image">
                  <img src="<?php echo $img_url; ?>" alt="" class="">
               </div>
               <div class="single_page_details actors col col-lg-6">
                  <h2><?php echo $tel_agency_name;  
				  if($user_post_id == $post_ins_id){
				  ?>
<a href = <?php echo site_url().'/edit-profile/'; ?> ><div style="display: inline-block;float: right;">
<img src="<?php echo get_template_directory_uri().'/images/icons/edit.png'; ?>" style="width: 15px;"><span style="font-size: 13px;position: relative;top: -6px;left: 4px;">Edit Profile</span></div></a>
<?php } ?></h2>
				  <label class="cat_label">Vendor</label>
                  <div class="meta_fields_show actors">
                     <ul>
                        <?php 

                        if ( is_user_logged_in() ) {                
							$user = wp_get_current_user();
							$allowed_roles = array('administrator', 'agency');
							
							if( array_intersect($allowed_roles, $user->roles ) ) { 
?> 
							
							<?php if($email){ ?><li><strong>Email:</strong>  &nbsp; <span><?php echo $email; ?></span> </li> <?php } ?>
							<?php if($phone){ ?><li><strong>Phone:</strong>  &nbsp; <span><?php echo $phone; ?></span> </li> <?php } ?>	
							<?php }
						}else{
							$url = site_url();
							if ( wp_redirect( $url ) ) {
										exit;
									}
						}
                        
                        //Compare current user with viewing profile
                        global $current_user;
                        $viewing_title = get_the_title();

                        $current_username = $current_user->user_firstname." ".$current_user->user_lastname;
                        if($current_username == $viewing_title){ ?>
                        
                        <?php if($email){ ?><li><strong>Email:</strong>  &nbsp; <span><?php echo $email; ?></span> </li> <?php } ?>
                        <?php if($phone){ ?><li><strong>Phone:</strong>  &nbsp; <span><?php echo $phone; ?></span> </li> <?php } ?>
                         
                        <?php } ?>
                        <?php echo '<h2>'.$viewing_title.'</h2>'; ?>
                        <?php if($tel_agency_regno[0]){ ?><li><strong>Registration #:</strong>  &nbsp; <span><?php echo $tel_agency_regno[0]; ?></span> </li> <?php } ?>
                        <?php if($phone){ ?><li><strong>Contact Person:</strong>  &nbsp; <span><?php echo $tel_contact_person[0]; ?></span> </li> <?php } ?>
                        <?php if($city){ ?><li><strong>City:</strong>  &nbsp; <span><?php echo $city; ?></span> </li> <?php } ?>
                        <?php if($country){ ?><li><strong>Country:</strong>  &nbsp; <span><?php echo $country; ?></span> </li> <?php } ?>
                        <?php if($address){ ?><li><strong>Address:</strong>  &nbsp; <span><?php echo $address; ?></span> </li> <?php } ?>
                        <?php if($service_provided){ ?><li><strong>Service Provided:</strong>  &nbsp; <span><?php echo $service_provided;  ?></span> </li> <?php } ?> 
						<?php if($industry){ ?><li><strong>Industry:</strong>  &nbsp; <span><?php echo $industry;  ?></span> </li> <?php } ?>
						<?php if($owner_name){ ?><li><strong>Owner Name:</strong>  &nbsp; <span><?php echo $owner_name;  ?></span> </li> <?php } ?>
		
						<?php if($agency_experience){ ?><li><strong>Experience:</strong>  &nbsp; <span><?php echo $agency_experience;  ?></span> </li> <?php } ?>
                        
                     </ul>
                  </div>
               </div>
            </div>
            
         </div>
         
         <!-- HTML tabs --> 
         <script>
         function openCity(evt, cityName) {
             var i, tabcontent, tablinks;
             tabcontent = document.getElementsByClassName("tabcontent");
             for (i = 0; i < tabcontent.length; i++) {
                 tabcontent[i].style.display = "none";
             }
             tablinks = document.getElementsByClassName("tablinks");
             for (i = 0; i < tablinks.length; i++) {
                 tablinks[i].className = tablinks[i].className.replace(" active", "");
             }
             document.getElementById(cityName).style.display = "block";
             evt.currentTarget.className += " active";
         }

         // Get the element with id="defaultOpen" and click on it
        // document.getElementById("defaultOpen").click();
         </script>

         <div class="tab">
			   <button class="tablinks active" onclick="openCity(event, 'Biography')" id="defaultOpen">About us</button>
<!---        <button class="tablinks" onclick="openCity(event, 'Video')" id="">Video</button>
         <button class="tablinks" onclick="openCity(event, 'Gallery')">Gallery</button> -->
			   
         </div>
      		<div id="Biography" class="tabcontent active" style="display: block;">
                 <span onclick="this.parentElement.style.display='none'" class="topright">&times</span>
                 <?php echo $about_us; ?>		

         </div>
         <div id="Video" class="tabcontent" >
           <span onclick="this.parentElement.style.display='none'" class="topright">&times</span>
           
            <?php $all_videos = explode( ',', $talent_video_url[0] ); ?>
                       
            <?php 
            if($all_videos != NULL) {
                foreach($all_videos as $videos_val) {
					$videos_val_wo = strip_tags($videos_val);
                    ?>
                     <div style="display: inline-block;"><?php echo do_shortcode( '[youtube width=500 height=300] '.$videos_val_wo.' [/youtube]' ); ?></div>
                  <?php
                }
            }
            ?> 
            <?php if($custom_video_url[0]){?>
              <video src="<?php echo $custom_video_url[0];?>" width="500" height="300" controls>
                Your browser does not support the video tag.
              </video> 
            <?php } ?>
         </div>

         <div id="Gallery" class="tabcontent">
           <span onclick="this.parentElement.style.display='none'" class="topright">&times</span>
           <?php 
		   $all_images = explode(',', $talent_image[0] );
            if($all_images != NULL) {
				
				
                 foreach($all_images as $images_val) {
                     $images_val_wo = strip_tags($images_val);
					?>
                     <img style="width: 300px;" src="<?php echo $images_val_wo; ?>"/>
					<?php
                 }
             }
            ?> 
         </div>

        <!-- HTML tabs End -->          
      </div>
   </div>
   <!-- End Middle content align -->
</div>

<?php get_footer(); ?>