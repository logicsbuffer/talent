<?php
/**
 * Template Name: Agency Edit ptofile
 * Description: A Page Template that display portfolio items.
 *
 * @package Betheme
 * @author Muffin Group
 */

get_header(); 

?>
					
					<?php
					
				if (isset( $_POST[register_telent] )) {
					$current_user_id = get_current_user_id();
					$user_info = get_userdata($current_user_id);
					$user_pass = $user_info->user_pass;
					$post_ins_id = get_user_meta( $current_user_id, 'talent_post_id', true );
						$tel_agency_name = $_POST['agency_name'];
						$tel_agency_regno = $_POST['agency_regno'];
						$tel_agency_established = $_POST['agency_established'];
						$tel_agency_staff = $_POST['agency_staff'];
						$tel_contact_person = $_POST['contact_person'];
						$tel_agency_phone = $_POST['agency_phone'];
						$tel_agency_email = $_POST['agency_email'];
						$tel_agency_country = $_POST['agency_country'];
						$tel_agency_city = $_POST['agency_city'];
						$tel_agency_address = $_POST['agency_address'];
						$tel_agency_pass = $_POST['agency_pass'];
						$about_us = $_POST['about_us'];
						// create post object with the form values
						
						

						$userdata = array(
						    'user_login' =>  $tel_agency_name,
						    'user_email' =>  $tel_agency_email,
						    'user_pass'  =>  md5($tel_agency_pass), // no plain password here!
						    'role'  =>  'agency' // no plain password here!
						); 
						 
						//$user_id = wp_insert_user( $userdata );
					$user_status = wp_insert_user($userdata);
				
					update_user_meta( $user_status, 'talent_post_id', $post_ins_id );
					echo '<div class="alert alert-success text-center"><strong>Your</strong> profile has been successfully Updated.</div>';
					/* $my_cptpost_args = array(
													
						'post_title'    => $tel_agency_name,
						'post_content'  => $tel_agency_address,
						'post_status'   => 'publish',
						'post_type' => 'agency'

						);

						// insert the post into the database
						$post_ins_id = wp_insert_post( $my_cptpost_args, $wp_error); */
						//}

						//Set data to Post metas	
						update_post_meta( $post_ins_id, 'agency_name', $tel_agency_name );
						update_post_meta( $post_ins_id, 'agency_reg_no', $tel_agency_regno );
						update_post_meta( $post_ins_id, 'agency_established', $tel_agency_established );
						update_post_meta( $post_ins_id, 'staff', $tel_agency_staff );
						update_post_meta( $post_ins_id, 'tel_contact_person', $tel_contact_person );
						update_post_meta( $post_ins_id, 'phone_no', $tel_agency_phone );
						update_post_meta( $post_ins_id, 'e-mail', $tel_agency_email );
						update_post_meta( $post_ins_id, 'agency_country', $tel_agency_country );
						update_post_meta( $post_ins_id, 'agency_city', $tel_agency_city );
						update_post_meta( $post_ins_id, 'address', $tel_agency_address );
						update_post_meta( $post_ins_id, 'about_us', $about_us );
				}
			
			$current_user_id = get_current_user_id();
			$user_info = get_userdata($current_user_id);
			$user_pass = $user_info->user_pass;
			$post_ins_id = get_user_meta( $current_user_id, 'talent_post_id', true );
			$profile_link = get_permalink($post_ins_id);
			$tel_agency_name = get_post_meta( $post_ins_id, 'agency_name' );
			$tel_agency_regno = get_post_meta( $post_ins_id, 'agency_reg_no');
			$tel_agency_established = get_post_meta( $post_ins_id, 'agency_established' );
			$tel_agency_staff = get_post_meta( $post_ins_id, 'staff' );
			$tel_contact_person = get_post_meta( $post_ins_id, 'tel_contact_person');
			$tel_agency_phone= get_post_meta( $post_ins_id, 'phone_no'  );
			$tel_agency_email = get_post_meta( $post_ins_id, 'e-mail' );
			$tel_agency_country = get_post_meta( $post_ins_id, 'agency_country' );
			$tel_agency_city = get_post_meta( $post_ins_id, 'agency_city');
			$tel_agency_address = get_post_meta( $post_ins_id, 'address');
			$about_us_str = get_post_meta( $post_ins_id, 'about_us');
			$about_us = strip_tags($about_us_str[0]);

			
?>
<!-- #Content -->
	<div id="content_wrapper" class="span12">
			
					<div class="row-fluid">
				 
					<div class="span12 f_col moudle agency_wrapper">
					
			<h2><i class="fa fa-check"></i>I want to update my profile:</h2>
					
				<form class="panel-login agency-form" id="register_telent_form" action="" method="post"  >

				        <div class="row">

				            <div class="col-md-6">
								<label>Firmanavn / Oppdragsgiver</label>
				                <input type="text" name="agency_name" placeholder="Firmanavn / Oppdragsgiver" required="" value="<?php echo $tel_agency_name[0]; ?>" disabled>
				            </div>	
				            <div class="col-md-6">
								<label>Organisasjonsnummer</label>
				                <input type="text" name="agency_regno" placeholder="Organisasjonsnummer" value="<?php echo $tel_agency_regno[0]; ?>" required="" >
				            </div>
				            <div class="col-md-6">
								<label>Kontaktperson /Navn</label>
				                <input type="text" name="contact_person" placeholder="Kontaktperson /Navn" value="<?php echo $tel_contact_person[0]; ?>" required="">
				            </div>
				            <div class="col-md-6">
								<label>Telefon</label>
				                <input type="text" name="agency_phone" placeholder="Telefon" value="<?php echo $tel_agency_phone[0]; ?>" required="">
				            </div>
				            <div class="col-md-6">
								<label>Email</label>
				                <input type="text" name="agency_email" placeholder="Email" value="<?php echo $tel_agency_email[0]; ?>" required="">
				            </div>
				            <div class="col-md-6 hide">
								<label>Country</label>
				                <input type="text" name="agency_country" placeholder="Country" value="norway" class="" required="">
				            </div>
							 <div class="col-md-6">
							 	<label>Adresse</label>
				                <input type="text" name="agency_address" placeholder="Adresse" value="<?php echo $tel_agency_address[0]; ?>" required="">
				            </div>
				            <div class="col-md-6">
								<label>By/Stedsnavn</label>
				                <input type="text" name="agency_city" placeholder="City" value="<?php echo $tel_agency_city[0]; ?>" required="">
				            </div>
				            <div class="col-md-6">
								<label>Password</label>
				                <input type="password" name="agency_pass" placeholder="Passord" value="<?php echo $user_pass; ?>" required="">
				            </div>
							<div class="col-md-12">
								<label>Om oss</label>
				                <textarea name="about_us" placeholder="Om oss" style="z-index: auto; position: relative; line-height: 22px; font-size: 14px; transition: none 0s ease 0s; background: transparent !important;"><?php echo $about_us; ?></textarea>
				            </div>
				            	
				        </div>
				    

					<div class="form-group update_agency">
				        <div class="row">
				            <div class="col-sm-offset-3 col-sm-3">
				                 <input type="submit" name="register_telent" tabindex="4" class="form-control btn btn-register btn-pink" value="Update Profile"> 
				            </div>
							<div class="col-sm-3">
				                 <a href="<?php echo $profile_link; ?>"><input  type="button" name="register_telent" tabindex="4" class="view_profile form-control btn btn-register btn-pink" value="View Profile"></a>
				            </div>
				        </div>
				    </div>

				</form>
				
				</div>
				</div>
			
	</div>			

<?php get_footer(); ?>