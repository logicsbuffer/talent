<?php
/**
 * Template Name: Vendor Registration
 * Description: A Page Template that display portfolio items.
 *
 * @package Betheme
 * @author Muffin Group
 */

get_header(); 

?>
<section class="pagetitlewrap" style='height:<?php echo $velocity_header_title_height; ?>px; background-image: url("<?php echo $velocity_htitle_bg; ?>"); '>


<div class="<?php echo $velocity_pagetitle_class ?> bgwithparallax" data-speed="<?php echo $velocity_pagetitle_pspeed?>"  style="background:url(<?php echo $velocity_pagetitle_img ?>) 50% 0% repeat;<?php echo $velocity_pagetitle_style ?> ;background-size:100%"></div>
<div class="bgwithparallax_overlay" style="background-color:rgba(<?php echo $velocity_pagetitle_rgba.$velocity_pagetitle_opacity ?>);"></div>	
					<?php

	if (isset( $_POST['register_telent'] )) {
					$tel_agency_name = $_POST['agency_name']; 
					$vendors_category = $_POST['industry']; 
					$service_provided=$_POST['service_provided']; 
					$owner_name=$_POST['owner_name']; 
					$vendor_phone=$_POST['vendor_phone']; 
					$agency_address=$_POST['agency_address']; 
					$agency_city=$_POST['agency_city']; 
					$tel_agency_email=$_POST['agency_email']; 
					$tel_agency_pass=$_POST['agency_pass'];
					$agency_experience=$_POST['agency_experience'];
					$vendor_file = $_POST['vendor_file'];
					$gallery_videos = $_POST['gallery_videos'];
						// create post object with the form values
					
					
					$user_id = get_current_user_id();
					//$website = 'http://example.com';
					$user_data = wp_update_user( array(
						'ID' => $user_id, 
						'first_name' => $tel_agency_name,
						'user_pass' => $tel_agency_pass,
						'user_email' => $email ) );
					
	/* 				$already_registered = $user_status->errors['existing_user_login'][0];
					$already_registered_user_email = $user_status->errors['existing_user_email'][0];
				if($already_registered){
					echo '<div class="alert alert-info text-center"><strong>'.$already_registered.'</strong></div>';
				}elseif($already_registered_user_email){
					echo '<div class="alert alert-info text-center"><strong>'.$already_registered_user_email.'</strong></div>';
				}else{
					echo '<div class="alert alert-success text-center"><strong>You</strong> have successfully registered on our website, Please check your email and click on the link we sent you to verify your email address.</div>'; */
					$post_ins_id = get_user_meta( $user_id, 'talent_post_id', true );

					$my_post_tel = array(
						      'ID'           => $talent_post_id,
						      'post_title'   => $tel_agency_name
						 );

						// Update the post into the database
						wp_update_post( $my_post_tel );	

					if($post_ins_id){
					// 
					wp_set_object_terms( $post_ins_id, $vendors_category,'vendors_category');

//Image
						$talent_image = $_POST['talent_image'];
						$tel_talent_image_arr = explode(',',$talent_image);
						$talent_image = $tel_talent_image_arr[0];
						if($talent_image){
							//Feature Image Code
							
							// Add Featured Image to Post
							$image_url        = $talent_image; // Define the image URL here
							$image_name       = 'telent.png';
							$upload_dir       = wp_upload_dir(); // Set upload folder
							$image_data       = file_get_contents($image_url); // Get image data
							$unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
							$filename         = basename( $unique_file_name ); // Create image file name

							// Check folder permission and define file location
							if( wp_mkdir_p( $upload_dir['path'] ) ) {
								$file = $upload_dir['path'] . '/' . $filename;
							} else {
								$file = $upload_dir['basedir'] . '/' . $filename;
							}

							// Create the image  file on the server
							file_put_contents( $file, $image_data );

							// Check image file type
							$wp_filetype = wp_check_filetype( $filename, null );

							// Set attachment data
							$attachment = array(
								'post_mime_type' => $wp_filetype['type'],
								'post_title'     => sanitize_file_name( $filename ),
								'post_content'   => '',
								'post_status'    => 'inherit'
							);

							// Create the attachment
							$attach_id = wp_insert_attachment( $attachment, $file, $post_ins_id );

							// Include image.php
							require_once(ABSPATH . 'wp-admin/includes/image.php');

							// Define attachment metadata
							$attach_data = wp_generate_attachment_metadata( $attach_id, $file );

							// Assign metadata to attachment
							wp_update_attachment_metadata( $attach_id, $attach_data );

							// And finally assign featured image to post
							set_post_thumbnail( $post_ins_id, $attach_id );
						}
						
						update_user_meta( $user_status, 'talent_post_id', $post_ins_id);
						
						//Set data to Post metas	
						update_post_meta( $post_ins_id, 'industry', $industry);
						update_post_meta( $post_ins_id, 'agency_experience', $agency_experience);
						update_post_meta( $post_ins_id, 'service_provided', $service_provided);
						update_post_meta( $post_ins_id, 'owner_name', $owner_name);
						update_post_meta( $post_ins_id, 'vendor_phone', $vendor_phone);
						update_post_meta( $post_ins_id, 'agency_city', $agency_city);
						update_post_meta( $post_ins_id, 'agency_address', $agency_address);
						update_post_meta( $post_ins_id, 'vendor_email', $tel_agency_email);
						update_post_meta( $post_ins_id, 'gallery_videos', $gallery_videos);
						update_post_meta( $post_ins_id, 'vendor_file', $vendor_file);
						update_post_meta( $post_ins_id, 'talent_image', $tel_agency_email);
					}
				//}
			}
$current_user_id = get_current_user_id();
$post_ins_id = get_user_meta( $current_user_id, 'talent_post_id', true );
$user_info = get_userdata($current_user_id);
$user_pass = $user_info->user_pass;
$company_name = get_the_title($post_ins_id);
$post_ins = get_post_meta( $post_ins_id);
$profile_link = get_permalink($post_ins_id);
$post_ins_id = get_user_meta( $current_user_id, 'talent_post_id', true );
$industry = get_post_meta( $post_ins_id, 'industry',true);
$agency_experience = get_post_meta( $post_ins_id, 'agency_experience', true);
$service_provided = get_post_meta( $post_ins_id, 'service_provided', true);
$owner_name = get_post_meta( $post_ins_id, 'owner_name',true);
$vendor_phone = get_post_meta( $post_ins_id, 'vendor_phone', true);
$agency_city = get_post_meta( $post_ins_id, 'agency_city', true);
$agency_address = get_post_meta( $post_ins_id, 'agency_address', true);
$vendor_email = get_post_meta( $post_ins_id, 'vendor_email', true);
$gallery_videos = get_post_meta( $post_ins_id, 'gallery_videos', true);
$vendor_file = get_post_meta( $post_ins_id, 'vendor_file',true);
$talent_image = get_post_meta( $post_ins_id, 'talent_image', true);
?>
<!-- #Content -->
	<div id="content_wrapper" class="span12 agency_wrapper">
			
					<div class="row row-fluid ">
					<div class="col col-sm-offset-2 col-sm-8 f_col moudle ">
					
					<h2><i class="fa fa-check"></i>Edit profile:</h2>
					
					<form class="panel-login agency-form" id="register_vendor_form" action="" method="post"  >

				        <div class="row">

				            <div class="col-md-6">
							<label for="phone_number1">Company</label>
				                <input type="text" name="agency_name" value="<?php echo $company_name; ?>" placeholder="Enter Company Name" required="" >
				            </div>	
				            <div class="col-md-6">
				               <div id="div_industry">
								<label for="phone_number1">Industry</label>
				                <select name="industry[]" id="talent_industry" required="">
								<?php if($industry) { ?>
									<option value="<?php echo $industry; ?>">- <?php echo $industry; ?> -</option>
								<?php } ?>
				                    <option value="">- Select Industry -</option>
				                    <option value="Photograph">Photograph</option>
				                    <option value="Film Maker">Film Maker</option>
				                    <option value="Make-up Artist">Make-up Artist</option>
				                </select>
							</div>
				            </div>
				            <div class="col-md-6">
							<label for="phone_number1">Service provided</label>
				                <input type="text" name="service_provided" value="<?php echo $service_provided; ?>" placeholder="Service provided" required="">
				            </div>
							<div class="col-md-6">
				               <div id="div_experience">
								<label for="phone_number1">Experience </label>
				                <select name="agency_experience" id="agency_experience" required="">
									<?php if($agency_experience) { ?>
									<option value="<?php echo $agency_experience; ?>">- <?php echo $agency_experience; ?> -</option>
								<?php } ?>
				                    <option value="">- Select Experience -</option>
				                    <option value="less than 1 year">Less than 1 Year</option>
				                    <option value="1 to 5 years">1 - 5 Years  </option>
				                    <option value="5 to 10 years">5 - 10 Years </option>
				                    <option value="10+ years">10+ Years </option>
				                </select>
								</div>
				            </div>
							
							<div class="col-md-6">
							<label for="phone_number1">Owner Name </label>
				                <input type="text" name="owner_name" value="<?php echo $owner_name; ?>" placeholder="Enter Owner Name" required="">
				            </div>
				            <div class="col-md-6">
							<label for="phone_number1">Phone Number</label>
				                <input type="text" name="vendor_phone" value="<?php echo $vendor_phone; ?>" placeholder="Enter Phone Number" required="">
				            </div>
				           
							<div class="col-md-6">
							<label for="phone_number1">Address</label>
				                <input type="text" name="agency_address" value="<?php echo $agency_address; ?>" placeholder="Address" required="">
				            </div>
							<!--
				            <div class="col-md-6">
				                <input type="text" name="agency_country" placeholder="Country" value="norway" class="" required="">
				            </div>
							 -->
				            <div class="col-md-6">
							<label for="phone_number1">City</label>
				                <input type="text" name="agency_city" value="<?php echo $agency_city; ?>" placeholder="City code and City Name" required="">
				            </div>
							<div class="col-md-6">
							<label for="phone_number1">Phone Email</label>
				                <input type="text" name="agency_email" value="<?php echo $vendor_email; ?>" placeholder="Enter Email" required="">
				            </div>
				            <div class="col-md-6">
							<label for="phone_number1">Password</label>
				                <input type="password" name="agency_pass" value="<?php echo $user_pass; ?>" placeholder="Enter Password"  required="">
				            </div>
							<div class="col-md-12">
								<label for="phone_number1">Reference</label>
								<?php echo do_shortcode('[ajax-file-upload disallow_remove_button="1" on_success_set_input_value="#vendor_file" on_success_alert="Your file has been successfully uploaded !"]'); ?>
								<div class="click_here">click here to upload !</div>
								<textarea class="form-control hide" name="vendor_file" value="" id="vendor_file">value="<?php echo $vendor_file; ?>"</textarea>
				         
							</div>
						   <div id="link_check" class="col-md-12" style="text-align: left;">
							   <input type="checkbox" id="youtube_link_check" name="terms" required1="" value="true"><span style="">Did you have Youtube Or other Video links ?</span>
							</div>
							<div class="col-md-12" id="div_youtube_link" style="display:none;">
								<label for="phone_number1">Reference links</label>
							   <div class="mini-box">
							   <textarea class="form-control" name="gallery_videos" value="" Placeholder="Enter links comma seprated"><?php echo $gallery_videos; ?></textarea>
							   </div>							
							</div>
							<div class="col-md-12">
								<label for="phone_number1">Profile Picture</label>
								<?php echo do_shortcode('[ajax-file-upload set_image_source="img.profile-pic" disallow_remove_button="1" on_success_set_input_value="#talent_image" on_success_alert="Your image has been successfully uploaded !"]'); ?>
								<div class="click_here">click here to upload !</div>
								<textarea class="form-control hide" name="talent_image" value="" id="talent_image"><?php echo $talent_image; ?></textarea>
								<div class="show_upload_img">
								<img src="" class="profile-pic"/>
								</div>
				            </div>
				        </div>
				    

							    <div class="form-group">
				        <div class="row">
				            <div class="col-sm-offset-3 col-sm-3">
				                 <input type="submit" name="register_telent" tabindex="4" class="form-control btn btn-register btn-pink" value="Update Profile"> 
				            </div>
							<div class="col-sm-3">
				                 <a href="<?php echo $profile_link; ?>"><input  type="button" tabindex="4" class="view_profile form-control btn btn-register btn-pink" value="View Profile"></a>
				            </div>
				        </div>
				    </div>

					</form>
					
					</div>
					</div>
			
	</div>			

<?php get_footer(); ?>