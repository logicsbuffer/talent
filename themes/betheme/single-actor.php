<?php
/**
* This loop data included "CPT Post Grid View & CPT Post Slider " widgets only
* If you want to customize this loop data
* Copy this file and add your theme root folder with same name(widget-loop.php)
*/

get_header();

	
	$post_ins_id = $post->ID;
		
	$img_url = get_the_post_thumbnail_url($post_ins_id, large);	
	
	if($img_url == NULL){
		$img_url = get_template_directory_uri().'/images/actor-avatar.png';	
	}

	$act_title = get_the_title();
	$act_age = get_post_meta( $post_ins_id, 'age');
	$act_gender = get_post_meta( $post_ins_id, 'gender');
	$act_height = get_post_meta( $post_ins_id, 'height' );
	$act_weight = get_post_meta( $post_ins_id, 'weight');
	$actor_hair_color = get_post_meta( $post_ins_id, 'hair_color');
	$act_dialect = get_post_meta( $post_ins_id, 'dialect');
	$act_language = get_post_meta( $post_ins_id, 'language');
	$act_experience = get_post_meta( $post_ins_id, 'experience');
	$act_video_url = get_post_meta( $post_ins_id, 'gallery_videos');
	$act_gal_images = get_post_meta( $post_ins_id, 'gallery_images');
	$phone = get_post_meta( $post_ins_id, 'phone' );
	$email = get_post_meta( $post_ins_id, 'email');
	$address = get_post_meta( $post_ins_id, 'address');
	$assignment = get_post_meta( $post_ins_id, 'assignment');
	$custom_video_url = get_post_meta( $post_ins_id, 'video_custom');	
	$current_user_id = get_current_user_id();
	$user_post_id = get_user_meta( $current_user_id, 'talent_post_id', true );
	// Get Refrence List
	$tel_company_name = get_post_meta( $post_ins_id, 'company_name', true  );
	$tel_designation = get_post_meta( $post_ins_id, 'designation', true  );
	$tel_job_profile = get_post_meta( $post_ins_id, 'job_profile', true );
	$tel_job_duration_from = get_post_meta( $post_ins_id, 'job_duration_from', true  );
	$tel_job_duration_to = get_post_meta( $post_ins_id, 'job_duration_to', true );
	$tel_company_name_arr1 = explode('|', $tel_company_name);
	$tel_designation_arr1 = explode('|', $tel_designation);
	$tel_job_profile_arr1 = explode('|', $tel_job_profile);
	$tel_job_duration_from_arr1 = explode('|', $tel_job_duration_from);
	$tel_job_duration_to_arr1 = explode('|', $tel_job_duration_to);
	$tel_company_name_arr = array_filter($tel_company_name_arr1, function($value) { return $value !== ''; });
	$tel_designation_arr = array_filter($tel_designation_arr1, function($value) { return $value !== ''; });
	$tel_job_profile_arr = array_filter($tel_job_profile_arr1, function($value) { return $value !== ''; });
	$tel_job_duration_from_arr = array_filter($tel_job_duration_from_arr1, function($value) { return $value !== ''; });
	$tel_job_duration_to_arr = array_filter($tel_job_duration_to_arr1, function($value) { return $value !== ''; });

?>
<div id="kaya-mid-content-wrapper">
   <div id="mid-content" class="site-content container">
      <div class="fullwidth mid-content">
         <!-- Middle content align -->
         
         <div class="post_single_page_content_wrapper item" id="816">
            <div class="actor_single_page row">
               <div class="single_page_image col col-lg-4">
                  <img src="<?php echo $img_url; ?>" alt="" class="">
               </div>
               <div class="single_page_details actors col col-lg-6">
                  <h2><?php echo $act_title; 
				  if($user_post_id == $post_ins_id){
				  ?>
<a href = <?php echo site_url().'/edit-profile/'; ?> ><div style="display: inline-block;float: right;">
<img src="<?php echo get_template_directory_uri().'/images/icons/edit.png'; ?>" style="width: 15px;"><span style="font-size: 13px;position: relative;top: -6px;left: 4px;">Edit Profile</span></div></a>
<?php } ?>
</h2>
				  <label class="cat_label">Actor</label>
                  <div class="meta_fields_show actors">
                     <ul>
						<?php 

                      if ( is_user_logged_in() ) {                
                      $user = wp_get_current_user();
                      $allowed_roles = array('administrator', 'agency');
                        
                      if( array_intersect($allowed_roles, $user->roles ) ) { ?> 
                      <?php if($email[0]){ ?><li><strong>Email:</strong>  &nbsp; <span><?php echo $email[0]; ?></span> </li> <?php } ?>
					  <?php if($phone[0]){ ?><li><strong>Phone:</strong>  &nbsp; <span><?php echo $phone[0]; ?></span> </li> <?php } ?>
					  
					  <?php } ?>
                      <?php } ?>
                      <?php
                        //Compare current user with viewing profile
                        global $current_user;
                        $viewing_title = get_the_title();

                        $current_username = $current_user->user_firstname." ".$current_user->user_lastname;
                        if($current_username == $viewing_title){ ?>
                        
                        <?php if($email[0]){ ?><li><strong>Email:</strong>  &nbsp; <span><?php echo $email[0]; ?></span> </li> <?php } ?>
                        <?php if($phone[0]){ ?><li><strong>Phone:</strong>  &nbsp; <span><?php echo $phone[0]; ?></span> </li> <?php } ?>
                         
                        <?php } ?>	

						<?php if($address[0]){ ?><li><strong>City:</strong>  &nbsp; <span><?php echo $address[0]; ?></span> </li> <?php } ?>
						<?php if($assignment[0]){ ?><li><strong>Can Take Assignment:</strong>  &nbsp; <span><?php echo $assignment[0]; ?></span> </li> <?php } ?>
                        <?php if($act_age[0]){ ?><li><strong>Age:</strong>  &nbsp; <span><?php echo $act_age[0]; ?></span> </li> <?php } ?>
                        <?php if($act_gender[0]){ ?><li><strong>Gender:</strong>  &nbsp; <span><?php echo $act_gender[0]; ?></span> </li> <?php } ?>
                        <?php if($act_height[0]){ ?><li><strong>Height:</strong>  &nbsp; <span><?php echo $act_height[0]; ?></span> </li> <?php } ?>
                        <?php if($act_weight[0]){ ?><li><strong>Weight:</strong>  &nbsp; <span><?php echo $act_weight[0]; ?></span> </li> <?php } ?>
                        <?php if($actor_hair_color[0]){ ?><li><strong>Hair Color:</strong>  &nbsp; <span><?php echo $actor_hair_color[0]; ?></span> </li> <?php } ?>
                        <?php if($act_dialect[0]){ ?><li><strong>Dealect:</strong>  &nbsp; <span><?php echo $act_dialect[0]; ?></span> </li> <?php } ?>
                        <?php if($act_language[0]){ ?><li><strong>Language:</strong>  &nbsp; <span><?php echo $act_language[0]; ?></span> </li> <?php } ?>
                        <?php if($act_experience[0]){ ?><li><strong>Experience:</strong>  &nbsp; <span><?php echo $act_experience[0]; ?></span> </li> <?php } ?>
                     </ul>
					 
					 <div id="tel_refrence_list" class="refrence_list">
					<label for="" style="font-size: 20px;">Refrence list</label>
						<div class="refrence_list_company col-company">
						<label for="phone_number1">Company Name</label>
						<?php 
							foreach($tel_company_name_arr as $company_name){
							?>
								<div class="col-company-name">
									<span required1="" ><?php echo $company_name; ?></span>
								</div>
						<?php
							} 
						?>
						</div>
					<div class="refrence_list_company col-company">
						<label for="phone_number1">Designation</label>
						<?php 
						
							foreach($tel_designation_arr as $designation){
									?>
								<div class="col-designation">
								
					              <span ><?php echo $designation; ?></span>
					            </div>
						<?php
							} 
						?>
						</div>
						<div class="refrence_list_company col-company">
						<label for="phone_number1">Job Profile</label>
						<?php 
						
						foreach($tel_job_profile_arr as $job_profile){
									?>
									<div class="col-job_profile">
								
					                <span ><?php echo $job_profile; ?></span>
					            </div>
						<?php
							} 
						?>
						</div>
						<div class="refrence_list_company col-company">
						<label for="phone_number1">Duration</label>
						<?php 
						
							foreach($tel_job_duration_from_arr as $job_duration_from){
									?>
									<div class="col-md-from">
										
						                <span><?php echo $job_duration_from; ?></span>
						            </div>
						<?php
							} 
						?>
						</div>
						<div class="refrence_list_company col-company">
						<label for="phone_number1">Duration To</label>
						<?php 
						
							foreach($tel_job_duration_to_arr as $job_duration_to){
									?>
									<div class="col-to">
						                <span ><?php echo $job_duration_to; ?></span>
						            </div>
						<?php
							} 
						?>
						</div>
					</div>
					 
					</div>
                  </div>
               </div>
            </div>
            
         </div>

        <!-- HTML tabs --> 
			<script>
			function openCity(evt, cityName) {
			    var i, tabcontent, tablinks;
			    tabcontent = document.getElementsByClassName("tabcontent");
			    for (i = 0; i < tabcontent.length; i++) {
			        tabcontent[i].style.display = "none";
			    }
			    tablinks = document.getElementsByClassName("tablinks");
			    for (i = 0; i < tablinks.length; i++) {
			        tablinks[i].className = tablinks[i].className.replace(" active", "");
			    }
			    document.getElementById(cityName).style.display = "block";
			    evt.currentTarget.className += " active";
			}

			// Get the element with id="defaultOpen" and click on it
			//document.getElementById("defaultOpen").click();
			</script>

		   <div class="tab">
			  <button class="tablinks active" onclick="openCity(event, 'bio')" id="defaultOpen">Bio</button>
			  <button class="tablinks" onclick="openCity(event, 'London')" >Video</button>
			  <button class="tablinks" onclick="openCity(event, 'Paris')">Images</button>		  
			</div>

			<div id="bio" class="tabcontent active" style="display: block;">
			  <span onclick="this.parentElement.style.display='none'" class="topright">&times</span>
			  
			  <?php echo get_post_field('post_content', $post_ins_id); ?>
			  
			  
			</div>

			<div id="London" class="tabcontent">
			  <span onclick="this.parentElement.style.display='none'" class="topright">&times</span>
			  
				<?php $all_videos = explode( ',', $act_video_url[0] ); ?>
							  
				<?php	
				if($act_video_url != NULL) {
				    foreach($all_videos as $videos_val) {
				        ?>
				        <div style="display: inline-block;"><?php echo do_shortcode( '[youtube width=500 height=300] '.$videos_val.' [/youtube]' ); ?></div>
						<?php
				    }
				}
				?>
				<?php if($custom_video_url[0]){?>
					<video src="<?php echo $custom_video_url[0];?>" width="500" height="300" controls>
					  Your browser does not support the video tag.
					</video> 
				<?php } ?>

			</div>

			<div id="Paris" class="tabcontent">
			  <span onclick="this.parentElement.style.display='none'" class="topright">&times</span>
			  
			  <?php $all_images = explode( ',', $act_gal_images[0] ); ?>
			  
			  <?php	
				if($act_gal_images != NULL) {
			        foreach($all_images as $images_val) {
						 $images_val_wo = strip_tags($images_val);
			            ?>
			            <img style="width: 300px;" src="<?php echo $images_val_wo; ?>"/>
			  			<?php
			        }
			    }
			   ?> 
			</div>



        <!-- HTML tabs End --> 



        
      </div>
   </div>
   <!-- End Middle content align -->
</div>

<?php get_footer(); ?>