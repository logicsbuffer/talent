<?php
/**
 * Template Name: Agency
 * Description: A Page Template that display portfolio items.
 *
 * @package Betheme
 * @author Muffin Group
 */

get_header(); 

?>
		
					<?php
if ( is_user_logged_in() ) {
	$edit_profile_link = site_url().'/edit-profile/';
	wp_redirect($edit_profile_link);
	exit;
}
				if (isset( $_POST[register_telent] )) {

						$tel_agency_name = $_POST['agency_name'];
						$tel_agency_regno = $_POST['agency_regno'];
						$tel_agency_established = $_POST['agency_established'];
						$tel_agency_staff = $_POST['agency_staff'];
						$tel_contact_person = $_POST['contact_person'];
						$tel_agency_phone = $_POST['agency_phone'];
						$tel_agency_email = $_POST['agency_email'];
						$tel_agency_country = $_POST['agency_country'];
						$tel_agency_city = $_POST['agency_city'];
						$tel_agency_address = $_POST['agency_address'];
						$tel_agency_pass = $_POST['agency_pass'];
						
						// create post object with the form values
						
						$userdata = array(
						    'user_login' =>  $tel_agency_name,
						    'user_email' =>  $tel_agency_email,
						    'user_pass'  =>  md5($tel_agency_pass), // no plain password here!
						    'role'  =>  'agency' // no plain password here!
						); 
						 
						//$user_id = wp_insert_user( $userdata );

					$user_status = wp_insert_user($userdata);
					$already_registered = $user_status->errors['existing_user_login'][0];
					$already_registered_user_email = $user_status->errors['existing_user_email'][0];
					if($already_registered){
						echo '<div class="alert alert-info text-center"><strong>'.$already_registered.'</strong></div>';
					}elseif($already_registered_user_email){
						echo '<div class="alert alert-info text-center"><strong>'.$already_registered_user_email.'</strong></div>';
					}else{
					update_user_meta( $user_status, 'talent_post_id', $post_ins_id );
					
					echo '<div class="alert alert-success text-center"><strong>You</strong> have successfully registered on our website, Please check your email and click on the link we sent you to verify your email address.</div>';
					$my_cptpost_args = array(
													
						'post_title'    => $tel_agency_name,
						'post_content'  => $tel_agency_address,
						'post_status'   => 'publish',
						'post_type' => 'agency'

						);

						// insert the post into the database
						$post_ins_id = wp_insert_post( $my_cptpost_args, $wp_error);
						//}

						update_user_meta( $user_status, 'talent_post_id', $post_ins_id );
						
						//Set data to Post metas	
						update_post_meta( $post_ins_id, 'agency_name', $tel_agency_name );
						update_post_meta( $post_ins_id, 'agency_reg_no', $tel_agency_regno );
						update_post_meta( $post_ins_id, 'agency_established', $tel_agency_established );
						update_post_meta( $post_ins_id, 'staff', $tel_agency_staff );
						update_post_meta( $post_ins_id, 'tel_contact_person', $tel_contact_person );
						update_post_meta( $post_ins_id, 'phone_no', $tel_agency_phone );
						update_post_meta( $post_ins_id, 'e-mail', $tel_agency_email );
						update_post_meta( $post_ins_id, 'agency_country', $tel_agency_country );
						update_post_meta( $post_ins_id, 'agency_city', $tel_agency_city );
						update_post_meta( $post_ins_id, 'address', $tel_agency_address );
					}
				}
			
?>
<!-- #Content -->
	<div id="content_wrapper" class="span12">
			
					<div class="row-fluid">
				 
					<div class="span12 f_col moudle agency_wrapper">
					
			<h2><i class="fa fa-check"></i>I want to register my profile:</h2>
					
				<form class="panel-login agency-form" id="register_telent_form" action="" method="post"  >

				        <div class="row">

				            <div class="col-md-6">
				                <input type="text" name="agency_name" placeholder="Firmanavn / Oppdragsgiver " required="" >
				            </div>	
				            <div class="col-md-6">
				                <input type="text" name="agency_regno" placeholder="Organisasjonsnummer">
				            </div>
				            <div class="col-md-6">
				                <input type="text" name="contact_person" placeholder="Kontaktperson /Navn" required="">
				            </div>
				            <div class="col-md-6">
				                <input type="text" name="agency_phone" placeholder="Telefon" required="">
				            </div>
				            <div class="col-md-6">
				                <input type="text" name="agency_email" placeholder="Email" required="">
				            </div>
				            <div class="col-md-6 hide">
				                <input type="text" name="agency_country" placeholder="Country" value="norway" class="" required="">
				            </div>
							 <div class="col-md-6">
				                <input type="text" name="agency_address" placeholder="Adresse" required="">
				            </div>
				            <div class="col-md-6">
				                <input type="text" name="agency_city" placeholder="By/Stedsnavn" required="">
				            </div>
				            <div class="col-md-6">
				                <input type="password" name="agency_pass" placeholder="Passord"  required="">
				            </div>
				            	
				        </div>
				    

							    <div class="form-group">
				        <div class="row">
				            <div class="col-sm-offset-4 col-sm-4">
				                 <input type="submit" name="register_telent" tabindex="4" class="form-control btn btn-register btn-pink" value="Register"> 
				            </div>
				        </div>
				    </div>

				</form>
				
				</div>
				</div>
			
	</div>			

<?php get_footer(); ?>