<?php
/**
* This loop data included "CPT Post Grid View & CPT Post Slider " widgets only
* If you want to customize this loop data
* Copy this file and add your theme root folder with same name(widget-loop.php)
*/

get_header();

	$post_ins_id = $post->ID;
		
	$img_url = get_the_post_thumbnail_url($post_ins_id, large);	
	
	if($img_url == NULL){
		$img_url = get_template_directory_uri().'/images/male-avatar.png';	
	}

$music_title = get_the_title();
$music_dj_experience = get_post_meta( $post_ins_id, 'dj_experience');
$music_crew = get_post_meta( $post_ins_id, 'crew');
$music_video_url = get_post_meta( $post_ins_id, 'gallery_videos');
$music_gal_images = get_post_meta( $post_ins_id, 'gallery_images');
$phone = get_post_meta( $post_ins_id, 'phone' );
$email = get_post_meta( $post_ins_id, 'email');
$address = get_post_meta( $post_ins_id, 'address');
$assignment = get_post_meta( $post_ins_id, 'assignment');
$music_experience = get_post_meta( $post_ins_id, 'music_experience', $dj_experience );
$music_description = get_post_meta( $post_ins_id, 'music_experince_description', $dj_description );
$music_categoies = wp_get_object_terms( $post_ins_id,'music_category');
$video_custom = get_post_meta( $post_ins_id, 'video_custom');

$music_category = $music_categoies[0]->name;
$band_category = $music_categoies[1]->name;
if($music_category == 'band'){
$music_band = get_post_meta( $post_ins_id, 'band_type');
$music_genre = get_post_meta( $post_ins_id, 'genre' );
$music_repertoire = get_post_meta( $post_ins_id, 'repertoire');
$music_founded = get_post_meta( $post_ins_id, 'founded');
}
elseif($music_category == 'instrumentals'){
$type_instrument = get_post_meta( $post_ins_id, 'type_instrument', $type_instrument );
$instrumental_experience = get_post_meta( $post_ins_id, 'instrumental_experience', $instrumental_experience );
$instrumental_description = get_post_meta( $post_ins_id, 'music_experince_description', $instrumental_description );
$music_description = $instrumental_description;
}


// Get Refrence List
	$tel_company_name = get_post_meta( $post_ins_id, 'company_name', true  );
	$tel_designation = get_post_meta( $post_ins_id, 'designation', true  );
	$tel_job_profile = get_post_meta( $post_ins_id, 'job_profile', true );
	$tel_job_duration_from = get_post_meta( $post_ins_id, 'job_duration_from', true  );
	$tel_job_duration_to = get_post_meta( $post_ins_id, 'job_duration_to', true );
	$tel_company_name_arr1 = explode('|', $tel_company_name);
	$tel_designation_arr1 = explode('|', $tel_designation);
	$tel_job_profile_arr1 = explode('|', $tel_job_profile);
	$tel_job_duration_from_arr1 = explode('|', $tel_job_duration_from);
	$tel_job_duration_to_arr1 = explode('|', $tel_job_duration_to);
	$tel_company_name_arr = array_filter($tel_company_name_arr1, function($value) { return $value !== ''; });
	$tel_designation_arr = array_filter($tel_designation_arr1, function($value) { return $value !== ''; });
	$tel_job_profile_arr = array_filter($tel_job_profile_arr1, function($value) { return $value !== ''; });
	$tel_job_duration_from_arr = array_filter($tel_job_duration_from_arr1, function($value) { return $value !== ''; });
	$tel_job_duration_to_arr = array_filter($tel_job_duration_to_arr1, function($value) { return $value !== ''; });
?>
<div id="kaya-mid-content-wrapper">
   <div id="mid-content" class="site-content container">
      <div class="fullwidth mid-content">
         <!-- Middle content align -->
         
         <div class="post_single_page_content_wrapper item" id="816">
            <div class="actor_single_page row">
               <div class="single_page_image col col-lg-4">
                  <img src="<?php echo $img_url; ?>" alt="" class="">
               </div>
               <div class="single_page_details actors col col-lg-6">
                  <h2><?php echo $music_title;
				   if($user_post_id == $post_ins_id){
				  ?>
<a href = <?php echo site_url().'/edit-profile/'; ?> ><div style="display: inline-block;float: right;">
<img src="<?php echo get_template_directory_uri().'/images/icons/edit.png'; ?>" style="width: 15px;"><span style="font-size: 13px;position: relative;top: -6px;left: 4px;">Edit Profile</span></div></a>
<?php } ?></h2>
				  <label class="cat_label">Music</label>
                  <div class="meta_fields_show actors">
                     <ul>
					    <?php 

					    if ( is_user_logged_in() ) {    						
					    $user = wp_get_current_user();
						$allowed_roles = array('administrator', 'agency');
						
						if( array_intersect($allowed_roles, $user->roles ) ) { ?>	

							<?php if($email[0]){ ?><li><strong>Email:</strong>  &nbsp; <span><?php echo $email[0]; ?></span> </li> <?php } ?>
							<?php if($phone[0]){ ?><li><strong>Phone:</strong>  &nbsp; <span><?php echo $phone[0]; ?></span> </li> <?php } ?>
						<?php } ?>
						<?php } ?>
            <?php
            //Compare current user with viewing profile
            global $current_user;
            $viewing_title = get_the_title();

            $current_username = $current_user->user_firstname." ".$current_user->user_lastname;
            if($current_username == $viewing_title){ ?>
            
            <?php if($email[0]){ ?><li><strong>Email:</strong>  &nbsp; <span><?php echo $email[0]; ?></span> </li> <?php } ?>
            <?php if($phone[0]){ ?><li><strong>Phone:</strong>  &nbsp; <span><?php echo $phone[0]; ?></span> </li> <?php } ?>
             
            <?php } ?>
						

						<?php if($address[0]){ ?><li><strong>City:</strong>  &nbsp; <span><?php echo $address[0]; ?></span> </li> <?php } ?>
						<?php if($music_category){ ?><li><strong>Category:</strong>  &nbsp; <span><?php echo $music_category; ?></span> </li> <?php } ?>
						<?php if($assignment[0]){ ?><li><strong>Can Take Assignment:</strong>  &nbsp; <span><?php echo $assignment[0]; ?></span> </li> <?php } ?>
                        <?php if($music_band[0]){ ?><li><strong>Band:</strong>  &nbsp; <span><?php echo $music_band[0]; ?></span> </li> <?php } ?>
                        <?php if($music_genre[0]){ ?><li><strong>Genre:</strong>  &nbsp; <span><?php echo $music_genre[0]; ?></span> </li> <?php } ?>
                        <?php if($music_experience[0]){ ?><li><strong>Experience Level:</strong>  &nbsp; <span><?php echo $music_experience[0]; ?></span> </li> <?php } ?>
                        <?php if($music_dj_singer_x_solo_x_core_experience[0]){ ?><li><strong>Singer Solo Core Experience:</strong>  &nbsp; <span><?php echo $music_dj_singer_x_solo_x_core_experience[0]; ?></span> </li> <?php } ?>
                        <?php if($music_crew[0]){ ?><li><strong>Crew:</strong>  &nbsp; <span><?php echo $music_crew[0]; ?></span> </li> <?php } ?>
                        <?php if($music_repertoire[0]){ ?><li><strong>Repertoire:</strong>  &nbsp; <span><?php echo $music_repertoire[0]; ?></span> </li> <?php } ?>
                        <?php if($music_founded[0]){ ?><li><strong>Founded:</strong>  &nbsp; <span><?php echo $music_founded[0]; ?></span> </li> <?php } ?>
                        <?php if($type_instrument[0]){ ?><li><strong>Instruments Type:</strong>  &nbsp; <span><?php echo $type_instrument[0]; ?></span> </li> <?php } ?>
                        <?php if($instrumental_experience[0]){ ?><li><strong>Played Number of Years:</strong>  &nbsp; <span><?php echo $instrumental_experience[0]; ?></span> </li> <?php } ?>
                     </ul>
					<div id="tel_refrence_list" class="refrence_list">
					<label for="" style="font-size: 20px;">Refrence list</label>
						<div class="refrence_list_company col-company">
						<label for="phone_number1">Company Name</label>
						<?php 
							foreach($tel_company_name_arr as $company_name){
							?>
								<div class="col-company-name">
									<span required1="" ><?php echo $company_name; ?></span>
								</div>
						<?php
							} 
						?>
						</div>
					<div class="refrence_list_company col-company">
						<label for="phone_number1">Designation</label>
						<?php 
						
							foreach($tel_designation_arr as $designation){
									?>
								<div class="col-designation">
								
					              <span ><?php echo $designation; ?></span>
					            </div>
						<?php
							} 
						?>
						</div>
						<div class="refrence_list_company col-company">
						<label for="phone_number1">Job Profile</label>
						<?php 
						
						foreach($tel_job_profile_arr as $job_profile){
									?>
									<div class="col-job_profile">
								
					                <span ><?php echo $job_profile; ?></span>
					            </div>
						<?php
							} 
						?>
						</div>
						<div class="refrence_list_company col-company">
						<label for="phone_number1">Duration</label>
						<?php 
						
							foreach($tel_job_duration_from_arr as $job_duration_from){
									?>
									<div class="col-md-from">
										
						                <span><?php echo $job_duration_from; ?></span>
						            </div>
						<?php
							} 
						?>
						</div>
						<div class="refrence_list_company col-company">
						<label for="phone_number1">Duration To</label>
						<?php 
						
							foreach($tel_job_duration_to_arr as $job_duration_to){
									?>
									<div class="col-to">
						                <span ><?php echo $job_duration_to; ?></span>
						            </div>
						<?php
							} 
						?>
						</div>
					</div>
					
					
					</div>
                  </div>
               </div>
            </div>
            
         </div>

         <!-- HTML tabs --> 
         <script>
         function openCity(evt, cityName) {
             var i, tabcontent, tablinks;
             tabcontent = document.getElementsByClassName("tabcontent");
             for (i = 0; i < tabcontent.length; i++) {
                 tabcontent[i].style.display = "none";
             }
             tablinks = document.getElementsByClassName("tablinks");
             for (i = 0; i < tablinks.length; i++) {
                 tablinks[i].className = tablinks[i].className.replace(" active", "");
             }
             document.getElementById(cityName).style.display = "block";
             evt.currentTarget.className += " active";
         }

         // Get the element with id="defaultOpen" and click on it
       //  document.getElementById("defaultOpen").click();
         </script>

         <div class="tab">
           <button class="tablinks" onclick="openCity(event, 'bio')" id="defaultOpen">Bio</button>
           <button class="tablinks" onclick="openCity(event, 'London')" id="defaultOpen">Video</button>
           <button class="tablinks" onclick="openCity(event, 'Paris')">Images</button>
         </div>

         <div id="bio" class="tabcontent active" style="display: block;">
          <span onclick="this.parentElement.style.display='none'" class="topright">&times</span>
          
          <?php echo get_post_field('post_content', $post_ins_id); ?>
          
          
        </div>
         <div id="London" class="tabcontent">
           <span onclick="this.parentElement.style.display='none'" class="topright">&times</span>
           <h3>Video</h3>
           
            <?php $all_videos = explode( ',', $music_video_url[0] ); ?>
                       
            <?php 
            if($music_video_url != NULL) {
                foreach($all_videos as $videos_val) {
                    ?>
                     <div style="display: inline-block;"><?php echo do_shortcode( '[youtube width=500 height=300] '.$videos_val.' [/youtube]' ); ?></div>
                  <?php
                }
            }
			if($video_custom[0]) {
				$all_video_custom = explode( ',', $video_custom[0] );
				    foreach($all_video_custom as $video_custom1) { ?>
				        <div style="display: inline-block;"><video width="320" height="240" controls><source src="<?php echo $video_custom1; ?>"> Your browser does not support the video tag.</video></div>
						<?php
				    }
				}
            ?> 

         </div>

         <div id="Paris" class="tabcontent">
           <span onclick="this.parentElement.style.display='none'" class="topright">&times</span>
           <h3>Images</h3>
           <?php $all_images = explode( ',', $music_gal_images[0] ); ?>
           
           <?php  
            if($music_gal_images != NULL) {
                 foreach($all_images as $images_val) {
					 $images_val = strip_tags($images_val);
                     ?>
                     <img style="width: 300px;" src="<?php echo $images_val; ?>"/>
                  <?php
                 }
             }
            ?> 
         </div>

        <!-- HTML tabs End -->           
      </div>
   </div>
   <!-- End Middle content align -->
</div>

<?php get_footer(); ?>