<?php
/**
 * Template Name: Register Telent
 * Description: A Page Template that display portfolio items.
 *
 * @package Betheme
 * @author Muffin Group
 */

get_header(); 

?>

<?php
if ( is_user_logged_in() ) {
	$edit_profile_link = site_url().'/edit-profile/';
	wp_redirect($edit_profile_link);
	exit;
}
			if (isset( $_POST['register_telent'] )) {
						$password = $_POST['password'];
						$firstname = $_POST['firstname'];
						$lastname = $_POST['lastname'];
						$city = $_POST['city'];
						$street = $_POST['street'];
						$email = $_POST['email'];
						$phone = $_POST['phone'];
						$slected_category = $_POST['category'];
						$category = $_POST['category'];
						$multi_category = $_POST['multi_category'];
						/* if($multi_category){
							$category = $multi_category;
						} */
						$dancer_gender =$_POST['dancer_gender'];
						$experience = $_POST['experience'];
						$assignment = $_POST['assignments'];
						$description = $_POST['description'];
						$talent_video_url = $_POST['video_url'];
						$address = $street.' '.$city;
						$full_name = $firstname.' '.$lastname;
						//Music
						$tel_music_category = $_POST['music_category'];
						$tel_band = $_POST['band'];
						$tel_genre = $_POST['genre'];
						$tel_crew = $_POST['crew'];
						$tel_dj_experience = $_POST['dj_experience'];
						$tel_singer_x_solo_exp = $_POST['singer_x_solo_exp'];
						$tel_instrumentals_type = $_POST['instrumentals_type'];
						$tel_played_number_of_years = $_POST['played_number_of_years'];
						$tel_music_gender = $_POST['music_gender'];

						//Actor
						$tel_actor_age = $_POST['actor_age'];
						$tel_actor_gender = $_POST['actor_gender'];
						$tel_actor_height = $_POST['actor_height'];
						$tel_actor_weight = $_POST['actor_weight'];
						$tel_actor_hair_color = $_POST['actor_hair_color'];
						$tel_actor_language = $_POST['actor_language'];
						$tel_actor_experience = $_POST['actor_experience'];
						$tel_email = $_POST['email'];

						//film_
					

						//Model
						$tel_model_name = $_POST['model_name'];
						$tel_model_age = $_POST['model_age'];
						$tel_model_gender = $_POST['model_gender'];
						$tel_model_height = $_POST['model_height'];
						$tel_model_weight = $_POST['model_weight'];
						$tel_model_exprience = $_POST['model_exprience'];
						
						//statist
						$tel_statist_age = $_POST['statist_age'];
						$tel_statist_gender = $_POST['statist_gender'];
						$tel_statist_height = $_POST['statist_height'];
						$statist_hair_color = $_POST['statist_hair_color'];
						$tel_statist_weight = $_POST['statist_weight'];
						$statist_dialect = $_POST['statist_dialect'];
						$statist_language = $_POST['statist_language'];
						$tel_statist_exprience = $_POST['statist_experience'];
						$film_category = $_POST['film_category'];
						
						//Voiceover
						$tel_voiceover_gender = $_POST['voiceover_gender'];
						$tel_voiceover_height = $_POST['voiceover_height'];
						$voiceover_hair_color = $_POST['voiceover_hair_color'];
						$tel_voiceover_weight = $_POST['voiceover_weight'];
						$voiceover_dialect = $_POST['voiceover_dialect'];
						$voiceover_language = $_POST['voiceover_language'];
						$tel_voiceover_exprience_arr = $_POST['voiceover_experience'];
						$tel_voiceover_exprience = implode(",",$tel_voiceover_exprience_arr);
						$voiceover_description = $_POST['voiceover_description'];
						$talent_audio_get = $_POST['talent_audio'];
						$talent_audio_arr = explode(',',$talent_audio_get);
						$talent_audio = $talent_audio_arr[0];
						$film_category = $_POST['film_category'];

						//Refrence List
						$tel_company_name = $_POST['company_name'];
						$tel_designation = $_POST['designation'];
						$tel_job_profile = $_POST['job_profile'];
						$tel_job_duration_from = $_POST['job_duration_from'];
						$tel_job_duration_to = $_POST['job_duration_to'];

						//$multi_category_count = count($multi_category);
					if($film_category == 'statist'){
							$tel_age = $tel_statist_age;
							$tel_gender = $tel_statist_gender;
							$tel_height = $tel_statist_height;
							$tel_weight = $tel_statist_weight;
							$tel_hair_color = $statist_hair_color;
							$tel_dialect = $statist_dialect;
							$tel_language = $statist_language;
 							$tel_experience = $tel_statist_exprience;
						}
						if($film_category == 'voiceover'){
							$tel_age = $tel_statist_age;
							$tel_gender = $tel_voiceover_gender;
							$tel_height = $tel_voiceover_height;
							$tel_weight = $tel_voiceover_weight;
							$tel_hair_color = $statist_hair_color;
							$tel_language = $statist_language;
 							$tel_experience = $tel_voiceover_exprience;
 							$tel_experienc_description = $voiceover_description;
 							$tel_audio = $talent_audio;
						}
						if($film_category == 'actor' || $film_category == 'model'){
							$category = $film_category;
						}
						if($category == 'sports'){
							$category = 'culture';
						}
						// Dancer
						$dance_experience = $_POST['dance_experience'];
						$dance_experience_description = $_POST['dance_experience_description'];
						$dancer_type = $_POST['dancer_type'];

						// create post object with the form values
						// $my_cptpost_args = array(
													
						// 'post_title'    => $firstname,
						// 'post_content'  => $description,
						// 'post_status'   => 'publish',
						// 'post_type' => 'dancer'

						// );

						//insert the post into the database
						// $post_ins_id = wp_insert_post( $my_cptpost_args);
					
					 $userdata1 = array(
						'user_login' =>  $firstname,
						'user_email' =>  $email,
						'user_pass'  =>  md5($password), // no plain password here!
						'role'  =>  'talent' // no plain password here!
					); 
					 
					$user_status = wp_insert_user($userdata1);
					$already_registered = $user_status->errors['existing_user_login'][0];
					$already_registered_user_email = $user_status->errors['existing_user_email'][0];
					if($already_registered){
						echo '<div class="alert alert-info text-center"><strong>'.$already_registered.'</strong></div>';
					}elseif($already_registered_user_email){
						echo '<div class="alert alert-info text-center"><strong>'.$already_registered_user_email.'</strong></div>';
					}else{
					echo '<div class="alert alert-success text-center"><strong>You</strong> have successfully registered on our website, Please check your email and click on the link we sent you to verify your email address.</div>';
					$my_post = array(
					  'post_title'    		 => $full_name,
					  'post_content' 		 => '',
					  'post_status'          => 'publish',				  
					  'post_type'            => $category
					 );
					// Insert the post into the database
					$post_ins_id = wp_insert_post( $my_post );
					update_user_meta( $user_status, 'talent_post_id', $post_ins_id );
					}
					
					if($post_ins_id){
						//update_user_meta( $post_ins_id, 'city', $city );
	
						//Image
						$talent_image = $_POST['talent_image'];
						$tel_talent_image_arr = explode(',',$talent_image);
						$talent_image = $tel_talent_image_arr[0];
						if($talent_image){
						//Feature Image Code
						
						// Add Featured Image to Post
						$image_url        = $talent_image; // Define the image URL here
						$image_name       = 'telent.png';
						$upload_dir       = wp_upload_dir(); // Set upload folder
						$image_data       = file_get_contents($image_url); // Get image data
						$unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
						$filename         = basename( $unique_file_name ); // Create image file name

						// Check folder permission and define file location
						if( wp_mkdir_p( $upload_dir['path'] ) ) {
						    $file = $upload_dir['path'] . '/' . $filename;
						} else {
						    $file = $upload_dir['basedir'] . '/' . $filename;
						}

						// Create the image  file on the server
						file_put_contents( $file, $image_data );

						// Check image file type
						$wp_filetype = wp_check_filetype( $filename, null );

						// Set attachment data
						$attachment = array(
						    'post_mime_type' => $wp_filetype['type'],
						    'post_title'     => sanitize_file_name( $filename ),
						    'post_content'   => '',
						    'post_status'    => 'inherit'
						);

						// Create the attachment
						$attach_id = wp_insert_attachment( $attachment, $file, $post_ins_id );

						// Include image.php
						require_once(ABSPATH . 'wp-admin/includes/image.php');

						// Define attachment metadata
						$attach_data = wp_generate_attachment_metadata( $attach_id, $file );

						// Assign metadata to attachment
						wp_update_attachment_metadata( $attach_id, $attach_data );

						// And finally assign featured image to post
						set_post_thumbnail( $post_ins_id, $attach_id );
						//Feature Image Code Ends	
						
						}
						//Default Image if no image Uplaoded
						else{
							
							$gender_check = '';	
							
							//Male
							if($tel_music_gender == 'male'){	
								$gender_check = 'male';
							 }
							if($tel_actor_gender == 'male'){	
								$gender_check = 'male';
							 }
							if($tel_model_gender == 'male'){	
								$gender_check = 'male';
							 }
							if($tel_statist_gender == 'male'){	
								$gender_check = 'male';
							 }
							if($tel_voiceover_gender == 'male'){	
								$gender_check = 'male';
							 }
							if($tel_gender == 'male'){	
								$gender_check = 'male';
							 }
							
							//Female
							if($tel_music_gender == 'female'){	
								$gender_check = 'female';
							 }
							if($tel_actor_gender == 'female'){	
								$gender_check = 'female';
							 }
							if($tel_model_gender == 'female'){	
								$gender_check = 'female';
							 }
							if($tel_statist_gender == 'female'){	
								$gender_check = 'female';
							 }
							if($tel_voiceover_gender == 'female'){	
								$gender_check = 'female';
							 }
							if($tel_gender == 'female'){	
								$gender_check = 'female';
							 }
							 
							 //Condition
							 if ($gender_check == 'male') {
							 	set_post_thumbnail( $post_ins_id, 4825 );
							 }
							 else if ($gender_check == 'female') {
							 	set_post_thumbnail( $post_ins_id, 4826 );
							 }	 	
						}
						//Default Image if no image Uplaoded Ends

						//Refrence
						update_post_meta( $post_ins_id, 'company_name', $tel_company_name );
						update_post_meta( $post_ins_id, 'designation', $tel_designation );
						update_post_meta( $post_ins_id, 'job_profile', $tel_job_profile );
						update_post_meta( $post_ins_id, 'job_duration_from', $tel_job_duration_from );
						update_post_meta( $post_ins_id, 'job_duration_to', $tel_job_duration_to );

						//Culture
						if($category == 'culture'){
						$tel_culture_expertise = $_POST['culture_expertise'];
						$tel_culture_experience = $_POST['culture_experience'];
						$tel_culture_experience_description = $_POST['culture_experience_description'];
						$tel_cul_gender = $_POST['cul_gender'];
						update_post_meta( $post_ins_id, 'gender', $tel_cul_gender );
						update_post_meta( $post_ins_id, 'gallery_images', $talent_image );
						update_post_meta( $post_ins_id, 'gallery_videos', $talent_video_url );
						update_post_meta( $post_ins_id, 'expertise', $tel_culture_expertise );
						update_post_meta( $post_ins_id, 'experience', $tel_culture_experience );
						update_post_meta( $post_ins_id, 'experince_description', $tel_culture_experience_description );
						update_post_meta( $post_ins_id, 'phone', $phone );
						update_post_meta( $post_ins_id, 'email', $email );
						update_post_meta( $post_ins_id, 'address', $address );
						update_post_meta( $post_ins_id, 'assignment', $assignment );
						}
						elseif($category == 'actor'){
						
						wp_set_object_terms( $post_ins_id, $category, 'actors_category');	
						update_post_meta( $post_ins_id, 'age', $tel_actor_age );
						update_post_meta( $post_ins_id, 'gender', $tel_actor_gender );
						update_post_meta( $post_ins_id, 'height', $tel_actor_height );
						update_post_meta( $post_ins_id, 'weight', $tel_actor_weight );
						update_post_meta( $post_ins_id, 'city', $city );
						update_post_meta( $post_ins_id, 'hair_color', $tel_actor_hair_color );
						update_post_meta( $post_ins_id, 'language', $tel_actor_language );
						update_post_meta( $post_ins_id, 'experience', $tel_actor_experience );
						update_post_meta( $post_ins_id, 'phone', $phone );
						update_post_meta( $post_ins_id, 'email', $email );
						update_post_meta( $post_ins_id, 'address', $address );
						update_post_meta( $post_ins_id, 'assignment', $assignment );
						update_post_meta( $post_ins_id, 'gallery_images', $talent_image );
						update_post_meta( $post_ins_id, 'gallery_videos', $talent_video_url );
						}
						 elseif($category == 'music'){
							$music_category = $_POST['music_category'];
							$band_category = $_POST['band_category'];
														
							update_post_meta( $post_ins_id, 'gender', $tel_music_gender );


							if($music_category == 'band'){
								$music_categoies = array($music_category,$band_category);
								$tel_band_genre = $_POST['band_genre'];
								$tel_founded = $_POST['founded'];
								$tel_repertoire = $_POST['repertoire'];
								$tel_crew = $_POST['crew'];
								
								update_post_meta( $post_ins_id, 'band_type', $band_category );
								update_post_meta( $post_ins_id, 'genre', $tel_band_genre );
								update_post_meta( $post_ins_id, 'crew', $tel_crew );
								update_post_meta( $post_ins_id, 'repertoire', $tel_repertoire );
								update_post_meta( $post_ins_id, 'founded', $tel_founded );
								update_post_meta( $post_ins_id, 'phone', $phone );
								update_post_meta( $post_ins_id, 'email', $email );
								update_post_meta( $post_ins_id, 'address', $address );
								update_post_meta( $post_ins_id, 'assignment', $assignment );
								update_post_meta( $post_ins_id, 'gallery_images', $talent_image );
						update_post_meta( $post_ins_id, 'gallery_videos', $talent_video_url );
								wp_set_object_terms( $post_ins_id, $music_categoies, 'music_category');
							}elseif($music_category == 'dj'){
								$dj_experience = $_POST['dj_experience'];
								$dj_description = $_POST['dj_description'];
								update_post_meta( $post_ins_id, 'experience', $dj_experience );
								update_post_meta( $post_ins_id, 'experince_description', $dj_description );
								update_post_meta( $post_ins_id, 'phone', $phone );
								update_post_meta( $post_ins_id, 'email', $email );
								update_post_meta( $post_ins_id, 'address', $address );
								update_post_meta( $post_ins_id, 'assignment', $assignment );
								update_post_meta( $post_ins_id, 'gallery_images', $talent_image );
								update_post_meta( $post_ins_id, 'gallery_videos', $talent_video_url );
								wp_set_object_terms( $post_ins_id, $music_category, 'music_category');
								
							}elseif($music_category == 'singer'){
								$singer_experience = $_POST['singer_experience'];
								$singer_description = $_POST['singe_description'];
								update_post_meta( $post_ins_id, 'experience', $singer_experience );
								update_post_meta( $post_ins_id, 'experince_description', $singer_description );
								update_post_meta( $post_ins_id, 'phone', $phone );
								update_post_meta( $post_ins_id, 'email', $email );
								update_post_meta( $post_ins_id, 'address', $address );
								update_post_meta( $post_ins_id, 'assignment', $assignment );
								update_post_meta( $post_ins_id, 'gallery_images', $talent_image );
								update_post_meta( $post_ins_id, 'gallery_videos', $talent_video_url );
								wp_set_object_terms( $post_ins_id, $music_category, 'music_category');
							}elseif($music_category == 'instrumentals'){
								$type_instrument = $_POST['type_instrument'];
								$instrumental_experience = $_POST['instrumental_experience'];
								$instrumental_description = $_POST['instrumental_description'];
								$tel_crew = $_POST['crew'];
								update_post_meta( $post_ins_id, 'type_instrument', $type_instrument );
								update_post_meta( $post_ins_id, 'instrumental_experience', $instrumental_experience );
								update_post_meta( $post_ins_id, 'experince_description', $instrumental_description );
								update_post_meta( $post_ins_id, 'phone', $phone );
								update_post_meta( $post_ins_id, 'email', $email );
								update_post_meta( $post_ins_id, 'address', $address );
								update_post_meta( $post_ins_id, 'assignment', $assignment );
								update_post_meta( $post_ins_id, 'gallery_images', $talent_image );
								update_post_meta( $post_ins_id, 'gallery_videos', $talent_video_url );
								wp_set_object_terms( $post_ins_id, $music_category, 'music_category');
							}
							//wp_set_object_terms( $post_ins_id, $band_category, 'music_category');

						}
						elseif($category == '22'){	
						}
						 elseif($category == 'film'){	
							update_post_meta( $post_ins_id, 'age', $tel_age );
							update_post_meta( $post_ins_id, 'gender', $tel_gender );
							update_post_meta( $post_ins_id, 'height', $tel_height );
							update_post_meta( $post_ins_id, 'weight', $tel_weight );
							update_post_meta( $post_ins_id, 'hair_color', $tel_hair_color );
							update_post_meta( $post_ins_id, 'dialect', $tel_dialect );
							update_post_meta( $post_ins_id, 'language', $tel_language );
							update_post_meta( $post_ins_id, 'experience', $tel_experience );
							update_post_meta( $post_ins_id, 'talent_audio', $talent_audio );
							update_post_meta( $post_ins_id, 'experience_description', $tel_experienc_description );
							update_post_meta( $post_ins_id, 'phone', $phone );
							update_post_meta( $post_ins_id, 'email', $email );
							update_post_meta( $post_ins_id, 'address', $address );
							update_post_meta( $post_ins_id, 'assignment', $assignment );
							update_post_meta( $post_ins_id, 'gallery_images', $talent_image );
							update_post_meta( $post_ins_id, 'gallery_videos', $talent_video_url );
							wp_set_object_terms( $post_ins_id, $film_category, 'film_category');
						 }
						 elseif($category == 'dancer'){
							update_post_meta( $post_ins_id, 'experience', $dance_experience );
							update_post_meta( $post_ins_id, 'experience_description', $dance_experience_description );
							update_post_meta( $post_ins_id, 'dancer_type', $dancer_type );
							update_post_meta( $post_ins_id, 'phone', $phone );
							update_post_meta( $post_ins_id, 'email', $email );
							update_post_meta( $post_ins_id, 'address', $address );
							update_post_meta( $post_ins_id, 'assignment', $assignment );
							update_post_meta( $post_ins_id, 'gallery_images', $talent_image );
							update_post_meta( $post_ins_id, 'gallery_videos', $talent_video_url );
							wp_set_object_terms( $post_ins_id, $dancer_type, 'dancer_category');
						 } 
						elseif($category == 'model'){	
						 //Model
							wp_set_object_terms( $post_ins_id, $category, 'models_category');
							update_post_meta( $post_ins_id, 'age', $tel_model_age );
							update_post_meta( $post_ins_id, 'gender', $tel_model_gender );
							update_post_meta( $post_ins_id, 'height', $tel_model_height );
							update_post_meta( $post_ins_id, 'weight', $tel_model_weight );
							update_post_meta( $post_ins_id, 'exprience', $tel_model_exprience );
							update_post_meta( $post_ins_id, 'phone', $phone );
							update_post_meta( $post_ins_id, 'email', $email );
							update_post_meta( $post_ins_id, 'address', $address );
							update_post_meta( $post_ins_id, 'gallery_images', $talent_image );
							update_post_meta( $post_ins_id, 'gallery_videos', $talent_video_url );
							update_post_meta( $post_ins_id, 'assignment', $assignment );
						}
						if($slected_category == 'sports'){
							$tel_sports_expertise = $_POST['sports_expertise'];
							$tel_sports_experience = $_POST['sports_experience'];
							$tel_sports_experience_description = $_POST['sports_experience_description'];
							$tel_cul_gender = $_POST['sports_gender'];
							$cul_sports_age = $_POST['sports_age'];
							$tel_cul_video_url = $talent_video_url;
							$tel_cul_image_url = $talent_image;
							update_post_meta( $post_ins_id, 'gender', $tel_cul_gender );
							update_post_meta( $post_ins_id, 'age', $cul_sports_age );
							update_post_meta( $post_ins_id, 'gallery_images', $talent_image );
							update_post_meta( $post_ins_id, 'gallery_videos', $talent_video_url );
							update_post_meta( $post_ins_id, 'expertise', $tel_sports_expertise );
							update_post_meta( $post_ins_id, 'experience', $tel_sports_experience );
							update_post_meta( $post_ins_id, 'experince_description', $tel_sports_experience_description );
							update_post_meta( $post_ins_id, 'phone', $phone );
							update_post_meta( $post_ins_id, 'email', $email );
							update_post_meta( $post_ins_id, 'address', $address );
							update_post_meta( $post_ins_id, 'assignment', $assignment );
							wp_set_object_terms( $post_ins_id, $slected_category, 'culture_categories');
						}
					update_post_meta( $post_ins_id, 'city', $city );
					}
			//	}
			}
				?>
<!-- #Content -->
<div id="content_wrapper" class="span12 reg_telent_wrapper">
	
<div class="row-fluid">
			
	<div class="span12 f_col moudle">
		<div id="form_container_main">
			<h2><i class="fa fa-check"></i>I want to register my profile:</h2>
			<form class="panel-login dashboard-form telent_register_form" id="register_telent_form" action="" method="post"  >

				    <div class="form-group">
				            <div class="col-md-6">
								 <label for="phone_number1">Fornavn</label>
				                <input type="text" name="firstname" placeholder="Fornavn" required="" >
				            </div>	

				            <div class="col-md-6">
				                 <label for="phone_number1">Etternavn</label>
								<input type="text" name="lastname" placeholder="Etternavn" required1="">
				            </div>
				            <div class="col-md-6">
							 <label for="phone_number1">By/Stedsnavn</label>
				                <input type="text" name="city" placeholder="City" required1="">
				            </div>
				            <div class="col-md-6">
							 <label for="phone_number1">Adresse</label>
				                <input type="text" name="street" placeholder="Adresse" required1="">
				            </div>

				            <div class="col-md-6">
							 <label for="phone_number1">Email</label>
				                <input type="email" name="email" placeholder="Email" required="">
				            </div>	
							<div class="col-md-6">
							 <label for="phone_number1">Telefon</label>
				                <input type="text" name="phone" placeholder="Telefon" required1="">
				            </div>	
				            <div class="col-md-6">
				       
							<div id="div_talent_category">
							 <label for="phone_number1">Kategory</label>
				                <select name="category" id="talent_category" required="">
				                    <option value="">-Talent Category -</option>
				                    <option value="dancer">Dance Type</option>
				                    <option value="film">Film & theater</option>
				                   <option value="actor" style="display: none;">Actor</option>
				                    <option value="music">Music</option>
				                    <option value="culture">Culture</option>
				                    <option value="sports">Sports</option>
				                </select>
							</div>
							<div id="new_category" class="text-left" style="display:none;">
							   <input type="checkbox" id="second_cat" name="terms" required1="" value="true"><span style="">Did you have En annen Kategory ?</span>
							</div>
				            </div>
							<div class="col-md-6" id="div_talent_category_second" style="display:none;">
							 <label for="phone_number1">En annen Kategory</label>
				           <div class="mini-box"><a target="_blank" href="<?php echo get_permalink().'/?profile_id='.$talent_post_id; ?>"><input type="button" name="view_profile" tabindex="4" class="view_profile form-control btn btn-register btn-pink" value="Click here"></a></div>
							<!--<div id="div_talent_category"><label for="phone_number1">Multi Talent Category</label>
							<div class="multi_category_input"><input type="checkbox" id="dancer" name="multi_category[]" class="multi_talent" required1="" value="dancer"><span style="">Dance</span></div>
							<div class="multi_category_input"><input type="checkbox" id="film" name="multi_category[]" class="multi_talent" required1="" value="film"><span style="">Film & theater</span></div>
							<div class="multi_category_input"><input type="checkbox" id="music" name="multi_category[]" class="multi_talent" required1="" value="music"><span style="">Music</span></div>
							<div class="multi_category_input"><input type="checkbox" id="culture" name="multi_category[]" class="multi_talent" required1="" value="culture"><span style="">Culture</span></div>
							<div class="multi_category_input"><input type="checkbox" id="sports" name="multi_category[]" class="multi_talent" required1="" value="sports"><span style="">Sports</span></div>
							</div> -->
							</div>
							
				   
				  <div id="tel_film" class="hide_this">
						<div class="col-md-6">
							 <label for="phone_number1">Film Category</label>
								<select name="film_category" id="film_category">
									<option value="">- Film Category -</option>
									<option value="statist">Statist</option>
									<option value="actor">Actor</option>
									<option value="model">Advertising Model</option>
									<option value="voiceover">Voiceover</option>
								</select>
						</div>
					</div>     

				    <div id="tel_music" class="hide_this">
						<div class="col-md-6">
						 <label for="phone_number1">Music Category</label>
				                <select name="music_category" id="music_category">
				                    <option value="">- Music Category -</option>
				                    <option value="band">Band</option>
				                    <option value="dj">Dj</option>
				                    <option value="singer">Singer</option>
				                    <option value="instrumentals">Instrumentals</option>
				                </select>
				    	</div>
					
					<div id="tel_music_gender" class="hide_this">
						<div class="col-md-6">
						 <label for="music_gender">Gender</label>
				            <select name="music_gender" id="music_gender">
								<option value="">- Select Gender -</option>
								<option value="Male">- Male -</option>
								<option value="female">- Female -</option>
							</select>
				    	</div>
					</div>
					</div>
				    <div id="tel_band" class="hide_this">
				   		<div class="col-md-6">
						 <label for="phone_number1">Band</label>
				                <select name="band_category">
				                    <option value="">-Select Band -</option>
				                    <option value="party_band">Party Band</option>
				                    <option value="jazz">Jazz</option>
				                    <option value="country">Country</option>
				                    <option value="rock">rock</option>
				                    <option value="heavy_rock">Heavy Rock</option>
				                    <option value="other">Other</option>
				                </select>
				    	</div>
						<div class="col-md-12 telent_description">
							<label for="phone_number1">Genre</label>
							<textarea name="band_genre" placeholder="Genre" required1="" ></textarea>
						</div>
						<div class="col-md-12">
							<label for="phone_number1">Founded</label>						
							<input type="text" name="founded" placeholder="Founded" />
				    	</div>
						<div class="col-md-12">
							<label for="phone_number1">Repertoire</label>						
							<textarea name="repertoire" placeholder="Repertoire" ></textarea>
				    	</div>
						<div class="col-md-12">
							<label for="phone_number1">Crew</label>						
							<textarea name="crew" placeholder="Crew" ></textarea>
				    	</div>
					</div>
					<div id="tel_dj_experience" class="hide_this">
						<div class="col-md-6">
							<label for="phone_number1">Dj Experience</label>			            
				                <select name="dj_experience">
				                    <option value="">- Dj Experience -</option>
				                    <option value="low_experience">Low experience</option>
				                    <option value="medium">Medium</option>
				                    <option value="professional">Professional</option>
				                </select>
						</div>
						<div class="col-md-12">
							<textarea name="dj_description" placeholder="Dj Experience" ></textarea>
				    	</div>
					</div>
					<div id="tel_singer" class="hide_this">
						<div class="col-md-6">
							<label for="phone_number1">Singer Experience</label>			            
				                <select name="singer_experience">
				                    <option value="">- Singer Experience -</option>
				                    <option value="low_experience">Low experience</option>
				                    <option value="medium">Medium</option>
				                    <option value="professional">Professional</option>
				                </select>
								</div>
						<div class="col-md-12">
								<textarea name="singer_description" placeholder="Singer Experience" ></textarea>
				    	</div>
					</div>
				<div id="tel_instrumentals" class="hide_this">
					<div class="col-md-6">
						<label for="phone_number1">Type of instrument</label>						
						<input type="text" name="type_instrument" placeholder="Type of instrument" />
					</div>
					<div class="col-md-6">
						<label for="phone_number1">Played number of years</label>
						<input type="text" name="instrumental_experience" placeholder="Enter years" />
					</div>
					<div class="col-md-12">
						<textarea name="instrumental_description" placeholder="instrumentals Experience" ></textarea>
					</div>
				</div>
				
				<div id="tel_dance" class="hide_this">
					<div class="col-md-6">
						<label for="phone_number1">Dance Type</label>			            
							<select name="dancer_type" id="dancer_type">
								<option value="">- Dance Type -</option>
								<option value="modern_dance">Modern dance</option>
								<option value="hip_hop">Hip Hop</option>
								<option value="ballet">Ballet</option>
							</select>
					</div>
				
					<div id="tel_modern_dance" class="hide_this">
			            <div class="col-md-6">
						<label for="phone_number1">Gender</label>
							<select name="dancer_gender">
								<option value="">- Select Gender -</option>
								<option value="Male">- Male -</option>
								<option value="female">- Female -</option>
							</select>
							
			            </div>
						<div class="col-md-6">
							<label for="phone_number1"> Dance Experience</label>			            
				                <select name="dance_experience">
				                    <option value="">- Dance Experience -</option>
				                    <option value="low_experience">Low experience</option>
				                    <option value="medium">Medium</option>
				                    <option value="professional">Professional</option>
				                </select>
								</div>
							<div class="col-md-12">
								<textarea name="dance_experience_description" placeholder="Dance Experience" ></textarea>
							</div>
					</div>
					
				</div>
				    </div>
					<div id="tel_statist" class="hide_this">
			            <div class="col-md-6">
						<label for="phone_number1">Age</label>
							<select name="statist_age" required="">  <option value="18"> - Select Age -</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select>
			            </div>	
			    	
			            <div class="col-md-6">
						<label for="phone_number1">Gender</label>
							<select name="statist_gender">
								<option value="">- Select Gender -</option>
								<option value="Male">- Male -</option>
								<option value="female">- Female -</option>
							</select>
			            </div>	

			            <div class="col-md-6">
						<label for="phone_number1">Height</label>
			                <input type="text" name="statist_height" placeholder="Height" >
			            </div>	
			           
			            <div class="col-md-6">
						<label for="phone_number1">Weight</label>
			                <input type="text" name="statist_weight" placeholder="Weight" >
			            </div>	
			            <div class="col-md-6">
						<label for="phone_number1">Hair color</label>
			                <input type="text" name="statist_hair_color" placeholder="Hair color" >
			            </div>	

			            <div class="col-md-6">
						<label for="phone_number1">Dialect</label>
			                <input type="text" name="statist_dialect" placeholder="Dialect" >
			            </div>	
				    	
			            <div class="col-md-6">
						<label for="phone_number1">Language</label>
			                <input type="text" name="statist_language" placeholder="Language" >
			            </div>	
			    	
			            <div class="col-md-6">
						<label for="phone_number1">Experience</label>
							<select name="statist_experience">
								<option value="">- Select Experience -</option>
								<option value="low_experience">Low experience</option>
								<option value="medium">Medium</option>
								<option value="professional">Professional</option>
							</select>	
							
			            </div>	
				    </div>
				    <div id="tel_actor" class="hide_this">
			            <div class="col-md-6">
						<label for="phone_number1">Age</label>
							<select name="actor_age" required=""><option value="18"> - Select Age -</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select>
			            </div>	
			    	
			            <div class="col-md-6">
						<label for="phone_number1">Gender</label>
							<select name="actor_gender">
								<option value="">- Select Gender -</option>
								<option value="Male">- Male -</option>
								<option value="female">- Female -</option>
							</select>
							
			            </div>	

			            <div class="col-md-6">
						<label for="phone_number1">Height</label>
			                <input type="text" name="actor_height" placeholder="Height" >
			            </div>	
			           
			            <div class="col-md-6">
						<label for="phone_number1">Weight</label>
			                <input type="text" name="actor_weight" placeholder="Weight" >
			            </div>	
			            <div class="col-md-6">
						<label for="phone_number1">Hair color</label>
			                <input type="text" name="actor_hair_color" placeholder="Hair color" >
			            </div>
			            <div class="col-md-6">
						<label for="phone_number1">Language</label>
			                <input type="text" name="actor_language" placeholder="Language" >
			            </div>	
			    	
			            <div class="col-md-6">
						<label for="phone_number1">Experience</label>
							<select name="actor_experience">
								<option value="">- Select Experience -</option>
								<option value="low_experience">Low experience</option>
								<option value="medium">Medium</option>
								<option value="professional">Professional</option>
							</select>	
			            </div>	
				    </div>
				    <div id="tel_model" class="hide_this">
			            <div class="col-md-6">
						<label for="phone_number1">Age</label>
							<select name="model_age" required="">  <option value="18"> - Select Age -</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select>
			            </div>	

			            <div class="col-md-6">
						<label for="phone_number1">Gender</label>
							<select name="model_gender">
								<option value="">- Select Gender -</option>
								<option value="Male">- Male -</option>
								<option value="female">- Female -</option>
							</select>
			            </div>	
			           
			            <div class="col-md-6">
						<label for="phone_number1">Height</label>
			                <input type="text" name="model_height" placeholder="Enter Height" >
			            </div>	
			            <div class="col-md-6">
						<label for="phone_number1">Weight</label>
			                <input type="text" name="model_weight" placeholder="Enter Weight" >
			            </div>	
						 <div class="col-md-6">
							<label for="phone_number1">Model Experience</label>			            
							<select name="model_exprience">
								<option value="">- Select Experience -</option>
								<option value="low_experience">Low experience</option>
								<option value="medium">Medium</option>
								<option value="professional">Professional</option>
							</select>				    	
						</div>
				    </div>
					<div id="tel_voiceover" class="hide_this">
			           <div class="col-md-6">
						<label for="phone_number1">Gender</label>
							<select name="voiceover_gender">
								<option value="">- Select Gender -</option>
								<option value="Male">- Male -</option>
								<option value="female">- Female -</option>
							</select>
			            </div>
						 <div class="col-md-6 text-left">
							<label for="phone_number1">Voiceover Experience</label>			            
							<input type="checkbox" name="voiceover_experience[]" required1="" value="advertising"><span class="label_span">Advertising</span>				    	
							<input type="checkbox" name="voiceover_experience[]" required1="" value="informative"><span class="label_span">Informative</span>				    	
						</div>
						<div class="col-md-12">
								<textarea name="voiceover_description" placeholder="Voiceover Experience" ></textarea>
				    	</div>
						<div class="col-md-12 text-left">
							<label for="email">Upload your voice</label>
							<?php echo do_shortcode('[ajax-file-upload disallow_remove_button="1" unique_identifier="my_talent_audio" on_success_set_input_value="#talent_audio" allowed_extensions="mp3,wav" on_success_alert="Your audio file was successfully uploaded !" max_size=10000]'); ?>
							<div class="click_here">click here to upload !</div>
							<textarea type="text"  class="form-control hide" name="talent_audio" value="" id="talent_audio"></textarea>
						</div>
				    </div>
				    <div id="tel_culture" class="hide_this">
						<div class="col-md-12">
								<label for="phone_number1">Experties</label>
								<textarea name="culture_expertise" placeholder="Player Experties" ></textarea>
				    	</div>
			            <div class="col-md-6">
						<label for="phone_number1">Experience</label>
							<select name="culture_experience">
								<option value="">- Select Experience -</option>
								<option value="low_experience">Low experience</option>
								<option value="medium">Medium</option>
								<option value="professional">Professional</option>
							</select>
					   </div>		
			           <div class="col-md-12">
								<textarea name="culture_experience_description" placeholder="Description" ></textarea>
				    	</div>
			            <div class="col-md-6">
						<label for="phone_number1">Gender</label>
							<select name="cul_gender">
								<option value="">- Select Gender -</option>
								<option value="Male">- Male -</option>
								<option value="female">- Female -</option>
							</select>
			            </div>
				    </div>
					<div id="tel_sports" class="hide_this">
						<div class="col-md-12">
								<label for="phone_number1">Player Experties</label>
								<textarea name="sports_expertise" placeholder="Sports Experties" ></textarea>
				    	</div>
			    	
			            <div class="col-md-6">
						<label for="phone_number1">Experience</label>
							<select name="sports_experience">
								<option value="">- Select Experience -</option>
								<option value="low_experience">Low experience</option>
								<option value="medium">Medium</option>
								<option value="professional">Professional</option>
							</select>
					   </div>		
			            <div class="col-md-12">
							<textarea name="sports_experience_description" placeholder="Description" ></textarea>
				    	</div>
			            <div class="col-md-6">
						<label for="phone_number1">Gender</label>
							<select name="sports_gender">
								<option value="">- Select Gender -</option>
								<option value="Male">- Male -</option>
								<option value="female">- Female -</option>
							</select>
			            </div>	 

			            <div class="col-md-6">
						<label for="phone_number1">Age</label>
							<select name="sports_age" required="">  <option value="18"> - Select Age -</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select>
							
			            </div>
				    </div>

	 <div class="col-md-6">
					<label for="phone_number1">Can take assignments ?</label>
		                <select name="assignments" required1="">
		                    <option value=""> - Can take assignments - </option>
		                    <option value="Anytime"> Anytime </option>
		                    <option value="By appointment"> By appointment </option>
		                    <option value="Only via agency and agent">Only via agency and agent</option>
		                </select>
		            </div>

		            <div class="col-md-6 tel_password">
		                <label for="phone_number1">Passord</label>
						<input type="password" name="password" placeholder="Password" required1="" >
		            </div>	
					<div class="form-group" id="tel_gallery">
					  <div class="col-md-12">
						<label for="phone_number1">Video URLs</label>
			                <textarea name="video_url" placeholder="Please put Youtube video URLs e.g https://www.youtube.com/watch?v=pvXsvUOfgfo" ></textarea>
							<div class="text-left">(Please enter comma seprated)</div>
			            </div>
						<div class="col-md-12 telent_image">
							<label for="email">Profile Image</label>
							<?php echo do_shortcode('[ajax-file-upload disallow_remove_button="1" on_success_set_input_value="#talent_image" on_success_alert="Your images was successfully uploaded !"]'); ?>
							<div class="click_here">click here to upload !</div>
							<textarea class="form-control hide" name="talent_image" value="" id="talent_image"></textarea>
						</div>
					</div>

					<div class="col-md-12 text-left agree">
						<div><input type="checkbox" name="ag18" required="" value="18">  <span>I am over 18 years old and I do not need consent to register my profile </span></div>	
						<div><input type="checkbox" name="terms" required="" value="18">  <span>I Agree to <a class="term_and_conditions">terms & conditions</a> for this site</span></div>	
					</div>
				    <div class="form-group">
				            <div class="col-sm-offset-3 col-sm-6">
				                 <input type="submit" name="register_telent" tabindex="4" class="form-control btn btn-register btn-pink" value="Register"> 
				            </div>
				    </div>
				</div>
			</form>	
		</div>
	</div>
</div>	

<?php 


get_footer(); ?>