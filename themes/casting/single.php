<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package casting_kaya
 */
get_header();

while (have_posts()):
	the_post();

	// Cpt Post shortlist session

	$selected = '';
	if (isset($_SESSION['shortlist']))
		{
		if (in_array(get_the_ID() , $_SESSION['shortlist']))
			{
			$selected = 'item_selected';
			}
		}

	echo '<div class="post_single_page_content_wrapper item ' . $selected . '" id="' . get_the_ID() . '">'; // Post Data content wrapper note:don't delete this ID and class
	echo '<div class="one_fourth media-section">';
	$img_url = wp_get_attachment_url(get_post_thumbnail_id());
	if( !empty($img_url) ){
		echo '<img src="'.casting_kaya_image_sizes($img_url, '500', '800').'" class="" />';
		}else{
		echo '<img src="'.get_template_directory_uri().'/images/default_image.jpg" class="" />';
		}
	if (function_exists('kaya_pods_cpt_shortlist_text_buttons'))
		{
		echo kaya_pods_cpt_shortlist_text_buttons();
		}
	if (function_exists('kaya_pods_cpt_compcard_images'))
		{ // Compcard Button
		echo kaya_pods_cpt_compcard_images(get_query_var('post_type'));
		}
	
	echo '<div class="pods-post-tags">'	;
		echo kaya_pods_post_tags(get_query_var('post_type'));	
	echo '</div>';
	echo '<div class="client-form">';
		dynamic_sidebar( 'sidebar_form' );
	echo '</div>';
	echo '</div>';
	echo '<div class="three_fourth_last talents_single_page">';
	echo '<div class="talents_single_page_details">';
	echo '<h3>' . get_the_title() . '</h3>';
	if (function_exists('kaya_general_info_section'))
		{
		kaya_general_info_section(get_query_var('post_type')); // Fields Information
		}
	
	if (function_exists('kaya_media_section'))
		{
		kaya_media_section(get_query_var('post_type')); // Images, Videos & Rich Textarea Information
		}
	
	echo '</div>';
	echo '</div>';
	echo '</div>';
endwhile; // End of the loop.
get_footer();
?>
