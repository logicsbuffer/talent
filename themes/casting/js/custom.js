/**
 * File Custom.js.
 *
 * Theme custom jquery functionality
 */

( function( $ ) {

	/**
	* Jquery Smart Menu 
	*/
	$('nav #main-menu').smartmenus({
		subMenusSubOffsetX: 1,
		subMenusSubOffsetY: -8
	});

	var $mainMenuState = $('#main-menu-state');

	if ($mainMenuState.length) {
		// animate mobile menu
		$mainMenuState.change(function(e) {
			var $menu = $('nav #main-menu');
			if (this.checked) {
				$menu.hide().slideDown(250).removeClass('add-mobile-menu').addClass('hide-mobile-menu');
			} else {
				$menu.show().slideUp(250).addClass('add-mobile-menu').removeClass('hide-mobile-menu');
			}
		});
		// hide mobile menu beforeunload
		$(window).bind('beforeunload unload', function() {
			if ($mainMenuState[0].checked) {
			$mainMenuState[0].click();
		}
		});
	}

	/**
     * Scroll Top Animation
     */
	$(window).scroll(function() {
		if ($(this).scrollTop() > 100) {
			$('.scrolltop').fadeIn();
		} else {
			$('.scrolltop').fadeOut();
		}
	});
	$('.scrolltop').click(function() {
		$("html, body").animate({
		scrollTop: 0
		}, 600);
		return false;
	});
	
	$("#flip, .search_close").click(function(){
        $("#panel").slideToggle("0");
    });
    
//Image Hover Effect
 function talents_image_hover_details() {
    $('.talent_image_details_wrapper').each(function() {
        $(this).hover(function() {
            $(this).find('.general-meta-fields-info-wrapper').stop(true, true).animate({
               // 'right':'100%',
				'left':'0',
				'opacity':'1'
            },200);
        }, function() {
            $(this).find('.general-meta-fields-info-wrapper').stop(true, true).animate({
                //'right':'0',
				'left':'-100%',
				'opacity':'0'
            }, 200);
        });
    });
}
talents_image_hover_details();
	/* Single page Tabs Content 
    $('.single_tabs_content_wrapper').each(function() {
        $(".single-page-meta-content-wrapper").hide(); //Hide all content
        $("ul.tabs_content_wrapper li:first").addClass("tab-active").show();
        $(".single-page-meta-content-wrapper:first").stop(true, true).fadeIn(0);
        $("ul.tabs_content_wrapper li").click(function() {
            $("ul.tabs_content_wrapper li").removeClass("tab-active");
            $(this).addClass("tab-active");
            $(".single-page-meta-content-wrapper").stop(true, true).fadeOut(0);
            var activeTab = $(this).find("a").attr("href");
            $(activeTab).stop(true, true).fadeIn(800);
            return false;
        });
    }); */
    // Page content based on footer position fixed / not
   function kaya_footer_position(){
   		var header_height = $('#kaya-header-content-wrapper').outerHeight();
   		var menu_height = $('.kaya-page-titlebar-wrapper').outerHeight();
   		var footer_height = $('#kaya-footer-content-wrapper').outerHeight();
	   	if ($('body').height() < $(window).height()){
	   		$('#kaya-mid-content-wrapper').css('height', Math.ceil($(document).height() - (parseInt(header_height) + parseInt(menu_height) + parseInt(footer_height))));
	        $("#kaya-footer-content-wrapper").addClass("footer_bottom_position_fix");
	    }else{
	    	//lert('test2');
	    	$('#kaya-mid-content-wrapper').removeAttr('style');
	        $("#kaya-footer-content-wrapper").removeClass("footer_bottom_position_fix");
	    }
   }
   $(window).load(function(){
   		kaya_footer_position();
   });
   $(window).resize(function(){
   		kaya_footer_position();
   });
 	
 	$('.cpt-post-content-wrapper ul li, .kaya-post-content-wrapper ul li').each(function(){
 		$(this).hover(function(){
 			$(this).find('a.item_button.add').show(250);
 		}, function(){
 			$(this).find('a.item_button.add').hide(150);
 		});
 	});
   
} )( jQuery );
