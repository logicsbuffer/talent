<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package casting
 */

?>

	</div><!-- #content -->
	</div>
	<?php
		$main_footer_page = get_theme_mod('main_footer_page') ? get_theme_mod('main_footer_page') : '0';
		if(!empty($main_footer_page) && ( $main_footer_page != '0' ) ){
			echo '<div class="kaya-page-content-footer">';
			echo '<div class="container">';
				$post = get_page($main_footer_page); 
				$content = apply_filters('the_content', $post->post_content); 
				echo $content;
			echo '</div></div>';
		}
	?>
	<footer id="kaya-footer-content-wrapper">
		<div class="container">
			<div class="one_half"><span class="copyright"><?php echo get_theme_mod('footer_copy_rights') ? get_theme_mod('footer_copy_rights') : __('Footer Copy Rights Text', 'casting') ?></span></div>
			<?php if ( has_nav_menu( 'footermenu' ) ) : ?>
				<div class="footer-menu-wrapper one_half_last">
					<?php wp_nav_menu( array('theme_location' => 'footermenu', 'menu_class' => 'footer-menu' ) ); ?>
				</nav><!-- .social-navigation -->
			<?php endif; ?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->
<a href="#" class="scrolltop"><i class="fa fa-long-arrow-up"></i></a>
<?php wp_footer(); ?>

</body>
</html>
