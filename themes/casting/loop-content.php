<?php
/**
 * Cpt post loop customization
 * This loop work only "taxonomy & Shortlist" pages only
 */
global $kaya_options, $taxonomy_cpt_name, $kaya_shortlist_options;
$cpt_slug_name = kaya_get_post_type(); // cpt slug name
if( !is_author() && !is_tax() && is_search() ){
	$columns = isset($kaya_shortlist_options['shortlist_display_columns']) ? $kaya_shortlist_options['shortlist_display_columns'] : '4'; 
}elseif(is_tax() || is_search()){
	$columns =  !empty($kaya_options->taxonomy_columns) ? $kaya_options->taxonomy_columns : '4'; // this columns working on cpt taxonomy page only
}else{
	$columns = '3';
}
// Featured Image Sizes
 $image_cropping_type = !empty($kaya_options->choose_image_sizes) ? $kaya_options->choose_image_sizes : 'wp_image_sizes';
if( $image_cropping_type == 'wp_image_sizes' ){
	$image_sizes = !empty($kaya_options->choose_image_sizes) ? $kaya_options->choose_image_sizes : 'full';
}else{
	$image_size_width = !empty($kaya_options->taxonomy_gallery_width) ? $kaya_options->taxonomy_gallery_width : '380';
	$image_size_height = !empty($kaya_options->taxonomy_gallery_height) ? $kaya_options->taxonomy_gallery_height : '600';
	$image_sizes = array( $image_size_width, $image_size_height );
}		
// Session for shortlist data
if(isset($_SESSION['shortlist'])) {
	if ( in_array(get_the_ID(), $_SESSION['shortlist']) ) {
		$selected = 'item_selected';
	}else{
		$selected = '';
	}
}else{
	$selected = '';
}

echo '<li class="column'.$columns.' '.$selected.' item" id="'.get_the_ID().'">';
	echo '<div class="talent_image_details_wrapper">';
		echo '<a href="'.get_the_permalink().' "class="img_hover_effect">';
		$img_url = get_the_post_thumbnail_url();	
		
	// post featured image with permalink
	echo kaya_pod_featured_img( $image_sizes, $image_cropping_type ); // Featured Image
	// Cpt Meta fields information wrapper
	echo '<div class="talents_details">'; 
		//echo '<h4>'.get_the_title().'</h4>'; // post title section
		if( function_exists('kaya_general_info_section') ){
			kaya_general_info_section($cpt_slug_name);  // Cpt post meta data information
		}
	echo '</div>';
	// check this function to enabled shortlist icons or not
	if( !empty($kaya_shortlist_options['enable_cpt_shortlist']) ){
		if( in_array($cpt_name, $kaya_shortlist_options['enable_cpt_shortlist']) ){
			do_action('kaya_pods_cpt_shortlist_icons'); // Shortlist Icons
		}
	}
		echo '</a>';
	// End Cpt Meta fields information wrapper

echo '</li>';
?>