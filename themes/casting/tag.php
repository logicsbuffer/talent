<?php
/**
 * The template for displaying Tag Archive pages.
 */
get_header(); 
	?>
<!--Start Middle Section  -->
<div class="fullwidth mid-content"> <!-- Middle content align -->
	<?php
	if( !empty($_REQUEST['post_type']) ){
		echo '<div class="taxonomy-content-wrapper kaya-post-content-wrapper">';
			echo '<ul class="column-extra">';
				 if(have_posts()) : 
      				while(have_posts()) : the_post();
							kaya_get_template_part( 'pods-taxonomy-view-style' );
						endwhile;
					endif;
       		echo '</ul>';
       	echo '</div>';
       }else{
       	get_template_part( 'loop');
       }		
    ?>
</div> <!-- End -->
<?php get_footer(); ?>