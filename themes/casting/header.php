<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package casting
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11"> 

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="kaya-page-content-wrapper" class="">
		<header id="kaya-header-content-wrapper" class="site-header" role="banner"> <!-- Header Section -->
			<div class="container">
				<div id="logo"> <!-- Header Logo -->
					<?php casting_kaya_logo();
					echo '<div class="mobile_toggle_menu_icons">'; ?>
						<input id="main-menu-state" type="checkbox" />
							<label class="main-menu-btn" for="main-menu-state">
							<span class="main-menu-btn-icon"></span>
						</label>
					</div>
				</div><!-- End Header Logo -->
				<nav id="header-navigation"> <!-- Header Navigation -->
					<?php
					if ( has_nav_menu('primary') ) { 
						wp_nav_menu(array('container_id' => 'main-nav','menu_id'=> 'main-menu', 'container_class' => 'menu','theme_location' => 'primary', 'menu_class'=> 'top-nav'));
					}else{
						wp_nav_menu(array('container_id' => 'main-nav', 'container'=> 'ul', 'menu_id'=> 'main-menu', 'container_class' => 'menu', 'menu_class'=> 'top-nav'));
					}
					if(function_exists('kaya_user_dashboard_menu')){
						echo kaya_user_dashboard_menu(); 
					}  
					?>
					<?php					
					if ( is_active_sidebar( 'top_search_widget_area' ) ) :
						echo '<div id="panel">';
						echo '<div class="kaya_toggle_search kta-search-content-wrapper toggle_search_wrapper">';
							 echo'<i class="fa fa-times search_close" aria-hidden="true"></i>';
             				dynamic_sidebar( 'top_search_widget_area' );
						echo '</div>';
						echo '</div>';
						echo '<div id="flip"><i class="fa fa-search"></i></div>';
					endif;
					?>
				</nav><!-- End navigation -->
			</div>
		</header><!-- End Header Section -->
		<?php casting_kaya_page_title(); 

		// Slider Functionality
		casting_kaya_slider_shortcode(); ?>
		<!-- Page title section -->
		<!-- Middle content alignment start here -->
		<div id="kaya-mid-content-wrapper">
			<div id="mid-content" class="site-content container">
