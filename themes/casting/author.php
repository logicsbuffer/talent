<?php 
if( isset($_REQUEST['type'])&&( $_REQUEST['type'] == 'pet' ) ){
get_header(); ?>
<div class="two_third mid-content"> 
<?php

	if(isset($_GET['author_name'])) :
	$curauth = get_userdatabylogin($author_name);
	else :
	$curauth = get_userdata(intval($author));
	endif;
	echo '<div class="author_meta_info">';
		echo '<div class="one_fifth">';
			echo get_avatar( $_REQUEST['author_id'] , 200 ); 
		echo '</div>';
		echo '<div class="four_fifth_last">';
			echo '<ul>';
				echo '<li><strong>'.__('Name', 'casting').' : </strong> '.get_the_author_meta( 'user_nicename', $_REQUEST['author_id']).'</li>';
				echo '<li><strong>'.__('Email', 'casting').' : </strong> '.get_the_author_meta( 'email', $_REQUEST['author_id']).'</li>';
			echo '</ul>';
		echo '</div>';
	echo '</div>';
	echo '<div class="clear"></div>';

	echo '<div class="author_biography">';
		echo '<div class="fullwidth">';
			echo '<h3>'.__('About the Author', 'casting').'</h3>';
			echo get_the_author_meta( 'description', $_REQUEST['author_id']); 
	echo '</div>';
echo '</div>';
// get his posts 'ASC'
$lastposts = array(
    'post_type'        => $_REQUEST['type'],
    'author'        => $_REQUEST['author_id'], // I could also use $user_ID, right?
    'orderby'       =>  'post_date',
    'order'         =>  'ASC' 
);
query_posts($lastposts);
//print_r($lastposts);
echo '<div class="cpt-post-content-wrapper">';
	 ?>
		<?php // if (have_posts() ):
			if( have_posts() ):
					echo '<h3>'.__('Current Author Posts', 'casting').'</h3>';
				echo '<ul class="isotope-container ta-extra">';
					while (have_posts()) : the_post();
						kaya_get_template_part( 'loop', 'content' );
					endwhile;
				echo '</ul>';
			endif;	
	
echo '</div>';
comment_form();
?>
</div>
<div id="sidebar" class="one_third_last">
		<?php dynamic_sidebar( 'dynamic_sidebar_widget_area' ); ?>
	</div>
<?php get_footer();
 }else{
	get_template_part('archive');
} ?>

