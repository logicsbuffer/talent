<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 *
 * You also should read the changelog to know what has been changed before updating.
 *
 * For more information, please visit:
 *
 * @link http://metabox.io/docs/registering-meta-boxes/
 */
add_filter( 'rwmb_meta_boxes', 'casting_kaya_register_meta_boxes' );
/**
 * Register meta boxes
 *
 * Remember to change "your_prefix" to actual prefix in your project
 *
 * @param array $meta_boxes List of meta boxes
 *
 * @return array
 */
function casting_kaya_register_meta_boxes( $meta_boxes ) {
	$meta_boxes[] = array(
		'id'         => 'kaya_page_secttings',
		'title'      => esc_html__( 'Page Settings', 'casting' ),
		'post_types' => array( 'page' ),
		'context'    => 'normal',
		'priority'   => 'high',
		'autosave'   => true,
		'fields'     => array(
			array(
				'name' => esc_html__( 'Disable Page Titlebar', 'casting' ),
				'id'   => "disable_page_title_bar",
				'type' => 'checkbox',
				'std'  => 0,
			),
			array(
				'name' => esc_html__( 'Slider Shortcode', 'casting' ),
				'id'   => "main_slider_shortcode",
				'type' => 'text',
			),
		),
	);
	return $meta_boxes;
}