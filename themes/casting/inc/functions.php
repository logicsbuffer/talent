<?php
/*---------------------------------------------------------------------------------------
Theme Logo
---------------------------------------------------------------------------------------*/
function casting_kaya_logo(){
	//echo get_theme_mod( 'logo_image' );
	$logo = get_theme_mod( 'logo_image' ) ? get_theme_mod( 'logo_image' ) : get_template_directory_uri().'/images/logo.png';
	$choose_logo = get_theme_mod( 'choose_logo' ) ? get_theme_mod( 'choose_logo' ) :'img_logo';
	$display_header_text = get_theme_mod( 'display_header_text' ) ? get_theme_mod( 'display_header_text' ) : '';
	if( $choose_logo == 'img_logo' ){
		echo '<a href="'.esc_url(home_url('/')).'" title="'.get_the_title().'"><img src="'.$logo.'" alt="'.get_the_title().'" /></a>';
	}else{
		echo '<h1 class="site-title"><a href="'.esc_url( home_url( '/' ) ).'">'.get_bloginfo( 'name' ).'</a></h1>';
		$description = get_bloginfo( 'description', 'display' );
		if (( $description || is_customize_preview()) && ( $display_header_text != '1' ) ) : ?>
			<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ echo '</p>';
		endif;
	}
}
/*---------------------------------------------------------------------------------------
All pages, posts, archives and category page title bar
---------------------------------------------------------------------------------------*/
if(!function_exists('casting_kaya_page_title')){
	function casting_kaya_page_title()
	{
		$post_id = get_the_ID();
		$disable_page_title_bar = get_post_meta($post_id, 'disable_page_title_bar', true) ? get_post_meta($post_id, 'disable_page_title_bar', true) : '0'; 
		if( $disable_page_title_bar != '1' ){
			echo '<section class="kaya-page-titlebar-wrapper">';
				echo '<div class="container" style="">';			
					if(is_page()){
						echo '<h2 class="page-title">'.get_the_title($post_id).'</h2>';			
					}elseif(is_home()){
						echo '<h2 class="page-title">'.get_the_title( get_option('page_for_posts', true) ).'</h2>';

					}elseif( is_single()){ 
						echo '<h2 class="page-title">'.get_the_title($post_id).'</h2>';
					} elseif(is_tag()){
						echo '<h2 class="page-title">'.__( 'Tag Archives: %s', 'casting' ). single_cat_title( '', false ).'</h2>';
					}
					elseif ( is_author() ) {
						the_post();
						echo '<h2 class="page-title">'. __( 'Author Archives:', 'casting' ). ' <span class="vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( "ID" ) ) ) . '" title="' . esc_attr( get_the_author() ) . '" rel="me">' . get_the_author() . '</a></span></h2>';
						rewind_posts();

					} elseif (is_category()) { 
						echo '<h2 class="page-title">'. __( 'Category Archives: ', 'casting' ) . single_cat_title( '', false ).'</h2>';
					} elseif( is_tax() ){
						global $post;
						$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
						echo '<h2 class="page-title">' .$term->name.'</h2>';
					}elseif (is_search()) { ?>
						<?php echo '<h2 class="page-title">'.__( 'Search Results for:', 'casting' ).' '. get_search_query().'</h2>';
					}elseif (is_404()) {
						echo '<h2  class="page-title">'.esc_html__( 'Error 404 - Not Found', 'casting' ).'</h2>';
					}elseif ( is_day() ){
						echo '<h2 class="page-title">'.__( 'Daily Archives:', 'casting' ), '<span>' . get_the_date() . '</span></h2>';
					}			 
					elseif ( is_month() ) { 
						echo '<h2 class="page-title">'. __( 'Monthly Archives:', 'casting' ), '<span>' . get_the_date( 'F Y' ) . '</span></h2>';
					} elseif ( is_year() ){
						echo '<h2 class="page-title">'. __( 'Yearly Archives:', 'casting' ), '<span>' . get_the_date( 'Y' ) . '</span></h2>';
					}else{ }
				echo '</div>';
			echo'</section>';
		}
	}
}

/*---------------------------------------------------------------------------------------
 Image resizer Functionality
---------------------------------------------------------------------------------------*/
if( !function_exists('casting_kaya_image_sizes') ){
	function casting_kaya_image_sizes($url, $width, $height=0, $align='') {
		return casting_kaya_image_resize($url, $width, $height, true, $align, false);
	}
}

/*---------------------------------------------------------------------------------------
 * Limit the wordpress post in Words
 * @param int limit
 ---------------------------------------------------------------------------------------*/
if( !function_exists('casting_kaya_content_dsiplay_words') ){
  function casting_kaya_content_dsiplay_words($limit) {
    //$content = explode(' ', get_the_content(), $limit);
    //$content = wp_trim_words( the_content(), $limit, '' );
    //return $content;
  }
} 

/*---------------------------------------------------------------------------------------
 Slider Functionality
---------------------------------------------------------------------------------------*/
if( !function_exists('casting_kaya_slider_shortcode') ){
	function casting_kaya_slider_shortcode() {
		$post_id = get_the_ID();
		$main_slider_shortcode =  get_post_meta($post_id, 'main_slider_shortcode', true) ? get_post_meta($post_id, 'main_slider_shortcode', true) : '0'; 
		if( $main_slider_shortcode ){
			echo '<div class="main-pages-slider-wrapper">';
					echo do_shortcode($main_slider_shortcode);
			echo '</div>';
		}
	}
}
/*---------------------------------------------------------------------------------------
 * RGBA Color Settings
 * @param (int) color
 * @param (float) opacity
 ---------------------------------------------------------------------------------------*/
function casting_kaya_rgba_color( $color, $opacity='0.5' ) {
    if ( $color[0] == '#' ) {
            $color = substr( $color, 1 );
    }
    if ( strlen( $color ) == 6 ) {
            list( $r, $g, $b ) = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
    } elseif ( strlen( $color ) == 3 ) {
            list( $r, $g, $b ) = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
    } else {
            return false;
    }
    $r = hexdec( $r );
    $g = hexdec( $g );
    $b = hexdec( $b );
    return 'rgba('.$r.','.$g.','.$b.', '.$opacity.')';
}

/*---------------------------------------------------------------------------------------
 Login / Registration menu 
---------------------------------------------------------------------------------------*/
if( !function_exists('casting_kaya_reg_login_menu') ){
    function casting_kaya_reg_login_menu() {
        if ( has_nav_menu('login_reg_menu') ) { 
                wp_nav_menu(array('container_id' => 'user-reg-login-menu-wrapper','menu_id'=> 'user-reg-login-menu', 'container_class' => 'user-reg-login-menu','theme_location' => 'login_reg_menu', 'menu_class'=> ''));
            }
    }
}

/*---------------------------------------------------------------------------------------
 * Posts Pagination
 ---------------------------------------------------------------------------------------*/
if ( ! function_exists( 'casting_kaya_pagination' ) ) :
function casting_kaya_pagination() {
    // Don't print empty markup if there's only one page.
    if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
        return;
    }
    $paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
    $pagenum_link = html_entity_decode( get_pagenum_link() );
    $query_args   = array();
    $url_parts    = explode( '?', $pagenum_link );
    if ( isset( $url_parts[1] ) ) {
        wp_parse_str( $url_parts[1], $query_args );
    }
    $pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
    $pagenum_link = trailingslashit( $pagenum_link ) . '%_%';
    $format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
    $format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';
    // Set up paginated links.
    $links = paginate_links( array(
        'base'     => $pagenum_link,
        'format'   => $format,
        'total'    => $GLOBALS['wp_query']->max_num_pages,
        'current'  => $paged,
        'mid_size' => 3,
        //'add_args' => array_map( 'urlencode', $query_args ),
        'prev_text' => '<i class="fa fa-angle-left"></i>',
        'next_text' => '<i class="fa fa-angle-right"></i>',
        'type'      => 'list',
    ) );
    $pagination_allowed_tags = array(
        'a' => array(
            'href' => array(),
            'title' => array(),
            'class' => array()
        ),
        'i' => array(
            'class' => array()
        ),
        'span' => array(
            'class' => array()
        ),
        'ul' => array(
            'class' => array()
        ),
        'li' => array(),
    );
    if ( $links ) :
    ?>  <div class="pagination">
            <?php echo wp_kses($links,$pagination_allowed_tags); ?>
        </div>       
    <?php
    endif;
}
endif;
?>