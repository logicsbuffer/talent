<?php
function casting_kaya_customizer_styles(){
	// Text Logo Color Section
	$text_logo_color = get_theme_mod('text_logo_color') ? get_theme_mod('text_logo_color') : '#353535';
	$text_logo_tagline_color = get_theme_mod('text_logo_tagline_color') ? get_theme_mod('text_logo_tagline_color') : '#757575';

	// Header Color Section
	$header_bg_color = get_theme_mod('header_bg_color') ? get_theme_mod('header_bg_color') : '#fff';
	$header_top_border_color = get_theme_mod('header_top_border_color') ? get_theme_mod('header_top_border_color') : '#e63f19';
	
	// Menu Section
	$menu_link_color = get_theme_mod('menu_link_color') ? get_theme_mod('menu_link_color') : '#333';
	$menu_link_hover_color = get_theme_mod('menu_link_hover_color') ? get_theme_mod('menu_link_hover_color') : '#e63f19';
	$menu_link_hover_top_border_color = get_theme_mod('menu_link_hover_top_border_color') ? get_theme_mod('menu_link_hover_top_border_color') : '#e63f19';
	$menu_active_link_color = get_theme_mod('menu_active_link_color') ? get_theme_mod('menu_active_link_color') : '#e63f19';
	$menu_active_border_top_color = get_theme_mod('menu_active_border_top_color') ? get_theme_mod('menu_active_border_top_color') : '#e63f19';
	$menu_padding_top = get_theme_mod('menu_padding_top') ? get_theme_mod('menu_padding_top') : '10';
	$child_menu_bg_color = get_theme_mod('child_menu_bg_color') ? get_theme_mod('child_menu_bg_color') : '#1a1a1a';
	$child_menu_link_color = get_theme_mod('child_menu_link_color') ? get_theme_mod('child_menu_link_color') : '#fff';
	$child_menu_link_hover_color = get_theme_mod('child_menu_link_hover_color') ? get_theme_mod('child_menu_link_hover_color') : '#fff';
	$child_menu_hover_bg_color = get_theme_mod('child_menu_hover_bg_color') ? get_theme_mod('child_menu_hover_bg_color') : '#454545';
	$child_menu_active_bg_color = get_theme_mod('child_menu_active_bg_color') ? get_theme_mod('child_menu_active_bg_color') : '#454545';
	$child_menu_active_link_color = get_theme_mod('child_menu_active_link_color') ? get_theme_mod('child_menu_active_link_color') : '#fff';
	
	// page title Section
	$page_titlebar_bg_color = get_theme_mod('page_titlebar_bg_color') ?  get_theme_mod('page_titlebar_bg_color')  : '#dedede';
	$page_titlebar_color = get_theme_mod('page_titlebar_color') ?  get_theme_mod('page_titlebar_color')  : '#353535';

	// Page mid content Section
	$page_mid_contant_bg_color = get_theme_mod('page_mid_contant_bg_color') ?  get_theme_mod('page_mid_contant_bg_color')  : '#e5e5e5';
	$page_mid_content_title_color = get_theme_mod('page_mid_content_title_color') ?  get_theme_mod('page_mid_content_title_color')  : '#353535';
	$page_mid_content_color = get_theme_mod('page_mid_content_color') ?  get_theme_mod('page_mid_content_color')  : '#757575';
	$page_mid_contant_links_color = get_theme_mod('page_mid_contant_links_color') ?  get_theme_mod('page_mid_contant_links_color')  : '#353535';
	$page_mid_contant_links_hover_color = get_theme_mod('page_mid_contant_links_hover_color') ?  get_theme_mod('page_mid_contant_links_hover_color')  : '#e63f19';

	// page sidebar Section
	$sidebar_bg_color = get_theme_mod('sidebar_bg_color') ?  get_theme_mod('sidebar_bg_color')  : '';
	$sidebar_title_color = get_theme_mod('sidebar_title_color') ?  get_theme_mod('sidebar_title_color')  : '#353535';
	$sidebar_content_color = get_theme_mod('sidebar_content_color') ?  get_theme_mod('sidebar_content_color')  : '#757575';
	$sidebar_links_color = get_theme_mod('sidebar_links_color') ?  get_theme_mod('sidebar_links_color')  : '#353535';
	$sidebar_links_hover_color = get_theme_mod('sidebar_links_hover_color') ?  get_theme_mod('sidebar_links_hover_color')  : '#e63f19';

	// Talent Section
    $talent_details_bg = get_theme_mod('talent_details_bg')? get_theme_mod('talent_details_bg') : '#e63f19';
    $talent_details_font_color = get_theme_mod('talent_details_font_color')? get_theme_mod('talent_details_font_color') : '#fff';
    $talent_single_page_title_color = get_theme_mod('talent_single_page_title_color')? get_theme_mod('talent_single_page_title_color') : '#333333';
    $talent_single_page_details_color = get_theme_mod('talent_single_page_details_color')? get_theme_mod('talent_single_page_details_color') : '#666';
    $talent_single_page_border_color = get_theme_mod('talent_single_page_border_color')? get_theme_mod('talent_single_page_border_color') : '#e2e2e2';
    $talent_title_border_color = get_theme_mod('talent_title_border_color')? get_theme_mod('talent_title_border_color') : '#e63f19';

    // Advance Search Section
    $advance_search_icon_bg = get_theme_mod('advance_search_icon_bg')? get_theme_mod('advance_search_icon_bg') : '#e63f19';
    $advance_search_icon_color = get_theme_mod('advance_search_icon_color')? get_theme_mod('advance_search_icon_color') : '#fff';
    $advance_search_bg = get_theme_mod('advance_search_bg')? get_theme_mod('advance_search_bg') : '#fff';
    $advance_search_bottom_border = get_theme_mod('advance_search_bottom_border')? get_theme_mod('advance_search_bottom_border') : '#e63f19';
    $advance_search_title_bg = get_theme_mod('advance_search_title_bg')? get_theme_mod('advance_search_title_bg') : '#e63f19';
    $advance_search_title_color = get_theme_mod('advance_search_title_color')? get_theme_mod('advance_search_title_color') : '#fff';
    $advance_search_select_bg_color = get_theme_mod('advance_search_select_bg_color')? get_theme_mod('advance_search_select_bg_color') : '#f2f2f2';
    $advance_search_select_font_color = get_theme_mod('advance_search_select_font_color')? get_theme_mod('advance_search_select_font_color') : '#333';
    $advance_search_select_border_color = get_theme_mod('advance_search_select_border_color')? get_theme_mod('advance_search_select_border_color') : '#ccc';
    $advance_search_button_bg_color = get_theme_mod('advance_search_button_bg_color')? get_theme_mod('advance_search_button_bg_color') : '#e63f19';
    $advance_search_button_border_color = get_theme_mod('advance_search_button_border_color')? get_theme_mod('advance_search_button_border_color') : '#e63f19';
    $advance_search_button_font_color = get_theme_mod('advance_search_button_font_color')? get_theme_mod('advance_search_button_font_color') : '#fff';
     
	// Footer Section
	$footer_bg_color = get_theme_mod('footer_bg_color') ?  get_theme_mod('footer_bg_color')  : '#353535';
	$footer_titles_color = get_theme_mod('footer_titles_color') ?  get_theme_mod('footer_titles_color')  : '#bcbcbc';
	$footer_content_color = get_theme_mod('footer_content_color') ?  get_theme_mod('footer_content_color')  : '#bcbcbc';
	$footer_link_color = get_theme_mod('footer_link_color') ?  get_theme_mod('footer_link_color')  : '#bcbcbc';
	$footer_link_hover_color = get_theme_mod('footer_link_hover_color') ?  get_theme_mod('footer_link_hover_color')  : '#fff';

	// Footer Section
	$page_footer_bg_color = get_theme_mod('page_footer_bg_color') ?  get_theme_mod('page_footer_bg_color')  : '#151515';
	$page_footer_content_color = get_theme_mod('page_footer_content_color') ?  get_theme_mod('page_footer_content_color')  : '#f5f5f5';
	$page_footer_link_color = get_theme_mod('page_footer_link_color') ?  get_theme_mod('page_footer_link_color')  : '#f5f5f5';
	$page_footer_link_hover_color = get_theme_mod('page_footer_link_hover_color') ?  get_theme_mod('page_footer_link_hover_color')  : '#fff';

 	/* Font Sizes */
    /* Title Font sizes H1 */
    $h1_title_font_size=get_theme_mod( 'h1_title_fontsize', '' ) ? get_theme_mod( 'h1_title_fontsize', '' ) : '30'; // H1
    $h2_title_font_size=get_theme_mod( 'h2_title_fontsize', '' ) ? get_theme_mod( 'h2_title_fontsize', '' ) : '27'; // H2
    $h3_title_font_size=get_theme_mod( 'h3_title_fontsize', '' ) ? get_theme_mod( 'h3_title_fontsize', '' ) : '25'; // H3
    $h4_title_font_size=get_theme_mod( 'h4_title_fontsize', '' ) ? get_theme_mod( 'h4_title_fontsize', '' ) : '18'; // H4
    $h5_title_font_size=get_theme_mod( 'h5_title_fontsize', '' ) ? get_theme_mod( 'h5_title_fontsize', '' ) : '16'; // H5
    $h6_title_font_size=get_theme_mod( 'h6_title_fontsize', '' ) ? get_theme_mod( 'h6_title_fontsize', '' ) : '12'; // H6
    // Letter Spaceing
    $h1_font_letter_space=get_theme_mod( 'h1_font_letter_space') ? get_theme_mod( 'h1_font_letter_space') : '0'; // H1
    $h2_font_letter_space=get_theme_mod( 'h2_font_letter_space') ? get_theme_mod( 'h2_font_letter_space') : '0'; // H2
    $h3_font_letter_space=get_theme_mod( 'h3_font_letter_space') ? get_theme_mod( 'h3_font_letter_space') : '0'; // H3
    $h4_font_letter_space=get_theme_mod( 'h4_font_letter_space') ? get_theme_mod( 'h4_font_letter_space') : '0'; // H4
    $h5_font_letter_space=get_theme_mod( 'h5_font_letter_space') ? get_theme_mod( 'h5_font_letter_space') : '0'; // H5
    $h6_font_letter_space=get_theme_mod( 'h6_font_letter_space') ? get_theme_mod( 'h6_font_letter_space') : '0'; // H6
    // Font Weight
    $h1_font_weight_bold=get_theme_mod( 'h1_font_weight_bold') ? get_theme_mod( 'h1_font_weight_bold') : 'bold'; // H1
    $h2_font_weight_bold=get_theme_mod( 'h2_font_weight_bold') ? get_theme_mod( 'h2_font_weight_bold') : 'bold'; // H2
    $h3_font_weight_bold=get_theme_mod( 'h3_font_weight_bold') ? get_theme_mod( 'h3_font_weight_bold') : 'bold'; // H3
    $h4_font_weight_bold=get_theme_mod( 'h4_font_weight_bold') ? get_theme_mod( 'h4_font_weight_bold') : 'bold'; // H4
    $h5_font_weight_bold=get_theme_mod( 'h5_font_weight_bold') ? get_theme_mod( 'h5_font_weight_bold') : 'bold'; // H5
    $h6_font_weight_bold=get_theme_mod( 'h6_font_weight_bold') ? get_theme_mod( 'h6_font_weight_bold') : 'bold'; // H6
    // Body & Menu
    $body_font_weight_bold=get_theme_mod( 'body_font_weight_bold') ? get_theme_mod( 'body_font_weight_bold') : 'normal'; // body
    $menu_font_weight=get_theme_mod( 'menu_font_weight') ? get_theme_mod( 'menu_font_weight') : 'normal'; // Menu
    $child_menu_font_weight=get_theme_mod( 'child_menu_font_weight') ? get_theme_mod( 'child_menu_font_weight') : 'normal'; // Child Menu
    // Menu Latter Sapcing
    $body_font_letter_space=get_theme_mod( 'body_font_letter_space') ? get_theme_mod( 'body_font_letter_space') : '0'; // H4
    $menu_font_letter_space=get_theme_mod( 'menu_font_letter_space') ? get_theme_mod( 'menu_font_letter_space') : '0'; // H5
    $child_menu_font_letter_space=get_theme_mod( 'child_menu_font_letter_space') ? get_theme_mod( 'child_menu_font_letter_space') : '0'; // H6
    $body_font_size=get_theme_mod( 'body_font_size', '' ) ? get_theme_mod( 'body_font_size', '' ) : '15'; // Body Font Size
    $menu_font_size=get_theme_mod( 'menu_font_size', '' ) ? get_theme_mod( 'menu_font_size', '' ) : '15'; // Body Font Size
    $child_menu_font_size=get_theme_mod( 'child_menu_font_size', '' ) ? get_theme_mod( 'child_menu_font_size', '' ) : '13'; // Body Font Size

    // Font family Names
    $google_body_font=get_theme_mod( 'google_body_font' ) ? get_theme_mod( 'google_body_font') : 'Open Sans';
    $google_bodyfont= ( $google_body_font == '0' ) ? 'arial' : $google_body_font;
    $google_menu_font=get_theme_mod( 'google_menu_font' ) ? get_theme_mod( 'google_menu_font' ) : 'Open Sans';
    $google_menufont= ( $google_menu_font == '0' ) ? 'arial' : $google_menu_font;
    $google_general_titlefont=get_theme_mod( 'google_heading_font') ? get_theme_mod( 'google_heading_font' ) : 'Open Sans';
    $google_generaltitlefont= ( $google_general_titlefont == '0' ) ? 'arial' : $google_general_titlefont;	
	/* Body & Menu & Title's Font Line Height  */
	$lineheight_body = round((1.9 * $body_font_size));
	$lineheight_h1 = round((1.6 * $h1_title_font_size));
	$lineheight_h2 = round((1.6 * $h2_title_font_size));
	$lineheight_h3 = round((1.6 * $h3_title_font_size));
	$lineheight_h4 = round((1.6 * $h4_title_font_size)); 
	$lineheight_h5 = round((1.6 * $h5_title_font_size));
	$lineheight_h6 = round((1.6 * $h6_title_font_size));

	$css = '';
    $css .= 'body, p{
			font-family:'.esc_attr( $google_bodyfont ).';
			line-height:'.$lineheight_body.'px;
			font-size:'.$body_font_size.'px;
			letter-spacing:'.$body_font_letter_space.'px;
			font-weight:'.$body_font_weight_bold.';
			font-style:normal;
    }
    .menu ul li a{
			font-family:'.$google_menufont.';
			font-size:'.$menu_font_size.'px;
			line-height: 100%;
			letter-spacing:'.$menu_font_letter_space.'px;
			font-weight:'.$menu_font_weight.';
    }
    .menu ul ul li a{
	        font-size:'.$child_menu_font_size.'px;
	        font-weight:'.$child_menu_font_weight.';
    }
   	a, span{
        	font-family:'.esc_attr( $google_bodyfont ).';
    }
    p{
        padding-bottom:'.$lineheight_body.'px;
    }
    /* Heading Font Family */
    h1, h2, h3, h4, h5, h6, h1 a, h2 a, h3 a, h4 a, h5 a, h6 a{
       	font-family:'.$google_generaltitlefont.';
    }
    h1{
        font-size:'.$h1_title_font_size.'px;
        line-height:'.$lineheight_h1.'px;
        letter-spacing:'.$h1_font_letter_space.'px;
        font-weight: '.$h1_font_weight_bold.';
    }
    h2{
        font-size:'.$h2_title_font_size.'px;
        line-height:'.$lineheight_h2.'px;
        letter-spacing:'.$h2_font_letter_space.'px;
        font-weight: '.$h2_font_weight_bold.';
    }
    h3{
        font-size:'.$h3_title_font_size.'px;
        line-height:'.$lineheight_h3.'px;
        letter-spacing:'.$h3_font_letter_space.'px;
        font-weight: '.$h3_font_weight_bold.';
    }
    h4{
        font-size:'.$h4_title_font_size.'px;
        line-height:'.$lineheight_h4.'px;
        letter-spacing:'.$h4_font_letter_space.'px;
        font-weight: '.$h4_font_weight_bold.';
    }
    h5{
        font-size:'.$h5_title_font_size .'px;
        line-height:'. $lineheight_h5 .'px;
        letter-spacing:'.$h5_font_letter_space.'px;
        font-weight: '.$h5_font_weight_bold.';
    }
    h6{
        font-size:'.$h6_title_font_size.'px;
        line-height:'.$lineheight_h6.'px;
        letter-spacing:'.$h6_font_letter_space.'px;
        font-weight: '.$h6_font_weight_bold.';
    }';

	// Text Logo Settings
	$css .= '#logo h1.site-title a{
			color:'.$text_logo_tagline_color.';
		}
		#logo p{
			color:'.$text_logo_color.';
		}';
	// Header Color Section
	$css .='#kaya-header-content-wrapper{
		background:'.$header_bg_color.';
		border-top:5px solid '.$header_top_border_color.';
	}';
	// Menu  Color settings	
	$css .= '#header-navigation ul li a{
				color:'.$menu_link_color.';
			}
			#header-navigation ul li > a:hover{
				color:'.$menu_link_hover_color.';
				border-top:3px solid '.$menu_link_hover_top_border_color.';
			}
			#header-navigation ul li.current-menu-item.current_page_item > a, #header-navigation .current-menu-ancestor.current-menu-parent.current_page_parent > a,  #header-navigation .current-menu-ancestor.current-menu-parent > a{
				color:'.$menu_active_link_color.';
				border-top:3px solid '.$menu_active_border_top_color.';
			}
			#header-navigation ul ul li a,
			ul.filter_submenu,
			#header-navigation #user-dashboard-menu li a{
				background:'.$child_menu_bg_color.';
			}
			#header-navigation ul ul li a, ul.filter_submenu a,
			#header-navigation #user-dashboard-menu li a{
				color:'.$child_menu_link_color.';
			}
			ul.filter_submenu a{
				color:'.$child_menu_link_color.'!important;
			}
			#header-navigation ul ul li a:hover,
			ul.filter_submenu li a:hover,
			#header-navigation #user-dashboard-menu li a:hover{
				background:'.$child_menu_hover_bg_color.';
				color:'.$child_menu_link_hover_color.';
			}
			#header-navigation ul ul li.current-menu-item.current_page_item > a, .filter_submenu a.active{
				color:'.$child_menu_active_link_color.';
				background:'.$child_menu_active_bg_color.';
			}
			
			#header-navigation{
				padding-top:'.$menu_padding_top.'px!important;
			}';

	// Page title  Color settings			
	$css .= '.kaya-page-titlebar-wrapper{
				background:'.$page_titlebar_bg_color.';
			}
			.kaya-page-titlebar-wrapper .page-title{
				color:'.$page_titlebar_color.';
			}';

	// Page Mid Content	 Color settings		
	$css .= '#kaya-mid-content-wrapper{
				background:'.$page_mid_contant_bg_color.';
				color:'.$page_mid_content_color.';
			}
			#kaya-mid-content-wrapper h1, #kaya-mid-content-wrapper h2, #kaya-mid-content-wrapper h3, #kaya-mid-content-wrapper h4, #kaya-mid-content-wrapper h5, #kaya-mid-content-wrapper h6,
			#kaya-mid-content-wrapper h1 a, #kaya-mid-content-wrapper h2 a, #kaya-mid-content-wrapper h3 a, #kaya-mid-content-wrapper h4 a, #kaya-mid-content-wrapper h5 a, #kaya-mid-content-wrapper h6 a{
				color:'.$page_mid_content_title_color.';
			}
			#kaya-mid-content-wrapper a{
				color:'.$page_mid_contant_links_color.';
			}
			#kaya-mid-content-wrapper a:hover{
				color:'.$page_mid_contant_links_hover_color.';
			}';

	// Page sidebar Color settings	
	$css .= '#sidebar{
				background:'.$sidebar_bg_color.';
				color:'.$sidebar_content_color.';
			}
			#sidebar h1, #sidebar h2, #sidebar h3, #sidebar h4, #sidebar h5, #sidebar h6{
				color:'.$sidebar_title_color.';
			}
			#sidebar a{
				color:'.$sidebar_links_color.';
			}
			#sidebar a:hover{
				color:'.$sidebar_links_hover_color.';
			}';

	// Talent Color settings	
	$css .= '.talents_details .general-meta-fields-info-wrapper{
				background:'.$talent_details_bg.';				
				overflow:hidden!important;
			}
			.talents_details .general-meta-fields-info-wrapper ul li{
				color:'.$talent_details_font_color.'!important;
			}

			.post_single_page_details h3{
				color:'.$talent_single_page_title_color.'!important;
			}
			.general-meta-fields-info-wrapper ul li{
				color:'.$talent_single_page_details_color.';
				border-bottom:1px solid '.$talent_single_page_border_color.';
			}
			.single-page-meta-content-wrapper h3{
				border-bottom:3px solid'.$talent_title_border_color.';
			}';

	// Advance Search Color settings	
	$css .= '#flip{
				background:'.$advance_search_icon_bg.';
				color:'.$advance_search_icon_color.';
			}
			#panel{
				background:'.$advance_search_bg.';
				border-bottom:3px solid '.$advance_search_bottom_border.';
			}
			.uform_title{
				background:'.$advance_search_title_bg.'!important;
				color:'.$advance_search_title_color.';
			}
			.uwpqsf_class select{
				background:'.$advance_search_select_bg_color.';
				border:1px solid '.$advance_search_select_border_color.';
				color:'.$advance_search_select_font_color.';
			}
			#uwpqsf_id button, #uwpqsf_id input[type="button"], #uwpqsf_id input[type="reset"], #uwpqsf_id input[type="submit"]{
				background:'.$advance_search_button_bg_color.';
				border:1px solid '.$advance_search_button_border_color.';
				color:'.$advance_search_button_font_color.';
			}';		
	

	// Footer Color settings	
	$css .= '#kaya-footer-content-wrapper{
				background:'.$footer_bg_color.';
				color:'.$footer_content_color.';
			}
			#kaya-footer-content-wrapper h1, #kaya-footer-content-wrapper h2, #kaya-footer-content-wrapper h3, #kaya-footer-content-wrapper h4, #kaya-footer-content-wrapper h5, #kaya-footer-content-wrapper h6{
				color:'.$footer_titles_color.';
			}
			#kaya-footer-content-wrapper a{
				color:'.$footer_link_color.';
			}
			#kaya-footer-content-wrapper a:hover{
				color:'.$footer_link_hover_color.';
			}';	

	// Footer Footer Color settings	
	$css .= '.kaya-page-content-footer{
				background:'.$page_footer_bg_color.';
				color:'.$page_footer_content_color.';
			}
			.kaya-page-content-footer p, .kaya-page-content-footer span {
				color:'.$page_footer_content_color.';
			}
			.kaya-page-content-footer a{
				color:'.$page_footer_link_color.';
			}
			.kaya-page-content-footer a:hover{
				color:'.$page_footer_link_hover_color.';
			}';				
	// End Styles
			
	$css = preg_replace( '/\s+/', ' ', $css ); 
    echo "<style type=\"text/css\">\n" .trim( $css ). "\n</style>";	
}
add_action('wp_head', 'casting_kaya_customizer_styles');
?>