<?php
/**
 * casting Theme Customizer
 *
 * @package casting
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */

require_once get_template_directory() . '/inc/customizer/customizer-styles.php'; //header functions
get_template_part( 'inc/customizer/customizer-controles');
get_template_part( 'inc/customizer/customize-import-export-settings');

/**
 * Remove wordpress default panels and sections from theme customizer
 */
add_action( "customize_register", "casting_kaya_remove_customize_register" );
function casting_kaya_remove_customize_register( $wp_customize ) {
	$wp_customize->remove_control("header_image");
	$wp_customize->remove_section("colors");
	$wp_customize->remove_section("background_image");
	$wp_customize->remove_section("static_front_page");
	$wp_customize->remove_control('display_header_text');
}

/**
 * Change default wordpress "site identity" section name our our new name
 */
function casting_kaya_change_default_logo_title( $wp_customize ) {
    $wp_customize->get_section( 'title_tagline' )->title = __('Header Settings', 'casting');
}
add_action( 'customize_register', 'casting_kaya_change_default_logo_title', 1000 );

/**
 * Header Section 
 */
add_action( 'customize_register', 'casting_kaya_header_customize_register' );
function casting_kaya_header_customize_register($wp_customize) {
	$wp_customize->add_panel( 'kaya_header_panel_section', array(
		'priority'       => 10,
		'capability'     => 'edit_theme_options',
		'theme_supports' => '',
		'title'          => __('Header Section', 'casting'),
	) );
	$wp_customize->add_section( 'title_tagline', array(
		'panel'		=> 'kaya_header_panel_section',
	    'title'     => __( 'Header Settings', 'casting' ),
	    'priority'  => 1,
	));

	$wp_customize->add_setting( 'header_bg_color' , array(
	    'default'     => '#fff',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'header_bg_color', array(
		'label'        => __( 'Header Background Color', 'casting' ),
		'section'    => 'title_tagline',
		'settings'   => 'header_bg_color',
		'priority'  => 0,
	) ) );

	$wp_customize->add_setting( 'header_top_border_color' , array(
	    'default'     => '#e63f19',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'header_top_border_color', array(
		'label'        => __( 'Header Top Border Color', 'casting' ),
		'section'    => 'title_tagline',
		'settings'   => 'header_top_border_color',
		'priority'  => 1,
	) ) );

	$wp_customize->add_setting( 'choose_logo', array(
	    'default'        => 'img_logo',
	    'capability'     => 'edit_theme_options',
	     'sanitize_callback' => 'wp_filter_nohtml_kses',
	) );
	$wp_customize->add_control('choose_logo', array(
	    'label'    => __( 'Choose Logo', 'casting' ),
	    'section'  => 'title_tagline',
	    'settings' => 'choose_logo',
	    'type' => 'select',
	    'choices' => array(
	    		'img_logo' => __('Image Logo', 'casting'),
	    		'text_logo' => __('Text Logo', 'casting'),
	    	),
	    'priority' => '1'
	) );
	// Upload header Logo Image
	$wp_customize->add_setting( 'logo_image', array(
		'type' => 'theme_mod',
	    'default'        => '',
	    'capability'     => 'edit_theme_options',
	    'transport'   => 'postMessage',
	    'sanitize_callback' => 'esc_url_raw'
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo_image', array(
	    'label'    => __( 'Logo', 'casting' ),
	    'section'  => 'title_tagline',
	    'settings' => 'logo_image',
	    'priority' => '2'
	) ) );

	// Text Logo Colors
	$wp_customize->add_setting( 'text_logo_color' , array(
	    'default'     => '#353535',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'text_logo_color', array(
		'label'        => __( 'Site Title color', 'casting' ),
		'section'    => 'title_tagline',
		'settings'   => 'text_logo_color',
	) ) );

	$wp_customize->add_setting( 'text_logo_tagline_color' , array(
	    'default'     => '#757575',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'text_logo_tagline_color', array(
		'label'        => __( 'Tag Line color', 'casting' ),
		'section'    => 'title_tagline',
		'settings'   => 'text_logo_tagline_color',
	) ) );
}

/**
 * Menu Section 
 */
add_action( 'customize_register', 'casting_kaya_menu_customize_register' );
function casting_kaya_menu_customize_register($wp_customize) {
	$wp_customize->add_section( 'kaya_menu_section', array(
		'panel'		=> 'kaya_header_panel_section',
	    'title'     => __( 'Menu Settings', 'casting' ),
	    'priority'  => 35,
	));
	$wp_customize->add_setting( 'menu_link_color' , array(
	    'default'     => '#333',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'menu_link_color', array(
		'label'        => __( 'Menu Links color', 'casting' ),
		'section'    => 'kaya_menu_section',
		'settings'   => 'menu_link_color',
	) ) );
	$wp_customize->add_setting( 'menu_link_hover_color' , array(
	    'default'     => '#e63f19',
	   'transport'   => 'postMessage',
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'menu_link_hover_color', array(
		'label'        => __( 'Menu Links Hover color', 'casting' ),
		'section'    => 'kaya_menu_section',
		'settings'   => 'menu_link_hover_color',
	) ) );
	
	$wp_customize->add_setting( 'menu_link_hover_top_border_color' , array(
	    'default'     => '#e63f19',
	   'transport'   => 'postMessage',
	   'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'menu_link_hover_top_border_color', array(
		'label'        => __( 'Menu Links Hover Top Border color', 'casting' ),
		'section'    => 'kaya_menu_section',
		'settings'   => 'menu_link_hover_top_border_color',
	) ) );

	$wp_customize->add_setting( 'menu_active_link_color' , array(
	    'default'     => '#e63f19',
	   'transport'   => 'postMessage',
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'menu_active_link_color', array(
		'label'        => __( 'Menu Links Active color', 'casting' ),
		'section'    => 'kaya_menu_section',
		'settings'   => 'menu_active_link_color',
	) ) );

	$wp_customize->add_setting( 'menu_active_border_top_color' , array(
	    'default'     => '#e63f19',
	   'transport'   => 'postMessage',
	   'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'menu_active_border_top_color', array(
		'label'        => __( 'Menu Active Border Top color', 'casting' ),
		'section'    => 'kaya_menu_section',
		'settings'   => 'menu_active_border_top_color',
	) ) );

	$wp_customize->add_setting( 'child_menu_bg_color' , array(
	    'default'     => '#1a1a1a',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'child_menu_bg_color', array(
		'label'        => __( 'Child Menu Background color', 'casting' ),
		'section'    => 'kaya_menu_section',
		'settings'   => 'child_menu_bg_color',
	) ) );
	$wp_customize->add_setting( 'child_menu_link_color' , array(
	    'default'     => '#fff',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'child_menu_link_color', array(
		'label'        => __( 'Child Menu links color', 'casting' ),
		'section'    => 'kaya_menu_section',
		'settings'   => 'child_menu_link_color',
	) ) );
	$wp_customize->add_setting( 'child_menu_hover_bg_color' , array(
	    'default'     => '#454545',
	   'transport'   => 'postMessage',
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'child_menu_hover_bg_color', array(
		'label'        => __( 'Child Menu Hover Background color', 'casting' ),
		'section'    => 'kaya_menu_section',
		'settings'   => 'child_menu_hover_bg_color',
	) ) );
	
	$wp_customize->add_setting( 'child_menu_link_hover_color' , array(
	    'default'     => '#fff',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'child_menu_link_hover_color', array(
		'label'        => __( 'Child Menu Links Hover color', 'casting' ),
		'section'    => 'kaya_menu_section',
		'settings'   => 'child_menu_link_hover_color',
	) ) );
	$wp_customize->add_setting( 'child_menu_active_bg_color' , array(
	    'default'     => '#454545',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'child_menu_active_bg_color', array(
		'label'        => __( 'Child Menu Active Background color', 'casting' ),
		'section'    => 'kaya_menu_section',
		'settings'   => 'child_menu_active_bg_color',
	) ) );
	
	$wp_customize->add_setting( 'child_menu_active_link_color' , array(
	    'default'     => '#fff',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'child_menu_active_link_color', array(
		'label'        => __( 'Child Menu Active Link color', 'casting' ),
		'section'    => 'kaya_menu_section',
		'settings'   => 'child_menu_active_link_color',
	) ) );

	$wp_customize->add_setting( 'menu_padding_top', array(
		'default'        => '10',
		'transport' => 'refresh',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
	) );
	$wp_customize->add_control( new casting_kaya_Customize_Sliderui_Control ( $wp_customize,'menu_padding_top', array(
		'label'   => __('Menu Padding Top','casting'),
		'section' => 'kaya_menu_section',
		'settings'    => 'menu_padding_top',
		'priority'    => 120,
		'choices'  => array(
				'min'  => 0,
				'max'  => 105,
				'step' => 1
			),
	)));
	
}

/**
 * Page Section
 */
add_action( 'customize_register', 'casting_kaya_page_customize_register' );
function casting_kaya_page_customize_register($wp_customize) {
	
	$wp_customize->add_panel( 'kaya_page_panel_section', array(
		'priority'       => 20,
		'capability'     => 'edit_theme_options',
		'theme_supports' => '',
		'title'          => __('Page Section', 'casting'),
	) );

	$wp_customize->add_section( 'kaya_page_section', array(
		'panel'		=> 'kaya_page_panel_section',
	    'title'     => __( 'Page Titlebar Settings', 'casting' ),
	    'priority'  => 35,
	));
	
	$wp_customize->add_setting( 'page_titlebar_bg_color' , array(
	    'default'     => '#dedede',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'page_titlebar_bg_color', array(
		'label'        => __( 'Page Titlebar color', 'casting' ),
		'section'    => 'kaya_page_section',
		'settings'   => 'page_titlebar_bg_color',
	) ) );

	$wp_customize->add_setting( 'page_titlebar_color' , array(
	    'default'     => '#353535',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'page_titlebar_color', array(
		'label'        => __( 'Page Titlebar Title color', 'casting' ),
		'section'    => 'kaya_page_section',
		'settings'   => 'page_titlebar_color',
	) ) );
	
}

/**
 * Page Middle Content
 */
add_action( 'customize_register', 'casting_kaya_page_mid_content_customize_register' );
function casting_kaya_page_mid_content_customize_register($wp_customize) {
	
	$wp_customize->add_section( 'kaya_mid_content_section', array(
		'panel'		=> 'kaya_page_panel_section',
	    'title'     => __( 'Page Middle Contant Settings', 'casting' ),
	    'priority'  => 35,
	));
	
	$wp_customize->add_setting( 'page_mid_contant_bg_color' , array(
	    'default'     => '#fff',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'page_mid_contant_bg_color', array(
		'label'        => __( 'Background color', 'casting' ),
		'section'    => 'kaya_mid_content_section',
		'settings'   => 'page_mid_contant_bg_color',
	) ) );

	$wp_customize->add_setting( 'page_mid_content_title_color' , array(
	    'default'     => '#353535',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'page_mid_content_title_color', array(
		'label'        => __( 'Titles color', 'casting' ),
		'section'    => 'kaya_mid_content_section',
		'settings'   => 'page_mid_content_title_color',
	) ) );
	$wp_customize->add_setting( 'page_mid_content_color' , array(
	    'default'     => '#787878',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'page_mid_content_color', array(
		'label'        => __( 'Content color', 'casting' ),
		'section'    => 'kaya_mid_content_section',
		'settings'   => 'page_mid_content_color',
	) ) );
	$wp_customize->add_setting( 'page_mid_contant_links_color' , array(
	    'default'     => '#333',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'page_mid_contant_links_color', array(
		'label'        => __( 'Links color', 'casting' ),
		'section'    => 'kaya_mid_content_section',
		'settings'   => 'page_mid_contant_links_color',
	) ) );

	$wp_customize->add_setting( 'page_mid_contant_links_hover_color' , array(
	    'default'     => '#353535',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'page_mid_contant_links_hover_color', array(
		'label'        => __( 'Links Hover color', 'casting' ),
		'section'    => 'kaya_mid_content_section',
		'settings'   => 'page_mid_contant_links_hover_color',
	) ) );
	
}

/**
 * Sidebar Color Settings
 */
add_action( 'customize_register', 'casting_kaya_sidebar_customize_register' );
function casting_kaya_sidebar_customize_register($wp_customize) {	
	$wp_customize->add_section( 'kaya_sidebar_section', array(
		'panel'		=> 'kaya_page_panel_section',
	    'title'     => __( 'Sidebar Contant Settings', 'casting' ),
	    'priority'  => 35,
	));
	
	$wp_customize->add_setting( 'sidebar_bg_color' , array(
	    'default'     => '',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sidebar_bg_color', array(
		'label'        => __( 'Background color', 'casting' ),
		'section'    => 'kaya_sidebar_section',
		'transport'   => 'postMessage',
	) ) );

	$wp_customize->add_setting( 'sidebar_title_color' , array(
	    'default'     => '#353535',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sidebar_title_color', array(
		'label'        => __( 'Titles color', 'casting' ),
		'section'    => 'kaya_sidebar_section',
		'settings'   => 'sidebar_title_color',
	) ) );

	$wp_customize->add_setting( 'sidebar_content_color' , array(
	    'default'     => '#787878',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sidebar_content_color', array(
		'label'        => __( 'Contant color', 'casting' ),
		'section'    => 'kaya_sidebar_section',
		'settings'   => 'sidebar_content_color',
	) ) );

	$wp_customize->add_setting( 'sidebar_links_color' , array(
	    'default'     => '#333',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sidebar_links_color', array(
		'label'        => __( 'Links color', 'casting' ),
		'section'    => 'kaya_sidebar_section',
		'settings'   => 'sidebar_links_color',
	) ) );

	$wp_customize->add_setting( 'sidebar_links_hover_color' , array(
	    'default'     => '#353535',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'sidebar_links_hover_color', array(
		'label'        => __( 'Links Hover color', 'casting' ),
		'section'    => 'kaya_sidebar_section',
		'settings'   => 'sidebar_links_hover_color',
	) ) );
}

/**
 * Talent Section
 */
add_action( 'customize_register', 'casting_kaya_talent_customize' );
function casting_kaya_talent_customize($wp_customize) {
	
	$wp_customize->add_panel( 'kaya_talent_panel_section', array(
		'priority'       => 25,
		'capability'     => 'edit_theme_options',
		'theme_supports' => '',
		'title'          => __('Talent Section', 'casting'),
	) );

	$wp_customize->add_section( 'kaya_talent_section', array(
		'panel'		=> 'kaya_talent_panel_section',
	    'title'     => __( 'Talent Settings', 'casting' ),
	    'priority'  => 35,
	));


	$wp_customize->add_setting( 'talent_details_bg' , array(
	    'default'     => '#e63f19',
	    'transport'   => '',
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'talent_details_bg', array(
		'label'        => __( 'Talent Details Bg color', 'casting' ),
		'section'    => 'kaya_talent_section',
		'settings'   => 'talent_details_bg',
	) ) );

	$wp_customize->add_setting( 'talent_details_font_color' , array(
	    'default'     => '#fff',
	    'transport'   => '',
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'talent_details_font_color', array(
		'label'        => __( 'Talent Details Font color', 'casting' ),
		'section'    => 'kaya_talent_section',
		'settings'   => 'talent_details_font_color',
	) ) );

	$wp_customize->add_section( 'kaya_talent_single_page_section', array(
		'panel'		=> 'kaya_talent_panel_section',
	    'title'     => __( 'Talent Single Page Settings', 'casting' ),
	    'priority'  => 36,
	));

	$wp_customize->add_setting( 'talent_single_page_title_color' , array(
	    'default'     => '#333',
	    'transport'   => '',
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'talent_single_page_title_color', array(
		'label'        => __( 'Talent Single Page Title color', 'casting' ),
		'section'    => 'kaya_talent_single_page_section',
		'settings'   => 'talent_single_page_title_color',
	) ) );

	$wp_customize->add_setting( 'talent_single_page_details_color' , array(
	    'default'     => '#666',
	    'transport'   => '',
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'talent_single_page_details_color', array(
		'label'        => __( 'Talent Single Page Detail color', 'casting' ),
		'section'    => 'kaya_talent_single_page_section',
		'settings'   => 'talent_single_page_details_color',
	) ) );

	$wp_customize->add_setting( 'talent_single_page_border_color' , array(
	    'default'     => '#e2e2e2',
	    'transport'   => '',
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'talent_single_page_border_color', array(
		'label'        => __( 'Talent Single Page Detail Border color', 'casting' ),
		'section'    => 'kaya_talent_single_page_section',
		'settings'   => 'talent_single_page_border_color',
	) ) );
	
	$wp_customize->add_setting( 'talent_title_border_color' , array(
	    'default'     => '#e63f19',
	    'transport'   => '',
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'talent_title_border_color', array(
		'label'        => __( 'Talent Single Page Title Border Color', 'casting' ),
		'section'    => 'kaya_talent_single_page_section',
		'settings'   => 'talent_title_border_color',
	) ) );
}

/**
 * Advance Search Section
 */
add_action( 'customize_register', 'casting_kaya_advance_search_customize_register' );
function casting_kaya_advance_search_customize_register($wp_customize) {
	$wp_customize->add_section( 'kaya_advance_search_section', array(
	    'title'     => __( 'Advance Search Settings', 'casting' ),
	    'priority'  => 35,
	));
	$wp_customize->add_setting( 'advance_search_icon_bg' , array(
	    'default'     => '#e63f19',
	    'transport'   => '',
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'advance_search_icon_bg', array(
		'label'        => __( 'Advance Search Icon Bg color', 'casting' ),
		'section'    => 'kaya_advance_search_section',
		'settings'   => 'advance_search_icon_bg',
	) ) );
	$wp_customize->add_setting( 'advance_search_icon_color' , array(
	    'default'     => '#fff',
	    'transport'   => '',
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'advance_search_icon_color', array(
		'label'        => __( 'Advance Search Icon color', 'casting' ),
		'section'    => 'kaya_advance_search_section',
		'settings'   => 'advance_search_icon_color',
	) ) );
	$wp_customize->add_setting( 'advance_search_bg' , array(
	    'default'     => '#fff',
	    'transport'   => '',
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'advance_search_bg', array(
		'label'        => __( 'Advance Search Bg color', 'casting' ),
		'section'    => 'kaya_advance_search_section',
		'settings'   => 'advance_search_bg',
	) ) );

	$wp_customize->add_setting( 'advance_search_bottom_border' , array(
	    'default'     => '#e63f19',
	    'transport'   => '',
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'advance_search_bottom_border', array(
		'label'        => __( 'Advance Search Bottom Border color', 'casting' ),
		'section'    => 'kaya_advance_search_section',
		'settings'   => 'advance_search_bottom_border',
	) ) );

	$wp_customize->add_setting( 'advance_search_title_bg' , array(
	    'default'     => '#e63f19',
	    'transport'   => '',
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'advance_search_title_bg', array(
		'label'        => __( 'Advance Search Title Bg color', 'casting' ),
		'section'    => 'kaya_advance_search_section',
		'settings'   => 'advance_search_title_bg',
	) ) );

	$wp_customize->add_setting( 'advance_search_title_color' , array(
	    'default'     => '#fff',
	    'transport'   => '',
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'advance_search_title_color', array(
		'label'        => __( 'Advance Search Title color', 'casting' ),
		'section'    => 'kaya_advance_search_section',
		'settings'   => 'advance_search_title_color',
	) ) );

	$wp_customize->add_setting( 'advance_search_select_bg_color' , array(
	    'default'     => '#f2f2f2',
	    'transport'   => '',
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'advance_search_select_bg_color', array(
		'label'        => __( 'Advance Search Select Bg color', 'casting' ),
		'section'    => 'kaya_advance_search_section',
		'settings'   => 'advance_search_select_bg_color',
	) ) );

	$wp_customize->add_setting( 'advance_search_select_border_color' , array(
	    'default'     => '#ccc',
	    'transport'   => '',
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'advance_search_select_border_color', array(
		'label'        => __( 'Advance Search Select Border color', 'casting' ),
		'section'    => 'kaya_advance_search_section',
		'settings'   => 'advance_search_select_border_color',
	) ) );

	$wp_customize->add_setting( 'advance_search_select_font_color' , array(
	    'default'     => '#333',
	    'transport'   => '',
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'advance_search_select_font_color', array(
		'label'        => __( 'Advance Search Select Font color', 'casting' ),
		'section'    => 'kaya_advance_search_section',
		'settings'   => 'advance_search_select_font_color',
	) ) );

	$wp_customize->add_setting( 'advance_search_button_bg_color' , array(
	    'default'     => '#e63f19',
	    'transport'   => '',
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'advance_search_button_bg_color', array(
		'label'        => __( 'Advance Search Button Bg color', 'casting' ),
		'section'    => 'kaya_advance_search_section',
		'settings'   => 'advance_search_button_bg_color',
	) ) );

	$wp_customize->add_setting( 'advance_search_button_border_color' , array(
	    'default'     => '#e63f19',
	    'transport'   => '',
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'advance_search_button_border_color', array(
		'label'        => __( 'Advance Search Button Border color', 'casting' ),
		'section'    => 'kaya_advance_search_section',
		'settings'   => 'advance_search_button_border_color',
	) ) );

	$wp_customize->add_setting( 'advance_search_button_font_color' , array(
	    'default'     => '#fff',
	    'transport'   => '',
	    'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'advance_search_button_font_color', array(
		'label'        => __( 'Advance Search Button Font color', 'casting' ),
		'section'    => 'kaya_advance_search_section',
		'settings'   => 'advance_search_button_font_color',
	) ) );

}

/**
 * Footer Section
 */
add_action( 'customize_register', 'casting_kaya_footer_customize_register' );
function casting_kaya_footer_customize_register($wp_customize) {
	
	$wp_customize->add_panel( 'kaya_footer_panel_section', array(
		'priority'       => 25,
		'capability'     => 'edit_theme_options',
		'theme_supports' => '',
		'title'          => __('Footer Section', 'casting'),
	) );

	$wp_customize->add_section( 'kaya_footer_section', array(
		'panel'		=> 'kaya_footer_panel_section',
	    'title'     => __( 'Footer Settings', 'casting' ),
	    'priority'  => 35,
	));
	$wp_customize->add_setting('footer_copy_rights', array(
		'default'  => __('Footer Copy Rights Text', 'casting'),
		 'transport'   => 'postMessage',
	));
	$wp_customize->add_control('footer_copy_rights', array(
		'label'   => __('Footer Copy Rights Text', 'casting'),
		'section' => 'kaya_footer_section',
		'type'    => 'textarea',
	));

	$wp_customize->add_setting( 'footer_bg_color' , array(
	    'default'     => '#353535',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_bg_color', array(
		'label'        => __( 'Background color', 'casting' ),
		'section'    => 'kaya_footer_section',
		'settings'   => 'footer_bg_color',
	) ) );

	$wp_customize->add_setting( 'footer_content_color' , array(
	    'default'     => '#bcbcbc',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_content_color', array(
		'label'        => __( 'Content color', 'casting' ),
		'section'    => 'kaya_footer_section',
		'settings'   => 'footer_content_color',
	) ) );

	$wp_customize->add_setting( 'footer_link_color' , array(
	    'default'     => '#bcbcbc',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_link_color', array(
		'label'        => __( 'Link color', 'casting' ),
		'section'    => 'kaya_footer_section',
		'settings'   => 'footer_link_color',
	) ) );

	$wp_customize->add_setting( 'footer_link_hover_color' , array(
	    'default'     => '#fff',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_link_hover_color', array(
		'label'        => __( 'Link Hover color', 'casting' ),
		'section'    => 'kaya_footer_section',
		'settings'   => 'footer_link_hover_color',
	) ) );	
}

/**
 * Page Footer
 */
add_action( 'customize_register', 'casting_kaya_page_footer' );
function casting_kaya_page_footer($wp_customize){

	$wp_customize->add_section( 'kaya_page_footer_section', array(
		'panel'		=> 'kaya_footer_panel_section',
	    'title'     => __( 'Page Footer Settings', 'casting' ),
	    'priority'  => 35,
	));

	$wp_customize->add_setting('main_footer_page', array(
		'default'  =>0,
		 'transport'   => '',
		 'sanitize_callback' => 'absint',
	));
	$wp_customize->add_control( 'main_footer_page', array(
	    'label'          => __( 'Page Footer', 'casting' ),
	    'section'        => 'kaya_page_footer_section',
	    'type'           => 'dropdown-pages',
	) );

	$wp_customize->add_setting( 'page_footer_bg_color' , array(
	    'default'     => '#151515',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'page_footer_bg_color', array(
		'label'        => __( 'Background color', 'casting' ),
		'section'    => 'kaya_page_footer_section',
		'settings'   => 'page_footer_bg_color',
	) ) );

	$wp_customize->add_setting( 'page_footer_content_color' , array(
	    'default'     => '#fff',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'page_footer_content_color', array(
		'label'        => __( 'Content color', 'casting' ),
		'section'    => 'kaya_page_footer_section',
		'settings'   => 'page_footer_content_color',
	) ) );

	$wp_customize->add_setting( 'page_footer_link_color' , array(
	    'default'     => '#f5f5f5',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'page_footer_link_color', array(
		'label'        => __( 'Link color', 'casting' ),
		'section'    => 'kaya_page_footer_section',
		'settings'   => 'page_footer_link_color',
	) ) );

	$wp_customize->add_setting( 'page_footer_link_hover_color' , array(
	    'default'     => '#fff',
	    'transport'   => 'postMessage',
	     'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'page_footer_link_hover_color', array(
		'label'        => __( 'Link Hover color', 'casting' ),
		'section'    => 'kaya_page_footer_section',
		'settings'   => 'page_footer_link_hover_color',
	) ) );	

}
/**
 * Blog Page Settings
 */
add_action( 'customize_register', 'casting_kaya_blog_page_settings' );
function casting_kaya_blog_page_settings($wp_customize) {
  $wp_customize->add_section( 'kaya_blog_page_section', array(
      'title'     => __( 'Blog page', 'casting' ),
      'priority'  => 5,
  ));
  $wp_customize->add_setting( 'blog_page_post_limit' , array(
      'default'     => '10',
       'sanitize_callback' => 'wp_filter_nohtml_kses',
  ) );
  $wp_customize->add_control('blog_page_post_limit',
  array(
      'label' => __('Posts Limit','casting'),
       'section'  => 'kaya_blog_page_section',
      'type' => 'text',
      'priority' => 1,
  ));

  // Blog Page Sidebar Position
  $wp_customize->add_setting( 'blog_page_sidebar_position', array(
      'default' => 'right',
      'sanitize_callback' => 'wp_filter_nohtml_kses',
  ));
  $wp_customize->add_control('blog_page_sidebar_position', array(
      'type' => 'select',
      'label' => __('Blog Page Sidebar Position','casting'),
      'section' => 'kaya_blog_page_section',
      'choices' => array(
          'right' => __('Right', 'casting'),
          'left' => __('Left', 'casting'),
          'none' => __('None', 'casting'),
        ),
  'priority' => 2,
  ));

}


// Typography
function casting_kaya_typography( $wp_customize ){
	global $kaya_julia_customze_note_settings;
	$wp_customize->add_panel( 'typography_panel_section', array(
	    'priority'       => 140,
	    'capability'     => 'edit_theme_options',
	    'theme_supports' => '',
	  	'title' => esc_html__( 'Typography Section', 'casting'),
	) );
	$wp_customize->add_section(
	// ID
	'typography_section',
	// Arguments array
	array(
		'title' => esc_html__( 'Google Font Family', 'casting'),
		'priority'       => 140,
		'capability' => 'edit_theme_options',
		'panel' => 'typography_panel_section',
	));	
	$wp_customize->add_setting( 'google_body_font',  array( 
		'default' => 'Nova Square',
    	'transport' => 'postMessage',
    	'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
	$wp_customize->add_control( new casting_kaya_Customize_google_fonts_Control( $wp_customize, 'google_body_font', array(
		'label'   => esc_html__('Select font for Body','casting'),
		'section' => 'typography_section',
		'settings'    => 'google_body_font',
		'priority'    => 0,
	)));
 	$wp_customize->add_setting( 'google_heading_font', array(
 		'default' => '2',
    	'transport' => 'postMessage',
    	'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
 	$wp_customize->add_control( new casting_kaya_Customize_google_fonts_Control( $wp_customize, 'google_heading_font', array(
		'label'   => esc_html__('Select font for Headings','casting'),
		'section' => 'typography_section',
		'settings'    => 'google_heading_font',
		'priority'    =>10,
	)));
	$wp_customize->add_setting( 'google_menu_font', array( 
		'default' => '2',
    	'transport' => 'postMessage',
    	'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
	$wp_customize->add_control( new casting_kaya_Customize_google_fonts_Control( $wp_customize, 'google_menu_font', array(
		'label'   => esc_html__('Select font for Menu','casting'),
		'section' => 'typography_section',
		'settings'    => 'google_menu_font',
		'priority'    => 20,
	))); 
}
add_action( 'customize_register', 'casting_kaya_typography' );
/* --------------------------------------------
Typography
-----------------------------------------------*/
function casting_kaya_font_panel_section( $wp_customize ){
	$wp_customize->add_section(
	// ID
	'font-panel-section',
	// Arguments array
	array(
		'title' => esc_html__( 'Font Settings', 'casting'),
		'priority'       => 140,
		'capability' => 'edit_theme_options',
		'panel' => 'typography_panel_section'
	));	
	$font_weight_names = array('normal' => 'Normal', 'bold' => 'Bold', 'lighter' => 'Lighter');	
	// Body Font Size
	$wp_customize->add_setting('body_font_size', array(
		'default' => '15',
    	'transport' => 'postMessage',
    	'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
 	$wp_customize->add_control( new casting_kaya_Customize_Sliderui_Control( $wp_customize, 'body_font_size', array(
		'label'   => esc_html__('Body Font Size','casting'),
		'section' => 'font-panel-section',
		'settings'    => 'body_font_size',
		'priority'    => 3,
		'choices'  => array(
			'min'  => 10,
			'max'  => 30,
			'step' => 1
		),
	)));
	$wp_customize->add_setting('body_font_letter_space', array(
		'default' => '0',
    	'transport' => 'postMessage',
    	'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
 	$wp_customize->add_control( new casting_kaya_Customize_Sliderui_Control( $wp_customize, 'body_font_letter_space', array(
		 'label'   => esc_html__('Body Font Letter Spacing','casting'),
		'section' => 'font-panel-section',
		'settings'    => 'body_font_letter_space',
		'priority'    => 4,
		'choices'  => array(
			'min'  => 0,
			'max'  => 30,
			'step' => 1
		),
	)));
	$wp_customize->add_setting( 'body_font_weight_bold', array(
        'default' => 'normal',
        'transport' => 'postMessage',
        'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
    $wp_customize->add_control('body_font_weight_bold', array(
        'type' => 'select',
        'label' => esc_html__('Select Body Font Weight','casting'),
        'section' => 'font-panel-section',
        'choices' => $font_weight_names,
		'priority' => 9,
    ));
	// Menu Font Size

	$wp_customize->add_setting('menu_font_size',array(
		'default' => '15',
    	'transport' => 'postMessage',
    	'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
 	$wp_customize->add_control( new casting_kaya_Customize_Sliderui_Control( $wp_customize, 'menu_font_size', array(
		 'label'   => esc_html__('Menu Font Size','casting'),
		'section' => 'font-panel-section',
		'settings'    => 'menu_font_size',
		'priority'    => 11,
		'choices'  => array(
			'min'  => 10,
			'max'  => 30,
			'step' => 1
		),
	)));
 	$wp_customize->add_setting('menu_font_letter_space', array( 
 		'default' => '0',
    	'transport' => 'postMessage',
    	'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
 	$wp_customize->add_control( new casting_kaya_Customize_Sliderui_Control( $wp_customize, 'menu_font_letter_space', array(
		 'label'   => esc_html__('Menu Font Letter Spacing','casting'),
		'section' => 'font-panel-section',
		'settings'    => 'menu_font_letter_space',
		'priority'    => 20,
		'choices'  => array(
			'min'  => 0,
			'max'  => 30,
			'step' => 1
		),
		)));
	$wp_customize->add_setting( 'menu_font_weight', array(
        'default' => 'normal',
        'transport' => 'postMessage',
       'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
    $wp_customize->add_control('menu_font_weight', array(
        'type' => 'select',
        'label' => esc_html__('Select Menu Font Weight','casting'),
        'section' => 'font-panel-section',
        'choices' => $font_weight_names,
		'priority' => 21,
    ));
    $wp_customize->add_setting( 'main_menu_uppercase', array(
		'default'        => 0,
		//'type'           => 'option',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
		'capability'     => 'edit_theme_options' )
	);
	$wp_customize->add_control('main_menu_uppercase', array(
		'label'    => esc_html__( 'Enable Uppercase Letters ','casting'),
		'section'  => 'font-panel-section',
		'type'     => 'checkbox',
		'priority' => 22
	) );
    
	// Menu Font Size
	$wp_customize->add_setting('child_menu_font_size', array(
		'default' => '13',
    	'transport' => 'postMessage',
    	'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
 	$wp_customize->add_control( new casting_kaya_Customize_Sliderui_Control( $wp_customize, 'child_menu_font_size', array(
		 'label'   => esc_html__('Child Menu Font Size','casting'),
		'section' => 'font-panel-section',
		'settings'    => 'child_menu_font_size',
		'priority'    => 60,
		'choices'  => array(
			'min'  => 10,
			'max'  => 30,
			'step' => 1
		),
		)));
  	$wp_customize->add_setting('child_menu_font_letter_space',array(
  		'default' => '0',
    	'transport' => 'postMessage',
    	'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
 	$wp_customize->add_control( new casting_kaya_Customize_Sliderui_Control( $wp_customize, 'child_menu_font_letter_space', array(
		 'label'   => esc_html__('Child Menu Font Letter Spacing','casting'),
		'section' => 'font-panel-section',
		'settings'    => 'child_menu_font_letter_space',
		'priority'    => 70,
		'choices'  => array(
			'min'  => 0,
			'max'  => 30,
			'step' => 1
		),
		)));
 	$wp_customize->add_setting( 'child_menu_font_weight', array(
        'default' => 'normal',
        'transport' => 'postMessage',
       'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
    $wp_customize->add_control('child_menu_font_weight', array(
        'type' => 'select',
        'label' => esc_html__('Select Child Menu Font Weight','casting'),
        'section' => 'font-panel-section',
        'choices' => $font_weight_names,
		'priority' => 80,
    ));
    $wp_customize->add_setting( 'child_menu_uppercase', array(
		'default'        => 0,
		//'type'           => 'option',
		'transport' => 'postMessage',
		'sanitize_callback' => 'wp_filter_nohtml_kses',
		'capability'     => 'edit_theme_options' )
	);
	$wp_customize->add_control('child_menu_uppercase', array(
		'label'    => esc_html__( 'Enable Uppercase Letters ','casting'),
		'section'  => 'font-panel-section',
		'type'     => 'checkbox',
		'priority' => 90
	) );
	// Title Font Sizes

	// H1
	$wp_customize->add_setting('h1_title_fontsize', array(
		'default' => '30',
    	'transport' => 'postMessage',
    	'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
 	$wp_customize->add_control( new casting_kaya_Customize_Sliderui_Control( $wp_customize, 'h1_title_fontsize', array(
		'label'   => esc_html__('Font size for heading - H1','casting'),
		'section' => 'font-panel-section',
		'settings'    => 'h1_title_fontsize',
		'priority'    => 105,
		'choices'  => array(
			'min'  => 10,
			'max'  => 80,
			'step' => 1
		),
		)));
  	$wp_customize->add_setting('h1_font_letter_space',
    array( 'default' => '0',
    	'transport' => 'postMessage',
    	'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
 	$wp_customize->add_control( new casting_kaya_customIze_slideRui_control( $wp_customize, 'h1_font_letter_space', array(
		 'label'   => esc_html__('Font Letter Spacing - H1','casting'),
		'section' => 'font-panel-section',
		'settings'    => 'h1_font_letter_space',
		'priority'    => 110,
		'choices'  => array(
			'min'  => 0,
			'max'  => 30,
			'step' => 1
		),
		)));
	$wp_customize->add_setting( 'h1_font_weight_bold', array(
        'default' => 'normal',
        'transport' => 'postMessage',
        'sanitize_callback' => 'wp_filter_nohtml_kses',

    ));
    $wp_customize->add_control('h1_font_weight_bold', array(
        'type' => 'select',
        'label' => esc_html__('Select Font Weight - H1','casting'),
        'section' => 'font-panel-section',
        'choices' => $font_weight_names,
		'priority' => 120,
    ));

	// H2
	$wp_customize->add_setting('h2_title_fontsize',array(
    	 'default' => '24',
    	'transport' => 'postMessage',
    	'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
 	$wp_customize->add_control( new casting_kaya_Customize_Sliderui_Control( $wp_customize, 'h2_title_fontsize', array(
		'label'   => esc_html__('Font size for heading - H2','casting'),
		'section' => 'font-panel-section',
		'settings'    => 'h2_title_fontsize',
		'priority'    => 140,
		'choices'  => array(
			'min'  => 10,
			'max'  => 80,
			'step' => 1
		),
		)));
  	$wp_customize->add_setting('h2_font_letter_space', array( 
  		'default' => '0',
    	'transport' => 'postMessage',
    	'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
 	$wp_customize->add_control( new casting_kaya_Customize_Sliderui_Control( $wp_customize, 'h2_font_letter_space', array(
		 'label'   => esc_html__('Font Letter Spacing - H2','casting'),
		'section' => 'font-panel-section',
		'settings'    => 'h2_font_letter_space',
		'priority'    => 150,
		'choices'  => array(
			'min'  => 0,
			'max'  => 30,
			'step' => 1
		),
		)));
	$wp_customize->add_setting( 'h2_font_weight_bold', array(
        'default' => 'normal',
        'transport' => 'postMessage',
        'sanitize_callback' => 'wp_filter_nohtml_kses',

    ));
    $wp_customize->add_control('h2_font_weight_bold', array(
        'type' => 'select',
        'label' => esc_html__('Select Font Weight - H2','casting'),
        'section' => 'font-panel-section',
        'choices' => $font_weight_names,
		'priority' => 160,
    ));        	 
	// H3
	$wp_customize->add_setting('h3_title_fontsize',array( 
		'default' => '20',
    	'transport' => 'postMessage',
    	'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
 	$wp_customize->add_control( new casting_kaya_Customize_Sliderui_Control( $wp_customize, 'h3_title_fontsize', array(
		 'label'   => esc_html__('Font size for heading - H3','casting'),
		'section' => 'font-panel-section',
		'settings'    => 'h3_title_fontsize',
		'priority'    => 180,
		'choices'  => array(
			'min'  => 10,
			'max'  => 80,
			'step' => 1
		),
		)));
  	$wp_customize->add_setting('h3_font_letter_space', array(
  		'default' => '2',
    	'transport' => 'postMessage',
    	'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
 	$wp_customize->add_control( new casting_kaya_Customize_Sliderui_Control( $wp_customize, 'h3_font_letter_space', array(
		 'label'   => esc_html__('Font Letter Spacing - H3','casting'),
		'section' => 'font-panel-section',
		'settings'    => 'h3_font_letter_space',
		'priority'    => 190,
		'choices'  => array(
			'min'  => 0,
			'max'  => 30,
			'step' => 1
		),
		)));
	$wp_customize->add_setting( 'h3_font_weight_bold', array(
        'default' => 'bold',
        'transport' => 'postMessage',
        'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
    $wp_customize->add_control('h3_font_weight_bold', array(
        'type' => 'select',
        'label' => esc_html__('Select Font Weight - H3','casting'),
        'section' => 'font-panel-section',
        'choices' => $font_weight_names,
		'priority' => 200,
    ));
	$wp_customize->add_setting( 'h4_title_fontsize', array(
		'default' => '18',
    	'transport' => 'postMessage',
    	'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
 	$wp_customize->add_control( new casting_kaya_Customize_Sliderui_Control( $wp_customize, 'h4_title_fontsize', array(
		'label'   => esc_html__('Font size for heading - H4','casting'),
		'section' => 'font-panel-section',
		'settings'    => 'h4_title_fontsize',
		'priority'    => 220,
		'choices'  => array(
			'min'  => 10,
			'max'  => 80,
			'step' => 1
		),
		)));
  	$wp_customize->add_setting('h4_font_letter_space', array(
  		'default' => '0',
    	'transport' => 'postMessage',
    	'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
 	$wp_customize->add_control( new casting_kaya_Customize_Sliderui_Control( $wp_customize, 'h4_font_letter_space', array(
		 'label'   => esc_html__('Font Letter Spacing - H4','casting'),
		'section' => 'font-panel-section',
		'settings'    => 'h4_font_letter_space',
		'priority'    => 230,
		'choices'  => array(
			'min'  => 0,
			'max'  => 30,
			'step' => 1
		),
		))); 
	$wp_customize->add_setting( 'h4_font_weight_bold', array(
        'default' => 'normal',
        'transport' => 'postMessage',
        'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
    $wp_customize->add_control('h4_font_weight_bold', array(
        'type' => 'select',
        'label' => esc_html__('Select Font Weight - H4','casting'),
        'section' => 'font-panel-section',
        'choices' => $font_weight_names,
		'priority' => 240,
    ));
	// H5
	$wp_customize->add_setting('h5_title_fontsize', array( 
		'default' => '16',
    	'transport' => 'postMessage',
    	'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
	$wp_customize->add_control( new casting_kaya_Customize_Sliderui_Control( $wp_customize, 'h5_title_fontsize', array(
		'label'   => esc_html__('Font size for heading - H5','casting'),
		'section' => 'font-panel-section',
		'settings'    => 'h5_title_fontsize',
		'priority'    => 260,
		'choices'  => array(
			'min'  => 10,
			'max'  => 80,
			'step' => 1
		),
	)));
   	$wp_customize->add_setting('h5_font_letter_space',array(
   		'default' => '0',
    	'transport' => 'postMessage',
    	'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
 	$wp_customize->add_control( new casting_kaya_Customize_Sliderui_Control( $wp_customize, 'h5_font_letter_space', array(
		 'label'   => esc_html__('Font Letter Spacing - H5','casting'),
		'section' => 'font-panel-section',
		'settings'    => 'h5_font_letter_space',
		'priority'    => 270,
		'choices'  => array(
			'min'  => 0,
			'max'  => 30,
			'step' => 1
		),
	)));
	$wp_customize->add_setting( 'h5_font_weight_bold', array(
        'default' => 'normal',
        'transport' => 'postMessage',
        'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
    $wp_customize->add_control('h5_font_weight_bold', array(
        'type' => 'select',
        'label' => esc_html__('Select Font Weight - H5','casting'),
        'section' => 'font-panel-section',
        'choices' => $font_weight_names,
		'priority' => 280
    ));	 


	// H6
	$wp_customize->add_setting('h6_title_fontsize', array( 
		'default' => '14',
	    'transport' => 'postMessage',
	    'sanitize_callback' => 'wp_filter_nohtml_kses',
	 ));
 	$wp_customize->add_control( new casting_kaya_Customize_Sliderui_Control( $wp_customize, 'h6_title_fontsize', array(
		'label'   => esc_html__('Font size for heading - H6','casting'),
		'section' => 'font-panel-section',
		'settings'    => 'h6_title_fontsize',
		'priority'    => 300,
		'choices'  => array(
			'min'  => 10,
			'max'  => 80,
			'step' => 1
		),
	)));
    $wp_customize->add_setting('h6_font_letter_space', array(
    	'default' => '0',
    	'transport' => 'postMessage',
    	'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
 	$wp_customize->add_control( new casting_kaya_Customize_Sliderui_Control( $wp_customize, 'h6_font_letter_space', array(
		'label'   => esc_html__('Font Letter Spacing - H6','casting'),
		'section' => 'font-panel-section',
		'settings'    => 'h6_font_letter_space',
		'priority'    => 310,
		'choices'  => array(
			'min'  => 0,
			'max'  => 30,
			'step' => 1
		),
		)));
	$wp_customize->add_setting( 'h6_font_weight_bold', array(
        'default' => 'normal',
        'transport' => 'postMessage',
        'sanitize_callback' => 'wp_filter_nohtml_kses',
    ));
    $wp_customize->add_control('h6_font_weight_bold', array(
        'type' => 'select',
        'label' => esc_html__('Select Font Weight - H6','casting'),
        'section' => 'font-panel-section',
        'choices' => $font_weight_names,
		'priority' => 320,
    ));	 
}
add_action( 'customize_register', 'casting_kaya_font_panel_section' );