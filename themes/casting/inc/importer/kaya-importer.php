<?php 
if( !class_exists('casting_Kaya_Demo_Import') ){
    class casting_Kaya_Demo_Import
    {
        function __construct() {
            if ( ! is_admin() ) {
                return;
            }
            add_action('admin_menu', array(&$this, 'casting_kaya_admin_option_page'),15);
        }
        /**
         * Importing XML Demo content
         */
        function casting_kaya_admin_option_page(){
            add_theme_page( 'Theme Option', 'One Click Demo', 'manage_options', 'one_click_import.php',  array(&$this,'casting_kaya_import_xml_content'));  
            remove_submenu_page( 'kaya_theme_options', 'kaya_theme_options' );
        }
        function casting_kaya_import_xml_content(){
             echo '<div class="kaya_demo_note"><h3><strong style="color:red;">'. __('WARNING','casting').' : </strong>'.__('</h3>'.'<h3>1. Before importing demo content make sure that you have installed and activated  theme required plugins. </h3>', 'casting').'</div>'; 
              $pods = pods_api()->load_pods( array( 'names' => true ) );
                ?>

            <div id="import_xml_content_wrapper">            
                <div class="content_loading_wrapper">
                    <img class="content_loading" style="display:none" src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.GIF" class="" /><div class="import_message"> </div>
                </div>
                <span class="clear"> </span>

                <label>
                   <input type="radio" name="demo_content" id="demo_content" value="demo_content" checked />
                    <img src="<?php echo get_template_directory_uri().'/images/demo_content.jpg'; ?> " />
                </label>
                <?php 
                    if( empty($pods) ){ ?>
                      <input type="button" class="xml_demo_import button button-primary button-large" name="pods_content" value="<?php echo _e( 'Step - 1 Install Demo Configuration Data', 'casting'); ?>" />
                    <?php }else{ ?>
                        <input type="button" class="xml_demo_import button button-primary button-large" name="demo_content" value="<?php echo _e( 'Final Step - Import Demo Data', 'casting'); ?>" />
                    <?php } ?>  
                <?php
                echo '<div class="clear"> </div>';
                echo '</div>';
                $css ='';
                $css .='.content_loading_wrapper {
                        margin-top: 30px;
                    }
                    .content_loading_wrapper img {
                        float: left;
                        margin-right: 15px;
                    }
                    label > input{ 
                      visibility: hidden;
                      position: absolute;
                    }
                    #import_xml_content_wrapper label {
                       float: left;
                        margin-bottom: 30px;
                        margin-right: 1.5%;
                        overflow: hidden;
                        text-align: center;
                        width: 23.5%;
                    }
                    .xml_demo_import {
                        float: left;
                        margin-bottom: 30px !important;
                       margin-right: 1.5% !important;
                    }
                    .kaya_demo_note {
                        border: 1px solid #e5e5e5;
                        margin-bottom: 30px;
                        padding: 15px;
                    }
                    .import_message{
                        margin-top: 30px;
                        margin-bottom: 30px;
                    }
                    .import_message .updated{
                        margin-left:0; 
                    }
                    #import_xml_content_wrapper label img {
                        background: none repeat scroll 0 0 #f5f5f5;
                        border: 1px solid #dfdfdf;
                        display: table;
                        margin-right: 16px;
                        overflow: hidden;
                        padding: 6px;
                        width: 95%;
                       float:left;
                    }
                    .wp-core-ui .button.button-large{
                        clear:both;
                    }
                    label > input:checked + img {
                        border: 1px solid #2ea2cc!important;
                    }';
                $css = preg_replace( '/\s+/', ' ', $css );
                $output = "<style type=\"text/css\">\n" . $css . "\n</style>";
                echo $output;  ?>     
                 <script>
                    (function($) {
                    "use strict";
                         $("input[type='button']").click(function(){
                            var $action;
                            var $import_options = $(this).attr('name');
                            if( $import_options == 'pods_content'){
                               $action = 'casting_kaya_pods_content_import';
                            }else{
                               $action = 'casting_kaya_demo_xml_content_import';
                            }
                            if( $import_options == undefined){
                                alert('Please Choose Your Demo Content');
                                return;
                            }
                            var $import_true = confirm('are you sure to import dummy content ? it will overwrite the existing data');
                            if($import_true == false) return;
                            $('.import_message').html(' Data is being imported please be patient, while the awesomeness is being created :)  ');
                            $('.content_loading').show();
                            $('.xml_demo_import').attr('disabled','disable');
                            $('html, body').animate({scrollTop: $("body").offset().top}, 400);
                            var data = {
                                action: $action,
                                xml: $import_options,     
                            };
                            $.post(ajaxurl, data, function(response) {                                
                                if( $import_options == 'pods_content'){
                                    window.setTimeout(function(){location.reload()},500);
                                }else{
                                     $('.content_loading').hide();
                                    $('.xml_demo_import').removeAttr('disabled');
                                }
                                $('.import_message').html('<div class="import_message_success">'+ response +'</div>');
                            });
                        });
                    })(jQuery);
                </script>
            <?php 
        }
    }
    $globel_options = new casting_Kaya_Demo_Import();
    add_action( 'wp_ajax_casting_kaya_demo_xml_content_import', 'casting_kaya_demo_xml_content_import' );
}
if( !function_exists('casting_kaya_demo_xml_content_import') ){
    function casting_kaya_demo_xml_content_import() 
    {    
        WP_Filesystem();
        global $wp_filesystem, $julia_plugin_name;
        if ( !defined('WP_LOAD_IMPORTERS') ) define('WP_LOAD_IMPORTERS', true);      
        // Load Importer API
        require_once ABSPATH . 'wp-admin/includes/import.php';
        if ( ! class_exists( 'WP_Importer' ) ) {
            $class_wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
            if ( file_exists( $class_wp_importer ) )
            {
                require $class_wp_importer;
            }
        }
        if ( ! class_exists( 'WP_Import' ) ) {
             
            $class_wp_importer = get_template_directory().'/inc/importer/wordpress-importer.php';
            //die();
            if ( file_exists( $class_wp_importer ) ){
                  // die('import');
                   require $class_wp_importer;
               }
        }
        //ob_start();
        if ( class_exists( 'WP_Import' ) ) 
        { 
            if( $_POST['xml'] ){
                $files = $_POST['xml'];
            }else{
                
            }
            // Importing Main Demo Content
            $import_filepath =  get_template_directory().'/inc/kaya-xml-files/demo_content.xml';
            $wp_import = new WP_Import();
            set_time_limit(0);
            $wp_import->fetch_attachments = false;
           
            $wp_import->import($import_filepath);
           
           // Customizer options 
            $file_name = $files.'.json'; // Get the name of file
            if( $file_name ){
                $file_path = explode('.', $file_name);
                $file_ext = end($file_path);  // Get extension of file
                if (($file_ext == "json")) {
                    $customizer_content = get_template_directory().'/inc/kaya-xml-files/'.$file_name;
                    $widgets = get_template_directory().'/inc/kaya-xml-files/widgets.json';
                    $pod_options = get_template_directory().'/inc/kaya-xml-files/pods-options.json';
                    $shortlist_options = get_template_directory().'/inc/kaya-xml-files/shortlist_options.json';
                    $reg_login_options = get_template_directory().'/inc/kaya-xml-files/reg_login_options.json';

                    // Widegts Info
                    $data = $wp_filesystem->get_contents( $widgets );
                    $data = json_decode($data );
                    casting_kaya_import_widgets( $data );
                    // Importing Pods CPT Views  Options Data 
                    if( $pod_options ){
                          $pod_data = $wp_filesystem->get_contents( $pod_options );
                         $pod_data= json_decode($pod_data );
                         $kaya_options = array();
                         foreach ($pod_data as $key => $opt_val) {
                            $kaya_options[$key] = $opt_val;
                            update_option( 'kaya_options', $kaya_options );
                         }
                    }
                    // Importing Shortlist list Data 
                    if( $shortlist_options ){
                          $shortlist_data = $wp_filesystem->get_contents( $shortlist_options );
                         $shortlist_data= json_decode($shortlist_data );
                         $kaya_shortlist_options = array();
                         foreach ($shortlist_data as $key => $opt_val) {
                            $kaya_shortlist_options[$key] = $opt_val;
                            update_option( 'kaya_shortlist_options', $kaya_shortlist_options );
                         }
                    }

                     // Importing reg login list Data 
                    if( $reg_login_options ){
                          $reg_login_data = $wp_filesystem->get_contents( $reg_login_options );
                         $reg_login_data= json_decode($reg_login_data );
                         $kaya_settings = array();
                         foreach ($reg_login_data as $key => $opt_val) {
                            $kaya_settings[$key] = $opt_val;
                            update_option( 'kaya_settings', $kaya_settings );
                         }
                    }

                    // Importing roles and capabilities
                    $roles_capabilities = get_template_directory().'/inc/kaya-xml-files/roles_capabilities.json';
                    $encode_options = $wp_filesystem->get_contents($roles_capabilities);
                    $roles_capabilities_data = json_decode($encode_options, true);
                    $kaya_roles_settings = array();
                    global $wpdb;
                    $wp_roles = $wpdb->prefix . 'user_roles';
                     foreach ($roles_capabilities_data as $key => $opt_val) {
                        $kaya_roles_settings[$key] = $opt_val;
                        update_option( $wp_roles, $kaya_roles_settings);
                     }
                     
                    // Importing Sliders
                    if (class_exists('SmartSlider3')) {
                       for ($i=1; $i < 12 ; $i++) { 
                           SmartSlider3::import(get_template_directory()."/smartslider-demo-content/slider-demo".$i.".ss3");
                       }
                    }
            
                    // Importing customizer Content
                    if( $customizer_content ){ // Customizer Data Read
                        $encode_options = $wp_filesystem->get_contents( $customizer_content);
                        $options = json_decode($encode_options, true);
                        foreach ($options as $key => $value) {
                            set_theme_mod($key, $value);
                        }
                        $locations = array();// Menu Options Read
                        if (is_array($options['nav_menu_locations'])) {
                               foreach ($options['nav_menu_locations'] as $menu_name => $menu_id) {
                                $locations[$menu_name] = $menu_id;
                                set_theme_mod( 'nav_menu_locations', $locations);
                            }
                        }                    
                        $front_page = !empty( $options['front_page_name'] ) ?  $options['front_page_name'] : '2'; // Front Page 
                         $page_for_posts = !empty( $options['page_for_posts'] ) ?  $options['page_for_posts'] : '0';
                        $page_title = get_the_title( $front_page );
                        $front_page_name = get_page_by_title( $page_title );
                        if( $front_page_name == 'Sample Page' ){ }
                        else{
                            update_option( 'page_on_front', $front_page );
                            update_option( 'show_on_front', 'page' );
                        }                       
                        update_option( 'page_for_posts', $options['page_for_posts']);
                         update_option( 'page_for_posts', $page_for_posts);


                        // Caldera form json import
                        if( class_exists('Caldera_Forms_Forms') ){
                            $caldera_forms = array('become-a-model' => 'Become A Model', 'contact-form' => 'Contact Form', 'client-form' => 'Client Form');
                            foreach ($caldera_forms as $key => $caldera_form) {
                                $data = json_decode( file_get_contents(get_template_directory().'/inc/kaya-xml-files/'.trim($key).'.json' ), true );
                                $data[ 'name' ] = strip_tags($caldera_form);
                                $new_form_id = Caldera_Forms_Forms::import_form( $data );
                            }                            
                        } // End Forms


                        echo "<div class='updated'><p>".__('All Pages, Posts and options are imported successfully','casting')."</p></div>";
                    }else{
                        echo "<div class='error'><p>".__('Error occured while loading / File not found','casting')."</p></div>";
                    }
                    }
                }else{
                     echo "<div class='error'><p>".__('Error occured while loading / File not found','casting')."</p></div>";
                }
       // ob_get_clean();
        }
        die(); // this is required to return a proper result
    }
}

// Import Pods Data
add_action( 'wp_ajax_casting_kaya_pods_content_import', 'casting_kaya_pods_content_import' );
if( !function_exists('casting_kaya_pods_content_import') ){
    function casting_kaya_pods_content_import() 
    {    
     // Import Pods Data
        $pods_data = get_template_directory().'/inc/kaya-xml-files/pods_data.json';
        $encode_pods_options = file_get_contents($pods_data);
        $pod_options = json_decode($encode_pods_options, true);  
        $pods_data = pods_api()->import_package($pod_options, true);
        die(); // this is required to return a proper result
    }
}

// Widgets Importing Demo Content 
function casting_kaya_available_widgets() {
        global $wp_registered_widget_controls;
        $widget_controls = $wp_registered_widget_controls;
        $available_widgets = array();
        foreach ( $widget_controls as $widget ) {
            if ( ! empty( $widget['id_base'] ) && ! isset( $available_widgets[$widget['id_base']] ) ) { // no dupes
                $available_widgets[$widget['id_base']]['id_base'] = $widget['id_base'];
                $available_widgets[$widget['id_base']]['name'] = $widget['name'];
            }
        }
        return apply_filters( 'casting_kaya_available_widgets', $available_widgets );
    }
    
  function casting_kaya_import_widgets( $data ) {
        global $wp_registered_sidebars;
        if ( empty( $data ) ) {
            wp_die(
                esc_html__( 'Widgets data not found', 'casting' ),
                '',
                array( 'back_link' => true )
            );
        }
        $data = apply_filters( 'theme_import_widget_data', $data );
        $available_widgets = casting_kaya_available_widgets();
        $widget_instances = array();
        foreach ( $available_widgets as $widget_data ) {
            $widget_instances[$widget_data['id_base']] = get_option( 'widget_' . $widget_data['id_base'] );
        }
        $results = array();
        foreach ( $data as $sidebar_id => $widgets ) {
            if ( 'wp_inactive_widgets' == $sidebar_id ) {
                continue;
            }
            if ( isset( $wp_registered_sidebars[$sidebar_id] ) ) {
                $sidebar_available = true;
                $use_sidebar_id = $sidebar_id;
                $sidebar_message_type = 'success';
                $sidebar_message = '';
            } else {
                $sidebar_available = false;
                $use_sidebar_id = 'wp_inactive_widgets';
                $sidebar_message_type = 'error';
                $sidebar_message = esc_html__( 'Sidebar does not exist', 'casting' );
            }
            $results[$sidebar_id]['name'] = ! empty( $wp_registered_sidebars[$sidebar_id]['name'] ) ? $wp_registered_sidebars[$sidebar_id]['name'] : $sidebar_id; 
            $results[$sidebar_id]['message_type'] = $sidebar_message_type;
            $results[$sidebar_id]['message'] = $sidebar_message;
            $results[$sidebar_id]['widgets'] = array();
            foreach ( $widgets as $widget_instance_id => $widget ) {
                $fail = false;
                $id_base = preg_replace( '/-[0-9]+$/', '', $widget_instance_id );
                $instance_id_number = str_replace( $id_base . '-', '', $widget_instance_id );
                if ( ! $fail && ! isset( $available_widgets[$id_base] ) ) {
                    $fail = true;
                    $widget_message_type = 'error';
                    $widget_message = esc_html__( 'Error, while importing widgets data', 'casting' );
                }
                $widget = apply_filters( 'widget_import_widget_settings', $widget );
                if ( ! $fail && isset( $widget_instances[$id_base] ) ) {
                    $sidebars_widgets = get_option( 'sidebars_widgets' );
                    $sidebar_widgets = isset( $sidebars_widgets[$use_sidebar_id] ) ? $sidebars_widgets[$use_sidebar_id] : array(); 
                    $single_widget_instances = ! empty( $widget_instances[$id_base] ) ? $widget_instances[$id_base] : array();
                    foreach ( $single_widget_instances as $check_id => $check_widget ) {
                        if ( in_array( "$id_base-$check_id", $sidebar_widgets ) && (array) $widget == $check_widget ) {
                            $fail = true;
                            $widget_message_type = 'warning';
                            $widget_message = esc_html__( 'Widget already exists', 'casting' );
                            break;
                        }
                    }
                }
                // Widgts data exist
                if ( ! $fail ) {
                    $single_widget_instances = get_option( 'widget_' . $id_base ); // all instances for that widget ID base, get fresh every time
                    $single_widget_instances = ! empty( $single_widget_instances ) ? $single_widget_instances : array( '_multiwidget' => 1 ); // start fresh if have to
                    $single_widget_instances[] = (array) $widget;
                        end( $single_widget_instances );
                        $new_instance_id_number = key( $single_widget_instances );
                        if ( '0' === strval( $new_instance_id_number ) ) {
                            $new_instance_id_number = 1;
                            $single_widget_instances[$new_instance_id_number] = $single_widget_instances[0];
                            unset( $single_widget_instances[0] );
                        }
                        if ( isset( $single_widget_instances['_multiwidget'] ) ) {
                            $multiwidget = $single_widget_instances['_multiwidget'];
                            unset( $single_widget_instances['_multiwidget'] );
                            $single_widget_instances['_multiwidget'] = $multiwidget;
                        }
                    update_option( 'widget_' . $id_base, $single_widget_instances );
                    $sidebars_widgets = get_option( 'sidebars_widgets' );
                    $new_instance_id = $id_base . '-' . $new_instance_id_number;
                    $sidebars_widgets[$use_sidebar_id][] = $new_instance_id;
                    update_option( 'sidebars_widgets', $sidebars_widgets );
                    if ( $sidebar_available ) {
                        $widget_message_type = 'success';
                        $widget_message = esc_html__( 'Imported', 'casting' );
                    } else {
                        $widget_message_type = 'warning';
                        $widget_message = esc_html__( 'Imported to Inactive', 'casting' );
                    }
                }
                $results[$sidebar_id]['widgets'][$widget_instance_id]['name'] = isset( $available_widgets[$id_base]['name'] ) ? $available_widgets[$id_base]['name'] : $id_base;
                $results[$sidebar_id]['widgets'][$widget_instance_id]['title'] = ! empty( $widget->title ) ? $widget->title : esc_html__( 'No Title', 'casting' ); 
                $results[$sidebar_id]['widgets'][$widget_instance_id]['message_type'] = $widget_message_type;
                $results[$sidebar_id]['widgets'][$widget_instance_id]['message'] = $widget_message;
            }
        }
        return $results;
    }
?>