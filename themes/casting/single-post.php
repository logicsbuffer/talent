<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package casting
 */

get_header(); ?>
	<div class="two_third mid-content"> <!-- Middle content align -->
		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', get_post_format() );

			echo '<div class="post-next-prev-buttons">';
				the_post_navigation();
			echo '</div>';	

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>
	</div> <!-- End -->
	<div class="one_third_last">
		<?php get_sidebar(); ?>
	</div>
<?php get_footer(); ?>